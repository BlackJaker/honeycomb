//
//  NSAttributedString+TRCreat.m
//  iOS_TaoRen
//
//  Created by 陈亚勃 on 2018/8/23.
//  Copyright © 2018年 code.sogou.fengur. All rights reserved.
//

#import "NSAttributedString+TRCreat.h"

@implementation NSAttributedString (TRCreat)

- (NSAttributedString *)setLineSapce:(CGFloat)linespace{
    NSMutableAttributedString *str = self.mutableCopy;
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = linespace;
    [str addAttributes:@{NSParagraphStyleAttributeName:style} range:NSMakeRange(0, str.string.length)];
    return str;
}

+ (NSAttributedString *)attributedStringWithString:(NSString *)string
                                        lineSpaces:(CGFloat)linespace{
    NSMutableAttributedString *astr = [[NSMutableAttributedString alloc] initWithString:string];
    return [astr setLineSapce:linespace];
}

+ (NSAttributedString *)attributedStringWithString:(NSString *)string
                                  hightlightString:(NSString *)hgihtString
                                    highlightColor:(UIColor *)highlightColor{
    return [self attributedStringWithString:string
                           hightlightString:hgihtString
                             hightlightFont:nil
                             highlightColor:highlightColor];
}

+ (NSAttributedString *)attributedStringWithAttributedString:(NSAttributedString *)attributedString
                                            hightlightString:(NSString *)hgihtString
                                              highlightColor:(UIColor *)highlightColor{
    NSMutableAttributedString *newAttributedString = attributedString.mutableCopy;
    [newAttributedString addAttributes:@{NSForegroundColorAttributeName : highlightColor} range:[attributedString.string rangeOfString:hgihtString]];
    return attributedString;
}

+ (NSAttributedString *)attributedStringWithString:(NSString *)string
                                  hightlightString:(NSString *)hgihtString
                                    hightlightFont:(UIFont *)font
                                    highlightColor:(UIColor *)highlightColor{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSArray *rangeStringArr = [string rangeStringArrWithString:hgihtString];
    if (!font) {
        for (NSString *rangStr in rangeStringArr) {
            [attributedString addAttributes:@{NSForegroundColorAttributeName : highlightColor} range:NSRangeFromString(rangStr)];
        }
    }else{
        for (NSString *rangStr in rangeStringArr) {
            [attributedString addAttributes:@{NSForegroundColorAttributeName : highlightColor,NSFontAttributeName:font} range:NSRangeFromString(rangStr)];
        }
    }
    
    return attributedString;
    
}

@end

