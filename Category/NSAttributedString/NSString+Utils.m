//
//  NSString+Utils.m
//  iOS_TaoRen
//
//  Created by 陈亚勃 on 2018/8/27.
//  Copyright © 2018年 code.sogou.fengur. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (NSArray *)rangeStringArrWithString:(NSString *)rangeString{

    NSMutableArray *rangeArr = [NSMutableArray array];
    NSArray *arr = [self componentsSeparatedByString:rangeString];
    NSUInteger currentIndex = 0;
    for (NSString *itemString in arr) {
        currentIndex += itemString.length;
        NSRange range = NSMakeRange(currentIndex, rangeString.length);
        currentIndex += rangeString.length;
        if (currentIndex > self.length) {
            break;
        }
        [rangeArr addObject:NSStringFromRange(range)];
    }
    
    return rangeArr;
}

@end
