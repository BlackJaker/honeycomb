//
//  NSString+Utils.h
//  iOS_TaoRen
//
//  Created by 陈亚勃 on 2018/8/27.
//  Copyright © 2018年 code.sogou.fengur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

- (NSArray *)rangeStringArrWithString:(NSString *)rangeString;

@end
