//
//  NSAttributedString+TRCreat.h
//  iOS_TaoRen
//
//  Created by 陈亚勃 on 2018/8/23.
//  Copyright © 2018年 code.sogou.fengur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Utils.h"
@interface NSAttributedString (TRCreat)

- (NSAttributedString *)setLineSapce:(CGFloat)linespace;

+ (NSAttributedString *)attributedStringWithString:(NSString *)string
                                        lineSpaces:(CGFloat)linespace;

+ (NSAttributedString *)attributedStringWithString:(NSString *)string
                                  hightlightString:(NSString *)hgihtString
                                    highlightColor:(UIColor *)highlightColor;

+ (NSAttributedString *)attributedStringWithAttributedString:(NSAttributedString *)attributedString
                                  hightlightString:(NSString *)hgihtString
                                    highlightColor:(UIColor *)highlightColor;

+ (NSAttributedString *)attributedStringWithString:(NSString *)string
                                  hightlightString:(NSString *)hgihtString
                                    hightlightFont:(UIFont *)font
                                    highlightColor:(UIColor *)highlightColor;

@end
