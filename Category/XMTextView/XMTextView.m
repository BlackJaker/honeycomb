//
//  XMTextView.m
//  XMTextView
//
//  Created by XM on 2018/6/29.
//  Copyright © 2018年 XM. All rights reserved.
//

#import "XMTextView.h"
#import "UITextView+XMExtension.h"

@interface XMTextView ()<UITextViewDelegate>

/** textView */
@property (nonatomic, weak) UITextView *textView;
/** num */
@property (nonatomic, weak) UILabel *numLabel;

@end

@implementation XMTextView

- (UITextView *)textView{
    if (!_textView) {
        UITextView *textView = [[UITextView alloc] init];
        textView.delegate = self;
        textView.textAlignment = NSTextAlignmentJustified;
        textView.textContainerInset = UIEdgeInsetsZero;
        textView.textContainer.lineFragmentPadding = 0;
        textView.scrollEnabled = NO;
        [self addSubview:textView];
        _textView = textView;
    }
    return _textView;
}

- (UILabel *)numLabel{
    if (!_numLabel) {
        UILabel *numLabel = [[UILabel alloc] init];
        numLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:numLabel];
        _numLabel = numLabel;
    }
    return _numLabel;
}

- (instancetype)init{
    
    if (self = [super init]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        // 添加配置
        [self addConfig];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        // 添加配置
        [self addConfig];
    }
    return self;
}

#pragma mark - 添加配置
- (void)addConfig{
    
    self.isSetBorder = YES;
//    self.borderLineWidth = 1;
//    self.borderLineColor = [UIColor lightGrayColor];
    self.topSpace = 0;
    self.LRSpace = 0;
//    self.textMaxNum = 60;
    self.tvColor = [UIColor blackColor];
    self.tvFont = [UIFont systemFontOfSize:14];
    self.placeholder = @"请输入内容";
    self.placeholderColor = kGrayColor;
    self.maxNumColor = kGrayColor;
    self.maxNumFont = [UIFont systemFontOfSize:12];
    self.maxNumState = XMMaxNumStateNormal;
    
    // 显示内容
    [self showContent];
}

- (void)setIsSetBorder:(BOOL)isSetBorder{
    
    _isSetBorder = isSetBorder;
    
    [self showContent];
}

- (void)setTextMaxNum:(int)textMaxNum{
    
    _textMaxNum = textMaxNum;
    
    [self showContent];
}

- (void)setContentStr:(NSString *)contentStr{
    
    _contentStr = contentStr;
    
    [self showContent];
}

- (void)setBorderLineColor:(UIColor *)borderLineColor{
    
    _borderLineColor = borderLineColor;
    
    [self showContent];
}

- (void)setBorderLineWidth:(CGFloat)borderLineWidth{
    
    _borderLineWidth = borderLineWidth;
    
    [self showContent];
}

- (void)setTvColor:(UIColor *)tvColor{
    
    _tvColor = tvColor;
    
    [self showContent];
}

- (void)setTvFont:(UIFont *)tvFont{
    
    _tvFont = tvFont;
    
    [self showContent];
}

- (void)setPlaceholder:(NSString *)placeholder{
    
    _placeholder = placeholder;
    
    [self showContent];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor{
    
    _placeholderColor = placeholderColor;
    
    [self showContent];
}

- (void)setMaxNumColor:(UIColor *)maxNumColor{
    
    _maxNumColor = maxNumColor;
    
    [self showContent];
}

- (void)setMaxNumFont:(UIFont *)maxNumFont{
    
    _maxNumFont = maxNumFont;
    
    [self showContent];
}

- (void)setMaxNumState:(XMMaxNumState)maxNumState{
    
    _maxNumState = maxNumState;
    
    [self showContent];
}

#pragma mark - 显示内容
- (void)showContent{
    
    // 布局
    [self layoutFrame];
    
    // 重新配置信息
    if (self.isSetBorder) {
        self.layer.borderWidth = self.borderLineWidth;
        self.layer.borderColor = self.borderLineColor.CGColor;
    }else{
        
        self.layer.borderWidth = 0.00;
        self.layer.borderColor = [UIColor clearColor].CGColor;
    }
    self.textView.textColor = self.tvColor;
    self.textView.font = self.tvFont;
    self.numLabel.textColor = self.maxNumColor;
    self.numLabel.font = self.maxNumFont;
    
    self.textView.placeholder = self.placeholder;
    self.textView.placeholderColor = self.placeholderColor;

    self.textView.text = self.contentStr;
    
    [self textViewDidChange:self.textView];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView{
    
    if (self.textView.text.length>self.textMaxNum) {
        self.textView.text = [self.textView.text substringToIndex:self.textMaxNum];
    }
    
    if (self.maxNumState == XMMaxNumStateNormal) {
        
        self.numLabel.text = [NSString stringWithFormat:@"%d/%d",self.textView.text.length,self.textMaxNum];
    }else{
        
        self.numLabel.text = [NSString stringWithFormat:@"%d",self.textMaxNum-self.textView.text.length];
    }
    
    if (self.textViewListening) {
        self.textViewListening(self.textView.text);
    }
}

#pragma mark - 布局
- (void)layoutFrame{

    self.numLabel.frame = CGRectMake(self.LRSpace, self.frame.size.height-30, self.frame.size.width-self.LRSpace * 2, 30);
    self.textView.frame = CGRectMake(self.LRSpace, self.topSpace, self.numLabel.frame.size.width, self.frame.size.height- 30 + self.topSpace);
    
    if (self.textView.height == 90) {
        self.textView.scrollEnabled = YES;
    }else{
        self.textView.scrollEnabled = NO;
    }
    
}

- (void)dealloc{
    
//    NSLog(@"dealloc");
    [self removeFromSuperview];
}

@end
