//
//  NSString+FC_String.m
//  HoneyComb
//
//  Created by afc on 2018/11/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "NSString+FC_String.h"

@implementation NSString (FC_String)

+(NSString *)stateStringWithType:(FC_OrderStateType)stateType{
    
    NSString * stateStr = nil;
    
    switch (stateType) {
        case FC_OrderStateTypeNoPay:
            stateStr = @"未付款";
            break;
        case FC_OrderStateTypeStayTicket:
            stateStr = @"待出票";
            break;
        case FC_OrderStateTypeStayDraw:
            stateStr = @"已出票";
            break;
        case FC_OrderStateTypeWin:
            stateStr = @"已中奖";
            break;
        case FC_OrderStateTypeLose:
            stateStr = @"未中奖";
            break;
        case FC_OrderStateTypeEndSend:
            stateStr = @"已派奖";
            break;
        case FC_OrderStateTypeCancle:
            stateStr = @"已撤单";
            break;
        case FC_OrderStateTypeWillTakeTicket:
            stateStr = @"待取票";
            break;
        case FC_OrderStateTypeTakeTicket:
            stateStr = @"已取票";
            break;
        default:
            break;
    }
    
    return  stateStr;
}

@end
