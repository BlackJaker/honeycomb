//
//  NSString+FC_String.h
//  HoneyComb
//
//  Created by afc on 2018/11/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (FC_String)

+(NSString *)stateStringWithType:(FC_OrderStateType)stateType;

@end

NS_ASSUME_NONNULL_END
