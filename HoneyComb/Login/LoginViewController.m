//
//  LoginViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/16.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "LoginViewController.h"
#import "UIButton+Timer.h"
@interface LoginViewController ()<UITextFieldDelegate>

@property (nonatomic,strong) UIImageView *logoView;
@property (nonatomic,strong) UIImageView *phoneView;
@property (nonatomic,strong) UIImageView *passWordView;
@property (nonatomic,strong) UIImageView *codeView;
@property (nonatomic,strong) UIView *oneView;
@property (nonatomic,strong) UIView *towView;
@property (nonatomic,strong) UITextField *phoneText;//手机号输入框
@property (nonatomic,strong) UITextField *passWordText;//密码输入框
@property (nonatomic,strong) UITextField *codeText;//短信验证码输入框
@property (nonatomic,strong) UIButton *codeBtn;//短信验证码登录
@property (nonatomic,strong) UIButton *passBtn;//密码登录按钮
@property (nonatomic,strong) UIButton *forgetBtn;//忘记密码
@property (nonatomic,strong) UIButton *loginBtn;//登录按钮
@property (nonatomic,strong) UIButton *sendBtn;//获取验证码
@property (nonatomic,assign) NSInteger codeInteger;
@property (nonatomic,assign) NSInteger passWordInteger;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
    self.view.backgroundColor = RGB(249, 249, 249,1);
    [self layoutView];
    
    _codeInteger = 1;
    
}
-(void)layoutView
{
    CGFloat f ;
    if (TabBarHeight == 83) {
        f = 667;
    }else
    {
        f = kHeight;
    }
    self.logoView = [[UIImageView alloc]init];
    self.logoView.image =[UIImage imageNamed:@"安蜂巢logo0603"];
    [self.view addSubview:self.logoView];
    [self.logoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(95);
        make.height.mas_equalTo(103);
        make.top.mas_equalTo(kHeight/8.55);
        make.centerX.mas_equalTo(0);
    }];
    
#pragma mark 手机号
    self.phoneView = [[UIImageView alloc]init];
    self.phoneView.image =[UIImage imageNamed:@"手机"];
    [self.view addSubview:self.phoneView];
    [self.phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(12);
        make.height.mas_equalTo(18);
        make.top.mas_equalTo(kHeight/2.689);
        make.left.mas_equalTo(kWidth/6.6964);
    }];
    self.phoneText = [[UITextField alloc]init];
    self.phoneText.placeholder = @"请输入手机号";
    self.phoneText.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneText.textColor = RGB(51, 51, 51,1);
    self.phoneText.delegate = self;
    self.phoneText.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.view addSubview:_phoneText];
    [self.phoneText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/1.65);
        make.height.mas_equalTo(kHeight/37);
        make.top.mas_equalTo(self.phoneView.mas_top);
        make.left.mas_equalTo(self.phoneView.mas_right).offset(kWidth/16.625);
    }];
#pragma mark 横条背景
    self.oneView = [[UIView alloc]init];
    self.oneView.backgroundColor = RGB(212, 212, 212,1);
    [self.view addSubview:self.oneView];
    [self.oneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/1.399);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.phoneView.mas_bottom).offset(kHeight/47.64);
        make.left.mas_equalTo(kWidth/6.696);
    }];
    
#pragma mark 密码输入框
    self.passWordView = [[UIImageView alloc]init];
    self.passWordView.image =[UIImage imageNamed:@"密码"];
    self.passWordView.hidden = YES;
    [self.view addSubview:self.passWordView];
    [self.passWordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/28.84);
        make.height.mas_equalTo(kHeight/44.46);
        make.top.mas_equalTo(self.oneView.mas_bottom).offset(kHeight/30.31);
        make.left.mas_equalTo(kWidth/6.696);
    }];
    
    self.passWordText = [[UITextField alloc]init];
    self.passWordText.placeholder = @"请输入密码";
    self.passWordText.hidden = YES;
    self.passWordText.secureTextEntry = YES;
    self.passWordText.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.passWordText.textColor = RGB(51, 51, 51,1);
    self.passWordText.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.view addSubview:_passWordText];
    [self.passWordText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/1.651);
        make.height.mas_equalTo(kHeight/37);
        make.top.mas_equalTo(self.passWordView.mas_top);
        make.left.mas_equalTo(self.passWordView.mas_right).offset(kWidth/17);
    }];
    
#pragma mark 验证码输入框
    self.codeView = [[UIImageView alloc]init];
    self.codeView.image =[UIImage imageNamed:@"验证码"];
    [self.view addSubview:self.codeView];
    [self.codeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/20.8);
        make.height.mas_equalTo(kHeight/39.23);
        make.top.mas_equalTo(self.oneView.mas_bottom).offset(kHeight/30.31);
        make.left.mas_equalTo(kWidth/6.696);
    }];
    
    self.codeText = [[UITextField alloc]init];
    self.codeText.placeholder = @"请输入验证码";
    self.codeText.textColor = RGB(153, 153, 153,1);
    self.codeText.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.view addSubview:_codeText];
    [self.codeText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/3.125);
        make.height.mas_equalTo(kHeight/37);
        make.top.mas_equalTo(self.codeView.mas_top);
        make.left.mas_equalTo(self.codeView.mas_right).offset(kWidth/23.4375);
    }];
    
    self.sendBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [self.sendBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.sendBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.sendBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.sendBtn setTitleColor:[UIColor redColor] forState:(UIControlStateNormal)];
    [self.view addSubview:self.sendBtn];
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/5.357);
        make.height.mas_equalTo(kHeight/37);
        make.top.mas_equalTo(self.codeView.mas_top);
        make.right.mas_equalTo(-kWidth/7.8125);
    }];
    
    
    self.towView = [[UIView alloc]init];
    self.towView.backgroundColor = RGB(212, 212, 212,1);
    [self.view addSubview:self.towView];
    [self.towView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/1.399);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(self.passWordView.mas_bottom).offset(kHeight/47.64);
        make.left.mas_equalTo(kWidth/6.696);
    }];
    
    self.codeBtn = [[UIButton alloc]init];
    self.codeBtn.hidden = YES;
    [self.codeBtn setTitleColor:RGB(51, 51, 51,1) forState:(UIControlStateNormal)];
    self.codeBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.codeBtn setTitle:@"短信验证码登录" forState:(UIControlStateNormal)];
    [self.codeBtn addTarget:self action:@selector(codeAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.codeBtn];
    [self.codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/3.9);
        make.height.mas_equalTo(kHeight/37);
        make.top.mas_equalTo(self.towView.mas_bottom).offset(kHeight/39.23);
        make.left.mas_equalTo(kWidth/6.8181);
    }];
    self.passBtn = [[UIButton alloc]init];
    [self.passBtn setTitleColor:RGB(51, 51, 51,1) forState:(UIControlStateNormal)];
    self.passBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.passBtn setTitle:@"密码登录" forState:(UIControlStateNormal)];
    [self.passBtn addTarget:self action:@selector(passAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.passBtn];
    [self.passBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/5.769);
        make.height.mas_equalTo(kHeight/37);
        make.top.mas_equalTo(self.towView.mas_bottom).offset(kHeight/39.23);
        make.left.mas_equalTo(kWidth/7.07);
    }];
    
//    self.forgetBtn = [[UIButton alloc]init];
//    [self.forgetBtn setTitleColor:RGB(51, 51, 51,1) forState:(UIControlStateNormal)];
//    self.forgetBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
//    [self.forgetBtn setTitle:@"忘记密码" forState:(UIControlStateNormal)];
//    [self.view addSubview:self.forgetBtn];
//    [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/5.769);
//        make.height.mas_equalTo(kHeight/37);
//        make.top.mas_equalTo(self.towView.mas_bottom).offset(kHeight/39.23);
//        make.right.mas_equalTo(-kWidth/7.5);
//    }];
    
    self.loginBtn = [[UIButton alloc]init];
    [self.loginBtn setImage:[UIImage imageNamed:@"登录按钮"] forState:(UIControlStateNormal)];
    [self.loginBtn addTarget:self action:@selector(loginAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.loginBtn];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/1.3345);
        make.height.mas_equalTo(kHeight/10.93);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-kHeight/3.7);
        make.right.mas_equalTo(-kWidth/7.9787);
    }];
    
    
}
#pragma mark --倒计时
-(void)buttonAction:(UIButton *)button
{
    if (_phoneText.text.length == 0) {
        NSLog(@"手机号不能为空");
        [STTextHudTool showText:@"手机号不能为空"];
        return;
    }
    
    if (![Check isMobileNumber:self.phoneText.text]) {
        [STTextHudTool showText:@"请输入正确的手机号"];
        return;
    }
    
    NSDictionary *dic = @{@"mobile":_phoneText.text};
    
    
    NSLog(@"%@,接口地址:%@",dic,PostSendCode);
    [PPNetworkHelper POST:PostSendCode parameters:dic success:^(id responseObject) {
        NSLog(@"成功-%@",responseObject);
        [STTextHudTool showSuccessText:@"验证码已发送"];
    } failure:^(NSError *error) {
        NSLog(@"失败-%@",error);
        [STTextHudTool showErrorText:@"验证码发送失败"];
    }];
    
    button.time = 60;
    button.format = @"%ld秒后重试";
    [button startTimer];
}
#pragma mark 切换短信登录
-(void)codeAction
{
    _codeInteger = 1;
    _passWordInteger = 2;
    [self.codeText setText:@""];
    self.codeView.hidden = NO;
    self.codeText.hidden = NO;
    self.sendBtn.hidden = NO;
    self.passBtn.hidden = NO;
    self.passWordView.hidden = YES;
    self.passWordText.hidden = YES;
    self.codeBtn.hidden = YES;
}
-(void)passAction
{
    _codeInteger = 2;
    _passWordInteger = 1;
    [self.passWordText setText:@""];
    self.codeView.hidden = YES;
    self.codeText.hidden = YES;
    self.sendBtn.hidden = YES;
    self.passBtn.hidden = YES;
    self.passWordView.hidden = NO;
    self.passWordText.hidden = NO;
    self.codeBtn.hidden = NO;
    
}

#pragma mark  --  text field delegate  --

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
    if (range.length == 1 && string.length == 0) {
        return YES;
    }else if(textField == self.phoneText) {
        return [self validateNumber:string];
    }else if(textField == self.codeText){
        return [self validateCode:string];
    }else if(textField == self.passWordText){
        return [self validatePassword:string];
    }
    return YES;
    
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:NUMBER];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    
    if (self.phoneText.text.length == 0 && [number isEqualToString:@"0"]) {
        return NO;
    }
    
    if (self.phoneText.text.length == 11) {
        return NO;
    }
    
    return res;
}

-(BOOL)validateCode:(NSString *)string{
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBER] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    BOOL isRight = [string isEqualToString:filtered];
    
    if (self.codeText.text.length >= 6) {
        isRight = NO;
    }
    
    return isRight;
}

-(BOOL)validatePassword:(NSString *)string{
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ALPHANUM] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    BOOL isRight = [string isEqualToString:filtered];
    
    if (self.passWordText.text.length >= 12) {
        isRight = NO;
    }
    
    return isRight;
    
}

#pragma mark 登录
-(void)loginAction
{
    if (_phoneText.text.length == 0) {
        NSLog(@"手机号不能为空");
        [STTextHudTool showText:@"手机号不能为空"];
        return;
    }
    
    if (![Check isMobileNumber:self.phoneText.text]) {
        [STTextHudTool showText:@"请输入正确的手机号"];
        return;
    }
    
    if (_codeInteger == 1) {

        NSLog(@"~~~%@",_codeText.text);
        if (_codeText.text.length == 0) {
            NSLog(@"请输入验证码");
            [STTextHudTool showText:@"验证码不能为空"];
            return;
        }
        if (_codeText.text.length != 6) {
            [STTextHudTool showText:@"验证码格式不正确"];
            return;
        }
        [STTextHudTool loadingWithTitle:@"登录中..."];
        
        NSDictionary *dic = @{@"mobile":_phoneText.text,@"verifyCode":_codeText.text};
        
        @WeakObj(self)
        [PPNetworkHelper POST:PostLoginCode
                   parameters:dic
                      success:^(id responseObject) {
                          
                          @StrongObj(self)
                          
                          [STTextHudTool hideSTHud];
                          NSLog(@"成功-%@-%@",responseObject,dic);
                          if ([[responseObject objectForKey:@"msg"] isEqualToString:@"success"]) {
                              
                              [self loginSuccessedWithResponse:responseObject];
                              
                          }else{
                              [STTextHudTool showSuccessText:[responseObject objectForKey:@"msg"]];
                          }
                      } failure:^(NSError *error) {
                          NSLog(@"失败-%@",error);
                          [STTextHudTool showText:@"登录失败"];
                      }];
        
        NSLog(@"2222");
    }else
    {
        NSLog(@"---%@",_passWordText.text);
        if (_passWordText.text.length == 0) {
            NSLog(@"请输入密码");
            [STTextHudTool showText:@"密码不能为空"];
            return;
        }
        if (_passWordText.text.length < 6 || _passWordText.text.length > 12) {
            [STTextHudTool showText:@"密码格式不正确"];
            return;
        }
        [STTextHudTool loadingWithTitle:@"登录中..."];
        
        NSDictionary *dic = @{@"mobile":_phoneText.text,@"password":_passWordText.text};
        
        //        @WeakObj(self)
        [PPNetworkHelper POST:PostLoginPassWord
                   parameters:dic
                      success:^(id responseObject) {
                          
                          NSLog(@"%@",responseObject);
                          
                          //                          @StrongObj(self)
                          
                          [STTextHudTool hideSTHud];
                          
                          if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                              [self loginSuccessedWithResponse:responseObject];
                          }else{
                              [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                          }
                      } failure:^(NSError *error) {
                          NSLog(@"%@",error);
                          [STTextHudTool showText:@"登录失败"];
                      }];
        
        
    }
}

-(void)loginSuccessedWithResponse:(NSDictionary *)response{
    
    [STTextHudTool showSuccessText:@"登录成功"];
    NSDictionary *dic = [solveJsonData changeType:response];
    [[LoginUser shareLoginUser] setShopId:[[dic objectForKey:@"data"]objectForKey:@"shopId"]];
    
    [self requestShopPushBind];
    
    @WeakObj(self)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        @StrongObj(self)
        
        self.tabBarController.selectedIndex = 0;
        [self.navigationController popViewControllerAnimated:YES];
    });
    
}

#pragma mark  --  request push bind  --

-(void)requestShopPushBind{
    
    NSMutableDictionary * loginDica = [NSMutableDictionary new];
    [loginDica setValue:[LoginUser shareLoginUser].shopId forKey:@"shopId"];
    [loginDica setValue:[LoginUser shareLoginUser].devToken forKey:@"pushId"];
    [loginDica setValue:@"1" forKey:@"type"];
    
    [PPNetworkHelper POST:ShopPushBind
               parameters:loginDica
                  success:^(id responseObject) {
                      
                  } failure:^(NSError *error) {
                      
                  }];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.phoneText resignFirstResponder];
    [self.passWordText resignFirstResponder];
    [self.codeText resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
