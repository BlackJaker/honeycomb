//
//  GCLoadingView.h
//  Guangfa
//
//  Created by 罗大勇 on 2017/12/19.
//  Copyright © 2017年 ldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GCLoadingView : UIView

- (void)startAnimating;
- (void)stopAnimating;

@end
