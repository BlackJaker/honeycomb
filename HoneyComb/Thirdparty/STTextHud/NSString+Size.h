//
//  NSString+Size.h
//  Guangfa
//
//  Created by 罗大勇 on 2017/12/19.
//  Copyright © 2017年 ldy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (Size)

- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

@end
