//
//  AllViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <YYKit/YYKit.h>
@interface AllViewController : UIViewController

@property (nonatomic,strong) YYLabel *titleLabe;
@property (nonatomic,strong)UIButton *left;

@property (nonatomic, strong) UIView *  topNaviView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UIButton *  backButton;
@property (nonatomic, strong) UIButton * rightButton;
@property (nonatomic, copy) NSMutableArray * rightButtons;

-(void)leftAction;
-(void)rightAction:(UIButton *)sender;
-(void)setRightButtonWithTitle:(NSString *)rightTitle;

-(void)updateFrameWithMargin:(CGFloat)margin;

@end
