//
//  AllModel.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/24.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,OrderCelltype)
{
    typeA,
    typeB,
    typeC,
    typeD
};
@interface AllModel : NSObject

@property (nonatomic,copy) NSString *orderCellType;
@property(nonatomic, assign) BOOL     isSelected;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *cellIdentifier;
/********************个人中心**************************/
@property (nonatomic,copy) NSString *img;
@property (nonatomic,copy) NSString *drawBill;
@property (nonatomic,copy) NSString *appName;
//@property (nonatomic,copy) NSString *totalMoney;
@property (nonatomic,copy) NSString *shopState;
@property (nonatomic,copy) NSString *point;
@property (nonatomic,copy) NSString *mobile;
@property (nonatomic,copy) NSString *wechat;
@property (nonatomic,copy) NSString *notice;
@property (nonatomic,copy) NSString *SendPrize;
/********************用户管理**************************/
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *cost;//花费总金额
@property (nonatomic,copy) NSString *money;
@property (nonatomic,copy) NSString *userMoney;//剩余总金额
@property (nonatomic,copy) NSString *nickName;//昵称
@property (nonatomic,copy) NSString *orderDate;//最后下单时间
@property (nonatomic,copy) NSString *award;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *playName;
@property (nonatomic,copy) NSString *state;
@property (nonatomic,copy) NSString *createDate;
@property (nonatomic,copy) NSString *issue;
/********************店长**************************/
@property (nonatomic,copy) NSString *league;
@property (nonatomic,copy) NSString *visitingTeam;
@property (nonatomic,copy) NSString *orderTotalId;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *hostTeam;//
@property (nonatomic,copy) NSString *number;
@property (nonatomic,copy) NSString *times;
@property (nonatomic,copy) NSString *startTime;
@property (nonatomic,copy) NSString *bunch;
@property (nonatomic,copy) NSString *account;
@property (nonatomic,copy) NSString *red;
@property (nonatomic,copy) NSString *redBile;
@property (nonatomic,copy) NSString *blue;
@property (nonatomic,copy) NSString *blueBile;
@property (nonatomic,copy) NSString *content;
//订单
@property (nonatomic,copy) NSString *dan;
@property (nonatomic,copy) NSString *letCount;
@property (nonatomic,copy) NSString *endScore;
@property (nonatomic,copy) NSString *orderDetailId;


@property (nonatomic,copy) NSString *id;
@end
