//
//  SendPrizeView.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "SendPrizeView.h"
#import "SectionChooseView.h"


@interface SendPrizeView()
{
    NSString *styleStr;
    NSString *textLabel;
}
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIWindow *window;
@property (nonatomic,strong) NSMutableArray *clickArr;

@property (nonatomic,copy) NSArray * titles;

@property (nonatomic,strong)UIButton *recentlyBtn;
@property (nonatomic,assign)NSInteger selectIndex;
@property (nonatomic,strong) NSMutableArray *clickArrTwo;
@property (nonatomic,strong)UIButton *priceBtn;
@property (nonatomic,assign)NSInteger selectIndexPrice;

@end
@implementation SendPrizeView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        //获取通知中心
//        NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
//        [centerMore addObserver:self selector:@selector(selectAction:) name:@"selectIndexTongzhi" object:nil];
        [self layoutView];
    }
    return self;
    
}
-(void)selectAction:(NSNotification *)dic
{
    NSLog(@"选择器%@",dic.userInfo);
}
-(void)layoutView
{
    _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    _bgView.alpha = 0.4;
    _bgView.backgroundColor = [UIColor blackColor];
    [self addSubview:_bgView];
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 150)];
    v.backgroundColor = [UIColor whiteColor];
    [self addSubview:v];
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissContactView:)];
    [_bgView addGestureRecognizer:tapGes];
  
    
    //。
    if (!self.clickArr) {
        self.clickArr = [NSMutableArray array];
    }
    
    for (int i = 0; i<self.titles.count; i++) {
        _recentlyBtn = [[UIButton alloc]initWithFrame:
                        CGRectMake(12+(kWidth/4-12+11)*(i%4),(7*2)+45*(i/4), 74, 33)];
        _recentlyBtn.tag = 100+i;
        NSString *a =@"0";
        [self.clickArr addObject:a];
        //设置圆角的半径
        [self.recentlyBtn.layer setCornerRadius:3];
        //切割超出圆角范围的子视图
        self.recentlyBtn.layer.masksToBounds = YES;
        //设置边框的颜色
        [self.recentlyBtn.layer setBorderColor:RGB(153, 153, 153, 1).CGColor];
        //设置边框的粗细
        [self.recentlyBtn.layer setBorderWidth:1.0];
        //        [_recentlyBtn setBackgroundImage:[UIImage imageNamed:@"ico_未选择"] forState:(UIControlStateNormal)];
        [_recentlyBtn setTitle:self.titles[i] forState:(UIControlStateNormal)];
        [_recentlyBtn addTarget:self action:@selector(recentlyAction:) forControlEvents:(UIControlEventTouchUpInside)];
        // _featuresBtn.backgroundColor =UIColorHex(#f5f5f5);
        _recentlyBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
        [_recentlyBtn setTitleColor:RGB(51, 51, 51, 1) forState:(UIControlStateNormal)];
        
        [v addSubview:_recentlyBtn];
    }
    
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"全部彩种",@"竞彩足球",@"竞彩篮球",@"大乐透",@"排列3",@"排列5",@"七星彩",@"胜负彩",@"任选九"];
    }
    return _titles;
}

-(void)recentlyAction:(UIButton *)btn
{
    _selectIndex = btn.tag -100;
    for (int i = 0; i<self.titles.count; i++) {
        if (i==_selectIndex) {
            UIButton *button = (UIButton *)[self viewWithTag:btn.tag];
            //            [button setBackgroundImage:[UIImage imageNamed:@"ico_选中"] forState:(UIControlStateNormal)];
            [button setTitleColor:RGB(253, 38, 38, 1) forState:(UIControlStateNormal)];
            [button.layer setBorderColor:RGB(253, 38, 38, 1).CGColor];
        }else {
            UIButton *btn = (UIButton *)[self viewWithTag:i+ 100];
            [btn setTitleColor:RGB(51, 51, 51, 1) forState:(UIControlStateNormal)];
            [btn.layer setBorderColor:RGB(153, 153, 153, 1).CGColor];
        }
    }
    UIButton *btn1 =btn;
    for (int i =0; i<self.titles.count; i++) {
        UIButton *button = [UIButton new];
        button.tag = 100+i;
        if (btn1.tag ==button.tag) {
            //            self.paiXu = btn1.titleLabel.text;
            NSLog(@"选择 -- %@",btn1.titleLabel.text);
            if ([btn1.titleLabel.text isEqualToString:@"全部彩种"]) {
                styleStr = @"";
            }else if ([btn1.titleLabel.text isEqualToString:@"竞彩足球"]){
                styleStr = @"1";
            }else if ([btn1.titleLabel.text isEqualToString:@"竞彩篮球"]){
                styleStr = @"2";
            }else if ([btn1.titleLabel.text isEqualToString:@"大乐透"]){
                styleStr = @"3";
            }else if ([btn1.titleLabel.text isEqualToString:@"排列3"]){
                styleStr = @"4";
            }else if ([btn1.titleLabel.text isEqualToString:@"排列5"]){
                styleStr = @"5";
            }else if ([btn1.titleLabel.text isEqualToString:@"七星彩"]){
                styleStr = @"6";
            }else if ([btn1.titleLabel.text isEqualToString:@"胜负彩"]){
                styleStr = @"7";
            }else if ([btn1.titleLabel.text isEqualToString:@"任选九"]){
                styleStr = @"8";
            }
        }
        textLabel = btn1.titleLabel.text;
    }
    NSLog(@"彩种%@",styleStr);
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:styleStr forKey:@"sendChoose"];
    [dic setValue:textLabel forKey:@"titleLabel"];
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"sendChooseTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    [self dismissView];
}

-(void)dismissContactView:(UITapGestureRecognizer *)tagGes
{
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"sendPrizeTongzhi" object:nil userInfo:nil];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    [self dismissView];
}
-(void)showView
{
    _window = [[UIApplication sharedApplication].windows lastObject];
    [_window addSubview:self];
}
-(void)dismissView
{
    __weak typeof(self)weakSelf =self;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.alpha = 0;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
        [self removeFromSuperview];
    }];
}
@end
