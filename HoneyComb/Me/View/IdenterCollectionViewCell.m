//
//  IdenterCollectionViewCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/14.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "IdenterCollectionViewCell.h"
@interface IdenterCollectionViewCell()
@end
@implementation IdenterCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self layoutView];
    }
    return self;
}
-(void)layoutView
{
    self.imageV = [[UIImageView alloc]init];
    self.imageV.contentMode = UIViewContentModeScaleAspectFit;
    self.imageV.image = [UIImage imageNamed:@"照片框"];
    self.imageV.userInteractionEnabled = YES;
    [self.contentView addSubview:self.imageV];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/4.2613);
        make.height.mas_equalTo(kHeight/5.9);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    
}
@end
