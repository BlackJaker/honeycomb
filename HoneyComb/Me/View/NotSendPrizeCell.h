//
//  NotSendPrizeCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotSendPrizeCell : UITableViewCell
@property (nonatomic,strong) AllModel *model;
@property (nonatomic,strong) UIImageView *backView;
@property (nonatomic,strong) UIImageView *trueView;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UILabel *numberLabel;
@property (nonatomic,strong) UILabel *styleLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UILabel *bonusLabel;
@end
