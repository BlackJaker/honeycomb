//
//  SaleCollectionViewCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/14.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaleCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)UIImageView *imageV;
@end
