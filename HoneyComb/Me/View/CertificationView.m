//
//  CertificationView.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "CertificationView.h"
@interface CertificationView()
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIWindow *window;




@end
@implementation CertificationView
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self layoutView];
    }
    return self;
    
}
-(void)layoutView
{
    _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    _bgView.alpha = 0.4;
    _bgView.backgroundColor = [UIColor blackColor];
    [self addSubview:_bgView];
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(42,216, kWidth-42*2, 172)];
    v.userInteractionEnabled = YES;
    v.backgroundColor = [UIColor whiteColor];
    [self addSubview:v];
//    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissContactView:)];
//    [_bgView addGestureRecognizer:tapGes];
    UILabel *recenLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, kWidth-42*2, 17)];
    recenLabel.text =@"重要通知";
    
    recenLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    recenLabel.textColor = RGB(51, 51, 51, 1);
    recenLabel.textAlignment = NSTextAlignmentCenter;
    [v addSubview:recenLabel];
    
   
    UILabel *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(recenLabel.frame)+15, kWidth-42*2-60, 50)];
    priceLabel.text =@"您的资料已经成功提交,我们将于一个工作日内电话通知您审核结果。";
    priceLabel.numberOfLines = 0;
    priceLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    priceLabel.textColor = kGrayColor;
    priceLabel.textAlignment = NSTextAlignmentLeft;
    [v addSubview:priceLabel];
    
    self.recentlyBtn = [[UIButton alloc]init];
    [self.recentlyBtn setTitle:@"去我的店铺" forState:(UIControlStateNormal)];
    [self.recentlyBtn setTitleColor:kBlackColor forState:(UIControlStateNormal)];
    self.recentlyBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    self.recentlyBtn.layer.cornerRadius =4;
    self.recentlyBtn.layer.masksToBounds = YES;
    //边框宽度
    [self.recentlyBtn.layer setBorderWidth:1.0];
    self.recentlyBtn.layer.borderColor=RGB(51, 51, 51, 0.3).CGColor;
    [v addSubview:self.recentlyBtn];
    [self.recentlyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(223);
        make.height.mas_equalTo(36);
        make.left.mas_equalTo(30);
        make.bottom.mas_equalTo(-25);
    }];
    
}


-(void)dismissContactView:(UITapGestureRecognizer *)tagGes
{
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"moreTongzhi" object:nil userInfo:nil];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    [self dismissView];
}
-(void)showView
{
    _window = [[UIApplication sharedApplication].windows lastObject];
    [_window addSubview:self];
}
-(void)dismissView
{
    __weak typeof(self)weakSelf =self;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.alpha = 0;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
        [self removeFromSuperview];
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
