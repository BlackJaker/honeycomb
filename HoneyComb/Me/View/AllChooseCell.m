//
//  AllChooseCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllChooseCell.h"

@implementation AllChooseCell
-(void)setModel:(AllModel *)model
{
    _model = model;
    
    
    self.nameLabel.text = model.playName;
    self.dateLabel.text = [NSString stringWithFormat:@"购买时间:%@",model.createDate];
    self.numberLabel.text = [NSString stringWithFormat:@"订单编号:%@",model.number];
    NSString *styleStr;
    if ([model.type isEqualToString:@"1"]) {
        if ([model.bunch isEqualToString:@"1"]) {
            styleStr = @"单关";
        }else
        {
            styleStr = [NSString stringWithFormat:@"%@串1",model.bunch];
            
        }
        self.styleLabel.text = [NSString stringWithFormat:@"过关方式:%@",styleStr];
        
    }else if([model.type isEqualToString:@"2"]){
        if ([model.bunch isEqualToString:@"1"]) {
            styleStr = @"单关";
        }else
        {
            styleStr = [NSString stringWithFormat:@"%@串1",model.bunch];
            
        }
        self.styleLabel.text = [NSString stringWithFormat:@"过关方式:%@",styleStr];
    }else if([model.type isEqualToString:@"3"]){
        if ([model.bunch isEqualToString:@"1"]) {
            styleStr = @"单式";
            
        }else
        {
            styleStr = @"复式";
            
        }
        self.styleLabel.text = [NSString stringWithFormat:@"过关方式:%@",styleStr];
    }else if([model.type isEqualToString:@"4"]){
        if ([model.bunch isEqualToString:@"1"]) {
            styleStr = @"单式";
            
        }else
        {
            styleStr = @"复式";
            
        }
        self.styleLabel.text = [NSString stringWithFormat:@"过关方式:%@",styleStr];
    }else if([model.type isEqualToString:@"5"]){
        if ([model.bunch isEqualToString:@"1"]) {
            styleStr = @"单式";
            
        }else
        {
            styleStr = @"复式";
            
        }
        self.styleLabel.text = [NSString stringWithFormat:@"过关方式:%@",styleStr];
    }else
    {
        if ([model.bunch isEqualToString:@"1"]) {
            styleStr = @"单式";
            
        }else
        {
            styleStr = @"复式";
            
        }
        self.styleLabel.text = [NSString stringWithFormat:@"过关方式:%@",styleStr];
    }
    NSString *a = [NSString stringWithFormat:@"%@注",model.account];
    NSString *b = [NSString stringWithFormat:@"%@倍",model.times];
    NSString *c =  [NSString stringWithFormat:@" %@元",model.price];
    NSString * ss = [NSString stringWithFormat:@"%@%@%@",a,b,c];
    
    NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:ss];
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(0, a.length-1)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(0, a.length-1)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(a.length-1, 1)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(a.length-1, 1)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(a.length, b.length)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(a.length, b.length)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(a.length+b.length-1, 1)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(a.length+b.length-1, 1)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(a.length+b.length, c.length)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(a.length+b.length, c.length)];
    self.priceLabel.attributedText = rushAttributed;
    
    NSString *bounsPrice = [NSString stringWithFormat:@"%@元",model.award];
    NSString * bonusStr = [NSString stringWithFormat:@"奖金: %@",bounsPrice];
    
    NSMutableAttributedString *bounAtt = [[NSMutableAttributedString alloc]initWithString:bonusStr];
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(0, 3)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, 3)];
    
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(4, bounsPrice.length)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(4, bounsPrice.length)];
    self.bonusLabel.attributedText = bounAtt;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}
-(void)layoutView
{
    self.backView = [[UIImageView alloc]init];
    self.backView.image = [UIImage imageNamed:@"圆角矩形9"];
    self.backView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-30);
        make.height.mas_equalTo(162);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(5);
    }];
    
    self.nameLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.nameLabel];
    self.nameLabel.text = @"足球胜平负啊";
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    self.nameLabel.textColor = kBlackColor;
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kWidth/2-60);
        make.height.mas_equalTo(18);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(16);
    }];
    
    //    CGRect tempRect = [rushAttributed boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    //  boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil
    //    //      CGRect tempRect = [str   boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000)options:NSStringDrawingUsesLineFragmentOriginattributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}context:nil];
    self.priceLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.priceLabel];
    
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    //    self.priceLabel.font = [UIFont fontWithName:kPingFangRegular size:17];
    //    self.priceLabel.textColor = kBlackColor;
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kWidth/2-20);
        make.height.mas_equalTo(18);
        make.left.mas_equalTo(kWidth/2-40);
        make.top.mas_equalTo(15);
    }];
    
    self.numberLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.numberLabel];
    //    self.numberLabel.text = @"订单号:TE20180718000018904624";
    self.numberLabel.textAlignment = NSTextAlignmentLeft;
    self.numberLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    self.numberLabel.textColor = kGrayColor;
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kWidth-70);
        make.height.mas_equalTo(13);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(15);
    }];
    
    self.styleLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.styleLabel];
    self.styleLabel.text = @"过关方式:单关";
    self.styleLabel.textAlignment = NSTextAlignmentLeft;
    self.styleLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    self.styleLabel.textColor = kGrayColor;
    [self.styleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kWidth-70);
        make.height.mas_equalTo(13);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.numberLabel.mas_bottom).offset(10);
    }];
    self.dateLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.dateLabel];
    self.dateLabel.text = @"购买时间:2018-07-19 19:20:20";
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    self.dateLabel.textColor = kGrayColor;
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kWidth-70);
        make.height.mas_equalTo(13);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.styleLabel.mas_bottom).offset(10);
    }];
    
    UIView *v = [[UIView alloc]init];
    v.backgroundColor = RGB(0, 0, 0, 0.15);
    [self.backView  addSubview:v];
    [v mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-60);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.dateLabel.mas_bottom).offset(10);
    }];
    
    
    self.bonusLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.bonusLabel];
    
    self.bonusLabel.textAlignment = NSTextAlignmentLeft;
    //    self.bonusLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    //    self.bonusLabel.textColor = kGrayColor;
    self.bonusLabel.userInteractionEnabled = YES;
    [self.bonusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kWidth-150);
        make.height.mas_equalTo(18);
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(v.mas_bottom).offset(10);
    }];
    
    self.chooseBtn = [[UIButton alloc]init];
    [self.chooseBtn setImage:[UIImage imageNamed:@"椭圆3"] forState:UIControlStateNormal];
    [self.chooseBtn setImage:[UIImage imageNamed:@"椭圆11"] forState:UIControlStateSelected];
    [self.chooseBtn addTarget:self action:@selector(choseBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.backView addSubview:self.chooseBtn];
    [self.chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(37);
        make.height.mas_equalTo(33);
        make.right.mas_equalTo(self.backView.mas_right).offset(-4);
        
        make.bottom.mas_equalTo(self.backView.mas_bottom).offset(-4);
    }];
    
}
- (void)setSelectedStutas:(BOOL)selectedStutas{
    self.chooseBtn.selected = selectedStutas;
}
#pragma mark - action
- (void)choseBtnAction{
    self.chooseBtn.selected = !self.chooseBtn.selected;
    if (self.ChoseBtnBlock) {
        self.ChoseBtnBlock(self, self.chooseBtn.selected);
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
