//
//  AllChooseCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllChooseCell : UITableViewCell

@property (nonatomic,copy) void(^ChoseBtnBlock)(id,BOOL);
@property (nonatomic,assign) NSInteger price;
@property (nonatomic,assign) BOOL selectedStutas;

@property (nonatomic,strong) AllModel *model;
@property (nonatomic,strong) UIImageView *backView;
@property (nonatomic,strong) UIImageView *trueView;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UILabel *numberLabel;
@property (nonatomic,strong) UILabel *styleLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UILabel *bonusLabel;
@property (nonatomic,strong) UIButton *chooseBtn;
@end
