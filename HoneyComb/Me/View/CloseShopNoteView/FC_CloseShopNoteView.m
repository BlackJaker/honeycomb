//
//  FC_CloseShopNoteView.m
//  HoneyComb
//
//  Created by afc on 2019/1/22.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import "FC_CloseShopNoteView.h"

@implementation FC_CloseShopNoteView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    UIView * backView = [[UIView alloc]init];
    backView.backgroundColor = [UIColor blackColor];
    backView.alpha = 0.51;
    [self addSubview:backView];
    
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    
    UIImageView * noteImgView = [[UIImageView alloc]init];
    noteImgView.image = [UIImage imageNamed:@"close_shop_note"];
    noteImgView.userInteractionEnabled = YES;
    [self addSubview:noteImgView];
    
    CGFloat  height = 64,margin = 60,top = 150 + NAVIGATION_HEIGHT;
    
    [noteImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.height.mas_equalTo(height);
        make.left.mas_equalTo(margin);
        make.right.mas_equalTo(-margin);
    }];
    
    UIImageView * arrowImgView = [[UIImageView alloc]init];
    arrowImgView.image = [UIImage imageNamed:@"close_shop_arrow"];
    arrowImgView.userInteractionEnabled = YES;
    [self addSubview:arrowImgView];
    
    CGFloat  arrowWidth = 60,arrowHeight = 110,arrowMargin = 38,arrowTop = 235 + NAVIGATION_HEIGHT;
    
    [arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(arrowTop);
        make.height.mas_equalTo(arrowHeight);
        make.right.mas_equalTo(-arrowMargin);
        make.width.mas_equalTo(arrowWidth);
    }];
    
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    NSString * key = [NSString stringWithFormat:@"%@_firstShopClose",[LoginUser shareLoginUser].shopId];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:key];
    
    [self removeFromSuperview];
    
}

-(void)show{
    
     [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:self];
    
}

@end
