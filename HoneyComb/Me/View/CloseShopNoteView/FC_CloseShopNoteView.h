//
//  FC_CloseShopNoteView.h
//  HoneyComb
//
//  Created by afc on 2019/1/22.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_CloseShopNoteView : UIView

-(void)show;

@end

NS_ASSUME_NONNULL_END
