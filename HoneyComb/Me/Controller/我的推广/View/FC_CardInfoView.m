//
//  FC_CardInfoView.m
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_CardInfoView.h"

@interface FC_CardInfoView()
{
    CGFloat  headHeight;
    CGFloat  leftMar;
    
    UILabel * currentSelectedLabel;
    UIMenuController * menuCon;
}

@property (nonatomic,copy) NSArray * images;

@property (nonatomic,copy) NSMutableArray * labels;

@end

@implementation FC_CardInfoView

-(instancetype)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame]) {
        [self contentSetup];
    }
    return self;
}

#pragma mark  --  contentSetup  --

-(void)contentSetup{
    
    leftMar = 12;
    headHeight = 40;
    
    [self contentViewSetup];
    
}


#pragma mark  --  lazy  --

-(UIImageView *)backImgView{
    
    if (!_backImgView) {
        _backImgView= [[UIImageView alloc]initWithFrame:self.bounds];
        _backImgView.image =[UIImage imageNamed:@"彩店背景"];
        _backImgView.userInteractionEnabled = YES;
        UIBezierPath *shadowPath = [UIBezierPath
                                    bezierPathWithRect:_backImgView.bounds];
        _backImgView.layer.masksToBounds = NO;
        _backImgView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _backImgView.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        _backImgView.layer.shadowOpacity = 0.2f;
        _backImgView.layer.shadowPath = shadowPath.CGPath;
        [self addSubview:_backImgView];
    }
    return _backImgView;
}

-(UIImageView *)bossImgView{
    
    CGFloat  topMar = 40,width = 60;
    
    if (!_bossImgView) {
        _bossImgView= [[UIImageView alloc]initWithFrame:self.bounds];
        _bossImgView.image =[UIImage imageNamed:@"默认头像"];
        _bossImgView.userInteractionEnabled = YES;
        _bossImgView.layer.cornerRadius = width/2;
        _bossImgView.clipsToBounds = YES;
        _bossImgView.layer.borderWidth = 2;
        _bossImgView.layer.borderColor = [UIColor whiteColor].CGColor;
        _bossImgView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_bossImgView];
        
        [_bossImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(width);
            make.centerX.mas_equalTo(0);
            make.top.mas_equalTo(topMar);
        }];
    }
    return _bossImgView;
    
}

-(UILabel *)shopNameLabel{
    
    CGFloat  yOrigin = 113,height = 15;
    CGRect   shopNameLabelRect = CGRectMake(0, yOrigin, self.width, height);
    
    if (!_shopNameLabel) {
        _shopNameLabel = [UILabel createLabelWithFrame:shopNameLabelRect text:@"" fontSize:height textColor:[UIColor whiteColor]];
        _shopNameLabel.textAlignment = NSTextAlignmentCenter;
        _shopNameLabel.font = [UIFont fontWithName:kBold size:height];
        [self addSubview:_shopNameLabel];
    }
    return _shopNameLabel;
}

-(void)contentViewSetup{
    
    CGFloat  mar = 28,imgWidth = 28,imgHeight = 28,centerYmar = 12,yOrigin = 167,labelMar = 63,rightMar = 44;
    
    for (int i = 0; i < self.images.count; i ++) {
        
        CGRect  frame = CGRectMake(mar, yOrigin + (imgHeight + centerYmar) * i, imgWidth, imgHeight);
        
        //
        UIImageView * imgView = [[UIImageView alloc]initWithFrame:frame];
        imgView.image = [UIImage imageNamed:[self.images objectAtIndex:i]];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.backImgView addSubview:imgView];
        
        frame = CGRectMake(labelMar, yOrigin + (imgHeight + centerYmar) * i + 5 * i, self.width - (labelMar + rightMar), imgHeight * (i + 1));
        // label
        UILabel * label = [[UILabel alloc]initWithFrame:frame];
        label.font = [UIFont fontWithName:kPingFangRegular size:13];
        label.textColor = [UIColor colorWithHexString:@"#333333"];
        label.tag = 1000 + i;
        label.userInteractionEnabled = YES;
        [self.backImgView addSubview:label];
        
        [self.labels addObject:label];
        
        // 长按复制功能
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPre:)];
        [label addGestureRecognizer:longPress];
        
        if (i == 0) {
            UIView * line = [[UIView alloc]initWithFrame:CGRectMake(labelMar, yOrigin + imgHeight + centerYmar/2,self.width - (labelMar + rightMar), 1)];
            line.backgroundColor = RGB(235, 235, 235, 1.0f);
            [self.backImgView addSubview:line];
        }
        
    }
    [self codeLabelSetup];
}

#pragma mark  --  长按复制  --

-(void)longPre:(UILongPressGestureRecognizer *)longGesture{
    
    
    UILabel * label = (UILabel *)longGesture.view;
    currentSelectedLabel = label;
    
    [self becomeFirstResponder]; // 用于UIMenuController显示，缺一不可
    
    [self setupMenuCon];  // 唤起复制
    
}

-(void)setupMenuCon{
    
    //UIMenuController：可以通过这个类实现点击内容，或者长按内容时展示出复制等选择的项，每个选项都是一个UIMenuItem对象
    
    UIMenuItem *copyLink = [[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copy:)];
    UIMenuItem *callLink = [[UIMenuItem alloc] initWithTitle:@"拨打" action:@selector(call:)];
    
    menuCon  = [UIMenuController sharedMenuController];
    if (currentSelectedLabel.tag > 1000) {
        [menuCon setMenuItems:[NSArray arrayWithObjects:copyLink, nil]];
    }else{
        [menuCon setMenuItems:[NSArray arrayWithObjects:copyLink,callLink, nil]];
    }
    
    [menuCon setTargetRect:currentSelectedLabel.frame inView:currentSelectedLabel.superview];
    [menuCon setMenuVisible:YES animated:YES];
    
}

// 使label能够成为响应事件，为了能接收到事件（能成为第一响应者）
- (BOOL)canBecomeFirstResponder{
    return YES;
}
// 可以控制响应的方法
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    
    UIMenuController * menuController = (UIMenuController *)sender;
    if (menuController.menuItems.count == 1) {
        return (action == @selector(copy:));
    }
    
    return (action == @selector(copy:) || action == @selector(call:));
}

//针对响应方法的实现，最主要的复制的两句代码
- (void)copy:(id)sender{
    
    //UIPasteboard：该类支持写入和读取数据，类似剪贴板
    UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard];
    
    pasteBoard.string = currentSelectedLabel.text;
    
}

-(void)call:(id)sender{
    
    NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",currentSelectedLabel.text];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(UIImageView *)QRcodeImgView{
    
    CGFloat  qrcodeImgWidth = 115;
    
    if (!_QRcodeImgView) {
        
        CGFloat  imgWidth = 146,imgTop = 550/2;
        UIImageView * imgView = [[UIImageView alloc]initWithFrame:CGRectMake((self.width - imgWidth)/2,imgTop, imgWidth,imgWidth)];
        imgView.image = [UIImage imageNamed:@"边"];
        [self addSubview:imgView];
        
        _QRcodeImgView = [[UIImageView alloc]init];
        [imgView addSubview:_QRcodeImgView];
        _QRcodeImgView.contentMode = UIViewContentModeScaleAspectFill;
        [_QRcodeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(qrcodeImgWidth);
            make.center.mas_equalTo(0);
        }];
        
        
    }
    return _QRcodeImgView;
}

-(void)codeLabelSetup{
    
    CGFloat labelHeight = 12,bottomMar = 25;
    
    UILabel *codeLabel = [[UILabel alloc]init];
    codeLabel.text = @"扫描二维码  下载店主APP";
    codeLabel.textAlignment = NSTextAlignmentCenter;
    codeLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    codeLabel.font = [UIFont fontWithName:kBold size:12];
    [self.backImgView addSubview:codeLabel];
    [codeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(labelHeight);
        make.bottom.mas_equalTo(-bottomMar);
    }];
    
}

#pragma mark  --  array new  --

-(NSArray *)images{
    if (!_images) {
        _images =  @[@"店主手机",@"彩店定位"];
    }
    return _images;
}

-(NSMutableArray *)labels{
    if (!_labels) {
        _labels =  [NSMutableArray new];
    }
    return _labels;
}

#pragma mark  --  reload data  --

-(void)reloadDataWithInfo:(NSDictionary *)info{
    
    [self.bossImgView sd_setImageWithURL:[NSURL URLWithString:info[@"img"]] placeholderImage:[UIImage imageNamed:@"默认头像"]];
    
    self.shopNameLabel.text = info[@"name"];
    
    UILabel * mobileLabel = [self.labels firstObject];
    mobileLabel.text = info[@"mobile"];
    
    if (info[@"address"]) {
        UILabel * addressLabel = [self.labels lastObject];
        addressLabel.text = info[@"address"];
        [self setIsTop:YES label:addressLabel];
    }
    
    [self.QRcodeImgView sd_setImageWithURL:[NSURL URLWithString:info[@"qrImg"]] placeholderImage:[UIImage imageNamed:@""]];
    
}

-(void)setIsTop:(BOOL)isTop label:(UILabel *)label{
    
    if (isTop) {
        
        CGSize fontSize = [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}];
        //控件的高度除以一行文字的高度
        int num = label.frame.size.height/fontSize.height;
        //计算需要添加换行符个数
        NSInteger newLinesToPad = num  - label.numberOfLines;
        label.numberOfLines = 0;
        for(NSInteger i=0; i < newLinesToPad; i++){
            //在文字后面添加换行符"/n"
            label.text = [label.text stringByAppendingString:@"\n"];
        }
        
    }
}

@end
