//
//  FC_CardInfoView.h
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_CardInfoView : UIView

@property (nonatomic,strong) UIImageView * backImgView;

@property (nonatomic,strong) UIImageView * bossImgView;
@property (nonatomic,strong) UIImageView * QRcodeImgView;
@property (nonatomic,strong) UILabel* shopNameLabel;
@property (nonatomic,strong) UIButton * saveQRcodeButton;

-(void)reloadDataWithInfo:(NSDictionary *)info;

@end

NS_ASSUME_NONNULL_END
