//
//  FC_BossCardViewController.m
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_BossCardViewController.h"

#import "FC_CardInfoView.h"

#import <Photos/Photos.h>

@interface FC_BossCardViewController ()<UIImagePickerControllerDelegate>

@property (nonatomic,strong) FC_CardInfoView *basicInfoView;

@property (nonatomic,copy)  NSDictionary * storeInfo;

@property (nonatomic,strong) UIButton * saveQRcodeButton;

@property (nonatomic,strong) UIButton * shareButton;

@end

@implementation FC_BossCardViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertySetup];
    
    [self storeInfoRequest];
    
    [self.shareButton setTitle:@"立即分享" forState:(UIControlStateNormal)];
    [self.saveQRcodeButton setTitle:@"保存名片" forState:(UIControlStateNormal)];
    
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    [self setupBackImgView];
    
    [self.view addSubview:self.topNaviView];

    [self.backButton setImage:[UIImage imageNamed:@"返回_白"] forState:UIControlStateNormal];
    self.titleLabel.text = @"店主名片";
    self.titleLabel.textColor = [UIColor whiteColor];
    self.topNaviView.backgroundColor = [UIColor clearColor];
}

-(void)setupBackImgView{
    UIImageView * imgView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    imgView.image = [UIImage imageNamed:@"店主名片背景"];
    imgView.userInteractionEnabled = YES;
    [self.view addSubview:imgView];
}

#pragma mark  --  lazy  --

-(FC_CardInfoView *)basicInfoView{
    
    CGFloat  infoLeftMar = 33,infoTopMar = 102 + NAVI_SAFE_HEIGHT,infoHeight = 470;
    
    if (!_basicInfoView) {
        _basicInfoView= [[FC_CardInfoView alloc]initWithFrame:CGRectMake(infoLeftMar, infoTopMar, kWidth - infoLeftMar * 2, infoHeight)];
        [self.view addSubview:_basicInfoView];
    }
    return _basicInfoView;
}

-(UIButton *)shareButton{
    
    CGFloat  mar = 32,centerMar = 24,width = (kWidth - mar * 2 - centerMar)/2,height = 40,top = 23;
    
    if (!_shareButton) {
        
        _shareButton =  [[UIButton alloc]init];
        
        _shareButton.layer.cornerRadius = 5;
        _shareButton.layer.masksToBounds = YES;
        
        CAGradientLayer *gradientLayer =  [CAGradientLayer layer];
        gradientLayer.frame = CGRectMake(0, 0, width, height);
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(1, 0);
        gradientLayer.locations = @[@(0.5),@(1.0)];//渐变点
        [gradientLayer setColors:@[(id)[[UIColor colorWithHexString:@"#f6aa3e"] CGColor],(id)[[UIColor colorWithHexString:@"#f9ce04"] CGColor]]];//渐变数组
        [_shareButton.layer addSublayer:gradientLayer];
        
        _shareButton.titleLabel.font = [UIFont fontWithName:kBold size:17];
        [_shareButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        
        [_shareButton addTarget:self action:@selector(storeShareAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:_shareButton];
        
        [_shareButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(mar);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(self.basicInfoView.mas_bottom).offset(top);
        }];
        
    }
    return _shareButton;
}
// 分享
-(void)storeShareAction{
    
    UIImage * shotImage = [UIView makeImageWithView:self.basicInfoView withSize:self.basicInfoView.bounds.size];
    
    [FC_Manager shareThumImage:nil shareImage:shotImage];
}

-(UIButton *)saveQRcodeButton{
    
    CGFloat  mar = 32,centerMar = 24,width = (kWidth - mar * 2 - centerMar)/2,height = 40,top = 23;
    
    if (!_saveQRcodeButton) {
        
        _saveQRcodeButton =  [[UIButton alloc]init];
        _saveQRcodeButton.titleLabel.font = [UIFont fontWithName:kBold size:17];
        [_saveQRcodeButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _saveQRcodeButton.layer.cornerRadius = 5;
        _saveQRcodeButton.layer.masksToBounds = YES;
        [_saveQRcodeButton setBackgroundColor:RGB(253, 37, 37, 1)];
        [_saveQRcodeButton addTarget:self action:@selector(storeSaveAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:_saveQRcodeButton];
        [_saveQRcodeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-mar);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(self.basicInfoView.mas_bottom).offset(top);
        }];
        
    }
    return _saveQRcodeButton;
}
// 保存名片
-(void)storeSaveAction{
    
    UIImage * shotImage = [UIView makeImageWithView:self.basicInfoView withSize:self.basicInfoView.bounds.size];
    
    @WeakObj(self)
    if (@available(iOS 11.0, *))
    {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            @StrongObj(self)
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (status == PHAuthorizationStatusNotDetermined || status == PHAuthorizationStatusAuthorized) {
                    UIImageWriteToSavedPhotosAlbum(shotImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
                }else{
                    [self showToast];
                }
            });
            
        }];
    }else{
        if ([self checkPHAuthorizationStatus]) {
            UIImageWriteToSavedPhotosAlbum(shotImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }else{
            [self showToast];
        }
    }
    
}

-(void)showToast{
    [FC_Manager showToastWithText:@"请先开启保存图片权限"];
}

-(BOOL)checkPHAuthorizationStatus{
    
    __block  BOOL  isAuthoriza = YES;
    
    PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
    if (authStatus == PHAuthorizationStatusRestricted|| authStatus == PHAuthorizationStatusDenied) {
        isAuthoriza = NO;
        return YES;
    }
    
    return isAuthoriza;
}

#pragma mark --- <保存到相册>  ---
-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    NSString *msg = nil ;
    
    if(error){
        msg = @"保存名片失败" ;
    }else{
        msg = @"保存名片成功" ;
    }
    
    [FC_Manager showToastWithText:msg];
}


#pragma mark  --  store info request  --

-(NSDictionary *)storeInfo{
    
    if (!_storeInfo) {
        _storeInfo = [NSDictionary new];
    }
    return _storeInfo;
}

-(void)storeInfoRequest{
    
    NSDictionary *storeInfoDic =@{
                                  @"shopId":[LoginUser shareLoginUser].shopId
                                  };
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopInfo
               parameters:storeInfoDic
                  success:^(id responseObject) {
                      
                      @StrongObj(self)
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          self.storeInfo = responseObject[@"data"];
                          
                          [self.basicInfoView reloadDataWithInfo:self.storeInfo];
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
    } failure:^(NSError *error) {
        
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
