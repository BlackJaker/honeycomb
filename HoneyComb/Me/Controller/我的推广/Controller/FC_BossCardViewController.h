//
//  FC_BossCardViewController.h
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FC_BossCardViewController : AllViewController

@property (nonatomic,strong) UIImage * headImg;

@end

NS_ASSUME_NONNULL_END
