//
//  MeViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "MeViewController.h"
#import "ChangeMeViewController.h"
#import "SendMsgViewController.h"
#import "SendPrizeViewController.h"
#import "SettingViewController.h"
#import "OrderQueryViewController.h"
#import "AppNameViewController.h"
#import "StoreCertificationVC.h"
#import "OrderLookViewController.h"
#import "NotTicketViewController.h"

#import "FC_BossCardViewController.h"
#import "FC_AccountDetailVC.h"
#import "AF_StoreCertificationVC.h"

#import "FC_TopUpViewController.h"
#import "FC_WithdrawViewController.h"
#import "FC_PushSettingViewController.h"

#import "WithdrawalViewController.h"

@interface MeViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    
    UIButton *editorBtn;//编辑
    NSDictionary *dataDic;
    
}
@property (nonatomic,strong) AllModel *model;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *modelArr;
@property (nonatomic,strong) UIImageView *headerImage;//头像
@property (nonatomic,strong) UILabel *nameLabel;//名称
@property (nonatomic,strong) UILabel *priceS;//金额
@property (nonatomic,strong) UILabel *colorS;//彩金
@property (nonatomic,strong) UIButton *certificationBtn;//认证
@property (nonatomic,strong) UILabel *notLabel;//未出票
@property (nonatomic,strong) UILabel *withdrawalLabel;//提现金额
@property (nonatomic,strong) UILabel *sendLabel;//未派奖

@property (nonatomic,strong) NSDictionary * safeInfoDic;

@end

@implementation MeViewController

static NSString *cellID = @"cell";
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden =NO;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self creatData];
    [self getShopSafetyRequest];
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layoutView];
   
}
-(void)creatData
{
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId]};
    
    [PPNetworkHelper POST:PostMeDetail parameters:dic success:^(id responseObject) {
        
        NSLog(@"个人中心-%@",responseObject);
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            self.modelArr = [AllModel mj_objectArrayWithKeyValuesArray:[responseObject objectForKey:@"data"]];
            
            dataDic = [solveJsonData changeType:[responseObject objectForKey:@"data"]];
            _sendLabel.text = [NSString stringWithFormat:@"%@单未派奖",[dataDic objectForKey:@"SendPrize"]];
            _notLabel.text = [NSString stringWithFormat:@"%@单未出",[dataDic objectForKey:@"drawBill"]];
            _withdrawalLabel.text = [NSString stringWithFormat:@"可用额度%@元",[dataDic objectForKey:@"money"]];
            
            [[LoginUser shareLoginUser]setDrawMoney:[dataDic objectForKey:@"money"]];
            
            NSString *storeStr = nil;
            if ([[dataDic objectForKey:@"shopState"] isEqualToString:@"NORMAL"]) {
                storeStr = @"已认证";
                [_certificationBtn setEnabled:NO];
            }else if ([[dataDic objectForKey:@"shopState"] isEqualToString:@"LOCKED"]){
                storeStr = @"锁定";
                [_certificationBtn setEnabled:NO];
            }else if ([[dataDic objectForKey:@"shopState"] isEqualToString:@"AUDIT"]){
                storeStr = @"审核中";
                [_certificationBtn setEnabled:NO];
            }else
            {
                storeStr = [dataDic[@"shopState"] isEqualToString:@"UNVERIFIED"]?@"未认证":@"被驳回";
                _certificationBtn.backgroundColor = RGB(249, 249, 249, 1);
                [_certificationBtn setTitleColor:kBlackColor forState:(UIControlStateNormal)];
                _certificationBtn.layer.cornerRadius = 4;
                _certificationBtn.layer.masksToBounds = YES;
                _certificationBtn.showsTouchWhenHighlighted = YES;
            }
            [_certificationBtn setTitle:storeStr forState:(UIControlStateNormal)];
            
            [[NSUserDefaults standardUserDefaults] setObject:dataDic[@"shopState"] forKey:@"shopStatus"];
            
            _colorS.text = [NSString stringWithFormat:@"%@",[dataDic objectForKey:@"userMoney"]];
            NSURL *url = [NSURL URLWithString:[dataDic objectForKey:@"img"]];
            [_headerImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"默认头像"]];
            _nameLabel.text = [NSString stringWithFormat:@"%@",[dataDic objectForKey:@"appName"]];
            _priceS.text = [NSString stringWithFormat:@"%@",[dataDic objectForKey:@"money"]];
        }else
        {
            
        }
    } failure:^(NSError *error) {
        NSLog(@"个人中心-%@",error);
    }];
}
-(void)layoutView
{
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-TabBarHeight)];
    self.tableView.dataSource=  self;
    self.tableView.delegate = self;
    self.tableView.separatorColor = RGB(0, 0, 0, 0.15);
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
    [self.view addSubview:self.tableView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 9;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;//右侧箭头
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 12, 0, 12)];//分割线延长
    cell.textLabel.textColor = RGB(51, 51, 51, 1);
    cell.textLabel.font = [UIFont fontWithName:kPingFangRegular size:kHeight/44.4666];
    
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame] ;
    cell.selectedBackgroundView.backgroundColor =RGB(0, 0, 0, 0.5);
    if (indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryNone;//右侧箭头
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight/2.79)];
        image.image = [UIImage imageNamed:@"组6"];
        image.userInteractionEnabled = YES;
        [cell.contentView addSubview:image];
        
        editorBtn = [[UIButton alloc]initWithFrame:CGRectMake(kWidth/25, kHeight/20.84, kWidth/23.4375, kHeight/15)];
        [editorBtn setImage:[UIImage imageNamed:@"编辑"] forState:(UIControlStateNormal)];
        [editorBtn addTarget:self action:@selector(editorAction) forControlEvents:(UIControlEventTouchUpInside)];
        [image addSubview:editorBtn];
        
   
        
        UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/9.375, NavgationBarHeight-kHeight/16.675, kWidth-kWidth/4.6875, kHeight/15.159)];
        titleLab.font = [UIFont fontWithName:kPingFangRegular size:kHeight/33.35];
        titleLab.text  = @"个人中心";
        titleLab.textAlignment = NSTextAlignmentCenter;
        titleLab.textColor = [UIColor whiteColor];
        [image addSubview:titleLab];
        
      
        _headerImage = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth/2-kWidth/12.5, CGRectGetMaxY(titleLab.frame)+10, kWidth/6.25, kWidth/6.25)];
        _headerImage.image = [UIImage imageNamed:@"默认头像"];
        _headerImage.layer.cornerRadius = (kWidth/6.25)/2;
        _headerImage.layer.masksToBounds = YES;
        _headerImage.userInteractionEnabled = YES;
        [cell.contentView addSubview:_headerImage];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editorAction)];
        [_headerImage addGestureRecognizer:tap];
        
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/6.25, CGRectGetMaxY(_headerImage.frame)+10, kWidth-kWidth/3.125, kHeight/37)];
        _nameLabel.font = [UIFont fontWithName:kPingFangRegular size:kHeight/47.6428];
        _nameLabel.userInteractionEnabled = YES;
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.textColor = [UIColor whiteColor];
        [image addSubview:_nameLabel];
        
        UITapGestureRecognizer *nameTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editorAction)];
        [_nameLabel addGestureRecognizer:nameTap];
        NSInteger i;
        if (TabBarHeight == 83) {
            i = 667;
        }else
        {
            i = kHeight;
        }
        _certificationBtn = [[UIButton alloc]initWithFrame:CGRectMake(kWidth/2-kWidth/15, CGRectGetMaxY(_nameLabel.frame)+1, kWidth/7.5, i/44.4666)];
      
        [_certificationBtn addTarget:self action:@selector(editorAction) forControlEvents:(UIControlEventTouchUpInside)];
        [_certificationBtn setTitleColor:RGB(255, 255, 255, 0.6) forState:(UIControlStateNormal)];
        _certificationBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
        [image addSubview:_certificationBtn];
        
        
        UILabel *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, kHeight/3.4921, kWidth/2, 12)];
        priceLabel.font = [UIFont fontWithName:kPingFangRegular size:kHeight/60.6363];
        priceLabel.text  = @"总额 (元)";
        priceLabel.textAlignment = NSTextAlignmentCenter;
        priceLabel.textColor = RGB(255, 255, 255, 0.6);
        [image addSubview:priceLabel];
        
        _priceS = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(priceLabel.frame)+10, kWidth/2, kHeight/33.35)];
        _priceS.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        _priceS.text  = @"0.00";
        
        _priceS.textAlignment = NSTextAlignmentCenter;
        _priceS.textColor = RGB(255, 255, 255, 1);
        [image addSubview:_priceS];
        
        UILabel *colorLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/2, kHeight/3.4921, kWidth/2, kHeight/55.5833)];
        colorLabel.font = [UIFont fontWithName:kPingFangRegular size:kHeight/60.6363];
        colorLabel.text  = @"托管记账 (元)";
        colorLabel.textAlignment = NSTextAlignmentCenter;
        colorLabel.textColor = RGB(255, 255, 255, 0.6);
        [image addSubview:colorLabel];
        
        _colorS= [[UILabel alloc]initWithFrame:CGRectMake(kWidth/2, CGRectGetMaxY(priceLabel.frame)+10, kWidth/2, kHeight/33.35)];
        _colorS.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        _colorS.text  = @"0.00";
        
        _colorS.textAlignment = NSTextAlignmentCenter;
        _colorS.textColor = RGB(255, 255, 255, 1);
        [image addSubview:_colorS];
    }
    NSInteger p;
    if (TabBarHeight == 83) {
        p = 0;
    }else{
        p =1;
    }
//    if (indexPath.row == 1) {
//        cell.textLabel.text = @"未出票";
//        cell.imageView.image = [UIImage imageNamed:@"not"];
//        _notLabel=[[UILabel alloc]init];
//        _notLabel.font=[UIFont systemFontOfSize:kHeight/55.58];
//        _notLabel.textAlignment = NSTextAlignmentRight;
//        _notLabel.textColor=RGB(153, 152, 153, 1);
//        [_notLabel sizeToFit];
//        _notLabel.frame=CGRectMake(kWidth /2, 0, kWidth/2-kWidth/15, kHeight/16.675-p);
//        [cell.contentView addSubview:_notLabel];
//    }
    if (indexPath.row == 1) {
        cell.textLabel.text = @"派奖";
        cell.imageView.image = [UIImage imageNamed:@"奖章"];
        _sendLabel=[[UILabel alloc]init];
        
        
        _sendLabel.font=[UIFont systemFontOfSize:kHeight/55.58];
        _sendLabel.textColor=RGB(153, 152, 153, 1);
        _sendLabel.textAlignment = NSTextAlignmentRight;
        [_sendLabel sizeToFit];
        _sendLabel.frame=CGRectMake(kWidth /2, 0, kWidth/2-kWidth/15, kHeight/16.675-p);
        [cell.contentView addSubview:_sendLabel];
        
    }
    
    if (indexPath.row == 2) {
        cell.textLabel.text = @"提现";
        cell.imageView.image = [UIImage imageNamed:@"钱包"];
        self.withdrawalLabel=[[UILabel alloc]init];
        
        self.withdrawalLabel.font=[UIFont systemFontOfSize:kHeight/55.58];
        self.withdrawalLabel.textColor=RGB(231, 31, 25, 1);
        self.withdrawalLabel.textAlignment = NSTextAlignmentRight;
        [ self.withdrawalLabel sizeToFit];
        self.withdrawalLabel.frame=CGRectMake(kWidth -kWidth/1.8 -kWidth/15, 0, kWidth/1.8, kHeight/16.675-p);
        [cell.contentView addSubview: self.withdrawalLabel];
        
    }
    if (indexPath.row == 3) {
        cell.textLabel.text = @"充值";
        cell.imageView.image = [UIImage imageNamed:@"充值"];
        UILabel *label=[[UILabel alloc]init];
        label.font=[UIFont systemFontOfSize:kHeight/55.58];
        label.textColor=RGB(231, 31, 25, 1);
        [label sizeToFit];
        label.frame=CGRectMake(kWidth - label.frame.size.width-kWidth/15, kHeight/55.5833, label.frame.size.width, label.frame.size.height);
        [cell.contentView addSubview:label];
    }
//    if (indexPath.row == 5) {
//        cell.textLabel.text = @"发送消息";
//        cell.imageView.image = [UIImage imageNamed:@"评论"];
//    }
    
    if (indexPath.row == 4) {
        cell.textLabel.text = @"推广彩民";
        cell.imageView.image = [UIImage imageNamed:@"分享"];
        UILabel *label=[[UILabel alloc]init];
        //            label.text=@"下载扫描";
        label.font=[UIFont systemFontOfSize:kHeight/55.58];
        label.textColor=RGB(153, 152, 153, 1);
        [label sizeToFit];
        label.frame=CGRectMake(kWidth - label.frame.size.width-kWidth/15, kHeight/55.5833, label.frame.size.width, label.frame.size.height);
        [cell.contentView addSubview:label];
        
    }

    if (indexPath.row == 5) {
        cell.textLabel.text = @"历史订单查询";
        cell.imageView.image = [UIImage imageNamed:@"订单"];
    }
//    if (indexPath.row == 7) {
//        cell.textLabel.text = @"使用教程";
//        cell.imageView.image = [UIImage imageNamed:@"教程－千牛"];
//    }
    if (indexPath.row == 6) {
        cell.textLabel.text = @"账户明细";
        cell.imageView.image = [UIImage imageNamed:@"裕农通-账户明细查询"];
    }
    if (indexPath.row == 7) {
        cell.textLabel.text = @"推送设置";
        cell.imageView.image = [UIImage imageNamed:@"gr_tssz_dz"];
    }
    if (indexPath.row == 8) {
        cell.textLabel.text = @"设置";
        cell.imageView.image = [UIImage imageNamed:@"设置(1)"];
    }
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return kHeight/2.7907;
    }
    
    return kHeight/16.675;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中颜色消失
    
    if (indexPath.row != 8) {
        NSInteger shopStatus = [self checkShopStatus];
        if (shopStatus != 1) {
            return;
        }
    }
    
//    if (indexPath.row == 1) {
//        self.hidesBottomBarWhenPushed=YES;
//        [self.navigationController pushViewController:[[NotTicketViewController alloc]init] animated:YES];
//        self.hidesBottomBarWhenPushed=NO;
//    }
    if (indexPath.row == 1) {
        self.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:[[SendPrizeViewController alloc]init] animated:YES];
        self.hidesBottomBarWhenPushed=NO;
    }
    if (indexPath.row == 2) {
        
        NSInteger isPay = [self.safeInfoDic[@"isPay"] integerValue];
        
        if (isPay == 0) {
            self.hidesBottomBarWhenPushed=YES;
            WithdrawalViewController * setWithdrawPasswordVc = [[WithdrawalViewController alloc]initType:1 controller:self safeInfoDic:self.safeInfoDic];
            [self.navigationController pushViewController:setWithdrawPasswordVc animated:YES];
            self.hidesBottomBarWhenPushed=NO;
            
            return;
        }
        
        self.hidesBottomBarWhenPushed=YES;
        FC_WithdrawViewController * fcVc = [[FC_WithdrawViewController alloc]init];
        fcVc.safeInfoDic = self.safeInfoDic;
        fcVc.rootVc = self;
        [self.navigationController pushViewController:fcVc animated:YES];
        self.hidesBottomBarWhenPushed=NO;
    }
    if (indexPath.row == 3) {
        self.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:[[FC_TopUpViewController alloc]init] animated:YES];
        self.hidesBottomBarWhenPushed=NO;
    }
//    if (indexPath.row == 5) {
//        self.hidesBottomBarWhenPushed=YES;
//        SendMsgViewController *senVC = [[SendMsgViewController alloc]init];
//        [self.navigationController pushViewController:senVC animated:YES];
//        self.hidesBottomBarWhenPushed=NO;
//    }
    if (indexPath.row == 4) {
        self.hidesBottomBarWhenPushed = YES;
        NSString * state = [dataDic objectForKey:@"shopState"];
        if ([state isEqualToString:@"NORMAL"]) {
            self.hidesBottomBarWhenPushed=YES;
            FC_BossCardViewController * bossCardVc = [[FC_BossCardViewController alloc]init];
            [self.navigationController pushViewController:bossCardVc animated:YES];
            self.hidesBottomBarWhenPushed=NO;
        }else if ([state isEqualToString:@"AUDIT"]){
            [FC_Manager showToastWithText:@"店铺审核中,请耐心等待"];
        }else if ([state isEqualToString:@"LOCKED"]){
            [FC_Manager showToastWithText:@"店铺已锁定,请联系客服人员咨询"];
        }else{
            [self showAlertViewController];
        }
        self.hidesBottomBarWhenPushed = NO;
    }
    if (indexPath.row ==5) {
        self.hidesBottomBarWhenPushed=YES;
        OrderLookViewController *orVC = [[OrderLookViewController alloc]init];
        [self.navigationController pushViewController:orVC animated:YES];
        self.hidesBottomBarWhenPushed=NO;
    }
    if (indexPath.row == 6) {
        self.hidesBottomBarWhenPushed=YES;
        FC_AccountDetailVC *accountVc = [[FC_AccountDetailVC alloc]init];
        [self.navigationController pushViewController:accountVc animated:YES];
        self.hidesBottomBarWhenPushed=NO;
    }
    if (indexPath.row == 7) {
        self.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:[[FC_PushSettingViewController alloc]init] animated:YES];
        self.hidesBottomBarWhenPushed=NO;
    }
    if (indexPath.row == 8) {
        self.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:[[SettingViewController alloc]init] animated:YES];
        self.hidesBottomBarWhenPushed=NO;
    }
}

-(NSString *)checkUserSafeInfo{
    
    NSString * errorMessage = nil;
    
    NSInteger isPay = [self.safeInfoDic[@"isPay"] integerValue];
    NSInteger isIdCard = [self.safeInfoDic[@"isIdCard"] integerValue];
    
    if (isPay == 0 && isIdCard == 0) {
        errorMessage = @"请前往个人信息进行身份认证和提现密码设置";
    }else if (isPay == 0){
        errorMessage = @"请前往个人信息进行提现密码设置";
    }else if (isIdCard == 0){
        errorMessage = @"请前往个人信息进行身份认证";
    }
    return errorMessage;
}



//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (section == 0) {
//        return 239;
//    }
//    return 0.0001;
//}
#pragma mark 立即认证
-(void)certifiAction:(UIButton *)btn
{
    //    NSLog(@"--%@",btn.titleLabel.text);
    //    if ([[dataDic objectForKey:@"appName"] isEqualToString:@"未设置"]) {
    //        NSLog(@"111");
    //        self.hidesBottomBarWhenPushed=YES;
    //        AppNameViewController *chVC = [[AppNameViewController alloc]init];
    //        [self.navigationController pushViewController:chVC animated:YES];
    //        self.hidesBottomBarWhenPushed=NO;
    //    }else
    //    {
    //          NSLog(@"222");
    //        self.hidesBottomBarWhenPushed=YES;
    //        StoreCertificationVC *chVC = [[StoreCertificationVC alloc]init];
    //        [self.navigationController pushViewController:chVC animated:YES];
    //        self.hidesBottomBarWhenPushed=NO;
    //    }
    self.hidesBottomBarWhenPushed=YES;
    ChangeMeViewController *chVC = [[ChangeMeViewController alloc]init];
    chVC.modeDic = dataDic;
    chVC.safeInfoDic = self.safeInfoDic;
    [self.navigationController pushViewController:chVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
}
#pragma mark 编辑点击事件
-(void)editorAction
{
    
    NSInteger shopState = [self checkShopStatus];
    
    if (shopState != 1) {
        return;
    }
    
    self.hidesBottomBarWhenPushed=YES;
    ChangeMeViewController *chVC = [[ChangeMeViewController alloc]init];
    chVC.modeDic = dataDic;
    chVC.safeInfoDic = self.safeInfoDic;
    [self.navigationController pushViewController:chVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
  
}

#pragma mark  --  shop safety request  --

-(void)getShopSafetyRequest{
    
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId]};
    @WeakObj(self)
    [PPNetworkHelper POST:GetShopSafety parameters:dic success:^(id responseObject) {
        @StrongObj(self)
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            self.safeInfoDic = responseObject[@"data"];
        }
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  --  check shop status  --

-(NSInteger)checkShopStatus{
    
    NSString * shopStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"shopStatus"];
    
    if ([shopStatus isEqualToString:@"NORMAL"]) {
        return 1;
    }else if ([shopStatus isEqualToString:@"AUDIT"]){
        [FC_Manager showToastWithText:@"店铺审核中"];
        return 2;
    }else{
        [self showAlertViewController];
        return 0;
    }
    
}

-(void)showAlertViewController{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"需要进行店铺认证,是否去认证?" preferredStyle:UIAlertControllerStyleAlert];
    @WeakObj(self)
    UIAlertAction *savePhotoAction = [UIAlertAction actionWithTitle:@"去认证" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @StrongObj(self)
        
       [self pushStoreCertificationVc];
        
       [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"再逛逛" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancelAction];
    [alertController addAction:savePhotoAction];
    
    
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    
}

-(void)pushStoreCertificationVc{
    
    self.hidesBottomBarWhenPushed=YES;
    AF_StoreCertificationVC *storeCertVc = [[AF_StoreCertificationVC alloc]init];
//    storeCertVc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:storeCertVc animated:YES];
    self.hidesBottomBarWhenPushed=NO;
    
}

@end
