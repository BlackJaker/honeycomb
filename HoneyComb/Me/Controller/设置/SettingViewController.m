//
//  SettingViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "SettingViewController.h"
#import "WeViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "FC_FollowExplainViewController.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource,UIApplicationDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIButton *caseBtn;

@property (nonatomic,copy) NSArray * titles;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"设置";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];

}

-(NSArray *)titles{
    
    if (!_titles) {
        NSString * shopStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"shopStatus"];
        _titles = [shopStatus isEqualToString:@"NORMAL"]?@[@"加入全网跟单",@"关于我们",@"清除缓存"]:@[@"关于我们",@"清除缓存"];
    }
    return _titles;
}

-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, self.titles.count * 45) style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorColor = RGB(0, 0, 0, 0.15);
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"退出登录" forState:(UIControlStateNormal)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];

    CGFloat  bottom = IPHONE_X?TAB_BAR_SAFE_HEIGHT:10;
    
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(DEFAULT_WIDTH);
        make.height.mas_equalTo(45);
        make.bottom.mas_equalTo(-bottom);
        make.left.mas_equalTo(kLeftMar);
    }];
}
-(void)caseAction
{
    //退出登录
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定退出当前账户" preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alertController animated:YES completion:nil];
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    
    @WeakObj(self)
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action){
        
        @StrongObj(self)
        
        [self requestShopPushUnBind];  // 解绑
        
        [self clearAllUserDefaultsData];
        
        [((AppDelegate *)[UIApplication sharedApplication].delegate) initMainVIew];
        
    }];
    
    [alertController addAction:cancleAction];
    [alertController addAction:defaultAction];
}

#pragma mark  --  request push unbind  --

-(void)requestShopPushUnBind{
    
    NSMutableDictionary * loginDica = [NSMutableDictionary new];
    [loginDica setValue:[LoginUser shareLoginUser].shopId forKey:@"shopid"];
    [loginDica setValue:[LoginUser shareLoginUser].devToken forKey:@"pushId"];
    [loginDica setValue:@"1" forKey:@"type"];
    
    [PPNetworkHelper POST:ShopPushUnBind
               parameters:loginDica
                  success:^(id responseObject) {
                      
                  } failure:^(NSError *error) {
                      
                  }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titles.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];//分割线延长
    cell.textLabel.textColor = RGB(51, 51, 51, 1);
    cell.textLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    
    cell.textLabel.text = [self.titles objectAtIndex:indexPath.row];
    if (indexPath.row < self.titles.count - 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;//右侧箭头
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中颜色消失
    
    if (indexPath.row < self.titles.count) {
        if (self.titles.count > 2) {
            if (indexPath.row == 0) {
                self.hidesBottomBarWhenPushed=YES;
                [self.navigationController pushViewController:[[FC_FollowExplainViewController alloc]init] animated:YES];
            }
        }
        if (indexPath.row == self.titles.count - 2) {
            self.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:[[WeViewController alloc]init] animated:YES];
        }
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
-(void)clearAllUserDefaultsData{
    
    NSUserDefaults *defatluts = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictionary = [defatluts dictionaryRepresentation];
    
    for(NSString *key in [dictionary allKeys]){

        if (![key isEqualToString:@"devToken"] && ![key isEqualToString:[LoginUser shareLoginUser].shopId] && ![key isEqualToString:[NSString stringWithFormat:@"%@_firstShopClose",[LoginUser shareLoginUser].shopId]]) {

                    [defatluts removeObjectForKey:key];
                    
                    [defatluts synchronize];
        }
    }
    
    
}
- (void)popAllController{
    NSArray *array = ((UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController).viewControllers;
    if ([array count] >= 1)
    {
        for (NSUInteger index= [array count] - 1;index > 0; index-- )
        {
            UIViewController *controller = [array objectAtIndex:index];
            [controller.navigationController popViewControllerAnimated:NO];
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
