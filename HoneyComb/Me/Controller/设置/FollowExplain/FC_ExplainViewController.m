//
//  FC_ExplainViewController.m
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_ExplainViewController.h"

#import <JavaScriptCore/JavaScriptCore.h>

#import <WebKit/WebKit.h>

@interface FC_ExplainViewController ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>

@property(nonatomic,strong)WKWebView *webView;
@property (strong, nonatomic) UIProgressView *progressView;

@end

@implementation FC_ExplainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.titleLabe.text = @"跟单说明";
    
    self.view.backgroundColor = RGB(235, 235, 235, 1.0f);
    
    //禁用掉自动设置的内边距，自行控制controller上index为0的控件以及scrollview控件的位置
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    // 设置偏好设置
    config.preferences = [[WKPreferences alloc] init];
    // 默认为0
    config.preferences.minimumFontSize = 10;
    // 默认认为YES
    config.preferences.javaScriptEnabled = YES;
    // 在iOS上默认为NO，表示不能自动通过窗口打开
    config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
    // web内容处理池，由于没有属性可以设置，也没有方法可以调用，不用手动创建
    config.processPool = [[WKProcessPool alloc] init];
    // 通过JS与webview内容交互
    config.userContentController = [[WKUserContentController alloc] init];
    
//    // 注入JS对象名称AppModel，当JS通过AppModel来调用时，
//    // 我们可以在WKScriptMessageHandler代理中接收到
//    [config.userContentController addScriptMessageHandler:self name:@"AppModel"];
    
    //    默认构造器
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, kWidth, kHeight - NAVIGATION_HEIGHT) configuration:config];
    _webView.backgroundColor = RGB(235, 235, 235, 1.0f);
    if (@available(iOS 11.0, *)) {
        _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //    _webView.delegate = self;
    _webView.UIDelegate  = self;
    _webView.navigationDelegate = self;
    [self.view addSubview:_webView];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"explain" ofType:@"html"];

    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
    
    
    
    // KVO，监听webView属性值得变化(estimatedProgress,title为特定的key)
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    // UIProgressView初始化
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    self.progressView.frame = CGRectMake(0, 0, self.webView.frame.size.width, 2);
    self.progressView.trackTintColor = [UIColor lightGrayColor]; // 设置进度条的色彩
    self.progressView.progressTintColor = [UIColor colorWithHexString:@"#25A4FD"];
    // 设置初始的进度，防止用户进来就懵逼了（微信大概也是一开始设置的10%的默认值）
    [self.progressView setProgress:0.1 animated:YES];
    [self.webView addSubview:self.progressView];
    
}

#pragma mark - KVO监听
// 第三部：完成监听方法
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([object isEqual:self.webView] && [keyPath isEqualToString:@"estimatedProgress"]) { // 进度条
        
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        NSLog(@"打印测试进度值：%f", newprogress);
        
        if (newprogress == 1) { // 加载完成
            // 首先加载到头
            [self.progressView setProgress:newprogress animated:YES];
            // 之后0.3秒延迟隐藏
            __weak typeof(self) weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                weakSelf.progressView.hidden = YES;
                [weakSelf.progressView setProgress:0 animated:NO];
            });
            
        } else { // 加载中
            self.progressView.hidden = NO;
            [self.progressView setProgress:newprogress animated:YES];
        }
    } else { // 其他
        
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
//    if ([message.name isEqualToString:@"AppModel"]) {
//        // 打印所传过来的参数，只支持NSNumber, NSString, NSDate, NSArray,
//        // NSDictionary, and NSNull类型
//        NSLog(@"%@", message.body);
//
//        NSArray *array = [[message.body objectForKey:@"body"] componentsSeparatedByString:@"-"];
//        if ([array[0] isEqualToString:@"bs"]) {
//            CompetitionDetailViewController *detailVC = [[CompetitionDetailViewController alloc] init];
//            detailVC.isCompetitionFlag = @"0";
//            detailVC.itemid = [message.body objectForKey:@"body"];
//            detailVC.type = @"1";
//            detailVC.flag = @"1";
//            [self.navigationController pushViewController:detailVC animated:YES];
//        }
//        if ([array[0] isEqualToString:@"order"]) {
//            if (SYS_SINGLETON.user.uid.length>0) {
//                PayViewController *payVC = [[PayViewController alloc] init];
//                payVC.isWebflag = @"1";
//                payVC.lottoryKindStr = @"1";
//                payVC.orderNum = [array lastObject];
//                [self.navigationController pushViewController:payVC animated:YES];
//            }else{
//                [self loginOrNot];
//            }
//        }
//        if ([array[0] isEqualToString:@"xy"]) {
//            WebPageViewController *webPageVC = [[WebPageViewController alloc] init];
//            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:webPageVC];
//            webPageVC.urlStr = @"/用户协议/用户协议.html";
//            webPageVC.isModalView = @"1";
//            [self presentViewController:nav animated:YES completion:nil];
//        }
//
//        
//    }
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    // 链接已经点击
    if (navigationAction.navigationType == WKNavigationTypeLinkActivated) {
        // 对于跨域，需要手动跳转
        //        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
        // 不允许web内跳转
        decisionHandler(WKNavigationActionPolicyCancel);
    } else {
        
        //        self.progressView.alpha = 1.0;
        decisionHandler(WKNavigationActionPolicyAllow);
    }
    NSString *requestString = [navigationAction.request.URL absoluteString];
    
    requestString = [requestString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //获取H5页面里面按钮的操作方法,根据这个进行判断返回是内部的还是push的上一级页面
    NSLog(@"requestString======= %@",requestString);
    
    if ([requestString hasPrefix:@"back://"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    NSLog(@"%s", __FUNCTION__);
}
//-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigatvon *)navigation{
//
//
//    JSContext *context = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
//    context.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue)
//    {
//        context.exception = exceptionValue;
//    };
//    //定义好JS要调用的方法, share就是调用的share方法名
//    context[@"goIosOrder"] = ^(NSString *orderNum) {
//        NSLog(@"+++++++Begin Log+++++++");
//        dispatch_async(dispatch_get_main_queue(), ^{
//            //                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ID  message:@"这是OC原生的弹出窗" delegate:self cancelButtonTitle:@"收到" otherButtonTitles:nil];
//            //                [alertView show];
//            if (SYS_SINGLETON.user.uid.length>0) {
//                NSArray *array = [orderNum componentsSeparatedByString:@"-"];
//                PayViewController *payVC = [[PayViewController alloc] init];
//                payVC.isWebflag = @"1";
//                payVC.lottoryKindStr = @"1";
//                payVC.orderNum = [array lastObject];
//                [self.navigationController pushViewController:payVC animated:YES];
//            }else{
//                [self loginOrNot];
//            }
//
//
//        });
//        NSLog(@"-------End Log-------");
//    };
//
//    //定义好JS要调用的方法, share就是调用的share方法名
//    context[@"goIosXieyi"] = ^(NSString *str) {
//        NSLog(@"+++++++Begin Log+++++++");
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            //            LoginViewController *loginVC = [[LoginViewController alloc] init];
//            //
//            //            [self presentViewController:nav animated:YES completion:nil];
//            WebPageViewController *webPageVC = [[WebPageViewController alloc] init];
//            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:webPageVC];
//            webPageVC.urlStr = @"/用户协议/用户协议.html";
//            webPageVC.isModalView = @"1";
//            [self presentViewController:nav animated:YES completion:nil];
//
//        });
//        NSLog(@"-------End Log-------");
//    };
//
//
//    //定义好JS要调用的方法, share就是调用的share方法名
//    context[@"goIosDetail"] = ^(NSString *itemsId) {
//        NSLog(@"+++++++Begin Log+++++++");
//        dispatch_async(dispatch_get_main_queue(), ^{
//            CompetitionDetailViewController *detailVC = [[CompetitionDetailViewController alloc] init];
//            detailVC.isCompetitionFlag = @"0";
//            detailVC.itemid = itemsId;
//            detailVC.type = @"1";
//            detailVC.flag = @"1";
//            [self.navigationController pushViewController:detailVC animated:YES];
//            //            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:itemsId  message:@"这是OC原生的弹出窗" delegate:self cancelButtonTitle:@"收到" otherButtonTitles:nil];
//            //            [alertView show];
//        });
//        NSLog(@"-------End Log-------");
//    };
//
//}

//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//    NSString * requestString = [[request URL] absoluteString];
//    requestString = [requestString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    //获取H5页面里面按钮的操作方法,根据这个进行判断返回是内部的还是push的上一级页面
//    NSLog(@"requestString======= %@",requestString);
//
//    if ([requestString hasPrefix:@"back://"])
//    {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    return YES;
//}

- (void)dealloc
{
    _webView.UIDelegate = nil;
    _webView.navigationDelegate = nil;
    [_webView loadHTMLString:@"" baseURL:nil];
    [_webView stopLoading];
    [_webView removeFromSuperview];
    // 最后一步：移除监听
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    _webView = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
