//
//  FC_FollowExplainViewController.m
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_FollowExplainViewController.h"

#import "UWHttpTool.h"

#import "FC_ExplainViewController.h"

@interface FC_FollowExplainViewController ()

@property (nonatomic,assign) BOOL  isOn;
@property (nonatomic,strong) UISwitch * sw;

@end

@implementation FC_FollowExplainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    [self propertySetup];
    
    [self addRightButton];
    
    [self contentSetup];
    
    [self getFollowSwitchStateRequest];
    
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    self.titleLabe.text = @"跟单设置";
    
    self.view.backgroundColor = RGB(235, 235, 235, 1.0f);
    
    self.isOn = YES;
    
}

-(void)addRightButton{
    
    UIButton *explainBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [explainBtn setTitle:@"说明" forState:UIControlStateNormal];
    [explainBtn.titleLabel setFont:[UIFont fontWithName:kMedium size:14]];
    [explainBtn addTarget:self action:@selector(explainAction) forControlEvents:UIControlEventTouchUpInside];
    
    //把右侧的两个按钮添加到rightBarButtonItem
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:explainBtn];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
}

-(void)explainAction{
    
    self.hidesBottomBarWhenPushed = YES;
    FC_ExplainViewController * explainVc = [[FC_ExplainViewController alloc]init];
    [self.navigationController pushViewController:explainVc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

-(void)contentSetup{
    
    CGFloat  height = 45;
    
    UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMar, kTopMar, DEFAULT_WIDTH, height)];
    backView.backgroundColor = [UIColor whiteColor];
    backView.layer.cornerRadius = 5.0f;
    backView.clipsToBounds = YES;
    [self.view addSubview:backView];
    
    UILabel *label = [UILabel createLabelWithFrame:CGRectMake(kLeftMar, 0, 200, height) text:@"加入全网跟单" fontSize:16 textColor:[UIColor blackColor]];
    label.textAlignment = NSTextAlignmentLeft;
    [backView addSubview:label];
    
    CGFloat  leftMargin = DEFAULT_WIDTH - 50,switchHeight = 30;
    
    _sw = [[UISwitch alloc]initWithFrame:CGRectMake(leftMargin, (height - switchHeight)/2, 0, 0)];
    _sw.on = self.isOn;
    _sw.transform = CGAffineTransformMakeScale(0.7, 0.7);
    [_sw addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    [backView addSubview:_sw];
    
}

-(void)switchAction:(UISwitch *)sw{
    
    [self setFollowSwitchStateRequest];
    
}

#pragma mark  --  request  --

-(void)getFollowSwitchStateRequest{
    
    [UWHttpTool getWithURL:GetAllFollow
                    params:@{@"shop_id":[LoginUser shareLoginUser].shopId}
                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                       
                       if ([responseObject[@"status"] integerValue] == 0) {
                           
                           [self reloadWithResponse:responseObject];
                           
                       }
                       
                   } failure:^(NSURLSessionDataTask * _Nullable task, NSString * _Nonnull errorMsg) {
                       
                   }];
    
}

-(void)setFollowSwitchStateRequest{
    
    [UWHttpTool getWithURL:SetAllFollow
                    params:@{@"shop_id":[LoginUser shareLoginUser].shopId}
                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                       
                   } failure:^(NSURLSessionDataTask * _Nullable task, NSString * _Nonnull errorMsg) {
                       
                   }];
    
}

-(void)reloadWithResponse:(NSDictionary *)response{
    
    self.isOn = [response[@"data"][@"follow"] boolValue];
    _sw.on = self.isOn;
}

@end
