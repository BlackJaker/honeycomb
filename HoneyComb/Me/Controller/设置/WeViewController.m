//
//  WeViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "WeViewController.h"

@interface WeViewController ()

@end

@implementation WeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"关于我们";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
}
-(void)layoutView
{
    UIImageView *image =[[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"安蜂巢logo0603"];
    [self.view addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(77);
        make.height.mas_equalTo(104);
        make.left.mas_equalTo(kWidth/2-38);
        make.top.mas_equalTo(65);
    }];
    UILabel *versionLabel = [[UILabel alloc]init];
    
    NSString *app_Version = [InfoDictionary objectForKey:@"CFBundleShortVersionString"];
    versionLabel.text = [NSString stringWithFormat:@"当前版本v%@",app_Version];
    versionLabel.textColor = kGrayColor;
    versionLabel.textAlignment = NSTextAlignmentCenter;
    versionLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.view addSubview:versionLabel];
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(14);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(image.mas_bottom).offset(32);
    }];
    UILabel *dateLabel = [[UILabel alloc]init];
    dateLabel.text = @"更新时间: 2018-08-03";
    dateLabel.textColor = kGrayColor;
    dateLabel.textAlignment = NSTextAlignmentCenter;
    dateLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.view addSubview:dateLabel];
    [dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(14);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(versionLabel.mas_bottom).offset(11);
    }];
    
    UILabel *nameLabel = [[UILabel alloc]init];
    nameLabel.text = @"安蜂巢网络科技(海南)有限公司";
    nameLabel.textColor = kBlackColor;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.view addSubview:nameLabel];
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(14);
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-16);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
