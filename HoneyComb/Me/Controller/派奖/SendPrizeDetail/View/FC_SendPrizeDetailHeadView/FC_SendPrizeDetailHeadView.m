//
//  FC_SendPrizeDetailHeadView.m
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_SendPrizeDetailHeadView.h"

#import "FC_NumberLottoryDetailModel.h"

#import "FC_AthDetailModel.h"

#define  LINE_COLOR    RGB(235,235,235,1.0f)

@interface FC_SendPrizeDetailHeadView ()

{
    CGFloat titleLabelHeight;
    
    BOOL _isSend;
}

@property (nonatomic,copy) NSArray * titles;

@end

@implementation FC_SendPrizeDetailHeadView

-(instancetype)initWithFrame:(CGRect)frame isSend:(BOOL)isSend{
    
    if (self == [super initWithFrame:frame]) {
        
        titleLabelHeight = 45;
        
        _isSend = isSend;
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self noteLabelSetup];
    
}

#pragma mark  --  lazy  --

-(UIView *)backView{
    
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 5.0f;
        [self addSubview:_backView];
        
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
        
    }
    return _backView;
}

-(UILabel *)titleLabel{
    
    CGFloat  titleLabelWidth = 80;
    
    if (!_titleLabel) {
        
        _titleLabel = [[UILabel alloc]init];
        [self reloadLabel:_titleLabel withFontSize:17 textColor:RGB(51, 51, 51, 1)];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(kTopMar);
            make.width.mas_equalTo(titleLabelWidth);
            make.height.mas_equalTo(titleLabelHeight);
        }];
        
        UIView * lineView = [[UIView alloc]init];
        lineView.backgroundColor = LINE_COLOR;
        [self.backView addSubview:lineView];
        
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabelHeight);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(1);
        }];
        
    }
    return _titleLabel;
}

-(UILabel *)issueLabel{
    
    if (!_issueLabel) {
        
        _issueLabel = [[UILabel alloc]init];
        [self reloadLabel:_issueLabel withFontSize:14 textColor:[UIColor colorWithHexString:@"#999999"]];
        _issueLabel.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:_issueLabel];
        
        [_issueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(self.titleLabel.mas_right).offset(kLeftMar);
            make.right.mas_equalTo(-kTopMar);
            make.height.mas_equalTo(titleLabelHeight);
        }];
    }
    return _issueLabel;
}

-(void)reloadLabel:(UILabel *)label withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    
    label.textColor = textColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:fontSize];
    label.text = @"-";
    label.textAlignment = NSTextAlignmentCenter;
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"中奖金额(元)",@"订单状态",@"投注金额(元)"];
    }
    return _titles;
}

-(void)noteLabelSetup{
    
    CGFloat  labelWitdth = DEFAULT_WIDTH/self.titles.count,labelHeight = 14;
    
    for (int i = 0; i < self.titles.count; i ++) {
        UILabel * label = [[UILabel alloc]init];
        [self reloadLabel:label withFontSize:12 textColor:RGB(153, 153, 153, 1.0f)];
        label.text = [self.titles objectAtIndex:i];
        [self.backView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-kTopMar);
            make.left.mas_equalTo(labelWitdth * i);
            make.width.mas_equalTo(labelWitdth);
            make.height.mas_equalTo(labelHeight);
        }];
        
    }
    
}

-(UILabel *)winnerPaidLabel{
    
    CGFloat  labelWitdth = DEFAULT_WIDTH/self.titles.count,labelHeight = 18;
    
    if (!_winnerPaidLabel) {
        
        _winnerPaidLabel = [[UILabel alloc]init];
        _winnerPaidLabel.hidden = YES;
        [self reloadLabel:_winnerPaidLabel withFontSize:17 textColor:RGB(253, 40, 39, 1)];
        [self.backView addSubview:_winnerPaidLabel];
        
        [_winnerPaidLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kTopMar + titleLabelHeight);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(labelWitdth);
            make.height.mas_equalTo(labelHeight);
        }];
        
    }
    return _winnerPaidLabel;
}

-(UIButton *)winnerButton{
    
    CGFloat  labelWitdth = DEFAULT_WIDTH/self.titles.count,labelHeight = 18;
    
    if (!_winnerButton) {
        
        _winnerButton = [[UIButton alloc]init];
        _winnerButton.hidden = YES;
        _winnerButton.selected = NO;
        _winnerButton.titleLabel.font = [UIFont fontWithName:kMedium size:17];
        [_winnerButton setTitleColor:RGB(253, 40, 39, 1) forState:UIControlStateNormal];
        [_winnerButton setImage:[UIImage imageNamed:@"cpxq_bj_je"] forState:UIControlStateNormal];
        [_winnerButton addTarget:self action:@selector(winnerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.backView addSubview:_winnerButton];
        
        [_winnerButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kTopMar + titleLabelHeight);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(labelWitdth);
            make.height.mas_equalTo(labelHeight);
        }];
        
    }
    return _winnerButton;
}

-(void)winnerButtonAction:(UIButton *)sender{
    
    sender.selected = !sender.selected;
    
    if (self.winnerSelectedBlock) {
        self.winnerSelectedBlock(sender.selected);
    }
    
}

-(UILabel *)stateLabel{
    
    CGFloat  labelWitdth = DEFAULT_WIDTH/self.titles.count,labelHeight = 18;
    
    if (!_stateLabel) {
        
        _stateLabel = [[UILabel alloc]init];
        [self reloadLabel:_stateLabel withFontSize:17 textColor:RGB(51, 51, 51, 1)];
        [self.backView addSubview:_stateLabel];
        
        [_stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kTopMar + titleLabelHeight);
            make.left.mas_equalTo(labelWitdth);
            make.width.mas_equalTo(labelWitdth);
            make.height.mas_equalTo(labelHeight);
        }];
        
    }
    return _stateLabel;
}

-(UILabel *)betMoneyLabel{
    
    CGFloat  labelWitdth = DEFAULT_WIDTH/self.titles.count,labelHeight = 18;
    
    if (!_betMoneyLabel) {
        
        _betMoneyLabel = [[UILabel alloc]init];
        [self reloadLabel:_betMoneyLabel withFontSize:17 textColor:RGB(253, 40, 39, 1)];
        [self.backView addSubview:_betMoneyLabel];
        
        [_betMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kTopMar  + titleLabelHeight);
            make.left.mas_equalTo(labelWitdth * 2);
            make.width.mas_equalTo(labelWitdth);
            make.height.mas_equalTo(labelHeight);
        }];
        
    }
    return _betMoneyLabel;
}

#pragma mark  --  reload info  --

-(void)reloadAthDataWithModel:(FC_AthDetailModel *)model{
    
    self.winnerPaidLabel.hidden = _isSend?NO:YES;
    self.winnerButton.hidden = _isSend?YES:NO;
    
    if (model.playName) {
        self.titleLabel.text = model.playName;
        CGFloat  playNameWidth =  [self labelWidthWithString:model.playName font:[UIFont fontWithName:kMedium size:17] height:titleLabelHeight];
        [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(playNameWidth);
        }];
    }
    
    if (model.issue && model.issue.length > 0 && ![model.issue isEqualToString:@"-"]) {
        self.issueLabel.text = [NSString stringWithFormat:@"第%@期",model.issue];
    }
    
    if (_isSend) {
        self.winnerPaidLabel.text = model.award;
    }else{
        [self.winnerButton setTitle:model.award forState:UIControlStateNormal];
    }
    self.betMoneyLabel.text = model.price;
    [self gainStateStrWithState:[model.state integerValue]];
}

- (CGFloat)labelWidthWithString:(NSString *)resultString font:(UIFont *)font height:(CGFloat)height{
    
    CGSize maxSize = CGSizeMake(MAXFLOAT, height);
    
    // 计算内容label的高度
    
    CGFloat width = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :font} context:nil].size.width;
    return width + 5;
}

-(void)reloadNumDataWithModel:(FC_NumberLottoryDetailModel *)model{
    
    self.winnerPaidLabel.hidden = _isSend?NO:YES;
    self.winnerButton.hidden = _isSend?YES:NO;
    
    if (model.playName) {
        self.titleLabel.text = model.playName;
        CGFloat  playNameWidth =  [self labelWidthWithString:model.playName font:[UIFont fontWithName:kMedium size:17] height:titleLabelHeight];
        [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(playNameWidth);
        }];
    }
    
    self.issueLabel.text = [NSString stringWithFormat:@"第%@期",model.issue];
    if (_isSend) {
        self.winnerPaidLabel.text = model.award;
    }else{
        [self.winnerButton setTitle:model.award forState:UIControlStateNormal];
    }
    self.betMoneyLabel.text = model.price;
    
    [self gainStateStrWithState:[model.state integerValue]];
    
}

-(void)gainStateStrWithState:(NSInteger)state{
    
    NSString * stateStr = nil;
    
    switch (state) {
        case 0:
            stateStr = @"未付款";
            break;
        case 1:
            stateStr = @"待出票";
            break;
        case 2:
            stateStr = @"待开奖";
            break;
        case 3:
            stateStr = @"已中奖";
            break;
        case 4:
            stateStr = @"未中奖";
            break;
        case 5:
            stateStr = @"已派奖";
            break;
        case 6:
            stateStr = @"已撤单";
            break;
        case 7:
            stateStr = @"已取票";
            break;
        default:
            break;
    }
    
    self.stateLabel.text = stateStr;
}


@end
