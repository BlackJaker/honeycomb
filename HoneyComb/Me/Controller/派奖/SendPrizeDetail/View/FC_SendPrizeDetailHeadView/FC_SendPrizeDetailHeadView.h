//
//  FC_SendPrizeDetailHeadView.h
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_AthDetailModel;
@class FC_NumberLottoryDetailModel;

typedef void(^WinnerButtonSelectedBlock)(BOOL isSelected);

NS_ASSUME_NONNULL_BEGIN

@interface FC_SendPrizeDetailHeadView : UIView

-(instancetype)initWithFrame:(CGRect)frame isSend:(BOOL)isSend;

@property (nonatomic,strong) UIView * backView;

@property (nonatomic,strong) UILabel * titleLabel;

@property (nonatomic,strong) UILabel * issueLabel;

@property (nonatomic,strong) UILabel * winnerPaidLabel;

@property (nonatomic,strong) UIButton * winnerButton;

@property (nonatomic,strong) UILabel * stateLabel;

@property (nonatomic,strong) UILabel * betMoneyLabel;

@property (nonatomic,copy) WinnerButtonSelectedBlock  winnerSelectedBlock;

-(void)reloadAthDataWithModel:(FC_AthDetailModel *)model;

-(void)reloadNumDataWithModel:(FC_NumberLottoryDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
