//
//  FC_ChangeMoneyView.m
//  HoneyComb
//
//  Created by afc on 2018/12/22.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_ChangeMoneyView.h"

@interface FC_ChangeMoneyView ()<UITextFieldDelegate>
{
    BOOL isHaveDot;
}
@property (nonatomic,copy) NSString * price;

@end

@implementation FC_ChangeMoneyView

-(instancetype)initWithFrame:(CGRect)frame price:(NSString *)price{
    
    if (self = [super initWithFrame:frame]) {
        
        self.price = price;
        isHaveDot = NO;
        
        [self contentSetup];
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    // 添加蒙层
    UIView * alphaView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.4;
    [self addSubview:alphaView];
    
    self.cancelButton.hidden = NO;
    self.confirmButton.hidden = NO;
    self.priceTextField.hidden = NO;
    
}

#pragma mark  --  lazy  --

-(UIView *)backView{
    
    CGFloat  leftMar = 30,height = 180,titleHeight = 60;
    
    if (!_backView) {
        _backView  = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 5.0f;
        _backView.clipsToBounds = YES;
        [self addSubview:_backView];
        
        [_backView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(leftMar);
            make.right.equalTo(-leftMar);
            make.height.equalTo(height);
            make.centerY.equalTo(0);
        }];
        
        UILabel * titleLabel = [UILabel lableWithText:@"修改金额" font:[UIFont boldSystemFontOfSize:18] textColor:[UIColor colorWithHexString:@"#333333"]];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [_backView addSubview:titleLabel];
        
        [titleLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.height.equalTo(titleHeight);
            make.top.equalTo(0);
        }];
        
    }
    return _backView;
}

-(UITextField *)priceTextField{
    
    CGFloat  left = 40,height = 48,top = 60;
    
    if (!_priceTextField) {
        _priceTextField = [[UITextField alloc]init];
        _priceTextField.text = self.price;
        _priceTextField.textAlignment = NSTextAlignmentCenter;
        _priceTextField.delegate = self;
        _priceTextField.textColor = [UIColor colorWithHexString:@"#FD2827"];
        _priceTextField.font = [UIFont boldSystemFontOfSize:21];
        _priceTextField.backgroundColor = [UIColor colorWithHexString:@"#F3F2F6"];
        [self.backView addSubview:_priceTextField];
        
        [_priceTextField makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(left);
            make.right.equalTo(-left);
            make.height.equalTo(height);
            make.top.equalTo(top);
        }];
    }
    return _priceTextField;
}

-(NSAttributedString *)attributeSubString{
    
    NSString * priceStr = [NSString stringWithFormat:@"%@元",self.price];
    
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc]initWithString:priceStr attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#FD2827"]}];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:21] range:[priceStr rangeOfString:self.price]];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:NSMakeRange(priceStr.length - 1, 1)];
    return attributeString;
}

-(UIButton *)cancelButton{
    
    CGFloat leftMar = 30, left = 40,height = 38,bottom = 20,centerMar = 15,width = (kWidth - (left + leftMar) * 2 - centerMar)/2;
    
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithTitle:@"取消" font:[UIFont fontWithName:kMedium size:15] titleColor:[UIColor colorWithHexString:@"#333333"] controlState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [self updateButton:_cancelButton];
        [self.backView addSubview:_cancelButton];
        
        [_cancelButton makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(left);
            make.width.equalTo(width);
            make.height.equalTo(height);
            make.bottom.equalTo(-bottom);
        }];
    }
    return _cancelButton;
}

-(void)updateButton:(UIButton *)button{
    
    NSString * colorStr = [button.titleLabel.text isEqualToString:@"取消"]?@"#E1E1E1":@"#FF527B";
    [button setBackGroundColor:[UIColor colorWithHexString:colorStr] controlState:UIControlStateNormal];
    button.layer.cornerRadius = 5.0f;
    button.clipsToBounds = YES;
}

-(void)cancelAction{
    [self dismiss];
}

-(UIButton *)confirmButton{
    
    CGFloat rightMar = 30, right = 40,height = 38,bottom = 20,centerMar = 15,width = (kWidth - (right + rightMar) * 2 - centerMar)/2;
    
    if (!_confirmButton) {
        _confirmButton = [UIButton buttonWithTitle:@"确定" font:[UIFont fontWithName:kMedium size:15] titleColor:[UIColor whiteColor] controlState:UIControlStateNormal];
        [_confirmButton addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
        [self updateButton:_confirmButton];
        [self.backView addSubview:_confirmButton];
        
        [_confirmButton makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(-right);
            make.width.equalTo(width);
            make.height.equalTo(height);
            make.bottom.equalTo(-bottom);
        }];
    }
    return _confirmButton;
}

-(void)confirmAction{
    
    if (self.priceTextField.text.length == 0 || [self.priceTextField.text floatValue] == 0.00) {
        [FC_Manager showToastWithText:@"请输入一个有效金额"];
        return;
    }
    
    if ([_priceTextField.text hasSuffix:@"."]) {
        _priceTextField.text = [_priceTextField.text substringToIndex:_priceTextField.text.length - 1];
    }
    
    if (self.confirmBlock) {
        self.confirmBlock(self.priceTextField.text);
    }
    [self dismiss];
}

-(void)show{
    
    UIView * view = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    
    [view addSubview:self];
    
}

-(void)dismiss{
    
    if (_dismissBlock) {
        self.dismissBlock();
    }
    [self removeFromSuperview];
}

#pragma mark  --  text field delegate  --

#pragma mark - 限定文本框输入
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    // 判断是否有小数点
    isHaveDot = [textField.text containsString:@"."]?YES:NO;
    
    NSString * aString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (string.length > 0) {
        
        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        
        // 不能输入.0-9以外的字符
        if (!((single >= '0' && single <= '9') || single == '.'))
        {
            return NO;
        }
        
        // 只能有一个小数点
        if (isHaveDot && single == '.') {
            return NO;
        }
        
        // 如果第一位是.则前面加上0.
        if ((textField.text.length == 0) && (single == '.')) {
            textField.text = @"0";
        }
        
        // 如果第一位是0则后面必须输入点，否则不能输入。
        if ([textField.text hasPrefix:@"0"]) {
            if (textField.text.length > 1) {
                NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                if (![secondStr isEqualToString:@"."]) {
                    return NO;
                }
            }else{
                if (![string isEqualToString:@"."]) {
                    return NO;
                }
            }
        }
        

        // 不能在第一位插入0和.
        if (![aString hasPrefix:@"0."]) {
            
            if (([[aString substringToIndex:1] isEqualToString:@"0"] && aString.length > 1) || [[aString substringToIndex:1] isEqualToString:@"."]) {
                return NO;
            }
        }
        
        // 小数点后最多能输入两位
        if (isHaveDot) {
            NSRange ran = [textField.text rangeOfString:@"."];
            // 由于range.location是NSUInteger类型的，所以这里不能通过(range.location - ran.location)>2来判断
            if (range.location > ran.location) {
                if ([textField.text pathExtension].length > 1) {
                    return NO;
                }
            }
        }
        
        // 当前显示包含小数，则整数位不能限制位小于等于10的长度,小数点后不能多于2位
        if ([aString containsString:@"."]) {
            NSArray * strings = [aString componentsSeparatedByString:@"."];
            NSString * firstString = [strings firstObject];
            NSString * lastString = [strings lastObject];
            if ([string isEqualToString:@""] && firstString.length + lastString.length > 10) {
                return NO;
            }else{
                if (lastString.length == 0) {
                    return (firstString.length < 11 && lastString.length < 3);
                }else{
                    if (lastString.length > 2) {
                        return NO;
                    }
                    return firstString.length < 11;
                }
            }
        }else{
            return textField.text.length < 10;
        }
        
    }
    // 如果是删除 没有小数的时候长度限制在10以内
    if ( aString.length > 10 && ![aString containsString:@"."]) {
        return NO;
    }else{
        // 如果没有小数,第一个不能为0，当为0的时候必须是不大于1的小数，第一位不能为小数点
        if (([aString hasPrefix:@"0"] && ![aString hasPrefix:@"0."]) || [aString hasPrefix:@"."]) {
            if (aString.length > 2) { // 当前字符不为@"0."
                return NO;
            }
            
        }
    }
    
    return YES;

}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if ([textField.text hasSuffix:@"."]) {
        textField.text = [textField.text substringToIndex:textField.text.length - 1];
    }

    if (textField.text.length != 0) {
        self.price = textField.text;
    }
    self.priceTextField.text = self.price;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UIView * view = [touches anyObject].view;
    
    if (![view isEqual:self.backView]) {
        [self.priceTextField resignFirstResponder];
    }
    
}

@end
