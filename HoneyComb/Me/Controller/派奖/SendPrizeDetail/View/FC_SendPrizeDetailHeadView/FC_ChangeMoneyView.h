//
//  FC_ChangeMoneyView.h
//  HoneyComb
//
//  Created by afc on 2018/12/22.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UITextField+FC_TextField.h"

typedef void(^ConfirmBlock)(NSString * price);

typedef void(^DismissBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface FC_ChangeMoneyView : UIView

-(instancetype)initWithFrame:(CGRect)frame price:(NSString *)price;

@property (nonatomic,strong) UIView * backView;

@property (nonatomic,strong) UITextField * priceTextField;

@property (nonatomic,strong) UIButton * cancelButton;

@property (nonatomic,strong) UIButton * confirmButton;

@property (nonatomic,copy) ConfirmBlock confirmBlock;

@property (nonatomic,copy) DismissBlock dismissBlock;

-(void)show;

-(void)dismiss;

@end

NS_ASSUME_NONNULL_END
