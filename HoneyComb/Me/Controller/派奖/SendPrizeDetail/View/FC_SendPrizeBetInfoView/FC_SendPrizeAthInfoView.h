//
//  FC_SendPrizeAthInfoView.h
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class FC_AthDetailModel;

@interface FC_SendPrizeAthInfoView : UIView

@property (nonatomic,strong) UIView * backView;

@property (nonatomic,strong) UILabel * titleLabel;

@property (nonatomic,assign) NSInteger type;

-(void)reloadAthBetDataWithModel:(FC_AthDetailModel *)model;

-(void)updateContentSizeWithModel:(FC_AthDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
