//
//  FC_SendPrizeAthInfoView.m
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_SendPrizeAthInfoView.h"

#import "FC_AthDetailModel.h"

#import "FC_AthDetailItemModel.h"

@interface FC_SendPrizeAthInfoView (){
    
    CGFloat titleLabelHeight;
    
}
@property (nonatomic,copy) NSArray * titles;

@property (nonatomic,copy) NSMutableArray * contentItems;

@end

@implementation FC_SendPrizeAthInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self == [super initWithFrame:frame]) {
        
        titleLabelHeight = 45;
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self noteLabelSetup];
    
    [self bottomLabelSetup];
    
}

#pragma mark  --  lazy  --

-(UIView *)backView{
    
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 5.0f;
        [self addSubview:_backView];
        
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
        
    }
    return _backView;
}

-(UILabel *)titleLabel{
    
    if (!_titleLabel) {
        
        _titleLabel = [[UILabel alloc]init];
        [self reloadLabel:_titleLabel withFontSize:15 textColor:RGB(51, 51, 51, 1)];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.text = @"投注信息";
        [self.backView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(titleLabelHeight);
        }];
        
    }
    return _titleLabel;
}

-(void)reloadLabel:(UILabel *)label withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    
    label.textColor = textColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:fontSize];
    label.text = @"-";
    label.textAlignment = NSTextAlignmentCenter;
}

-(NSArray *)titles{
    
    if (!_titles) {
        if (self.type != 2) {
            _titles = @[@"场次",@"主队VS客队",@"投注内容",@"赛果"];
        }else{
            _titles = @[@"场次",@"客队VS主队",@"投注内容",@"赛果"];
        }
        
    }
    return _titles;
}

-(void)noteLabelSetup{
    
    CGFloat  labelWitdth = (DEFAULT_WIDTH - kLeftMar * 2)/self.titles.count,labelHeight = 33;
    
    for (int i = 0; i < self.titles.count; i ++) {
        UILabel * label = [[UILabel alloc]init];
        [self reloadLabel:label withFontSize:12 textColor:RGB(153, 153, 153, 1.0f)];
        label.text = [self.titles objectAtIndex:i];
        label.backgroundColor = RGB(242, 242, 242, 1.0f);
        [self.backView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabelHeight);
            make.left.mas_equalTo(kLeftMar + labelWitdth * i);
            make.width.mas_equalTo(labelWitdth);
            make.height.mas_equalTo(labelHeight);
        }];
        
    }
    
}

-(void)bottomLabelSetup{
    
    UILabel * label = [[UILabel alloc]init];
    [self reloadLabel:label withFontSize:12 textColor:RGB(153, 153, 153, 1)];
    label.text = @"赔率以实际出票为准";
    [self.backView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(titleLabelHeight);
    }];
}

#pragma mark  --  update content size  --

-(void)updateContentSizeWithModel:(FC_AthDetailModel *)model{
    
    [self clear];
    
    CGFloat  width = (DEFAULT_WIDTH - kLeftMar * 2)/self.titles.count,originY = titleLabelHeight + 33;
    
    UIView * topLine = [[UIView alloc]init];
    topLine.backgroundColor = RGB(235, 235, 235, 1.0f);
    topLine.tag = -111;
    [self.backView addSubview:topLine];
    
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLabelHeight);
        make.left.mas_equalTo(kLeftMar);
        make.right.mas_equalTo(-kLeftMar);
        make.height.mas_equalTo(1);
    }];
    
    for (int i = 0; i < model.detailList.count; i ++) {
        
        FC_AthDetailItemModel * itemModel = [model.detailList objectAtIndex:i];
        
        CGFloat  normalHeight = [self getCurrentContentHeightWithItemModel:itemModel];
        
        NSMutableArray * arr = [NSMutableArray new];
        
        for (int j = 0; j < self.titles.count; j ++) {
            
            UILabel * label = [self createLotteryContentLabel];
            
            [self.backView addSubview:label];
            
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(originY);
                make.left.mas_equalTo(kLeftMar + width * j);
                make.width.mas_equalTo(width);
                make.height.mas_equalTo(normalHeight);
            }];
            
            [arr addObject:label];
            
            
            
            UIView * line2 = [[UIView alloc]init];
            line2.tag = -111;
            line2.backgroundColor = RGB(235, 235, 235, 1.0f);
            [self.backView addSubview:line2];
            
            [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(titleLabelHeight);
                make.left.mas_equalTo(kLeftMar + width * j);
                make.width.mas_equalTo(1);
                make.bottom.mas_equalTo(-titleLabelHeight);
            }];
            
        }
        
        originY += normalHeight;
        
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = RGB(235, 235, 235, 1.0f);
        line.tag = -111;
        [self.backView addSubview:line];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(originY);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(1);
        }];
        
        [self.contentItems addObject:arr];
    }
    
    UIView * rightLine= [[UIView alloc]init];
    rightLine.tag = -111;
    rightLine.backgroundColor = RGB(235, 235, 235, 1.0f);
    [self.backView addSubview:rightLine];
    
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(titleLabelHeight);
        make.width.mas_equalTo(1);
        make.right.mas_equalTo(-kLeftMar);
        make.bottom.mas_equalTo(-titleLabelHeight);
    }];
    
}

-(void)clear{
    
    UIView * line = [self viewWithTag:-111];
    [line removeFromSuperview];
    for (NSArray * items in self.contentItems) {
        for (UILabel * label in items) {
            [label removeFromSuperview];
        }
    }
    [self.contentItems removeAllObjects];
    
}

-(CGFloat)getCurrentContentHeightWithItemModel:(FC_AthDetailItemModel *)itemModel{
    
    CGFloat  normalHeight = 65,itemHeight = 18;
    
    NSArray * contentArr = [itemModel.content componentsSeparatedByString:@","];
    
    if (contentArr.count > 3) {
        normalHeight += (contentArr.count - 3) * itemHeight;
    }
    
    return normalHeight;
}

-(UILabel * )createLotteryContentLabel{
    
    UILabel * label = [[UILabel alloc]init];
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:kPingFangRegular size:11];
    label.textColor = RGB(51, 51, 51, 1.0f);
    
    return label;
}

-(NSMutableArray *)contentItems{
    
    if (!_contentItems) {
        _contentItems = [NSMutableArray new];
    }
    return _contentItems;
}

#pragma mark  --  reload info  竞技彩 --

-(void)reloadAthBetDataWithModel:(FC_AthDetailModel *)model{
    
    [self updateTitleWithModel:model]; // title
    
    [self  updateContentInfoWithModel:model]; // content
    
}

-(void)updateTitleWithModel:(FC_AthDetailModel *)model{
    
    NSString * bunchStr = @"";
    
    if (model.bunch && ![model.bunch isEqualToString:@""]) {
        bunchStr = [model.bunch intValue] < 2?@" 单关 ":[NSString stringWithFormat:@"  %@串1  ",model.bunch];
    }
    
    NSString * title = [NSString stringWithFormat:@"投注信息 %@ %@注  %@倍",bunchStr,model.account,model.times];
    
    self.titleLabel.text = title;
    
}

-(void)updateContentInfoWithModel:(FC_AthDetailModel *)model{
    
    for (int i = 0; i < model.detailList.count; i ++) {
        
        FC_AthDetailItemModel * itemModel = [model.detailList objectAtIndex:i];
        
        // 获取显示数据
        NSArray * contents = [self contentWithItemModel:itemModel];
        
        for (int j = 0; j < contents.count; j ++) {
            UILabel * label = [[self.contentItems objectAtIndex:i] objectAtIndex:j];
            label.text = [contents objectAtIndex:j];
        }
        
    }
}

-(NSArray *)contentWithItemModel:(FC_AthDetailItemModel *)itemModel{
    
    NSString * sessionStr = [NSString stringWithFormat:@"%@\n%@\n%@",itemModel.number,itemModel.league,[NSString stringWithFormat:@"%@截止",itemModel.startTime]];
    NSString * againstStr = self.type != 2?[NSString stringWithFormat:@"%@\nVS\n%@",itemModel.hostTeam,itemModel.visitingTeam]:[NSString stringWithFormat:@"%@\nVS\n%@",itemModel.visitingTeam,itemModel.hostTeam];
    NSString * content = [itemModel.content stringByReplacingOccurrencesOfString:@"," withString:@"\n"];
    NSString * result = self.type > 6?itemModel.result:itemModel.endScore;
    
    NSArray * contents = @[sessionStr,
                           againstStr,
                           content,
                           result];
    
    return contents;
    
}


@end
