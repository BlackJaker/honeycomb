//
//  FC_SendPrizeOrderInfoView.m
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_SendPrizeOrderInfoView.h"

#import "FC_AthDetailModel.h"

#import "FC_NumberLottoryDetailModel.h"

@interface FC_SendPrizeOrderInfoView ()
{
    CGFloat  titleLabelHeight;
}

@property (nonatomic,copy) NSArray * titles;

@end

@implementation FC_SendPrizeOrderInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self == [super initWithFrame:frame]) {
        
        titleLabelHeight = 42;
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self noteLabelSetup];
    
}

#pragma mark  --  lazy  --

-(UIView *)backView{
    
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 8.0f;
        [self addSubview:_backView];
        
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
    }
    return _backView;
}

-(void)reloadLabel:(UILabel *)label withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    
    label.textColor = textColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:fontSize];
    label.text = @"-";
    label.textAlignment = NSTextAlignmentCenter;
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"用 户 名 : ",@"订单编号 : ",@"购买时间 : ",@"出票时间 : ",@"开奖号码 : "];
    }
    return _titles;
}

-(void)noteLabelSetup{
    
    CGFloat  labelHeight = 17;
    
    UILabel * label = [[UILabel alloc]init];
    [self reloadLabel:label withFontSize:17 textColor:RGB(51, 51, 51, 1.0f)];
    label.text = @"订单信息";
    label.textAlignment = NSTextAlignmentLeft;
    [self.backView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kTopMar);
        make.left.mas_equalTo(kLeftMar);
        make.right.mas_equalTo(-kLeftMar);
        make.height.mas_equalTo(labelHeight);
    }];
    
}

#pragma mark  --  reload info  --

-(void)reloadAthOrderDataWithModel:(FC_AthDetailModel *)model{
    
    UIView * view = [self viewWithTag:-1235];
    [view removeFromSuperview];
    
    NSMutableArray * items = [NSMutableArray new];

    if ([self isItemWith:model.nickName]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.nickName forKey:@"用 户 名  : "];
        [items addObject:dic];
    }
    
    if ([self isItemWith:model.orderNumber]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.orderNumber forKey:@"订单信息 : "];
        [items addObject:dic];
    }
    
    if ([self isItemWith:model.createDate]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.createDate forKey:@"购买时间 : "];
        [items addObject:dic];
    }
    
    if ([self isItemWith:model.printDate]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.printDate forKey:@"出票时间 : "];
        [items addObject:dic];
    }
    
    CGFloat  itemHeight = 20;
    
    for (int i = 0; i < items.count; i ++) {
        UILabel * label = [[UILabel alloc]init];
        [self reloadLabel:label withFontSize:14 textColor:RGB(153, 153, 153, 1.0f)];
        label.tag = -1235;
        
        NSDictionary * dic = [items objectAtIndex:i];
        
        NSString * key = [dic.allKeys firstObject];
        NSString * value = [dic valueForKey:key];
        label.text = [NSString stringWithFormat:@"%@ %@",key,value];
        label.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabelHeight + itemHeight * i);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    
}

-(void)reloadNumberOrderDataWithModel:(FC_NumberLottoryDetailModel *)model{
    
    UIView * view = [self viewWithTag:-1234];
    [view removeFromSuperview];
    
    NSMutableArray * items = [NSMutableArray new];
    
    if ([self isItemWith:model.nickName]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.nickName forKey:@"用 户 名  : "];
        [items addObject:dic];
    }
    
    if ([self isItemWith:model.orderNumber]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.orderNumber forKey:@"订单信息 : "];
        [items addObject:dic];
    }
    
    if ([self isItemWith:model.createDate]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.createDate forKey:@"购买时间 : "];
        [items addObject:dic];
    }
    
    if ([self isItemWith:model.printDate]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.printDate forKey:@"出票时间 : "];
        [items addObject:dic];
    }
    
    if ([self isItemWith:model.result]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.result forKey:@"开奖号码 : "];
        [items addObject:dic];
    }
    
    CGFloat  itemHeight = 20;
    
    for (int i = 0; i < items.count; i ++) {
        
        UILabel * label = [[UILabel alloc]init];
        [self reloadLabel:label withFontSize:14 textColor:RGB(153, 153, 153, 1.0f)];
        label.tag = -1234;
        
        NSDictionary * dic = [items objectAtIndex:i];
        
        NSString * key = [dic.allKeys firstObject];
        NSString * value = [dic valueForKey:key];
        label.text = [NSString stringWithFormat:@"%@ %@",key,value];
        
        if ([key isEqualToString:@"开奖号码 : "]) {
            NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc]initWithString:label.text];
            NSRange subRange = [label.text rangeOfString:value];
            [attributeString addAttribute:NSForegroundColorAttributeName value:RGB(253, 37, 37, 1.0f) range:subRange];
            
            if ([model.playName containsString:@"大乐透"]) {
                NSArray * array = [model.result componentsSeparatedByString:@"|"];
                NSRange blueRange = [label.text rangeOfString:[array lastObject]];
                [attributeString addAttribute:NSForegroundColorAttributeName value:RGB(17, 58, 235, 1.0f) range:blueRange];
            }
            
            label.attributedText = attributeString;
        }
        
        label.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabelHeight + itemHeight * i);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    
}

-(BOOL)isItemWith:(NSString *)item{
    
    if ([item isEqualToString:@""] || !item) {
        return NO;
    }
    if ([item isEqualToString:@"-"]) {
        return  NO;
    }
    
    return  YES;
}


@end
