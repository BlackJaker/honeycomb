//
//  FC_SendPrizeOrderInfoView.h
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_AthDetailModel;

@class FC_NumberLottoryDetailModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_SendPrizeOrderInfoView : UIView

@property (nonatomic,strong) UIView * backView;

-(void)reloadAthOrderDataWithModel:(FC_AthDetailModel *)model;

-(void)reloadNumberOrderDataWithModel:(FC_NumberLottoryDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
