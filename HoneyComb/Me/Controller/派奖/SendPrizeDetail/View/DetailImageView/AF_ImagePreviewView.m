//
//  AF_ImagePreviewView.m
//  LotteryApp
//
//  Created by afc on 2018/12/11.
//  Copyright © 2018年 afc. All rights reserved.
//

#import "AF_ImagePreviewView.h"

#import "AF_ImagePreviewCell.h"

@interface AF_ImagePreviewView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,assign) NSInteger currentSelectedIndex;

@end

@implementation AF_ImagePreviewView

-(instancetype)initWithFrame:(CGRect)frame index:(NSInteger)index{
    
    if (self = [super initWithFrame:frame]) {
        
        self.currentSelectedIndex = index;
        
        [self contentViewSetup];
        
    }
    return self;
}

#pragma mark  --  content view setup  --

-(void)contentViewSetup{
    
    self.backgroundColor = [UIColor blackColor];
    
}

#pragma mark  --  lazy  --

-(UICollectionView *)collectionView{
    
    if (!_collectionView) {

        UICollectionViewFlowLayout *layout =[[UICollectionViewFlowLayout alloc]init];
        //    设置collectionView滚动方向
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        layout.minimumLineSpacing = 0.000001f;
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight) collectionViewLayout:layout];

        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.pagingEnabled = YES;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.showsHorizontalScrollIndicator = NO;
        
        [self addSubview:_collectionView];
        
        [self.collectionView registerClass:[AF_ImagePreviewCell class] forCellWithReuseIdentifier:@"previewCollectionCell"];
        
    }
    return _collectionView;
}

#pragma mark  --  collection veiw datasource  --

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.urls.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *previewCollectionCell = @"previewCollectionCell";
    AF_ImagePreviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:previewCollectionCell forIndexPath:indexPath];
    
    @WeakObj(self)
    cell.touchBlock = ^{
        @StrongObj(self)
        if (self.dismissBlock) {
            self.dismissBlock();
        }
        [self removeFromSuperview];
    };
    cell.url = [self.urls objectAtIndex:indexPath.row];
    
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kWidth, kHeight);
}

-(void)reloadWithIndex:(NSInteger)index{
    
    [self.collectionView reloadData];
    self.collectionView.contentOffset = CGPointMake(kWidth * index, 0);
    
}

@end
