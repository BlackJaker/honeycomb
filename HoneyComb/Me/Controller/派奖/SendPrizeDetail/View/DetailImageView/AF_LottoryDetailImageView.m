//
//  AF_LottoryDetailImageView.m
//  LotteryApp
//
//  Created by afc on 2018/12/10.
//  Copyright © 2018年 afc. All rights reserved.
//

#import "AF_LottoryDetailImageView.h"

#define  kNoteHieght  48

@interface AF_LottoryDetailImageView ()

@property (nonatomic,copy) NSArray * urls;

@end

@implementation AF_LottoryDetailImageView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 5.0f;
        self.clipsToBounds = YES;
        
        [self contentViewSetup];
    }
    return self;
}

#pragma mark  --  setup  --

-(void)contentViewSetup{
    
    [self noteLabelSetup];
    
}

-(void)noteLabelSetup{
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(kLeftMar,0, DEFAULT_WIDTH - kLeftMar * 2, kNoteHieght)];
    [self reloadLabel:label withFontSize:16 textColor:[UIColor colorWithHexString:@"#333333"]];
    label.text = @"彩票照片";
    [self addSubview:label];
    
}

-(void)reloadLabel:(UILabel *)label withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    
    label.textColor = textColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:fontSize];
    
}

#pragma mark  --  reload   --

-(void)reloadWithUrls:(NSArray *)urls{
    
    self.urls = urls;
    
    CGFloat  mar = kLeftMar,width = (DEFAULT_WIDTH - mar * 4)/3,bottom = kTopMar;
    
    NSInteger i = 0;
    for (NSString * urlString in urls) {
        UIImageView * imgView = [[UIImageView alloc]init];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.backgroundColor = RGB(249, 249, 249, 1.0f);
        imgView.userInteractionEnabled = YES;
        imgView.tag = 100 + i;
        [imgView sd_setImageWithURL:[NSURL URLWithString:urlString]];
        [self addSubview:imgView];
        
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kNoteHieght);
            make.left.mas_equalTo(mar + (mar + width) * i);
            make.width.mas_equalTo(width);
            make.bottom.mas_equalTo(-bottom);
        }];
        
        i ++;
        
        UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageViewClick:)];
        [imgView addGestureRecognizer:tapGesture];
    }
    
}

-(void)imageViewClick:(UITapGestureRecognizer *)tap{
    
    UIImageView * imgView = (UIImageView *)[tap view];
    
    if (self.imgClickBlock) {
        self.imgClickBlock(imgView.tag - 100,self.urls,imgView.frame);
    }
    
}

@end
