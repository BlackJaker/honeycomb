//
//  AF_ImagePreviewCell.m
//  LotteryApp
//
//  Created by afc on 2018/12/11.
//  Copyright © 2018年 afc. All rights reserved.
//

#import "AF_ImagePreviewCell.h"

@interface AF_ImagePreviewCell ()<UIScrollViewDelegate,UIGestureRecognizerDelegate>

@end

@implementation AF_ImagePreviewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

-(void)setUrl:(NSString *)url{
    
    [self.previewImgView sd_setImageWithURL:[NSURL URLWithString:url]];
}

-(UIScrollView *)scrollView{
    
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 0, kWidth, kHeight);
        _scrollView.minimumZoomScale = 0.5;
        _scrollView.maximumZoomScale = 3.0;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.userInteractionEnabled = YES;
        _scrollView.delegate = self;
        
        [self.contentView addSubview:_scrollView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [_scrollView addGestureRecognizer:tap];
   
    }
    return _scrollView;
    
}

-(UIImageView *)previewImgView{
    
    if (!_previewImgView) {
        self.previewImgView = [[UIImageView alloc] init];
        self.previewImgView.contentMode = UIViewContentModeScaleAspectFit;
        self.previewImgView.frame = self.scrollView.bounds;
        self.previewImgView.userInteractionEnabled = YES;
        [self.scrollView addSubview:self.previewImgView];
        
        //设置scroll的contentsize的frame
        self.scrollView.contentSize = self.previewImgView.frame.size;
    }
    return _previewImgView;
}

//控制缩放是在中心
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    self.previewImgView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                               
                               scrollView.contentSize.height * 0.5 + offsetY);
}

//这个方法的返回值决定了要缩放的内容(只能是UISCrollView的子控件)
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.previewImgView;
}

- (void)tap:(UITapGestureRecognizer *)tap{
    
    if (self.touchBlock) {
        self.touchBlock();
    }
    
}


@end
