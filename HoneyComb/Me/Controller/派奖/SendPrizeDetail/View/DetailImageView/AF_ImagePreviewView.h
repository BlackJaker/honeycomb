//
//  AF_ImagePreviewView.h
//  LotteryApp
//
//  Created by afc on 2018/12/11.
//  Copyright © 2018年 afc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DissmissBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface AF_ImagePreviewView : UIView

-(instancetype)initWithFrame:(CGRect)frame index:(NSInteger)index;

@property (nonatomic,copy) NSArray * urls;

@property (nonatomic,strong) UICollectionView * collectionView;
@property (nonatomic,strong) UIScrollView * contentScrollView;

@property (nonatomic,copy) DissmissBlock dismissBlock;

-(void)reloadWithIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
