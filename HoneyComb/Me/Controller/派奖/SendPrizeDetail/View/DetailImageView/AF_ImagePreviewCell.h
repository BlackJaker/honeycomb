//
//  AF_ImagePreviewCell.h
//  LotteryApp
//
//  Created by afc on 2018/12/11.
//  Copyright © 2018年 afc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TouchBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface AF_ImagePreviewCell : UICollectionViewCell

@property (nonatomic,copy) NSString * url;

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *previewImgView;

@property (nonatomic,copy) TouchBlock touchBlock;

@end

NS_ASSUME_NONNULL_END
