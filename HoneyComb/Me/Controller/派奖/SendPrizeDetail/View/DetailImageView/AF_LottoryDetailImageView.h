//
//  AF_LottoryDetailImageView.h
//  LotteryApp
//
//  Created by afc on 2018/12/10.
//  Copyright © 2018年 afc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ImageViewClickBlock)(NSInteger index,NSArray * urls,CGRect frame);

NS_ASSUME_NONNULL_BEGIN

@interface AF_LottoryDetailImageView : UIView

@property (nonatomic,strong) UILabel *  noteLabel;

-(void)reloadWithUrls:(NSArray *)urls;

@property (nonatomic,copy) ImageViewClickBlock  imgClickBlock;

@end

NS_ASSUME_NONNULL_END
