//
//  FC_SendPrizeDetailViewController.m
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_SendPrizeDetailViewController.h"

//#import "PayColorDetailsOneCell.h"

#import "FC_NumberLottoryDetailModel.h"

#import "FC_NumberLottoryDetailItemModel.h"

#import "FC_AthDetailModel.h"

#import "FC_AthDetailItemModel.h"

#import "FC_SendPrizeDetailHeadView.h"

#import "FC_SendPrizeAthInfoView.h"

#import "FC_SendPrizeOrderInfoView.h"

#import "FC_NumberLottoryBettingView.h"

#import "AF_LottoryDetailImageView.h"

#import "AF_ImagePreviewView.h"

#import "FC_ChangeMoneyView.h"

@interface FC_SendPrizeDetailViewController ()
{
    CGFloat headHeight;
    
    CGFloat betInfoHeight;
    
    CGFloat orderHeight;
    
    UIButton *relaseBtn;
    
    CGFloat  detailImgHeight;
    
    BOOL _isSend;  // 已派奖
    
    BOOL  isSending;
}

@property (nonatomic,strong) FC_AthDetailModel * athDetailModel;

@property (nonatomic,strong) FC_NumberLottoryDetailModel * numberDetailModel;

@property (nonatomic,strong) UIScrollView * detailContentScrollView;

@property (nonatomic,strong) FC_SendPrizeDetailHeadView * headView;

@property (nonatomic,strong) FC_SendPrizeAthInfoView * betInfoView;

@property (nonatomic,strong) FC_SendPrizeOrderInfoView * orderView;

@property (nonatomic,strong) FC_NumberLottoryBettingView *orderContentView;

@property (nonatomic,strong) AF_LottoryDetailImageView * detailImageView;

@property (nonatomic,strong) AF_ImagePreviewView * imgPreviewView;

@property (nonatomic,copy)  NSString * orderId;

@property (nonatomic,copy)  NSString * type;

@property (nonatomic,assign)  BOOL  statusBarIsHidden;

@property (nonatomic,strong)  UIButton * sendButton;

@property (nonatomic,strong)  UILabel * prizeLabel;

@property (nonatomic,strong)  FC_ChangeMoneyView * changeMoneyView;

@end

@implementation FC_SendPrizeDetailViewController

- (instancetype)initWithOrderId:(NSString *)orderId type:(NSString *)type isSend:(BOOL)isSend{
    
    if (self == [super init]) {
        
        self.orderId = orderId;
        self.type = type;
        _isSend = isSend;
        isSending = NO;
        [self athleticsOrderRequest];
        
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertySetup];
    
    if (!_isSend) {
        [self bottomViewSetup];
    }
    
}
#pragma mark  --  property setup  --

-(void)propertySetup{
    
    self.titleLabe.text = @"彩票详情";
    
    headHeight = 115;
    
    betInfoHeight = [self.type integerValue] < 3 || [self.type integerValue] > 6? 123:40;
    
    orderHeight = 42;
    
    detailImgHeight = 0;
    
    self.statusBarIsHidden = NO;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    
}

#pragma mark  --  lazy  --

-(UIScrollView *)detailContentScrollView{
    
    if (!_detailContentScrollView) {
        
        _detailContentScrollView = [[UIScrollView alloc] init];
        
        if (@available(iOS 11.0, *)) {
            _detailContentScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [self.view addSubview:_detailContentScrollView];
        
        CGFloat  bottom = _isSend?0:TAB_BAR_HEIGHT;
        
        [_detailContentScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(kWidth);
            make.bottom.mas_equalTo(- bottom);
        }];
        
    }
    return _detailContentScrollView;
}

-(FC_SendPrizeDetailHeadView *)headView{
    
    BOOL isSend = _isSend;
    if ([self.type integerValue] > 6) {
        isSend = YES;
    }
    
    if (!_headView) {
        
        _headView = [[FC_SendPrizeDetailHeadView alloc]initWithFrame:CGRectZero isSend:isSend];
        
        [self.detailContentScrollView addSubview:_headView];
        
        @WeakObj(self)
        _headView.winnerSelectedBlock = ^(BOOL isSelected) {
            
            @StrongObj(self)
            [ self.changeMoneyView show];
            
        };
        
        [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kTopMar);
            make.left.mas_equalTo(kLeftMar);
            make.width.mas_equalTo(DEFAULT_WIDTH);
            make.height.mas_equalTo(headHeight);
        }];
        
    }
    return _headView;
}

-(FC_SendPrizeAthInfoView *)betInfoView{
    
    if (!_betInfoView) {
        
        _betInfoView = [[FC_SendPrizeAthInfoView alloc]init];
        _betInfoView.hidden = YES;
        _betInfoView.type = [self.type integerValue];
        [self.detailContentScrollView addSubview:_betInfoView];
        
        [_betInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.headView.mas_bottom).offset(kTopMar);
            make.left.mas_equalTo(kLeftMar);
            make.width.mas_equalTo(DEFAULT_WIDTH);
            make.height.mas_equalTo(betInfoHeight);
        }];
        
    }
    return _betInfoView;
    
}

-(FC_NumberLottoryBettingView *)orderContentView{
    
    if (!_orderContentView) {
        _orderContentView = [[FC_NumberLottoryBettingView alloc]initWithFrame:CGRectZero type:[self.type integerValue]];
        _orderContentView.hidden = YES;
        [self.detailContentScrollView addSubview:_orderContentView];
        
        [_orderContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kWidth, betInfoHeight));
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(self.headView.mas_bottom).offset(kTopMar);
        }];
    }
    return _orderContentView;
}

-(FC_SendPrizeOrderInfoView *)orderView{
    
    if (!_orderView) {
        _orderView = [[FC_SendPrizeOrderInfoView alloc]init];
        
        [self.detailContentScrollView addSubview:_orderView];
        
        [_orderView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.betInfoView.mas_bottom).offset(kTopMar);
            make.left.mas_equalTo(kLeftMar);
            make.width.mas_equalTo(DEFAULT_WIDTH);
            make.height.mas_equalTo(orderHeight);
        }];
    }
    return _orderView;
}

-(FC_ChangeMoneyView *)changeMoneyView{
    
    NSString * award = self.headView.winnerButton.titleLabel.text;
    
    if (!_changeMoneyView) {
        _changeMoneyView = [[FC_ChangeMoneyView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight) price:award];
        
        @WeakObj(self)
        _changeMoneyView.dismissBlock = ^{
            @StrongObj(self)
            self.changeMoneyView = nil;
        };
        
        _changeMoneyView.confirmBlock = ^(NSString * price){
          @StrongObj(self)
            self.headView.winnerButton.selected = NO;
            
            [self changePrizeRequestWithPrize:price];
        };
    }
    return _changeMoneyView;
}

-(AF_LottoryDetailImageView *)detailImageView{
    
    if (!_detailImageView) {
        _detailImageView = [[AF_LottoryDetailImageView alloc]init];
        [self.detailContentScrollView addSubview:_detailImageView];
        
        [_detailImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.orderView.mas_bottom).offset(kTopMar);
            make.left.mas_equalTo(kLeftMar);
            make.width.mas_equalTo(DEFAULT_WIDTH);
            make.height.mas_equalTo(detailImgHeight);
        }];
        
        @WeakObj(self)
        _detailImageView.imgClickBlock = ^(NSInteger index,NSArray * urls,CGRect frame) {
            
            @StrongObj(self)
            
            [self imagePreviewSetupWithIndex:index urls:urls frame:frame];
            
        };
        
    }
    return _detailImageView;
}

-(void)imagePreviewSetupWithIndex:(NSInteger)index urls:(NSArray *)urls frame:(CGRect)frame{
    
    NSString * urlStr = [urls objectAtIndex:index];  // url string
    
    // 添加蒙层
    UIView * backview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    backview.backgroundColor = [UIColor blackColor];
    [[UIApplication sharedApplication].keyWindow addSubview:backview];
    
    // 获取位置 并创建一个imageView 过渡
    CGRect imgFrame = frame;
    imgFrame.origin.y = self.detailContentScrollView.contentSize.height - 15 - frame.size.height;
    imgFrame.origin.x = frame.origin.x + kLeftMar;
    frame = imgFrame;
    
    UIImageView * imageview = [[UIImageView alloc]initWithFrame:frame];
    imageview.image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:urlStr];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    
    [[UIApplication sharedApplication].keyWindow addSubview:imageview];
    
    
    CGFloat animatime = 0.2;
    //设置动画显示
    [UIView animateWithDuration:animatime animations:^{
        
        [self changeStatusBarHidden:YES];
        
        CGRect  frame = CGRectMake(0, 0, kWidth, kHeight);
        imageview.frame = frame;
        
    }];
    
    // 添加新的view
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(animatime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.imgPreviewView = [[AF_ImagePreviewView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight) index:index];
        self.imgPreviewView.urls = urls;
        
        @WeakObj(self)
        self.imgPreviewView.dismissBlock = ^{
            
            @StrongObj(self)
            
            [self changeStatusBarHidden:NO];
            
            self.detailImageView = nil;
        };
        
        // 删除蒙层
        [[[[UIApplication sharedApplication].keyWindow subviews] lastObject] removeFromSuperview];
        [[[[UIApplication sharedApplication].keyWindow subviews] lastObject] removeFromSuperview];
        
        // 添加预览并刷新
        [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:self.imgPreviewView];
        
        [self.imgPreviewView reloadWithIndex:index];
        
        
    });
    
}

-(void)bottomViewSetup
{
    
    UIView * bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, kHeight - TAB_BAR_HEIGHT - NAVIGATION_HEIGHT, kWidth, TAB_BAR_HEIGHT)];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    
    _prizeLabel = [UILabel lableWithText:@"" font:[UIFont fontWithName:kMedium size:15] textColor:[UIColor colorWithHexString:@"#999999"]];
    _prizeLabel.frame = CGRectMake(kLeftMar, 0, 200, TAB_BAR_ITEM_HEIGHT);
    _prizeLabel.attributedText = [self attributeSubString:@"0.00"];
    [bottomView addSubview:_prizeLabel];
    
    
    CGFloat  height = 37,top = (TAB_BAR_ITEM_HEIGHT - height)/2,width = 140;
    
    _sendButton =[[UIButton alloc]initWithFrame:CGRectMake(kWidth - (width + kLeftMar),top,width, height)];
    [_sendButton setTitle:@"派奖" forState:(UIControlStateNormal)];
    _sendButton.backgroundColor = RGB(237, 37, 37, 1);
    _sendButton.layer.cornerRadius = 5.0f;
    _sendButton.clipsToBounds = YES;
    [_sendButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [_sendButton addTarget:self action:@selector(sendPrizeAction) forControlEvents:(UIControlEventTouchUpInside)];
    _sendButton.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    [bottomView addSubview:_sendButton];
    
}

-(NSAttributedString *)attributeSubString:(NSString *)subString{
    
    NSString * prizeStr = [NSString stringWithFormat:@"奖金 : %@元",subString];
    
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc]initWithString:prizeStr attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#999999"]}];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FD2525"] range:[prizeStr rangeOfString:subString]];
    return attributeString;
}

-(UILabel *)prizeLabel{
    
    if (!_prizeLabel) {
        _prizeLabel = [UILabel lableWithText:@"奖金 : 0.00元" font:[UIFont fontWithName:kMedium size:15] textColor:[UIColor colorWithHexString:@"#999999"]];
        
    }
    return _prizeLabel;
}

-(void)sendPrizeAction{
    
    if (!isSending) {
        
        isSending = YES;
        
        NSArray * idList = @[self.orderId];
        
        NSDictionary *dic = @{@"type":@"5",@"orderTotalIdList":idList};
        @WeakObj(self)
        [PPNetworkHelper POST:PostSendPrice parameters:dic success:^(id responseObject) {
            @StrongObj(self)
            NSLog(@"派奖%@",responseObject);
            isSending = NO;
            
            if ([responseObject[@"state"] isEqualToString:@"success"]) {
                if (self.sendSuccessedBlock) {
                    self.sendSuccessedBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
            
        } failure:^(NSError *error) {
            isSending = NO;
            NSLog(@"派奖失败%@",error);
        }];
    }
 
}


#pragma mark  -- athletics order Request  --

-(void)athleticsOrderRequest{
    
    NSDictionary * athleticsOrderDic = @{
                                         @"orderTotalId":self.orderId,
                                         @"type":self.type
                                         };
    @WeakObj(self)
    
    
    
    NSString * urlStr = [self currentUrlString];
    
    [PPNetworkHelper POST:urlStr parameters:athleticsOrderDic success:^(id responseObject) {
        
        @StrongObj(self)
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            if ([self.type integerValue] < 3 || [self.type integerValue] > 6) {
                self.athDetailModel = [FC_AthDetailModel mj_objectWithKeyValues:responseObject[@"data"]];
            }else{
                self.numberDetailModel = [FC_NumberLottoryDetailModel mj_objectWithKeyValues:responseObject[@"data"]];
            }
            
            [self updateInfo];
        }else{
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

-(NSString *)currentUrlString{

    NSString * urlStr = [self.type integerValue] < 3?PostAthleticeOrder:PostNumberOrder;
    
    switch ([self.type integerValue]) {
        case 7:
            urlStr = PostSuccessOrder;
            break;
        case 8:
            urlStr = PostNineOrder;
            break;
        default:
            break;
    }
    
    return urlStr;
}

-(void)updateInfo{
    
    [self updateBottomPrize];
    
    [self updateContent];
    
    if ([self.type integerValue] < 3 || [self.type integerValue] > 6) {
        [self.headView reloadAthDataWithModel:self.athDetailModel];
        [self.betInfoView reloadAthBetDataWithModel:self.athDetailModel];
        [self.orderView reloadAthOrderDataWithModel:self.athDetailModel];
    }else{
        [self.headView reloadNumDataWithModel:self.numberDetailModel];
        [self.orderView reloadNumberOrderDataWithModel:self.numberDetailModel];
    }


}

-(void)updateBottomPrize{
    
    NSString * price = [self.type integerValue] < 3 || [self.type integerValue] > 6?self.athDetailModel.award:self.numberDetailModel.award;
    
    self.prizeLabel.attributedText = [self attributeSubString:price];
    
}

-(void)updateContent{
    
    [self updateCurrentBetInfoFrame];
    
    if ([self.type integerValue] < 3 || [self.type integerValue] > 6) {
        [self.betInfoView updateContentSizeWithModel:self.athDetailModel];
    }
}

-(void)updateCurrentBetInfoFrame{
    
    CGFloat  contentHeight = 0.0f,itemHeight = 18;
    
    if ([self.type integerValue] < 3 || [self.type integerValue] > 6) {
        
        self.betInfoView.hidden = NO;
        for (FC_AthDetailItemModel * itemModel in self.athDetailModel.detailList) {
            
            CGFloat  normalHeight = 65;
            
            NSArray * contentArr = [itemModel.content componentsSeparatedByString:@","];
            
            if (contentArr.count > 3) {
                normalHeight += (contentArr.count - 3) * itemHeight;
            }
            
            contentHeight += normalHeight;
        }
        
        betInfoHeight += contentHeight;
        
        [self.betInfoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(betInfoHeight);
        }];
        
    }else{
        self.orderContentView.hidden = NO;
        betInfoHeight = (self.numberDetailModel.detailList.count + 1) * 40;
        [self.orderContentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(betInfoHeight);
        }];
        [self.orderContentView reloadBettingWithModel:self.numberDetailModel];
    }

    [self reloadOrderFrame];
    
    [self reloadImageViewFrame];
    
    [self reloadScorllContentSize];
    
}

-(void)reloadOrderFrame{
    
    NSMutableArray * itemsArray = [self getOrderItems];
    CGFloat  orderItemHeight = 20;
    orderHeight += orderItemHeight * itemsArray.count + 10;
    
    [self.orderView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(orderHeight);
    }];
    
}

-(void)reloadImageViewFrame{
    
    if ([self.type integerValue] < 3 || [self.type integerValue] > 6) {
        if (self.athDetailModel.image.length > 0 && ![self.athDetailModel.image isEqualToString:@"-"]) {
            
            detailImgHeight = 233;
            [self.detailImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(detailImgHeight);
            }];
            
            NSString * imageString = self.athDetailModel.image;
            if ([imageString hasSuffix:@","]) {
                imageString = [imageString substringToIndex:imageString.length - 1];
            }
            NSArray * urls = [imageString componentsSeparatedByString:@","];
            
            [self.detailImageView reloadWithUrls:urls];
            
        }
    }else{
        if (self.numberDetailModel.image.length > 0 && ![self.numberDetailModel.image isEqualToString:@"-"]) {
            
            detailImgHeight = 233;
            [self.detailImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(detailImgHeight);
            }];
            NSString * imageString = self.numberDetailModel.image;
            if ([imageString hasSuffix:@","]) {
                imageString = [imageString substringToIndex:imageString.length - 1];
            }
            NSArray * urls = [imageString componentsSeparatedByString:@","];
            
            [self.detailImageView reloadWithUrls:urls];
            
        }
    }
}

-(NSMutableArray *)getOrderItems{
    
    NSMutableArray * items = [NSMutableArray new];
    
    if ([self.type integerValue] < 3 || [self.type integerValue] > 6) {
        
        if ([self isItemWith:self.athDetailModel.nickName]) {
            [items addObject:self.athDetailModel.nickName];
        }
        
        if ([self isItemWith:self.athDetailModel.orderNumber]) {
            [items addObject:self.athDetailModel.orderNumber];
        }
        
        if ([self isItemWith:self.athDetailModel.createDate]) {
            [items addObject:self.athDetailModel.createDate];
        }
        
        if ([self isItemWith:self.athDetailModel.printDate]) {
            [items addObject:self.athDetailModel.printDate];
        }
    }else{
        if ([self isItemWith:self.numberDetailModel.nickName]) {
            [items addObject:self.numberDetailModel.nickName];
        }
        if ([self isItemWith:self.numberDetailModel.orderNumber]) {
            [items addObject:self.numberDetailModel.orderNumber];
        }
        
        if ([self isItemWith:self.numberDetailModel.createDate]) {
            [items addObject:self.numberDetailModel.createDate];
        }
        
        if ([self isItemWith:self.numberDetailModel.printDate]) {
            [items addObject:self.numberDetailModel.printDate];
        }
        if ([self isItemWith:self.numberDetailModel.result]) {
            [items addObject:self.numberDetailModel.result];
        }
    }

    return items;
}

-(BOOL)isItemWith:(NSString *)item{
    
    if ([item isEqualToString:@""] || !item) {
        return NO;
    }
    if ([item isEqualToString:@"-"]) {
        return  NO;
    }
    
    return  YES;
}


-(void)reloadScorllContentSize{

    CGFloat  contentSizeHeight = kHeight - (NAVIGATION_HEIGHT + TAB_BAR_HEIGHT);

    CGFloat  currentHeight = headHeight + betInfoHeight + orderHeight + kTopMar * 4;

    if ([self.type integerValue] < 3 || [self.type integerValue] > 6) {
        if (self.athDetailModel.image.length > 0) {
            currentHeight += kTopMar + detailImgHeight;
        }
    }else{
        if (self.numberDetailModel.image.length > 0) {
            currentHeight += kTopMar + detailImgHeight;
        }
    }

    if (currentHeight > contentSizeHeight) {
        contentSizeHeight = currentHeight;
    }

    [self.detailContentScrollView setContentSize:CGSizeMake(kWidth, contentSizeHeight)];
    
}

#pragma mark  --  change prize   --

-(void)changePrizeRequestWithPrize:(NSString *)prize{
    
    NSDictionary *dic = @{@"award":prize,@"orderTotalId":self.orderId};
    @WeakObj(self)
    [PPNetworkHelper POST:PostChangePrizeOrder parameters:dic success:^(id responseObject) {
        @StrongObj(self)
        
        if ([responseObject[@"state"] isEqualToString:@"success"]) {
            
            [self.headView.winnerButton setTitle:prize forState:UIControlStateNormal];
            self.prizeLabel.attributedText = [self attributeSubString:prize];
            
            [FC_Manager showToastWithText:@"修改派奖金额成功"];
            if (self.changePrizeBlock) {
                self.changePrizeBlock();
            }
        }else{
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSError *error) {
        [FC_Manager showToastWithText:@"修改派奖金额失败,请稍后重试"];
    }];
    
}

#pragma mark  --  status bar state  --

-(void)changeStatusBarHidden:(BOOL)isHidden{
    
    self.statusBarIsHidden = isHidden;
    [self prefersStatusBarHidden];
    [self setNeedsStatusBarAppearanceUpdate];
    
}

- (BOOL)prefersStatusBarHidden{
    return self.statusBarIsHidden;
}

#pragma mark  --  view will appear  --

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.tabBarController.tabBar setHidden:YES];
    
}

@end
