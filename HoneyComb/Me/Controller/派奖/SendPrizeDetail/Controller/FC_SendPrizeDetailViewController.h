//
//  FC_SendPrizeDetailViewController.h
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

typedef void(^SendSuccessedBlock)(void);
typedef void(^ChangePrizeBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface FC_SendPrizeDetailViewController : AllViewController

- (instancetype)initWithOrderId:(NSString *)orderId type:(NSString *)type isSend:(BOOL)isSend;

@property (nonatomic,copy) SendSuccessedBlock sendSuccessedBlock;

@property (nonatomic,copy) ChangePrizeBlock changePrizeBlock;

@end

NS_ASSUME_NONNULL_END
