//
//  FC_AthDetailModel.m
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_AthDetailModel.h"

#import "FC_AthDetailItemModel.h"

@implementation FC_AthDetailModel

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key{
    
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property{
    
    if ([oldValue isKindOfClass:[NSString class]]) {
        if (oldValue == nil || [oldValue isEqualToString:@""]) {// 以字符串类型为例
            return  @"-";
        }
    }
    
    return oldValue;
}

+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"detailList":@"FC_AthDetailItemModel",
             };
}


@end
