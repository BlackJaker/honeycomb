//
//  FC_AthDetailModel.h
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_AthDetailModel : NSObject

@property  (nonatomic,copy) NSString * account;
@property  (nonatomic,copy) NSString * award;
@property  (nonatomic,copy) NSString * bunch;
@property  (nonatomic,copy) NSString * createDate;
@property  (nonatomic,copy) NSString * image;
@property  (nonatomic,copy) NSString * isImage;
@property  (nonatomic,copy) NSString * mobile;
@property  (nonatomic,copy) NSString * orderNumber;
@property  (nonatomic,copy) NSString * playName;
@property  (nonatomic,copy) NSString * price;
@property  (nonatomic,copy) NSString * state;
@property  (nonatomic,copy) NSString * times;
@property  (nonatomic,copy) NSString * printDate;
@property  (nonatomic,copy) NSString * cancelDate;
@property  (nonatomic,copy) NSString * issue;
@property  (nonatomic,copy) NSArray * detailList;
@property  (nonatomic,copy) NSString * nickName;

@end

NS_ASSUME_NONNULL_END
