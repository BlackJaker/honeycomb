//
//  FC_AthDetailItemModel.h
//  HoneyComb
//
//  Created by afc on 2018/12/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_AthDetailItemModel : NSObject

@property  (nonatomic,copy) NSString * content;
@property  (nonatomic,copy) NSString * hostTeam;
@property  (nonatomic,copy) NSString * league;
@property  (nonatomic,copy) NSString * number;
@property  (nonatomic,copy) NSString * orderDetailId;
@property  (nonatomic,copy) NSString * startTime;
@property  (nonatomic,copy) NSString * visitingTeam;
@property  (nonatomic,copy) NSString * dan;
@property  (nonatomic,copy) NSString * result;
@property  (nonatomic,copy) NSString * endScore;

@end

NS_ASSUME_NONNULL_END
