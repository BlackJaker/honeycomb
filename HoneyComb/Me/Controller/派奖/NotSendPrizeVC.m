//
//  NotSendPrizeVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "NotSendPrizeVC.h"
#import "AllChooseCell.h"

#import "FC_SendPrizeDetailViewController.h"

@interface NotSendPrizeVC ()<UITableViewDelegate,UITableViewDataSource>
{
      NSString *typeStr;
       NSString *total;
    BOOL allSelect;
        BOOL isUser;
    BOOL isSending;        // 派奖中
}
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIButton *allBtn;
@property (nonatomic,strong) UILabel *allPrice;
@property (nonatomic,strong) UIButton *sendPrice;
@property (nonatomic,strong) NSMutableArray *listArr;
@property (nonatomic,strong) NSMutableArray *selectArr;
@property (nonatomic,assign)NSInteger page;
@property (nonatomic,strong) NSSet *setArr;
@end

@implementation NotSendPrizeVC
-(NSMutableArray *)selectArr
{
    if (!_selectArr) {
        _selectArr = [NSMutableArray array];
    }
    return _selectArr;
}
-(NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isSending = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     isUser = YES;
    AllModel *model = [[AllModel alloc]init];
    model.isSelected = NO;
    allSelect = YES;
    isSending = NO;
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(tongzhiBetting:) name:@"sendChooseTongzhi" object:nil];
//    self.view.backgroundColor = [UIColor blueColor];
    [self layoutView];
    [self creatData];
      [self setupRefresh];
}
-(void)tongzhiBetting:(NSNotification *)dic
{
    if (dic.userInfo .count == 0) {
        
    }else{
        NSString *str = [dic.userInfo objectForKey:@"sendChoose"];
        if ([str  isEqualToString:@""] ) {
            NSString *s ;
            typeStr = s;
            NSLog(@"-%@",typeStr);
        }else
        {
            typeStr = [dic.userInfo objectForKey:@"sendChoose"];
        }
        
        
        [self setupRefresh];
    }
}
#pragma mark --创建上拉加载和下拉刷新
- (void)setupRefresh {
    __block NotSendPrizeVC *weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf creatData];
    }];
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadNewTopics];
    }];
}
-(void)creatData
{
    [self.selectArr removeAllObjects];
    AllModel *model = [[AllModel alloc]init];
    model.isSelected = NO;
    self.allBtn.selected = NO;
    [self.sendPrice setTitle:@"派奖(0)" forState:(UIControlStateNormal)];
    
    
    NSString *bounsPrice = @"0元";
    NSString * bonusStr = [NSString stringWithFormat:@"奖金: %@",bounsPrice];
    
    NSMutableAttributedString *bounAtt = [[NSMutableAttributedString alloc]initWithString:bonusStr];
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(0, 4)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(0, 4)];
    
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(4, bounsPrice.length)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(4, bounsPrice.length)];
    //    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(4+bounsPrice.length, 1)];
    //    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(4+bounsPrice.length, 1)];
    self.allPrice.attributedText =bounAtt;
    self.page = 1;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"state"] = @"3";
    dic[@"pageNum"] = pageStr;
    dic[@"type"] = typeStr;
    //    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser]shopId ],@"state":@"1",@"pageNum":pageStr,@"type":typeStr};
    NSLog(@"未派奖%@",dic);
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
        NSLog(@"未派奖%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            NSMutableArray *arr = [solveJsonData changeType:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
            if (arr.count > 0) {
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str] ) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData ];
                }else
                {
                    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        [self loadNewTopics];
                    }];
                }
                
                self.listArr = [AllModel mj_objectArrayWithKeyValuesArray:arr];
                
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }else
            {
                [self.listArr removeAllObjects];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
//                [STTextHudTool showText:@"暂无数据!"];
            }
            
        }else
        {
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
//            [self.tableView.mj_header endRefreshing];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)loadNewTopics
{
    self.page ++;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"state"] = @"3";
    dic[@"pageNum"] = pageStr;
    dic[@"type"] = typeStr;
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            NSArray  *arr =[AllModel mj_objectArrayWithKeyValuesArray:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
            if (arr == nil) {
                [STTextHudTool showText:@"暂无更多数据!"];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
//                                [self.view showError:@"暂无信息"];
            }else
            {
                [self.listArr addObjectsFromArray:arr];
                
                [self.tableView reloadData];
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                //                GFLog(@"total=%@",total);
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str]) {
                    //                    GFLog(@"2233");
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else
                {
                    [self.tableView.mj_footer endRefreshing];
                }
            }
            
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-NavgationBarHeight-15-60-TabBarHeight) style:(UITableViewStylePlain)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
//    self.tableView.backgroundColor = RGB(249, 249, 249, 1);
//   [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 10, 0)];
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView registerClass:[AllChooseCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
    
    self.allBtn = [[UIButton alloc]init];
    [self.allBtn setTitle:@"全选" forState:(UIControlStateNormal)];
    [self.allBtn addTarget:self action:@selector(allChooseAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.allBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.allBtn setImage:[UIImage imageNamed:@"椭圆4"] forState:(UIControlStateNormal)];
    [self.allBtn setTitle:@"全选" forState:(UIControlStateSelected)];

    [self.allBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateSelected)];
    [self.allBtn setImage:[UIImage imageNamed:@"组1"] forState:(UIControlStateSelected)];
    self.allBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    self.allBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
    [self.view addSubview:self.allBtn];
    [self.allBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(kHeight-NavgationBarHeight-15-60-TabBarHeight+(TabBarHeight/2-10));
    }];
    
    self.sendPrice = [[UIButton alloc]init];
    [self.sendPrice setTitle:@"派奖(0)" forState:(UIControlStateNormal)];
    [self.sendPrice setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.sendPrice addTarget:self action:@selector(sendAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.sendPrice.backgroundColor = RGB(253, 37, 37, 1);
    self.sendPrice.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [self.view addSubview:self.sendPrice];
    [self.sendPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(146);
        make.height.mas_equalTo(49);
        make.left.mas_equalTo(kWidth-146);
        make.top.mas_equalTo(kHeight-NavgationBarHeight-15-60-TabBarHeight+(TabBarHeight/2-24));
    }];
    
//    NSString *bounsPrice = @"24";
//    NSString * bonusStr = [NSString stringWithFormat:@"奖金:%@元",bounsPrice];
//
//    NSMutableAttributedString *bounAtt = [[NSMutableAttributedString alloc]initWithString:bonusStr];
//    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(0, 3)];
//    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, 3)];
//
//    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(3, bounsPrice.length+1)];
//    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(3, bounsPrice.length+1)];
    
  
    
    self.allPrice = [[UILabel alloc]init];
    [self.view addSubview:self.allPrice];
//    self.allPrice.attributedText = bounAtt;
    self.allPrice.textAlignment = NSTextAlignmentRight;
    //    self.bonusLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    //    self.bonusLabel.textColor = kGrayColor;
    [self.allPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(150);
        make.height.mas_equalTo(18);
        make.right.mas_equalTo(self.sendPrice.mas_left).offset(-10);
        make.top.mas_equalTo(kHeight-NavgationBarHeight-15-60-TabBarHeight+(TabBarHeight/2-9));
    }];
    
   
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllChooseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[AllChooseCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    AllModel *model = self.listArr[indexPath.row];
    cell.model = self.listArr[indexPath.row];
    cell.selectedStutas = model.isSelected;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = RGB(249, 249, 249, 1);
    
    //block callback
    __weak typeof(self) weakSelf = self;
    cell.ChoseBtnBlock = ^(__weak UITableViewCell *tapCell, BOOL selected) {
        //使用model动态记录cell按钮选中情况
        NSIndexPath *path = [tableView indexPathForCell:tapCell];
        AllModel *carModel = [self.listArr objectAtIndex:path.row];
        carModel.isSelected = selected;
        if (!selected) {
            weakSelf.allBtn.selected = selected;
            [self.selectArr removeObject:model.orderTotalId];
        }else{
            [self.selectArr addObject:model.orderTotalId];
        }

        [weakSelf calculateWhetherFutureGenerations];
        
    };
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    AllModel * model = [self.listArr objectAtIndex:indexPath.row];
    
    self.hidesBottomBarWhenPushed = YES;
    FC_SendPrizeDetailViewController * sendPrizeDetailVc = [[FC_SendPrizeDetailViewController alloc]initWithOrderId:model.orderTotalId type:model.type isSend:NO];
    @WeakObj(self)
    sendPrizeDetailVc.sendSuccessedBlock = ^{
        @StrongObj(self);
        [STTextHudTool showText:@"派奖成功"];
        [self creatData];
    };
    
    sendPrizeDetailVc.changePrizeBlock = ^{
      @StrongObj(self)
        [self creatData];
    };
    
    [self.navigationController pushViewController:sendPrizeDetailVc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
    
}
-(void)allChooseAction:(UIButton *)btn
{
    self.allBtn.selected = !self.allBtn.selected;
    CGFloat totalPrice = 0;
    NSInteger totalNum = 0;
    
    for (NSInteger i = 0; i < self.listArr.count; i ++) {
        AllModel *model = [self.listArr objectAtIndex:i];
        model.isSelected = self.allBtn.selected;
        if (self.allBtn.selected) {
            CGFloat price = [model.award floatValue];
            totalPrice +=price;
         
            NSMutableArray *arr = [NSMutableArray array];
            [arr addObject:model.orderTotalId];
            self.setArr = [NSSet setWithArray:arr];
            [self.selectArr addObjectsFromArray:[self.setArr allObjects]];
        }else
        {
            [self.selectArr removeObject:model.orderTotalId];
        }
      
    }
     if (self.allBtn.selected) {
         totalNum = self.listArr.count;
        
     }
    NSLog(@"订单=%@",self.selectArr);
    [self.sendPrice setTitle:[NSString stringWithFormat:@"派奖(%ld)",totalNum] forState:(UIControlStateNormal)];
    
    
    NSString *bounsPrice = [NSString stringWithFormat:@"%.2f元",totalPrice];
    NSString * bonusStr = [NSString stringWithFormat:@"奖金: %@",bounsPrice];
    
    NSMutableAttributedString *bounAtt = [[NSMutableAttributedString alloc]initWithString:bonusStr];
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(0, 4)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(0, 4)];
    
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(4, bounsPrice.length)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(4, bounsPrice.length)];
//    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(4+bounsPrice.length, 1)];
//    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(4+bounsPrice.length, 1)];
    self.allPrice.attributedText =bounAtt;
    [self.tableView reloadData];
}
//计算是否全选
- (void)calculateWhetherFutureGenerations{
    NSInteger totalSelected = 0;
    CGFloat totalPrice = 0;
    for (NSInteger i = 0; i < self.listArr.count; i ++){
        AllModel *model = [self.listArr objectAtIndex:i];
        if (model.isSelected) {
            totalSelected ++;
            CGFloat price = [model.award floatValue];
            totalPrice +=price;
            
           
        }
    }
    
    if (totalSelected == self.listArr.count) {
        self.allBtn.selected = YES;
    }else{
        self.allBtn.selected = NO;
    }
    
    [self.sendPrice setTitle:[NSString stringWithFormat:@"派奖(%ld)",totalSelected] forState:(UIControlStateNormal)];
    NSString *bounsPrice = [NSString stringWithFormat:@"%.2f",totalPrice];
    NSString * bonusStr = [NSString stringWithFormat:@"奖金: %@元",bounsPrice];
    
    NSMutableAttributedString *bounAtt = [[NSMutableAttributedString alloc]initWithString:bonusStr];
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(0, 4)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(0, 4)];
    
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(4, bounsPrice.length)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(4, bounsPrice.length)];
    [bounAtt setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(4+bounsPrice.length, 1)];
    [bounAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(4+bounsPrice.length, 1)];
    self.allPrice.attributedText =bounAtt;
}
-(void)sendAction
{
    
    if (!isSending) {

        if (self.selectArr.count == 0) {
            [FC_Manager showToastWithText:@"请选择派奖订单"];
            return;
        }
        
        isSending = YES;    //派奖中
        
        NSSet *set = [NSSet setWithArray:self.selectArr];
        NSLog(@"--%@",[set allObjects]);
        
        NSDictionary *dic = @{@"type":@"5",@"orderTotalIdList":[set allObjects]};
        
        [PPNetworkHelper POST:PostSendPrice parameters:dic success:^(id responseObject) {
            NSLog(@"派奖%@",responseObject);
            
            isSending = NO;  // 派奖完成
            
            if ([responseObject[@"state"] isEqualToString:@"success"]) {
                [self creatData];
                [STTextHudTool showText:@"派奖成功"];
            }else{
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
            
        } failure:^(NSError *error) {
            NSLog(@"派奖失败%@",error);
            isSending = NO;
        }];
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 172;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
