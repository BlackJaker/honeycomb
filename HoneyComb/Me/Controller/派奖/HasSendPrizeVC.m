//
//  HasSendPrizeVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "HasSendPrizeVC.h"
#import "NotSendPrizeCell.h"

#import "FC_SendPrizeDetailViewController.h"

@interface HasSendPrizeVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSString *typeStr;
    NSString *total;
}
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *listArr;
@property (nonatomic,assign)NSInteger page;
@end

@implementation HasSendPrizeVC
-(NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
// self.view.backgroundColor = [UIColor greenColor];
     [self layoutView];
    [self creatData];
    [self setupRefresh];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(tongzhiBetting:) name:@"sendChooseTongzhi" object:nil];
}
-(void)tongzhiBetting:(NSNotification *)dic
{
    if (dic.userInfo .count == 0) {
        
    }else{
        NSString *str = [dic.userInfo objectForKey:@"sendChoose"];
        if ([str  isEqualToString:@""] ) {
            NSString *s ;
            typeStr = s;
            NSLog(@"-%@",typeStr);
        }else
        {
            typeStr = [dic.userInfo objectForKey:@"sendChoose"];
        }
        
        
        [self setupRefresh];
    }
}
- (void)setupRefresh {
    __block HasSendPrizeVC *weakSelf = self;
        self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf creatData];
        }];
        [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadNewTopics];
    }];
}-(void)creatData
{
    self.page = 1;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"state"] = @"5";
    dic[@"pageNum"] = pageStr;
    dic[@"type"] = typeStr;
    //    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser]shopId ],@"state":@"1",@"pageNum":pageStr,@"type":typeStr};
    NSLog(@"已派奖%@",dic);
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
       DYLog(@"已派奖%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            NSMutableArray *arr = [solveJsonData changeType:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
            if (arr.count > 0) {
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str] ) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData ];
                }else
                {
                    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        [self loadNewTopics];
                    }];
                }
                
                self.listArr = [AllModel mj_objectArrayWithKeyValuesArray:arr];
                
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }else
            {
                [self.listArr removeAllObjects];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
//                [STTextHudTool showText:@"暂无数据!"];
            }
            
        }else
        {
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
              [self.tableView.mj_header endRefreshing];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)loadNewTopics
{
    self.page ++;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"state"] = @"5";
    dic[@"pageNum"] = pageStr;
    dic[@"type"] = typeStr;
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            NSArray  *arr =[AllModel mj_objectArrayWithKeyValuesArray:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
            if (arr == nil) {
                [STTextHudTool showText:@"暂无更多数据!"];
                                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
                //                [self.view showError:@"暂无信息"];
            }else
            {
                [self.listArr addObjectsFromArray:arr];
                
                [self.tableView reloadData];
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                //                GFLog(@"total=%@",total);
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str]) {
                    //                    GFLog(@"2233");
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else
                {
                    [self.tableView.mj_footer endRefreshing];
                }
            }
            
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
                [self.tableView.mj_header endRefreshing];
    }];
}
-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-NavgationBarHeight-15-60) style:(UITableViewStylePlain)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    //    self.tableView.backgroundColor = RGB(249, 249, 249, 1);
    //   [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 10, 0)];
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView registerClass:[NotSendPrizeCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotSendPrizeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = RGB(249, 249, 249, 1);
    if (cell == nil) {
        cell = [[NotSendPrizeCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.listArr[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AllModel * model = [self.listArr objectAtIndex:indexPath.row];
    
    self.hidesBottomBarWhenPushed = YES;
    FC_SendPrizeDetailViewController * sendPrizeDetailVc = [[FC_SendPrizeDetailViewController alloc]initWithOrderId:model.orderTotalId type:model.type isSend:YES];
    [self.navigationController pushViewController:sendPrizeDetailVc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 172;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
