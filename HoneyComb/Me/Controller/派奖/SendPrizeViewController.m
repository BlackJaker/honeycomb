//
//  SendPrizeViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "SendPrizeViewController.h"
#import "SendPrizeView.h"
#import "NotSendPrizeVC.h"
#import "HasSendPrizeVC.h"
#import "SectionChooseView.h"
@interface SendPrizeViewController ()<UIScrollViewDelegate,SectionChooseVCDelegate>
{
     BOOL isUser;
    NSString *selectStr;
    UIView *redView;
}

@property (nonatomic,strong) SendPrizeView *userView;
@property (nonatomic,strong) YYLabel *titleL;
@property (nonatomic,strong) UIImageView *titleImage;

//底部滚动ScrollView
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property(nonatomic,strong)SectionChooseView *sectionChooseView;
@end

@implementation SendPrizeViewController
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    if (@available(iOS 11.0, *)) {
        self.contentScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
      isUser = YES;
//    NSLog(@"-------------%@",selectStr);
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    [dic setValue:selectStr forKey:@"selectIndex"];
//    //创建通知
//    NSNotification *notification =[NSNotification notificationWithName:@"selectIndexTongzhi" object:nil userInfo:dic];
//    //通过通知中心发送通知
//    [[NSNotificationCenter defaultCenter] postNotification:notification];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(tongzhiMore:) name:@"sendPrizeTongzhi" object:nil];
    
    //获取通知中心
    NSNotificationCenter * chooseTitle =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(chooseAction:) name:@"sendChooseTongzhi" object:nil];
    
    // 首次进入加载第一个界面通知
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFirstVC) name:@"ABC" object:nil];
    
    //添加所有子控制器
    [self setupChildViewController];
    
    //初始化UIScrollView
    [self setupUIScrollView];
}
-(void)chooseAction:(NSNotification *)dic
{
    if (dic.userInfo.count == 0) {
        
    }else
    {
     self.titleL.text =   [dic.userInfo objectForKey:@"titleLabel"];
        NSString *str = self.titleL.text;
         NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        CGSize textSize = [str boundingRectWithSize:CGSizeMake(kWidth/4-3, 100) options:NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
        [self.titleImage mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(11);
            make.height.mas_equalTo(7);
            make.left.mas_equalTo(self.titleL.mas_left).offset(textSize.width+30);
            make.bottom.mas_equalTo(redView.mas_bottom).offset(-16);
        }];
        
    }
}
-(void)tongzhiMore:(NSNotification *)dic
{
    isUser = YES;
}
-(void)layoutView
{
    
    redView = [[UIView alloc]init];
    redView.backgroundColor = [UIColor redColor];
    redView.userInteractionEnabled = YES;
    [self.view addSubview:redView];
    [redView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(NavgationBarHeight);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.left = [[UIButton alloc]init];
    [self.left setImage:[UIImage imageNamed:@"返回"] forState:(UIControlStateNormal)];
    [self.left addTarget:self action:@selector(leftAction) forControlEvents:(UIControlEventTouchUpInside)];
    [redView addSubview:self.left];
    [self.left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(19);
        make.height.mas_equalTo(32);
        make.left.mas_equalTo(10);
        make.bottom.mas_equalTo(redView.mas_bottom).offset(-2);
    }];
    
    self.titleL = [[YYLabel alloc]init];
     self.titleL.text = @"全部彩种";
     self.titleL.textAlignment = NSTextAlignmentCenter;
     self.titleL.userInteractionEnabled = YES;
     self.titleL.textColor = [UIColor whiteColor];
     self.titleL.font = [UIFont fontWithName:kPingFangRegular size:20];
    [redView addSubview:self. self.titleL];
    [ self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
        make.left.mas_equalTo(kWidth/2-45);
        make.bottom.mas_equalTo(redView.mas_bottom).offset(-10);
    }];
    
    UITapGestureRecognizer *dressTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dressAction)];
    [ self.titleL addGestureRecognizer:dressTap];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
    
    NSString *str = self.titleL.text;
    CGSize textSize = [str boundingRectWithSize:CGSizeMake(kWidth/4-3, 100) options:NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    
    self.titleImage = [[UIImageView alloc]init];
    self.titleImage.image = [UIImage imageNamed:@"返回副本2"];
    [redView addSubview:self.titleImage];
    [self.titleImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(11);
        make.height.mas_equalTo(7);
        make.left.mas_equalTo(self.titleL.mas_left).offset(textSize.width+25);
        make.bottom.mas_equalTo(redView.mas_bottom).offset(-16);
    }];
    
}
-(void)dressAction
{
    if (isUser) {
        _userView = [[SendPrizeView alloc]initWithFrame:CGRectMake(0, NAVIGATION_HEIGHT, kWidth, kHeight)];
        UIWindow *keyWin = [[UIApplication sharedApplication]keyWindow];
        [keyWin addSubview:_userView];
        isUser = NO;
    }else{
        _userView.hidden = YES;
        isUser = YES;
    }
}
- (void)showFirstVC {
    NSLog(@"0000");
    selectStr = @"0";
    [self showVc:0];
}


- (void)setupUIScrollView {
    
    // 创建底部滚动视图
    self.contentScrollView = [[UIScrollView alloc] init];
    _contentScrollView.frame = CGRectMake(0, NavgationBarHeight+15+60, kWidth, kHeight-(NavgationBarHeight+15+60));
    _contentScrollView.contentSize = CGSizeMake(self.view.frame.size.width * 2, 0);
    _contentScrollView.backgroundColor = [UIColor whiteColor];
    // 开启分页
    _contentScrollView.pagingEnabled = YES;
    // 没有弹簧效果
    _contentScrollView.bounces = NO;
    // 隐藏水平滚动条
    _contentScrollView.showsHorizontalScrollIndicator = NO;
    // 设置代理
    _contentScrollView.delegate = self;
    [self.view addSubview:_contentScrollView];
    
    self.sectionChooseView = [[SectionChooseView alloc] initWithFrame:CGRectMake(0, NavgationBarHeight+15, self.view.frame.size.width, 60) titleArray:@[@"未派奖", @"已派奖"]];
    self.sectionChooseView.selectIndex = 0;
    self.sectionChooseView.delegate = self;
    self.sectionChooseView.normalBackgroundColor = [UIColor whiteColor];
    self.sectionChooseView.selectBackgroundColor = [UIColor redColor];;
    self.sectionChooseView.titleNormalColor = [UIColor redColor];
    self.sectionChooseView.titleSelectColor = [UIColor whiteColor];
    self.sectionChooseView.normalTitleFont = 15;
    self.sectionChooseView.selectTitleFont = 15;
    [self.view addSubview:self.sectionChooseView];
    
}

#pragma mark -添加所有子控制器

-(void)setupChildViewController {
    
    NotSendPrizeVC *voucherUnusedVC = [[NotSendPrizeVC alloc] init];
    
    [self addChildViewController:voucherUnusedVC];
    
    
    HasSendPrizeVC *voucherUsedVC = [[HasSendPrizeVC alloc] init];
    
    [self addChildViewController:voucherUsedVC];
    
}

#pragma mark -SMCustomSegmentDelegate

- (void)SectionSelectIndex:(NSInteger)selectIndex {
    
    NSLog(@"---------%ld",(long)selectIndex);
    selectStr = [NSString stringWithFormat:@"%ld",selectIndex];
    
  
    // 1 计算滚动的位置
    CGFloat offsetX = selectIndex * self.view.frame.size.width;
    self.contentScrollView.contentOffset = CGPointMake(offsetX, 0);
    
    // 2.给对应位置添加对应子控制器
    [self showVc:selectIndex];
}

#pragma mark -显示控制器的view
/**
 *  显示控制器的view
 *
 *  @param index 选择第几个
 *
 */
- (void)showVc:(NSInteger)index {
    
    CGFloat offsetX = index * self.view.frame.size.width;
    
    UIViewController *vc = self.childViewControllers[index];
    
    // 判断控制器的view有没有加载过,如果已经加载过,就不需要加载
    if (vc.isViewLoaded) return;
    
    [self.contentScrollView addSubview:vc.view];
    vc.view.frame = CGRectMake(offsetX, 0, self.view.frame.size.width, self.view.frame.size.height-138);
}

#pragma mark -UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    
    // 计算滚动到哪一页
    NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    // 1.添加子控制器view
    [self showVc:index];
    
    // 2.把对应的标题选中
    self.sectionChooseView.selectIndex = index;
    
    
}
-(void)leftAction
{
    [self.userView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
