//
//  FC_WithdrawViewController.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_WithdrawViewController.h"

#import "FC_BankCardViewController.h"

#import "FC_WithdrawAccountDetailVC.h"

@interface FC_WithdrawViewController ()<SGPageTitleViewDelegate, SGPageContentScrollViewDelegate>
{
    CGFloat  segmentHeight;
}
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentScrollView *pageContentScrollView;

@end

@implementation FC_WithdrawViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f7f7f7"];
    
    self.titleLabe.text = @"提现";
    
    segmentHeight = 50;
    [self layoutView];
    
}
-(void)layoutView
{
    NSArray *titleArr = @[@"银行卡",@"提现记录"];
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.indicatorHeight = 2;
    configure.indicatorCornerRadius = 2;
    configure.indicatorColor = [UIColor redColor];
    configure.titleColor = [UIColor colorWithHexString:@"#464E5F"];
    configure.titleSelectedColor = [UIColor colorWithHexString:@"#E73A59"];
    configure.titleSelectedFont = [UIFont fontWithName:kBold size:15];
    configure.titleFont = [UIFont fontWithName:kPingFangRegular size:14];
    configure.indicatorScrollStyle = SGIndicatorScrollStyleEnd;
    configure.showBottomSeparator = NO;
    configure.indicatorToBottomDistance = 9;
    
    /// pageTitleView
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, 0, kWidth, segmentHeight) delegate:self titleNames:titleArr configure:configure];
    self.pageTitleView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_pageTitleView];
    

    FC_BankCardViewController *bankCardVc = [[FC_BankCardViewController alloc]initWithName:self.safeInfoDic[@"name"]];
    @WeakObj(self)
    bankCardVc.drawMoneySuccessedBlock = ^{
        @StrongObj(self)
        [self.pageContentScrollView setPageContentScrollViewCurrentIndex:1];
    };
    
    self.hidesBottomBarWhenPushed=YES;
    FC_WithdrawAccountDetailVC *withdrawalVc = [[FC_WithdrawAccountDetailVC alloc]init];
    withdrawalVc.type = 1;
    NSArray *childArr = @[bankCardVc,withdrawalVc];
    /// pageContentScrollView
    CGFloat contentViewHeight = kHeight - CGRectGetMaxY(_pageTitleView.frame) - TAB_BAR_SAFE_HEIGHT - NAVIGATION_HEIGHT;
    self.pageContentScrollView = [[SGPageContentScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), kWidth, contentViewHeight) parentVC:self childVCs:childArr];
    _pageContentScrollView.delegatePageContentScrollView = self;
    [self.view addSubview:_pageContentScrollView];
}
- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentScrollView setPageContentScrollViewCurrentIndex:selectedIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView index:(NSInteger)index {
    if (index == 0) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    } else {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[self createImageWithColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setShadowImage:[self createImageWithColor:[UIColor clearColor]]];

    
}

- (UIImage *)createImageWithColor:(UIColor *)color{
    
    CGRect rect = CGRectMake(0.0f,0.0f,1.0f,1.0f);
    
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context =UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    
    CGContextFillRect(context, rect);
    
    UIImage *theImage =UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return theImage;
    
}


-(void)leftAction{
    
    [self.navigationController popToViewController:self.rootVc animated:YES];
    
}

@end
