//
//  FC_BankCardViewController.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_BankCardViewController.h"

#import "AF_BankNameViewController.h"

@interface FC_BankCardViewController ()<UITextFieldDelegate>
{
    CGFloat  itemHeight;
    CGFloat  moneyBackHeight;
    BOOL isHaveDot;
}
@property (nonatomic,strong) UILabel *priceLabel;      // 账户余额

@property (nonatomic,strong) UIView  *contentBackView; // 背景

@property (nonatomic,strong) NSArray  *titles;

@property (nonatomic,strong) UILabel *nameLabel;//姓名
@property (nonatomic,strong) UILabel *bankLabel;//开户银行
@property (nonatomic,strong) UITextField *bankNumberTextField;//银行卡号
@property (nonatomic,strong) UITextField *priceTf;//提现金额
@property (nonatomic,strong) UITextField *passWordTf;//密码

@property (nonatomic,copy) NSString * name;

@property (nonatomic,assign) BOOL isWithdrawing;

@end

@implementation FC_BankCardViewController

-(instancetype)initWithName:(NSString *)name{
    
    if (self = [super init]) {
        isHaveDot = NO;
        self.name = name;
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    itemHeight = 45;
    moneyBackHeight = 78;
    
    [self layoutView];
    
    self.isWithdrawing = NO;
    
}
-(void)layoutView
{
    
    self.priceLabel.attributedText = [self attributedStr];
    
    [self contentViewSetup];  // 信息
    
    [self confirmButtonSetup]; // 确认提现
    
}

-(NSMutableAttributedString *)attributedStr{
    
    NSString * string = [NSString stringWithFormat:@"%@元",[LoginUser shareLoginUser].drawMoney];
    NSRange subRange = [string rangeOfString:@"元"];
    
    NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:string];
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#E73A59"],NSFontAttributeName:[UIFont systemFontOfSize:25]} range:NSMakeRange(0, string.length)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont fontWithName:kMedium size:13] range:subRange];
    
    return  rushAttributed;
}

-(void)contentViewSetup{
    
    CGFloat  itemWidth = 85;
    
    for (int i = 0; i < self.titles.count; i++) {
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, itemHeight*i, itemWidth, itemHeight)];
        label.text = self.titles[i];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor colorWithHexString:@"#464e5f"];
        label.font = [UIFont fontWithName:kMedium size:15];
        [self.contentBackView addSubview:label];
        
        if (i > 0) {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, itemHeight*i, kWidth, 1)];
            view.backgroundColor = [UIColor colorWithHexString:@"#F3F2F6"];
            [self.contentBackView addSubview:view];
        }
        
    }
    
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.bankLabel.backgroundColor = [UIColor clearColor];
    self.bankNumberTextField.backgroundColor = [UIColor clearColor];
    self.priceTf.backgroundColor = [UIColor clearColor];
    self.passWordTf.backgroundColor = [UIColor clearColor];
    
}

-(void)confirmButtonSetup{
    
    CGFloat  noteOriginY = kTopMar * 5/2 + itemHeight * (self.titles.count + 2),height = 16;
    
    UILabel *noteLabel = [[UILabel alloc]initWithFrame:CGRectMake(kLeftMar, noteOriginY, DEFAULT_WIDTH, height)];
    noteLabel.text = @"注：提现金额需超过100元，方可提现～";
    noteLabel.textAlignment = NSTextAlignmentCenter;
    noteLabel.textColor = [UIColor colorWithHexString:@"#E73A59"];
    noteLabel.font = [UIFont fontWithName:kMedium size:13];
    [self.view addSubview:noteLabel];
    
    CGFloat  confirmOriginY = kTopMar * 2 + itemHeight * (self.titles.count + 3);
    
    UIButton * confirmButton = [[UIButton alloc]initWithFrame:CGRectMake(kLeftMar, confirmOriginY, DEFAULT_WIDTH, itemHeight)];
    
    CAGradientLayer *gradientLayer =  [CAGradientLayer layer];
    gradientLayer.frame = confirmButton.bounds;
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 0);
    gradientLayer.locations = @[@(0.5),@(1.0)];//渐变点
    [gradientLayer setColors:@[(id)[[UIColor colorWithHexString:@"#ea1d49"] CGColor],(id)[[UIColor colorWithHexString:@"#fb3024"] CGColor]]];//渐变数组
    [confirmButton.layer addSublayer:gradientLayer];
    
    [confirmButton setTitle:@"确认提现" forState:(UIControlStateNormal)];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    confirmButton.layer.cornerRadius = 8;
    [confirmButton addTarget:self action:@selector(confirmDrawAction) forControlEvents:UIControlEventTouchUpInside];
    confirmButton.titleLabel.font = [UIFont fontWithName:kBold size:17];
    confirmButton.layer.masksToBounds = YES;
    
    [self.view addSubview:confirmButton];
    
}

-(void)confirmDrawAction{
    
    if (!self.isWithdrawing) {
        
        self.isWithdrawing = YES;
        
        NSString * errorMessage = [self checkErrorMessage];
        
        if (errorMessage) {
            [FC_Manager showToastWithText:errorMessage];
            self.isWithdrawing = NO;
            return;
        }
        
        [self resignFirstRes];
        [self drawMoneyRequest];  // 提现
    }
    
    
}

-(NSString *)checkErrorMessage{
    
    NSString * errorMessage = nil;
    
    if (self.nameLabel.text.length == 0) {
        errorMessage = @"开户姓名不能为空";
    }else if (![Check isChineseWithName:self.nameLabel.text]) {
        errorMessage = @"请输入正确的开户姓名";
    }else if (self.bankLabel.text.length == 0) {
        errorMessage = @"开卡银行不能为空";
    }else if (![Check isChineseWithName:self.bankLabel.text]) {
        errorMessage = @"请输入正确的开卡银行";
    }else if (self.bankNumberTextField.text.length == 0) {
        errorMessage = @"银行卡号不能为空";
    }else if (self.bankNumberTextField.text.length < 12 || self.bankNumberTextField.text.length > 19) {
        errorMessage = @"请输入正确的银行卡号";
    }else if (self.priceTf.text.length == 0) {
        errorMessage = @"提现金额不能为空";
    }else if ([self.priceTf.text intValue] < 100) {
        errorMessage = @"提现金额不得少于100";
    }else if ([self.priceTf.text floatValue] > [[LoginUser shareLoginUser].drawMoney floatValue]) {
        errorMessage = @"提现金额不得超过账户余额";
    }else if (self.passWordTf.text.length == 0) {
        errorMessage = @"提现密码不能为空,如没设置,请到安全中心设置提现密码";
    }else if (self.passWordTf.text.length < 6 || self.passWordTf.text.length > 12){
        errorMessage = @"请输入正确的提现密码";
    }
    
    return errorMessage;
    
}

#pragma mark  --  lazy  --

-(UILabel *)priceLabel{
    
    if (!_priceLabel) {
        
        UIView * priceBackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth,moneyBackHeight)];
        priceBackView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:priceBackView];
        
        CGFloat  noteHeight = 15,noteBottomHeight = 48;
        UILabel * noteLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, moneyBackHeight - (noteHeight + noteBottomHeight), kWidth, noteHeight)];
        noteLabel.text = @"可提取余额";
        noteLabel.font = [UIFont fontWithName:kMedium size:15];
        noteLabel.textAlignment = NSTextAlignmentCenter;
        noteLabel.textColor = [UIColor colorWithHexString:@"#8C96AB"];
        [priceBackView addSubview:noteLabel];
        
        CGFloat priceBottomHeight = 12,priceHeight = 28;
        _priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, moneyBackHeight - (priceBottomHeight + priceHeight), kWidth, priceHeight)];
        _priceLabel.font = [UIFont fontWithName:kPingFangRegular size:25];
        _priceLabel.textAlignment = NSTextAlignmentCenter;
        _priceLabel.textColor = RGB(253, 37, 37, 1);
        [priceBackView addSubview:self.priceLabel];
    }
    return _priceLabel;
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"开户姓名:",@"开户银行:",@"银行卡号:",@"提现金额:",@"提现密码:"];
    }
    return _titles;
}

-(UIView *)contentBackView{
    
    if (!_contentBackView) {
        
        _contentBackView = [[UIView alloc]initWithFrame:CGRectMake(0, moneyBackHeight + kTopMar , kWidth, itemHeight * self.titles.count)];
        [self.view addSubview:_contentBackView];
        _contentBackView.backgroundColor= [UIColor whiteColor];
        [_contentBackView addProjectionWithShadowOpacity:1];
        
    }
    return _contentBackView;
}

-(UILabel *)nameLabel{
    
    CGFloat  nameleft = 100;
    
    CGRect frame = CGRectMake(nameleft, 0, kWidth - (nameleft + kLeftMar), itemHeight);
    
    if (!_nameLabel) {
        _nameLabel = [UILabel createLabelWithFrame:frame text:self.name fontSize:15 textColor:[UIColor colorWithHexString:@"#464e5f"]];
        _nameLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentBackView addSubview:_nameLabel];
        
    }
    return _nameLabel;
}

-(UILabel *)bankLabel{
    
    CGFloat  alipayLeft = 100;
    
    CGRect  frame = CGRectMake(alipayLeft, itemHeight, kWidth - (alipayLeft + kLeftMar), itemHeight);
    
    if (!_bankLabel) {
        NSString * bankName = [LoginUser shareLoginUser].bankName?[LoginUser shareLoginUser].bankName:@"";
        _bankLabel = [UILabel createLabelWithFrame:frame text:bankName fontSize:15 textColor:[UIColor colorWithHexString:@"#464e5f"]];
        _bankLabel.textAlignment = NSTextAlignmentLeft;
        _bankLabel.userInteractionEnabled = YES;
        
        UITapGestureRecognizer * bankTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bankTapGesture:)];
        [_bankLabel addGestureRecognizer:bankTapGesture];
        
        
        CGFloat  buttonWidth = 40;
        
        UIButton *nextButton = [[UIButton alloc]init];
        [nextButton setImage:[UIImage imageNamed:@"spread"] forState:UIControlStateNormal];
        nextButton.frame = CGRectMake(_bankLabel.width - buttonWidth, 0, buttonWidth, itemHeight);
        nextButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [nextButton addTarget:self action:@selector(withdrawNextButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [_bankLabel addSubview:nextButton];
        
        [self.contentBackView addSubview:_bankLabel];
    }
    return _bankLabel;
}

-(void)bankTapGesture:(UITapGestureRecognizer *)tap{
    
    [self withdrawNextButtonAction];
    
}

-(void)withdrawNextButtonAction{
    
    @WeakObj(self)
    AF_BankNameViewController * bankNameVc = [[AF_BankNameViewController alloc]init];
    bankNameVc.choosedBankBlock = ^(NSString *bankName) {
        @StrongObj(self)
        self.bankLabel.text = bankName;
    };
    [self.navigationController pushViewController:bankNameVc animated:YES];
    
}

-(UITextField *)bankNumberTextField{
    
    CGFloat  alipayLeft = 100;
    
    CGRect  frame = CGRectMake(alipayLeft, itemHeight * 2, kWidth - (alipayLeft + kLeftMar), itemHeight);
    
    if (!_bankNumberTextField) {
        _bankNumberTextField = [self createTextFieldWithFrame:frame placeholder:@"请输入银行卡号"];
        _bankNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
        if ([LoginUser shareLoginUser].bankNumber) {
            _bankNumberTextField.text = [LoginUser shareLoginUser].bankNumber;
        }
        [self.contentBackView addSubview:_bankNumberTextField];
    }
    return _bankNumberTextField;
}

-(UITextField *)priceTf{
    
    CGFloat  priceLeft = 100;
    
    CGRect  frame = CGRectMake(priceLeft, itemHeight*3, kWidth - (priceLeft + kLeftMar), itemHeight);
    
    if (!_priceTf) {
        _priceTf = [self createTextFieldWithFrame:frame placeholder:@"请输入提现金额(不低于100元)"];
        _priceTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [self.contentBackView addSubview:_priceTf];
    }
    
    return _priceTf;
}

-(UITextField *)passWordTf{
    
    CGFloat  passwordLeft = 100;
    
    CGRect  frame = CGRectMake(passwordLeft, itemHeight*4, kWidth - (passwordLeft + kLeftMar), itemHeight);
    
    if (!_passWordTf) {
        _passWordTf = [self createTextFieldWithFrame:frame placeholder:@"请输入提现密码(在个人信息设置)"];
        _passWordTf.secureTextEntry = YES;
        [self.contentBackView addSubview:_passWordTf];
    }
    
    return _passWordTf;
}

#pragma mark  --  create text field  --

-(UITextField *)createTextFieldWithFrame:(CGRect)frame placeholder:(NSString *)placeholder{
    
    UITextField * textField = [[UITextField alloc]initWithFrame:frame];
    textField.placeholder = placeholder;
    textField.textColor = [UIColor colorWithHexString:@"#464e5f"];
    textField.font = [UIFont fontWithName:kPingFangRegular size:15];
    [textField setValue:[UIColor colorWithHexString:@"#8C96AB"] forKeyPath:@"_placeholderLabel.textColor"];
    [textField setValue:[UIFont fontWithName:kPingFangRegular size:15] forKeyPath:@"_placeholderLabel.font"];
    textField.delegate = self;
    
    return textField;
}

#pragma mark  --  textField delegate  --

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (range.length == 1 && string.length == 0) {
        return YES;
    }else if (textField == self.bankNumberTextField) {
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        if (textField == self.bankNumberTextField) {
            return  textField.text.length < 19;
        }
    }else if(textField == self.priceTf){
            // 判断是否有小数点
            isHaveDot = [textField.text containsString:@"."]?YES:NO;
            
            NSString * aString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            if (string.length > 0) {
                
                //当前输入的字符
                unichar single = [string characterAtIndex:0];
                
                // 不能输入.0-9以外的字符
                if (!((single >= '0' && single <= '9') || single == '.'))
                {
                    return NO;
                }
                
                // 只能有一个小数点
                if (isHaveDot && single == '.') {
                    return NO;
                }
                
                // 如果第一位是.则前面加上0.
                if ((textField.text.length == 0) && (single == '.')) {
                    textField.text = @"0";
                }
                
                // 如果第一位是0则后面必须输入点，否则不能输入。
                if ([textField.text hasPrefix:@"0"]) {
                    if (textField.text.length > 1) {
                        NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                        if (![secondStr isEqualToString:@"."]) {
                            return NO;
                        }
                    }else{
                        if (![string isEqualToString:@"."]) {
                            return NO;
                        }
                    }
                }
                
                
                // 不能在第一位插入0和.
                if (![aString hasPrefix:@"0."]) {
                    
                    if (([[aString substringToIndex:1] isEqualToString:@"0"] && aString.length > 1) || [[aString substringToIndex:1] isEqualToString:@"."]) {
                        return NO;
                    }
                }
                
                // 小数点后最多能输入两位
                if (isHaveDot) {
                    NSRange ran = [textField.text rangeOfString:@"."];
                    // 由于range.location是NSUInteger类型的，所以这里不能通过(range.location - ran.location)>2来判断
                    if (range.location > ran.location) {
                        if ([textField.text pathExtension].length > 1) {
                            return NO;
                        }
                    }
                }
                
                // 当前显示包含小数，则整数位不能限制位小于等于10的长度,小数点后不能多于2位
                if ([aString containsString:@"."]) {
                    NSArray * strings = [aString componentsSeparatedByString:@"."];
                    NSString * firstString = [strings firstObject];
                    NSString * lastString = [strings lastObject];
                    if ([string isEqualToString:@""] && firstString.length + lastString.length > 10) {
                        return NO;
                    }else{
                        if (lastString.length == 0) {
                            return (firstString.length < 11 && lastString.length < 3);
                        }else{
                            if (lastString.length > 2) {
                                return NO;
                            }
                            return firstString.length < 11;
                        }
                    }
                }else{
                    return textField.text.length < 10;
                }
                
            }
            // 如果是删除 没有小数的时候长度限制在10以内
            if ( aString.length > 10 && ![aString containsString:@"."]) {
                return NO;
            }else{
                // 如果没有小数,第一个不能为0，当为0的时候必须是不大于1的小数，第一位不能为小数点
                if (([aString hasPrefix:@"0"] && ![aString hasPrefix:@"0."]) || [aString hasPrefix:@"."]) {
                    if (aString.length > 2) { // 当前字符不为@"0."
                        return NO;
                    }
                    
                }
            }
            
            return YES;
    }else if (textField == self.passWordTf){
        return textField.text.length < 12;
    }
    
    return YES;
    
}

#pragma mark  --  request  --

// 可提现金额

-(void)getMyCanDrawMoneyRequest{
    
    NSDictionary *myDrawMoneyDic =@{
                                  @"shopId":[LoginUser shareLoginUser].shopId
                                  };
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopDrawMoneyView
               parameters:myDrawMoneyDic
                  success:^(id responseObject) {
                      
                      @StrongObj(self)
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [[LoginUser shareLoginUser] setDrawMoney:responseObject[@"data"][@"drawMoney"]];
                          
                          self.priceLabel.attributedText = [self attributedStr];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  } failure:^(NSError *error) {

                  }];
    
}
// 提现
-(void)drawMoneyRequest{
    
    NSDictionary * drawMoneyDic = @{
                                    @"shopId":[LoginUser shareLoginUser].shopId,
                                    @"bank":self.bankLabel.text,
                                    @"sourceAccount":self.bankNumberTextField.text,
                                    @"money":self.priceTf.text,
                                    @"realName":self.nameLabel.text,
                                    @"payPassword":self.passWordTf.text
                                    };
    
    @WeakObj(self)
    
    [STTextHudTool loadingWithTitle:@"提现中..."];
    
    [PPNetworkHelper POST:ShopDrawMoney
               parameters:drawMoneyDic
                  success:^(id responseObject) {
                      
                      @StrongObj(self)
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [[LoginUser shareLoginUser] setBankName:self.bankLabel.text];
                          [[LoginUser shareLoginUser] setBankNumber:self.bankNumberTextField.text];
                          
                          [STTextHudTool showSuccessText:@"提现成功"];
                          
                          if (self.drawMoneySuccessedBlock) {
                              self.drawMoneySuccessedBlock();
                          }
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      self.isWithdrawing = NO;
                  } failure:^(NSError *error) {
                      @StrongObj(self)
                      self.isWithdrawing = NO;
                       [STTextHudTool showErrorText:@"网络链接错误,请稍后重试"];
                  }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self getMyCanDrawMoneyRequest];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self resignFirstRes];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self resignFirstRes];
    
}

-(void)resignFirstRes{
    
    [self.bankNumberTextField resignFirstResponder];
    [self.priceTf resignFirstResponder];
    [self.passWordTf resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
