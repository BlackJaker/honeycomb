//
//  AF_BankNameViewController.m
//  LotteryApp
//
//  Created by afc on 2018/12/25.
//  Copyright © 2018年 afc. All rights reserved.
//

#import "AF_BankNameViewController.h"

@interface AF_BankNameViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,copy) NSArray * bankNames;

@property (nonatomic,copy) NSArray * bankImgNames;

@property (nonatomic,strong) UITableView * bankNameTableView;

@end

@implementation AF_BankNameViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertySetup];
}

#pragma mark  --  content setup  --

-(void)propertySetup{
    
    self.titleLabe.text = @"选择银行卡";

    [self.view addSubview:self.bankNameTableView];
}


#pragma mark  -- table view setup  --

-(UITableView *)bankNameTableView{

    if (!_bankNameTableView) {
        
        _bankNameTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kTopMar, kWidth, kHeight - (TAB_BAR_SAFE_HEIGHT + NAVIGATION_HEIGHT + kTopMar))];
        _bankNameTableView.backgroundColor = [UIColor clearColor];
        _bankNameTableView.delegate = self;
        _bankNameTableView.dataSource=self;
        _bankNameTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
    }
    return _bankNameTableView;
}

#pragma mark  -- table viwe  datasource  --

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bankNames.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static  NSString * bankNameCell = @"bankNameCell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:bankNameCell];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:bankNameCell];
    }
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"xzyhk_%@",[self.bankImgNames objectAtIndex:indexPath.row]]];
    cell.textLabel.text = [self.bankNames objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

#pragma mark  -- table view delegate  --

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (self.choosedBankBlock) {
        self.choosedBankBlock(cell.textLabel.text);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark  --  bank names  --

-(NSArray *)bankNames{
    
    if (!_bankNames) {
        _bankNames = @[@"中国工商银行",@"中国农业银行",@"中国银行",@"中国建设银行",@"交通银行",@"中信银行",@"中国光大银行",@"华夏银行",@"中国民生银行",@"广东发展银行",@"平安银行",@"招商银行",@"兴业银行",@"上海浦东发展银行",@"北京银行",@"中国邮政储蓄银行"];
    }
    return _bankNames;
}

-(NSArray *)bankImgNames{
    
    if (!_bankImgNames) {
        _bankImgNames = @[@"gsyh",@"nyyh",@"zgyh",@"jsyh",@"jtyh",@"zxyh",@"zggdyh",@"hxyh",@"zgmsyh",@"gdfzyh",@"payh",@"zsyh",@"xyyh",@"shpdyh",@"bjyh",@"zgyzcxyh"];
    }
    return _bankImgNames;
}

@end
