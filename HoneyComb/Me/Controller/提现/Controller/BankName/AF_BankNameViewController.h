//
//  AF_BankNameViewController.h
//  LotteryApp
//
//  Created by afc on 2018/12/25.
//  Copyright © 2018年 afc. All rights reserved.
//

#import "AllViewController.h"

typedef void(^ChoosedBankBlock)(NSString * bankName);

NS_ASSUME_NONNULL_BEGIN

@interface AF_BankNameViewController : AllViewController

@property (nonatomic,copy) ChoosedBankBlock  choosedBankBlock;

@end

NS_ASSUME_NONNULL_END
