//
//  FC_BankCardViewController.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

typedef  void(^DrawMoneySuccessedBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface FC_BankCardViewController : AllViewController

@property (nonatomic,copy) DrawMoneySuccessedBlock  drawMoneySuccessedBlock;

-(instancetype)initWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
