//
//  FC_WithdrawViewController.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FC_WithdrawViewController : AllViewController

@property (nonatomic,strong) NSDictionary * safeInfoDic;

@property (nonatomic,strong) AllViewController * rootVc;

@end

NS_ASSUME_NONNULL_END
