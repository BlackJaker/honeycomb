//
//  FC_WithdrawSuccessedVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_WithdrawSuccessedVC.h"

#import "FC_WithdrawHeadView.h"

#import "FC_WithdrawSucContentView.h"

#import "FC_UserAccountModel.h"

@interface FC_WithdrawSuccessedVC (){
    CGFloat  headHeight;
    CGFloat  contentHeight;
}
@property (nonatomic,strong) FC_WithdrawHeadView * headView;

@property (nonatomic,strong) FC_WithdrawSucContentView * contentView;

@end

@implementation FC_WithdrawSuccessedVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertyInit];
    
    [self contentSetup];
    
    [self withdrawDetailRequest];
    
}

#pragma mark  --  property init  --

-(void)propertyInit{
    
    self.titleLabe.text = @"提现";
    headHeight = 95;
    contentHeight = 270;
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f7f7f7"];
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self.view addSubview:self.headView];
}

#pragma mark  --  lazy  --

-(FC_WithdrawHeadView *)headView{
    
    if (!_headView) {
        _headView = [[FC_WithdrawHeadView alloc]initWithFrame:CGRectMake(kLeftMar, kTopMar , DEFAULT_WIDTH, headHeight) state:[self.currentModel.status integerValue]];
        
        [self.view addSubview:self.headView];
    }
    return _headView;
}

-(FC_WithdrawSucContentView *)contentView{
    
    if (!_contentView) {
        _contentView = [[FC_WithdrawSucContentView alloc]initWithFrame:CGRectMake(kLeftMar, kTopMar*2 + headHeight, DEFAULT_WIDTH, contentHeight)];
        [self.view addSubview:self.contentView];
    }
    return _contentView;
}

#pragma mark  --  request  --

-(void)withdrawDetailRequest{
    
    NSDictionary *drawDetailDic =@{
                                    @"moneyId":self.currentModel.moneyId
                                    };
    @WeakObj(self)
    
    [PPNetworkHelper POST:GetDrawDetail
               parameters:drawDetailDic
                  success:^(id responseObject) {
                      
                      @StrongObj(self)
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          NSDictionary * dic = responseObject[@"data"];
                          [self.contentView uploadWithInfo:dic];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  } failure:^(NSError *error) {
                      
                  }];
    
}

@end
