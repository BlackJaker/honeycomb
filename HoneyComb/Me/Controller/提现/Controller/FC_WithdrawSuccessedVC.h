//
//  FC_WithdrawSuccessedVC.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

@class FC_UserAccountModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_WithdrawSuccessedVC : AllViewController

@property (nonatomic,strong) FC_UserAccountModel * currentModel;

@end

NS_ASSUME_NONNULL_END
