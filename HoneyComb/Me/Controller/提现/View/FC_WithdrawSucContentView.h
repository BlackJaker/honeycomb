//
//  FC_WithdrawSucContentView.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_WithdrawSucContentView : UIView

@property (nonatomic,strong) UILabel * infoLabel;
@property (nonatomic,strong) UILabel * nameLabel;
@property (nonatomic,strong) UILabel * priceLabel;
@property (nonatomic,strong) UILabel * bankNumberLabel;
@property (nonatomic,strong) UILabel * bankNameLabel;
@property (nonatomic,strong) UILabel * withdrawDateLabel;

-(void)uploadWithInfo:(NSDictionary *)info;

@end

NS_ASSUME_NONNULL_END
