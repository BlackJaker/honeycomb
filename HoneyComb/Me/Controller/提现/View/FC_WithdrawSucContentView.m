//
//  FC_WithdrawSucContentView.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_WithdrawSucContentView.h"

#import "FC_UserAccountModel.h"

@interface FC_WithdrawSucContentView()
{
    CGFloat  itemHeight;
}
@end

@implementation FC_WithdrawSucContentView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 5;
        [self addProjectionWithShadowOpacity:1];
        
        itemHeight = self.height/6;
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    self.infoLabel.font = [UIFont fontWithName:kBold size:15];
    
    for (int i = 0; i < 5; i ++) {
        UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, itemHeight * (i + 1), self.width, 1)];
        line.backgroundColor = RGB(235, 235, 235, 1.0f);
        [self addSubview:line];
    }
    
}

#pragma mark  --  lazy  --

-(UILabel *)infoLabel{
    
    if (!_infoLabel) {
        _infoLabel = [self createLabelWithFontSize:15 text:@"提现信息"];
        _infoLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.infoLabel];
        
        [self.infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    return _infoLabel;
}

-(UILabel *)nameLabel{
    
    if (!_nameLabel) {
        _nameLabel = [self createLabelWithFontSize:15 text:@"开户姓名:  "];
        [self addSubview:_nameLabel];
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(itemHeight);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    return _nameLabel;
}

-(UILabel *)bankNameLabel{
    
    if (!_bankNameLabel) {
        _bankNameLabel = [self createLabelWithFontSize:15 text:@"开户银行:  "];
        [self addSubview:_bankNameLabel];
        
        [self.bankNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(itemHeight*2);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    return _bankNameLabel;
}

-(UILabel *)bankNumberLabel{
    
    if (!_bankNumberLabel) {
        _bankNumberLabel = [self createLabelWithFontSize:15 text:@"银行卡号:  "];
        [self addSubview:_bankNumberLabel];
        
        [self.bankNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(itemHeight*3);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    return _bankNumberLabel;
}

-(UILabel *)priceLabel{
    
    if (!_priceLabel) {
        _priceLabel = [self createLabelWithFontSize:15 text:@"提现金额:"];
        [self addSubview:_priceLabel];
        
        [_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(itemHeight * 4);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    return _priceLabel;
}

-(UILabel *)withdrawDateLabel{
    
    if (!_withdrawDateLabel) {
        _withdrawDateLabel = [self createLabelWithFontSize:15 text:@"提现时间:"];
        [self addSubview:_withdrawDateLabel];
        
        [_withdrawDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(itemHeight * 5);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(itemHeight);
        }];
    }
    return _withdrawDateLabel;
}

-(UILabel *)createLabelWithFontSize:(CGFloat)fontSize text:(NSString *)text{
    
    UILabel * label = [[UILabel alloc]init];
    label.font = [UIFont fontWithName:kMedium size:fontSize];
    label.textColor = [UIColor colorWithHexString:@"#464e5f"];
    label.text = text;
    
    return label;
}

#pragma mark  --  upload  --

-(void)uploadWithInfo:(NSDictionary *)info{
    
    self.nameLabel.text = [NSString stringWithFormat:@"开户姓名:  %@",info[@"userName"]];
    self.bankNameLabel.text = [NSString stringWithFormat:@"开户银行:  %@",info[@"bankName"]];
    self.bankNumberLabel.text = [NSString stringWithFormat:@"银行卡号:  %@",info[@"number"]];
    
    [self reloadAttrStrWithMoney:info[@"money"]];
    self.withdrawDateLabel.text = [NSString stringWithFormat:@"提现时间:  %@",info[@"createDate"]];
    
}

-(void)reloadAttrStrWithMoney:(NSString *)money{
    
    NSString * string = [NSString stringWithFormat:@"提现金额:  ￥%@",money];
    
    NSString * subString = [NSString stringWithFormat:@"￥%@",money];
    
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc]initWithString:string attributes:@{NSFontAttributeName:[UIFont fontWithName:kMedium size:15],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#464e5f"]}];
    
    [attrString setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#f84a4a"],NSFontAttributeName:[UIFont fontWithName:kBold size:15]} range:[string rangeOfString:subString]];
    
    self.priceLabel.attributedText = attrString;
    
}


@end
