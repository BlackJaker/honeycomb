//
//  FC_WithdrawHeadView.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_WithdrawHeadView : UIImageView

-(instancetype)initWithFrame:(CGRect)frame state:(NSInteger)state;

@property (nonatomic,strong) UIImageView * imgView;
@property (nonatomic,strong) UILabel * noteLabel;



@end

NS_ASSUME_NONNULL_END
