//
//  FC_WithdrawCell.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_UserAccountModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_WithdrawCell : UITableViewCell

@property (nonatomic,strong) UIButton * stateButton;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UIImageView *backView;

-(void)reloadAccountCellWith:(FC_UserAccountModel *)model;

@end

NS_ASSUME_NONNULL_END
