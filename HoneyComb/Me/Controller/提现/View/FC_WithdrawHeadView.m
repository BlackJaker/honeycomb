//
//  FC_WithdrawHeadView.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_WithdrawHeadView.h"

@interface FC_WithdrawHeadView()

@property (nonatomic,assign) NSInteger state;

@end

@implementation FC_WithdrawHeadView

-(instancetype)initWithFrame:(CGRect)frame state:(NSInteger)state{
    
    if (self = [super initWithFrame:frame]) {
        
        self.state = state;
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    self.image = [UIImage imageNamed:@"withdraw_success_bg"];
    self.userInteractionEnabled = YES;
    
    CGFloat top = 18,width = 50,height = 30;
    
    [self addSubview:self.imgView];
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
    }];
    
    
    CGFloat  originY = 11,noteHeight = 18;
    
    [self addSubview:self.noteLabel];
    
    [self.noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.imgView.mas_bottom).offset(originY);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(noteHeight);
    }];
    
}

-(UIImageView *)imgView{
    
    if (!_imgView) {
        _imgView = [[UIImageView alloc]init];
        NSString * imgName = self.state == 1?@"audit":self.state == 2?@"withdraw_success":@"reject";
        _imgView.image = [UIImage imageNamed:imgName];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgView;
}

-(UILabel *)noteLabel{
    
    if (!_noteLabel) {
        NSString * note = self.state == 1?@"审核中":self.state == 2?@"汇款成功!":@"被驳回";
        _noteLabel = [self createLabelWithFontSize:18 text:note];
        _noteLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _noteLabel;
}


-(UILabel *)createLabelWithFontSize:(CGFloat)fontSize text:(NSString *)text{
    
    UILabel * label = [[UILabel alloc]init];
    label.font = [UIFont fontWithName:kBold size:fontSize];
    label.textColor = [UIColor whiteColor];
    label.text = text;
    
    return label;
}


@end
