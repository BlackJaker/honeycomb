//
//  FC_WithdrawCell.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_WithdrawCell.h"

#import "FC_UserAccountModel.h"

@implementation FC_WithdrawCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self layoutView];
        
    }
    return self;
}
-(void)layoutView
{
    
    CGFloat  cellHeight = 90,bottomMar = 10;
    
    self.backView = [[UIImageView alloc]initWithFrame:CGRectMake(kLeftMar, 0, DEFAULT_WIDTH, cellHeight - bottomMar)];
    _backView.backgroundColor = [UIColor whiteColor];
    self.backView.layer.cornerRadius = 8;
    [self.backView addProjectionWithShadowOpacity:1];
    [self.contentView addSubview:_backView];
    
    CGFloat  top = 20,height = 12;
    
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.text = @"提现日期: ";
    self.dateLabel.textColor = [UIColor colorWithHexString:@"#a2a9b8"];
    self.dateLabel.font = [UIFont fontWithName:kMedium size:height];
    [self.backView  addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(DEFAULT_WIDTH);
        make.height.mas_equalTo(height);
        make.left.mas_equalTo(kLeftMar);
        make.top.mas_equalTo(top);
    }];
    
    CGFloat  originY = 10,width = 160,priceHeight = 15;
    
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.text = @"金额: 0.00元";
    self.priceLabel.textColor = RGB(237, 37, 37, 1);
    self.priceLabel.font = [UIFont fontWithName:kMedium size:16];
    [self.backView  addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(priceHeight);
        make.left.mas_equalTo(kLeftMar);
        make.top.mas_equalTo(self.dateLabel.mas_bottom).offset(originY);
    }];
    
    CGFloat  right = 17,stateWidth = 75;
    
    self.stateButton = [[UIButton alloc]init];
    [self.stateButton setImage:[UIImage imageNamed:@"withdraw_next_step"] forState:UIControlStateNormal];
    [self.stateButton setTitleColor:[UIColor colorWithHexString:@"#f84a4a"] forState:UIControlStateNormal];
    self.stateButton.titleLabel.font = [UIFont fontWithName:kMedium size:12];
    [self.backView addSubview:self.stateButton];
    [self.stateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(stateWidth);
        make.height.mas_equalTo(17);
        make.right.mas_equalTo(-right);
        make.top.mas_equalTo(self.dateLabel.mas_bottom).offset(originY - 1);
    }];
    
    
}

-(void)reloadAccountCellWith:(FC_UserAccountModel *)model{
    
    self.dateLabel.text = [NSString stringWithFormat:@"提现日期: %@",model.createDate];
    
    NSInteger status = [model.status integerValue];
    
    NSString * title = status == 1?@"审核中":status == 2?@"汇款成功":@"驳回";
    
    [self.stateButton setTitle:title forState:UIControlStateNormal];
    
    [self.stateButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.stateButton.imageView.frame.size.width, 0, self.stateButton.imageView.frame.size.width + 8)];
    [self.stateButton setImageEdgeInsets:UIEdgeInsetsMake(0, self.stateButton.titleLabel.frame.size.width + 8, 0,-self.stateButton.titleLabel.frame.size.width)];
    self.stateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    [self reloadAttribuStringWithModel:model];
    
}

-(void)reloadAttribuStringWithModel:(FC_UserAccountModel *)model{
    
    NSString * string = [NSString stringWithFormat:@"金额:  %@  元",model.money];
    
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc]initWithString:string attributes:@{NSFontAttributeName:[UIFont fontWithName:kMedium size:15],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#464e5f"]}];
    
    [attrString setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#f84a4a"],NSFontAttributeName:[UIFont fontWithName:kBold size:15]} range:[string rangeOfString:model.money]];
    
    self.priceLabel.attributedText = attrString;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
