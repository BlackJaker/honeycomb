//
//  FC_TopUpCardView.h
//  HoneyComb
//
//  Created by afc on 2018/11/22.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BindBankBlock)(NSString * backNumber);

NS_ASSUME_NONNULL_BEGIN

@interface FC_TopUpCardView : UIImageView

@property (nonatomic,strong) UIButton * bindButton;
@property (nonatomic,strong) UITextField * numberTextField;

@property (nonatomic,assign) NSInteger  state;

@property (nonatomic,copy)  BindBankBlock bindBankBlock;

@end

NS_ASSUME_NONNULL_END
