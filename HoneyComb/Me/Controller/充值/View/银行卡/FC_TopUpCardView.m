//
//  FC_TopUpCardView.m
//  HoneyComb
//
//  Created by afc on 2018/11/22.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_TopUpCardView.h"

@interface FC_TopUpCardView()<UITextFieldDelegate>

@end

@implementation FC_TopUpCardView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self contentSetup];
    }
    return self;
}

#pragma mark  --  contentSetup  --

-(void)contentSetup{
    
    self.image = [UIImage imageNamed:@"银行卡背景"];
    self.userInteractionEnabled = YES;
    
    [self.bindButton setTitle:self.state == 0?@"确定绑定":@"重新绑定" forState:UIControlStateNormal];
}

#pragma mark  --  lazy  --

-(UIButton *)bindButton{
    
    CGFloat  width = 83,height = 32,rightMar = 22,bottomMar = 20;
    
    if (!_bindButton) {
        
        UIView * backIv = [[UIView alloc]init];
        backIv.layer.cornerRadius = 5;
        backIv.clipsToBounds = YES;
        backIv.backgroundColor = [UIColor colorWithHexString:@"#ffb54e"];
        backIv.alpha = 0.5;
        [self addSubview:backIv];
        
        [backIv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-rightMar);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.bottom.mas_equalTo(-bottomMar);
        }];
        
        _bindButton =  [[UIButton alloc]init];
        _bindButton.titleLabel.font = [UIFont fontWithName:kMedium size:14];
        [_bindButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [_bindButton addTarget:self action:@selector(bindBankCardAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_bindButton];
        
        [_bindButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-rightMar);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.bottom.mas_equalTo(-bottomMar);
        }];
    }
    return _bindButton;
}

-(void)bindBankCardAction:(UIButton *)sender{
    
    if ([sender.titleLabel.text isEqualToString:@"重新绑定"]) {
        [_numberTextField setEnabled:YES];
        self.numberTextField.text = @"";
        [self.numberTextField becomeFirstResponder];
        [sender setTitle:@"确定绑定" forState:UIControlStateNormal];
    }else{

        if (self.numberTextField.text.length  == 0) {
            [FC_Manager showToastWithText:@"银行卡不能为空"];
        }else{
            if (self.bindBankBlock) {
                self.bindBankBlock([self bankNumToNormalNum]);
            }
        }
    }
    
}

-(UITextField *)numberTextField{
    
    CGFloat  numberHeight = 30;
    
    if (!_numberTextField) {
        
        _numberTextField = [[UITextField alloc]init];
        _numberTextField.textColor = [UIColor whiteColor];
        _numberTextField.delegate = self;
        [_numberTextField setEnabled:NO];
        [_numberTextField becomeFirstResponder];
        _numberTextField.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_numberTextField];
        
        [_numberTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.height.mas_equalTo(numberHeight);
            make.centerY.mas_equalTo(0);
        }];
    }
    return _numberTextField;
}

#pragma mark  --  textField delegate  --

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.numberTextField) {
        NSString *text = [self.numberTextField text];
        
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *newString = @"";
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        
        // 限制长度
        if (newString.length > 23) {
            return NO;
        }
        
        [self.numberTextField setText:newString];
        
        return NO;
        
    }
    return YES;
}

// 银行卡号转正常号,去除4位间的空格

-(NSString *)bankNumToNormalNum
{
    return [self.numberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

@end
