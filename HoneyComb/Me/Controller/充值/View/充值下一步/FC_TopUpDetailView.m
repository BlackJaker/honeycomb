//
//  FC_TopUpDetailView.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_TopUpDetailView.h"

#define lMar    18
#define tMar    38
#define cMar    25
#define bWidth  30

@implementation FC_TopUpDetailView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 5;
    
    self.nameLabel.text = @"户名:  安蜂巢网络科技(海南)有限公司";
    self.numberLabel.text = @"卡号:  15000095431189";
    self.bankLabel.text = @"开户银行:  平安银行";
    
    [self.nameButton setTitle:@"复制" forState:UIControlStateNormal];
    [self.numberButton setTitle:@"复制" forState:UIControlStateNormal];
}

#pragma mark  --  lazy  --

-(UILabel *)nameLabel{
    
    CGFloat  nameHeight = 14;
    
    if (!_nameLabel) {
        
        _nameLabel = [[UILabel alloc]init];
        [self reloadLabel:_nameLabel];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-(lMar + bWidth));
            make.left.mas_equalTo(lMar);
            make.height.mas_equalTo(nameHeight);
            make.top.mas_equalTo(tMar);
        }];
        
    }
    return _nameLabel;
}

-(UILabel *)numberLabel{
    
    CGFloat  numberHeight = 14;
    
    if (!_numberLabel) {
        
        _numberLabel = [[UILabel alloc]init];
        [self reloadLabel:_numberLabel];
        [self addSubview:_numberLabel];
        
        [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-(lMar + bWidth));
            make.left.mas_equalTo(lMar);
            make.height.mas_equalTo(numberHeight);
            make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(cMar);
        }];
        
    }
    return _numberLabel;
}

-(UILabel *)bankLabel{
    
    CGFloat  bankHeight = 14;
    
    if (!_bankLabel) {
        
        _bankLabel = [[UILabel alloc]init];
        [self reloadLabel:_bankLabel];
        [self addSubview:_bankLabel];
        
        [_bankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-(lMar + bWidth));
            make.left.mas_equalTo(lMar);
            make.height.mas_equalTo(bankHeight);
            make.top.mas_equalTo(self.numberLabel.mas_bottom).offset(cMar);
        }];
        
    }
    return _bankLabel;
}

-(void)reloadLabel:(UILabel *)label{
    
    label.textColor = [UIColor colorWithHexString:@"#565d6e"];
    label.font = [UIFont fontWithName:kBold size:14];
    
}

-(UIButton *)nameButton{
    
    CGFloat  height = 16;
    
    if (!_nameButton) {
        _nameButton = [[UIButton alloc]init];
        _nameButton.tag = 1;
        [self reloadButton:_nameButton];
        [_nameButton addTarget:self action:@selector(copyAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_nameButton];
        
        [_nameButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-lMar);
            make.width.mas_equalTo(bWidth);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(tMar);
        }];
    }
    return _nameButton;
}

-(UIButton *)numberButton{
    
    CGFloat  height = 16;
    
    if (!_numberButton) {
        _numberButton = [[UIButton alloc]init];
        _numberButton.tag = 2;
        [self reloadButton:_numberButton];
        [_numberButton addTarget:self action:@selector(copyAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_numberButton];
        
        [_numberButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-lMar);
            make.width.mas_equalTo(bWidth);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(self.nameButton.mas_bottom).offset(cMar);
        }];
    }
    return _numberButton;
}

-(void)reloadButton:(UIButton *)button{
    
    [button setTitleColor:[UIColor colorWithHexString:@"#e63351"] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:kMedium size:15]];
    
}

-(void)copyAction:(UIButton *)sender{
    
    //UIPasteboard：该类支持写入和读取数据，类似剪贴板
    UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard];
    
    pasteBoard.string = sender.tag == 1?@"安蜂巢网络科技(海南)有限公司":@"15000095431189";
    
    [FC_Manager showToastWithText:sender.tag == 1?@"复制户名成功":@"复制卡号成功"];
    
}

@end
