//
//  FC_TopUpDetailView.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_TopUpDetailView : UIView

@property (nonatomic,strong) UIButton * nameButton;
@property (nonatomic,strong) UILabel * nameLabel;

@property (nonatomic,strong) UIButton * numberButton;
@property (nonatomic,strong) UILabel * numberLabel;

@property (nonatomic,strong) UILabel * bankLabel;

@end

NS_ASSUME_NONNULL_END
