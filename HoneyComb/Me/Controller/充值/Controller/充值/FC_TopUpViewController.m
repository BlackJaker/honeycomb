//
//  FC_TopUpViewController.m
//  HoneyComb
//
//  Created by afc on 2018/11/22.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_TopUpViewController.h"

#import "FC_TopUpCardView.h"

#import "FC_TopUpNextViewController.h"

@interface FC_TopUpViewController ()

@property (nonatomic,strong) FC_TopUpCardView * cardView;

@property (nonatomic ,strong) UILabel  * priceLabel;

@property (nonatomic ,strong) UILabel  * balanceLabel;

@property (nonatomic ,strong) UIView  * noteView;

@property (nonatomic ,strong) UIButton  * nextSetupButton;

@property (nonatomic ,copy) NSString * bankNumberStr;

@property (nonatomic ,assign) BOOL isBinding;

@end

@implementation FC_TopUpViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertySetup];
    
    [self contentSetup];
    
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    self.titleLabe.text = @"充值";
    
    _bankNumberStr = nil;
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f0eff4"];
    
    self.isBinding = NO;
}

#pragma mark  --  content Setup  --

-(void)contentSetup{
    
    [self bindTitleSetup];
    
    self.cardView.state = 0;
    
    [self isBindStateRequest];
    
    [self noteViewSetup];
    
   [self.nextSetupButton setTitle:@"下一步" forState:UIControlStateNormal];
}

-(void)bindTitleSetup{
    
    CGFloat  leftMargin = 20, topMargin = 25,height =  17;
    
    UILabel * bindTitleLabel = [[UILabel alloc]init];
    bindTitleLabel.text = @"绑定转账银行卡";
    bindTitleLabel.font = [UIFont fontWithName:kBold size:17];
    bindTitleLabel.textColor = [UIColor colorWithHexString:@"#464e5f"];
    [self.view addSubview:bindTitleLabel];
    
    [bindTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topMargin);
        make.left.mas_equalTo(leftMargin);
        make.right.mas_equalTo(-leftMargin);
        make.height.mas_equalTo(height);
    }];
    
}

-(void)noteViewSetup{
    
    CGFloat  leftMar = 20,height = 124,topMar = 26;
    
    _noteView = [[UIView alloc]init];
    [self.view addSubview:_noteView];
    
    [_noteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.cardView.mas_bottom).offset(topMar);
        make.left.mas_equalTo(leftMar);
        make.right.mas_equalTo(-leftMar);
        make.height.mas_equalTo(height);
    }];
    
    // 温馨提示
    
    [self warmPromptSetup];
    
    [self noteContentSetup];
    
}

// 转账说明

-(void)warmPromptSetup{
    
    CGFloat height = 15;
    
    UILabel * warmPromptLabel = [[UILabel alloc]init];
    warmPromptLabel.text = @"转账说明";
    warmPromptLabel.font = [UIFont fontWithName:kBold size:16];
    warmPromptLabel.textColor = RGB(51, 51, 51, 1.0f);
    [_noteView addSubview:warmPromptLabel];
    
    [warmPromptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(height);
    }];
    
}

-(void)noteContentSetup{
    
    CGFloat top = 24,leftMar = 12,height = 100,lineSpace = 4;
    
    NSString * content = @"1.使用非绑定银行卡进行转账,会导致系统无法识别转账店铺.造成加款失败,请知悉.\n2,如遇充值未到账,请及时联系客服处理:\n010-53777666";
    
    UILabel * noteContentLabel = [[UILabel alloc]init];
    noteContentLabel.numberOfLines = 0;
    noteContentLabel.text = content;
    noteContentLabel.font = [UIFont fontWithName:kMedium size:14];
    noteContentLabel.textColor = [UIColor colorWithHexString:@"#7f8491"];
    [_noteView addSubview:noteContentLabel];
    
    [noteContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.left.mas_equalTo(leftMar);
        make.right.mas_equalTo(-leftMar);
        make.height.mas_equalTo(height);
    }];
    
    [UILabel changeLineSpaceForLabel:noteContentLabel WithSpace:lineSpace];
}


#pragma mark  --  lazy  --

-(FC_TopUpCardView *)cardView{
    
    CGFloat  topMargin = 60,height = 200;
    
    if (!_cardView) {
        _cardView = [[FC_TopUpCardView alloc]init];
        [self.view addSubview:_cardView];
        
        @WeakObj(self)
        
        _cardView.bindBankBlock = ^(NSString *backNumber) {
            
            @StrongObj(self)
            
            [self bindBankRequest];
            
        };
        
        [_cardView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(topMargin);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(height);
        }];
    }
    return _cardView;
}

-(UIButton *)nextSetupButton{
    
    CGFloat  bottomMargin = 94 + TAB_BAR_SAFE_HEIGHT,width = 156,height = 47;
    
    if (!_nextSetupButton) {
        _nextSetupButton =  [[UIButton alloc]init];
        
        [_nextSetupButton addTarget:self action:@selector(topUpNextSetupAction) forControlEvents:UIControlEventTouchUpInside];
        _nextSetupButton.layer.cornerRadius = 5;
        _nextSetupButton.clipsToBounds = YES;

        CAGradientLayer *gradientLayer =  [CAGradientLayer layer];
        gradientLayer.frame = CGRectMake(0, 0, width, height);
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(1, 0);
        gradientLayer.locations = @[@(0.5),@(1.0)];//渐变点
        [gradientLayer setColors:@[(id)[[UIColor colorWithHexString:@"#ea1d49"] CGColor],(id)[[UIColor colorWithHexString:@"#f3381f"] CGColor]]];//渐变数组
        [_nextSetupButton.layer addSublayer:gradientLayer];
        
        [self.view addSubview:_nextSetupButton];
        
        _nextSetupButton.titleLabel.font = [UIFont fontWithName:kBold size:17];
        [_nextSetupButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        
        [_nextSetupButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(-bottomMargin);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.centerX.mas_equalTo(0);
        }];
    }
    return _nextSetupButton;
}

-(void)topUpNextSetupAction{
    
    if (!self.bankNumberStr) {
        [FC_Manager showToastWithText:@"请先绑定银行卡"];
        return;
    }
    
    if (self.cardView.numberTextField.text.length == 0) {
        [self.cardView.numberTextField resignFirstResponder];
        self.cardView.numberTextField.text = self.bankNumberStr;
        [self.cardView.bindButton setTitle:@"重新绑定" forState:UIControlStateNormal];
    }
    
    self.hidesBottomBarWhenPushed = YES;

    FC_TopUpNextViewController * nextVc = [[FC_TopUpNextViewController alloc]init];
    [self.navigationController pushViewController:nextVc animated:YES];
    
}

#pragma mark  --  touch began  --

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if (self.bankNumberStr) {
        [self.cardView.numberTextField setEnabled:NO];
        self.cardView.numberTextField.text = self.bankNumberStr;
        [self.cardView.bindButton setTitle:@"重新绑定" forState:UIControlStateNormal];
    }
    [self.cardView.numberTextField resignFirstResponder];
    
}

#pragma mark  --  request  --

#pragma mark  --  get bank  --

-(void)isBindStateRequest{
    
    NSDictionary * getBankDic = @{
                                  @"shopId":[LoginUser shareLoginUser].shopId
                                  };
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:GetBank
               parameters:getBankDic
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          if ([responseObject[@"data"][@"number"] isEqualToString:@""] || !responseObject[@"data"][@"number"]) {
                              [self.cardView.numberTextField setEnabled:YES];
                              
                          }else{
                              self.bankNumberStr = responseObject[@"data"][@"number"];
                              self.bankNumberStr = [self normalNumToBankNum];
                              self.cardView.numberTextField.text = self.bankNumberStr;
                              [self.cardView.bindButton setTitle:@"重新绑定" forState:UIControlStateNormal];
                          }
                      }
                      
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)bindBankRequest{
    
    if (!self.isBinding) {
        
        self.isBinding = YES;
        
        
        if ([[self bankNumToNormalNum] length] < 12 || [[self bankNumToNormalNum] length] > 19) {
            [STTextHudTool showText:@"请输入合法的银行卡号"];
            self.isBinding = NO;
            return;
        }
        
        NSDictionary * bindBankDic = @{
                                       @"shopId":[LoginUser shareLoginUser].shopId,
                                       @"number":[self bankNumToNormalNum]
                                       };

        @WeakObj(self)
        
        [PPNetworkHelper POST:ShopBank
                   parameters:bindBankDic
                      success:^(id responseObject) {

                          @StrongObj(self)
                          
                          if ([responseObject[@"state"] isEqualToString:@"success"]) {
                              
                              [FC_Manager showToastWithText:@"银行卡绑定成功"];
                              
                              self.bankNumberStr = self.cardView.numberTextField.text;
                              [self.cardView.numberTextField resignFirstResponder];
                              [self.cardView.numberTextField setEnabled:NO];
                              [self.cardView.bindButton setTitle:@"重新绑定" forState:UIControlStateNormal];
                              
                          }else{
                              [STTextHudTool showErrorText:responseObject[@"msg"]];
                          }
                          
                          self.isBinding = NO;
                          
                      }failure:^(NSError *error) {
                          @StrongObj(self)
                          self.isBinding = NO;
                          [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                      }];
        
        
    }
   
}
// 银行卡号转普通字符串
-(NSString *)bankNumToNormalNum
{
    return [self.cardView.numberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}
// 普通字符串转银行卡
-(NSString *)normalNumToBankNum{
    
    NSString * bankNumber = @"";
    int count = 0;
    for (int i = 0; i < self.bankNumberStr.length; i++) {
        
        count++;
        bankNumber = [bankNumber stringByAppendingString:[self.bankNumberStr substringWithRange:NSMakeRange(i, 1)]];
        if (count == 4) {
            bankNumber = [NSString stringWithFormat:@"%@ ", bankNumber];
            count = 0;
        }
    }

    return bankNumber;
    
}

@end
