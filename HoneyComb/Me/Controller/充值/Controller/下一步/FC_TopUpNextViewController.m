//
//  FC_TopUpNextViewController.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_TopUpNextViewController.h"

#import "FC_TopUpDetailView.h"

@interface FC_TopUpNextViewController ()

@property (nonatomic,strong) FC_TopUpDetailView * topUpDetailView;

@end

@implementation FC_TopUpNextViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertySetup];
    
    [self contentSetup];
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    self.titleLabe.text = @"充值";
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f0eff4"];
    
    
}

#pragma mark  --  content Setup  --

-(void)contentSetup{
    
    [self noteSetup];
    
    [self.topUpDetailView addProjectionWithShadowOpacity:1];
    
    [self noteContentSetup];
}

-(void)noteSetup{
    
    CGFloat  leftMargin = 0, topMargin = 28,height =  16;
    
    UILabel * noteLabel = [[UILabel alloc]init];
    noteLabel.text = @"请您复制下方信息进行银行转账";
    noteLabel.font = [UIFont fontWithName:kMedium size:16];
    noteLabel.textColor = [UIColor colorWithHexString:@"#464e5f"];
    noteLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:noteLabel];
    
    [noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topMargin);
        make.left.mas_equalTo(leftMargin);
        make.right.mas_equalTo(-leftMargin);
        make.height.mas_equalTo(height);
    }];
    
}

-(FC_TopUpDetailView *)topUpDetailView{
    
    CGFloat  topMargin = 64,height = 178;
    
    if (!_topUpDetailView) {
        
        _topUpDetailView = [[FC_TopUpDetailView alloc]init];
        [self.view addSubview:_topUpDetailView];
        
        [_topUpDetailView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(topMargin);
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.height.mas_equalTo(height);
        }];
        
    }
    return _topUpDetailView;
}

-(void)noteContentSetup{
    
    CGFloat top = 32,height = 50,lineSpace = 4;
    
    NSString * content = @"转账到账时间:  30分钟内\n转账充值服务: 9:30~18:00";
    
    UILabel * noteContentLabel = [[UILabel alloc]init];
    noteContentLabel.numberOfLines = 0;
    noteContentLabel.text = content;
    noteContentLabel.font = [UIFont fontWithName:kMedium size:15];
    noteContentLabel.textColor = [UIColor colorWithHexString:@"#7f8491"];
    [self.view addSubview:noteContentLabel];
    
    [noteContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.topUpDetailView.mas_bottom).offset(top);
        make.left.mas_equalTo(kLeftMar);
        make.right.mas_equalTo(-kLeftMar);
        make.height.mas_equalTo(height);
    }];
    
    [UILabel changeLineSpaceForLabel:noteContentLabel WithSpace:lineSpace];
}


@end
