//
//  FC_TrustAccountDetailVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_TrustAccountDetailVC.h"

#import "FC_AccountDetailCell.h"
#import "FC_UserAccountModel.h"

@interface FC_TrustAccountDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *trustAccountTableView;

@property (nonatomic,copy) NSMutableArray *trustAccountData;

@end

@implementation FC_TrustAccountDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self trustAccountRequest];
}

-(UITableView *)trustAccountTableView{
    
    CGFloat  topMar = 0,titleHieght = 36,height = kHeight - NAVIGATION_HEIGHT - TAB_BAR_SAFE_HEIGHT - topMar - titleHieght;
    
    if (!_trustAccountTableView) {
        _trustAccountTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topMar, kWidth, height) style:(UITableViewStylePlain)];
        _trustAccountTableView.dataSource =self;
        _trustAccountTableView.delegate =self;
        _trustAccountTableView.separatorColor = [UIColor clearColor];
        [self.view addSubview:_trustAccountTableView];
    }
    return _trustAccountTableView;
}

#pragma mark  --  table view datasource   --

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.trustAccountData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * trustAccountCell = @"trustAccountCell";
    
    FC_AccountDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:trustAccountCell];
    
    if (cell == nil) {
        cell = [[FC_AccountDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:trustAccountCell];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    [cell reloadAccountCellWith:[self.trustAccountData objectAtIndex:indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kTopMar)];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kTopMar;
}

#pragma mark  --  trust account info request  --

-(NSMutableArray *)trustAccountData{
    
    if (!_trustAccountData) {
        _trustAccountData = [NSMutableArray new];
    }
    return _trustAccountData;
}

-(void)trustAccountRequest{
    
    NSDictionary * trustAccountDic = @{
                                       @"shopId":[LoginUser shareLoginUser].shopId,
                                       @"type":@"1"};
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopAccount
               parameters:trustAccountDic
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      [STTextHudTool hideSTHud];
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [self reloadDataWithResponse:responseObject];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)reloadDataWithResponse:(id)response{
    
    
    for (NSDictionary * item in response[@"data"][@"result"]) {
        FC_UserAccountModel * model = [FC_UserAccountModel mj_objectWithKeyValues:item];
        [self.trustAccountData addObject:model];
    }
    
    [self.trustAccountTableView reloadData];
    
}

@end
