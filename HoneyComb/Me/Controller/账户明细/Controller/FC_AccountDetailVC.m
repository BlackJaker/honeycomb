//
//  FC_AccountDetailVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_AccountDetailVC.h"

#import "FC_AllAccountDetailVC.h"
#import "FC_TicketAccountDetailVC.h"
#import "FC_SendAccountDetailVC.h"
#import "FC_TrustAccountDetailVC.h"
#import "FC_TopUpAccountDetailVC.h"
#import "FC_WithdrawAccountDetailVC.h"

@interface FC_AccountDetailVC ()<SGPageTitleViewDelegate, SGPageContentScrollViewDelegate>
{
    CGFloat pageTitleHieght;
}
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentScrollView *pageContentScrollView;

@property (nonatomic, copy) NSArray *childControllers;

@end

@implementation FC_AccountDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertyInit];
    
    [self contentSetup];
}

#pragma mark  --  property  --

-(void)propertyInit{
    
    self.titleLabe.text = @"账户明细";
    
    pageTitleHieght = 36;
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f9f9f9"];
    
}

-(void)contentSetup
{
    // title
    
    [self.view addSubview:self.pageTitleView];
    
    
    /// pageContentScrollView
    
    [self.view addSubview:self.pageContentScrollView];
    
}

-(SGPageTitleView *)pageTitleView{
    
    if (!_pageTitleView) {
        
        NSArray *titleArr = @[@"全部",@"出票",@"派奖",@"充值",@"托管",@"提现"];
        SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
        configure.indicatorAdditionalWidth = 10;
        configure.showBottomSeparator = NO;
        
        CGRect  pageTitleRect = CGRectMake(0, 0, kWidth, pageTitleHieght);
        
        /// pageTitleView
        self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:pageTitleRect
                                                            delegate:self
                                                          titleNames:titleArr
                                                           configure:configure];
        
    }
    return _pageTitleView;
}

-(NSArray *)childControllers{
    
    if (!_childControllers) {
        
        self.hidesBottomBarWhenPushed=YES;
        
        FC_AllAccountDetailVC * allVc = [[FC_AllAccountDetailVC alloc]init];
        FC_TicketAccountDetailVC *ticketVc = [[FC_TicketAccountDetailVC alloc]init];
        FC_SendAccountDetailVC *sendVc = [[FC_SendAccountDetailVC alloc]init];
        FC_TopUpAccountDetailVC *topUpVc = [[FC_TopUpAccountDetailVC alloc]init];
        FC_TrustAccountDetailVC *trustVc = [[FC_TrustAccountDetailVC alloc]init];
        FC_WithdrawAccountDetailVC * withdrawVc = [[FC_WithdrawAccountDetailVC alloc]init];
        
        _childControllers = @[allVc,ticketVc,sendVc,topUpVc,trustVc,withdrawVc];
    }
    return _childControllers;
}

-(SGPageContentScrollView *)pageContentScrollView{
    
    CGFloat contentViewHeight = kHeight - NAVIGATION_HEIGHT - pageTitleHieght - TAB_BAR_SAFE_HEIGHT;
    
    CGRect  pageContentScrollViewRect = CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), kWidth, contentViewHeight);
    
    if (!_pageContentScrollView) {
        
        _pageContentScrollView = [[SGPageContentScrollView alloc] initWithFrame:pageContentScrollViewRect
                                                                       parentVC:self
                                                                       childVCs:self.childControllers];
        _pageContentScrollView.delegatePageContentScrollView = self;
    }
    return _pageContentScrollView;
}

#pragma mark  -- page title view delegate  --

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentScrollView setPageContentScrollViewCurrentIndex:selectedIndex];
}

#pragma mark  --  page content scoll view delegate  --

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView index:(NSInteger)index {
    if (index == 0) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    } else {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
