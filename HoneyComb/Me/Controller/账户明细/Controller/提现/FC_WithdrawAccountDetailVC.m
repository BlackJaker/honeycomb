//
//  FC_WithdrawAccountDetailVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_WithdrawAccountDetailVC.h"

#import "FC_WithdrawCell.h"
#import "FC_UserAccountModel.h"

#import "FC_WithdrawSuccessedVC.h"

@interface FC_WithdrawAccountDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *withdrawAccountTableView;

@property (nonatomic,copy) NSMutableArray *withdrawAccountData;

@end

@implementation FC_WithdrawAccountDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];

}

-(UITableView *)withdrawAccountTableView{
    
    CGFloat  topMar = 0,titleHieght = self.type == 0?36:50,height = kHeight - NAVIGATION_HEIGHT - TAB_BAR_SAFE_HEIGHT - topMar - titleHieght;
    
    if (!_withdrawAccountTableView) {
        _withdrawAccountTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topMar, kWidth, height) style:(UITableViewStylePlain)];
        _withdrawAccountTableView.dataSource =self;
        _withdrawAccountTableView.delegate =self;
        _withdrawAccountTableView.separatorColor = [UIColor clearColor];
        [self.view addSubview:_withdrawAccountTableView];
    }
    return _withdrawAccountTableView;
}

#pragma mark  --  table view datasource   --

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.withdrawAccountData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * withdrawAccountCell = @"withdrawAccountCell";
    
    FC_WithdrawCell *cell = [tableView dequeueReusableCellWithIdentifier:withdrawAccountCell];
    
    if (cell == nil) {
        cell = [[FC_WithdrawCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:withdrawAccountCell];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell reloadAccountCellWith:[self.withdrawAccountData objectAtIndex:indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kTopMar)];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kTopMar;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    FC_UserAccountModel * model = [self.withdrawAccountData objectAtIndex:indexPath.row];
    
    FC_WithdrawSuccessedVC * withdrawSuccessedVc = [[FC_WithdrawSuccessedVC alloc]init];
    withdrawSuccessedVc.currentModel = model;
    [self.navigationController pushViewController:withdrawSuccessedVc animated:YES];

}

#pragma mark  --  withdraw account info request  --

-(NSMutableArray *)withdrawAccountData{
    
    if (!_withdrawAccountData) {
        _withdrawAccountData = [NSMutableArray new];
    }
    return _withdrawAccountData;
}

-(void)withdrawAccountRequest{
    
    NSDictionary * withdrawAccountDic = @{
                                       @"shopId":[LoginUser shareLoginUser].shopId,
                                       @"type":@"4"};
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopAccount
               parameters:withdrawAccountDic
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      [STTextHudTool hideSTHud];
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [self reloadDataWithResponse:responseObject];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)reloadDataWithResponse:(id)response{
    
    
    for (NSDictionary * item in response[@"data"][@"result"]) {
        FC_UserAccountModel * model = [FC_UserAccountModel mj_objectWithKeyValues:item];
        [self.withdrawAccountData addObject:model];
    }
    
    [self.withdrawAccountTableView reloadData];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.withdrawAccountData removeAllObjects];
    [self withdrawAccountRequest];
}

@end
