//
//  FC_WithdrawAccountDetailVC.h
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FC_WithdrawAccountDetailVC : AllViewController

@property (nonatomic,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
