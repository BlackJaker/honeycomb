//
//  FC_AllAccountDetailVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_AllAccountDetailVC.h"

#import "FC_AccountDetailCell.h"
#import "FC_UserAccountModel.h"

@interface FC_AllAccountDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *allAccountTableView;

@property (nonatomic,copy) NSMutableArray *allAccountData;

@end

@implementation FC_AllAccountDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.topNaviView removeFromSuperview];
    
    [self userAllAccountRequest];
}

-(UITableView *)allAccountTableView{
    
    CGFloat  topMar = 0,titleHieght = 36,height = kHeight - NAVIGATION_HEIGHT - TAB_BAR_SAFE_HEIGHT - topMar - titleHieght;
    
    if (!_allAccountTableView) {
        _allAccountTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topMar, kWidth, height) style:(UITableViewStylePlain)];
        _allAccountTableView.dataSource =self;
        _allAccountTableView.delegate =self;
        _allAccountTableView.separatorColor = [UIColor clearColor];
        [self.view addSubview:_allAccountTableView];
    }
    return _allAccountTableView;
}

#pragma mark  --  table view datasource   --

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allAccountData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * allAccountCell = @"allAccountCell";
    
    FC_AccountDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:allAccountCell];
    
    if (cell == nil) {
        cell = [[FC_AccountDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:allAccountCell];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell reloadAccountCellWith:[self.allAccountData objectAtIndex:indexPath.row]];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kTopMar)];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kTopMar;
}

#pragma mark  --  all account info request  --

-(NSMutableArray *)allAccountData{
    
    if (!_allAccountData) {
        _allAccountData = [NSMutableArray new];
    }
    return _allAccountData;
}

-(void)userAllAccountRequest{
    
    NSDictionary * userAllAccountDic = @{
                                         @"shopId":[LoginUser shareLoginUser].shopId,
                                         };
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopAccount
               parameters:userAllAccountDic
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      [STTextHudTool hideSTHud];
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [self reloadDataWithResponse:responseObject];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)reloadDataWithResponse:(id)response{
    
    for (NSDictionary * item in response[@"data"][@"result"]) {
        FC_UserAccountModel * model = [FC_UserAccountModel mj_objectWithKeyValues:item];
        [self.allAccountData addObject:model];
    }
    
    [self.allAccountTableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
