//
//  FC_UserAccountModel.h
//  HoneyComb
//
//  Created by afc on 2018/11/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_UserAccountModel : NSObject

@property (nonatomic,copy) NSString * createDate;

@property (nonatomic,copy) NSString * flow;

@property (nonatomic,copy) NSString * money;

@property (nonatomic,copy) NSString * sense;

@property (nonatomic,copy) NSString * status;

@property (nonatomic,copy) NSString * moneyId;

@end

NS_ASSUME_NONNULL_END
