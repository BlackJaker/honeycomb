//
//  FC_AccountDetailCell.m
//  HoneyComb
//
//  Created by afc on 2018/11/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_AccountDetailCell.h"

#import "FC_UserAccountModel.h"

@implementation FC_AccountDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self layoutView];
        
    }
    return self;
}
-(void)layoutView
{
    
    CGFloat  cellHeight = 90,bottomMar = 10;
    
    self.backView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMar, 0, kWidth - kLeftMar * 2, cellHeight - bottomMar)];
    _backView.backgroundColor = [UIColor whiteColor];
    self.backView.layer.cornerRadius = 5;
    [self.backView addProjectionWithShadowOpacity:1];
    [self.contentView addSubview:_backView];
    
    CGFloat  nameHeight = 15,nameWidth = 220,top = 20;
    
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.text = @"-";
    self.nameLabel.textColor = [UIColor colorWithHexString:@"#464e5f"];
    self.nameLabel.font = [UIFont fontWithName:kMedium size:15];
    [self.backView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(nameWidth);
        make.height.mas_equalTo(nameHeight);
        make.left.mas_equalTo(kLeftMar);
        make.top.mas_equalTo(top);
    }];
    
    CGFloat  bottom = 20,height = 12;
    
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.text = @"-";
    self.dateLabel.textColor = [UIColor colorWithHexString:@"#a2a9b8"];
    self.dateLabel.font = [UIFont fontWithName:kMedium size:height];
    [self.backView  addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(height);
        make.left.mas_equalTo(kLeftMar);
        make.bottom.mas_equalTo(-bottom);
    }];
    
    CGFloat width = 80,priceHeight = 15,priceTop = 25;
    
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.text = @"-";
    self.priceLabel.textColor = [UIColor colorWithHexString:@"#f84a4a"];
    self.priceLabel.font = [UIFont fontWithName:kBold size:14];
    [self.backView  addSubview:self.priceLabel];
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(priceHeight);
        make.right.mas_equalTo(-kLeftMar);
        make.top.mas_equalTo(priceTop);
    }];
   
}

-(void)reloadAccountCellWith:(FC_UserAccountModel *)model{
    
    self.dateLabel.text = model.createDate;
    
    NSString * money = [model.flow isEqualToString:@"+"]?model.money:[NSString stringWithFormat:@"%@%@",model.flow,model.money];
    
    self.priceLabel.text = money;
    
    if ([model.flow isEqualToString:@"+"]) {
        self.priceLabel.textColor = [UIColor colorWithHexString:@"#f84a4a"];
    }else{
        self.priceLabel.textColor = [UIColor colorWithHexString:@"#464e5f"];
    }
    
    self.nameLabel.text = model.sense;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
