//
//  FC_AccountDetailCell.h
//  HoneyComb
//
//  Created by afc on 2018/11/21.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_UserAccountModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_AccountDetailCell : UITableViewCell

@property (nonatomic,strong) UILabel * nameLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UIView *backView;


-(void)reloadAccountCellWith:(FC_UserAccountModel *)model;

@end

NS_ASSUME_NONNULL_END
