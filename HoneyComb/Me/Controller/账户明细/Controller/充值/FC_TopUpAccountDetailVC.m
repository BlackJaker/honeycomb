//
//  FC_TopUpAccountDetailVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_TopUpAccountDetailVC.h"

#import "FC_AccountDetailCell.h"
#import "FC_UserAccountModel.h"

@interface FC_TopUpAccountDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *topUpAccountTableView;

@property (nonatomic,copy) NSMutableArray *topUpAccountData;

@end

@implementation FC_TopUpAccountDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self topUpAccountRequest];
}

-(UITableView *)topUpAccountTableView{
    
    CGFloat  topMar = 0,titleHieght = 36,height = kHeight - NAVIGATION_HEIGHT - TAB_BAR_SAFE_HEIGHT - topMar - titleHieght;
    
    if (!_topUpAccountTableView) {
        _topUpAccountTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topMar, kWidth, height) style:(UITableViewStylePlain)];
        _topUpAccountTableView.dataSource =self;
        _topUpAccountTableView.delegate =self;
        _topUpAccountTableView.separatorColor = [UIColor clearColor];
        [self.view addSubview:_topUpAccountTableView];
    }
    return _topUpAccountTableView;
}

#pragma mark  --  table view datasource   --

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.topUpAccountData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * topUpAccountCell = @"topUpAccountCell";
    
    FC_AccountDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:topUpAccountCell];
    
    if (cell == nil) {
        cell = [[FC_AccountDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:topUpAccountCell];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell reloadAccountCellWith:[self.topUpAccountData objectAtIndex:indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kTopMar)];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kTopMar;
}

#pragma mark  --  topUp account info request  --

-(NSMutableArray *)topUpAccountData{
    
    if (!_topUpAccountData) {
        _topUpAccountData = [NSMutableArray new];
    }
    return _topUpAccountData;
}

-(void)topUpAccountRequest{
    
    NSDictionary * topUpAccountDic = @{
                                      @"shopId":[LoginUser shareLoginUser].shopId,
                                      @"type":@"5"};
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopAccount
               parameters:topUpAccountDic
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      [STTextHudTool hideSTHud];
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [self reloadDataWithResponse:responseObject];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)reloadDataWithResponse:(id)response{
    
    
    for (NSDictionary * item in response[@"data"][@"result"]) {
        FC_UserAccountModel * model = [FC_UserAccountModel mj_objectWithKeyValues:item];
        [self.topUpAccountData addObject:model];
    }
    
    [self.topUpAccountTableView reloadData];
    
}


@end
