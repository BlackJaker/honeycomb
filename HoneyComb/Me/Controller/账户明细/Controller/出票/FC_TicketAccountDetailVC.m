//
//  FC_TicketAccountDetailVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_TicketAccountDetailVC.h"

#import "FC_AccountDetailCell.h"
#import "FC_UserAccountModel.h"

@interface FC_TicketAccountDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *ticketAccountTableView;

@property (nonatomic,copy) NSMutableArray *ticketAccountData;

@end

@implementation FC_TicketAccountDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.topNaviView removeFromSuperview];
    
    [self ticketAccountRequest];
}

-(UITableView *)ticketAccountTableView{
    
    CGFloat  topMar = 0,titleHieght = 36,height = kHeight - NAVIGATION_HEIGHT - TAB_BAR_SAFE_HEIGHT - topMar - titleHieght;
    
    if (!_ticketAccountTableView) {
        _ticketAccountTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topMar, kWidth, height) style:(UITableViewStylePlain)];
        _ticketAccountTableView.dataSource =self;
        _ticketAccountTableView.delegate =self;
        _ticketAccountTableView.separatorColor = [UIColor clearColor];
        [self.view addSubview:_ticketAccountTableView];
    }
    return _ticketAccountTableView;
}

#pragma mark  --  table view datasource   --

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ticketAccountData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * ticketAccountCell = @"ticketAccountCell";
    
    FC_AccountDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:ticketAccountCell];
    
    if (cell == nil) {
        cell = [[FC_AccountDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ticketAccountCell];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell reloadAccountCellWith:[self.ticketAccountData objectAtIndex:indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kTopMar)];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kTopMar;
}

#pragma mark  --  ticket account info request  --

-(NSMutableArray *)ticketAccountData{
    
    if (!_ticketAccountData) {
        _ticketAccountData = [NSMutableArray new];
    }
    return _ticketAccountData;
}

-(void)ticketAccountRequest{
    
    NSDictionary * ticketAccountDic = @{
                                         @"shopId":[LoginUser shareLoginUser].shopId,
                                         @"type":@"2"};
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopAccount
               parameters:ticketAccountDic
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      [STTextHudTool hideSTHud];
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [self reloadDataWithResponse:responseObject];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)reloadDataWithResponse:(id)response{
    
    
    for (NSDictionary * item in response[@"data"][@"result"]) {
        FC_UserAccountModel * model = [FC_UserAccountModel mj_objectWithKeyValues:item];
        [self.ticketAccountData addObject:model];
    }
    
    [self.ticketAccountTableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
