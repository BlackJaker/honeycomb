//
//  FC_SendAccountDetailVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_SendAccountDetailVC.h"

#import "FC_AccountDetailCell.h"
#import "FC_UserAccountModel.h"

@interface FC_SendAccountDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *sendAccountTableView;

@property (nonatomic,copy) NSMutableArray *sendAccountData;

@end

@implementation FC_SendAccountDetailVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self sendAccountRequest];
}

-(UITableView *)sendAccountTableView{
    
    CGFloat  topMar = 0,titleHieght = 36,height = kHeight - NAVIGATION_HEIGHT - TAB_BAR_SAFE_HEIGHT - topMar - titleHieght;
    
    if (!_sendAccountTableView) {
        _sendAccountTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topMar, kWidth, height) style:(UITableViewStylePlain)];
        _sendAccountTableView.dataSource =self;
        _sendAccountTableView.delegate =self;
        _sendAccountTableView.separatorColor = [UIColor clearColor];
        [self.view addSubview:_sendAccountTableView];
    }
    return _sendAccountTableView;
}

#pragma mark  --  table view datasource   --

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sendAccountData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * sendAccountCell = @"sendAccountCell";
    
    FC_AccountDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:sendAccountCell];
    
    if (cell == nil) {
        cell = [[FC_AccountDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sendAccountCell];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell reloadAccountCellWith:[self.sendAccountData objectAtIndex:indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kTopMar)];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return kTopMar;
}

#pragma mark  --  send account info request  --

-(NSMutableArray *)sendAccountData{
    
    if (!_sendAccountData) {
        _sendAccountData = [NSMutableArray new];
    }
    return _sendAccountData;
}

-(void)sendAccountRequest{
    
    NSDictionary * sendAccountDic = @{
                                        @"shopId":[LoginUser shareLoginUser].shopId,
                                        @"type":@"3"};
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:ShopAccount
               parameters:sendAccountDic
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      [STTextHudTool hideSTHud];
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          
                          [self reloadDataWithResponse:responseObject];
                          
                      }else{
                          [STTextHudTool showErrorText:responseObject[@"msg"]];
                      }
                      
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)reloadDataWithResponse:(id)response{
    
    
    for (NSDictionary * item in response[@"data"][@"result"]) {
        FC_UserAccountModel * model = [FC_UserAccountModel mj_objectWithKeyValues:item];
        [self.sendAccountData addObject:model];
    }
    
    [self.sendAccountTableView reloadData];
    
}

@end
