//
//  NotTicketViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/9.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "NotTicketViewController.h"
#import "SegmentViewController.h"
#import "OneViewController.h"
#import "TwoViewController.h"
#import "ThreeViewController.h"

#import "BettingViewController.h"
#import "ChippedViewController.h"
#import "DocumentaryViewController.h"

static CGFloat const ButtonHeight = 40;
@interface NotTicketViewController ()
@property (nonatomic,strong) NSMutableArray *arrCount;
@end

@implementation NotTicketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"全部订单";
    self.view.backgroundColor = RGB(249, 249, 249, 1);

    SegmentViewController *vc = [[SegmentViewController alloc]init];
    
//    NSArray *titleArray = @[@"投注",@"合买",@"跟单"];
//    vc.titleArray = titleArray;
//    NSMutableArray *controlArray = [[NSMutableArray alloc]init];
//
//    OneViewController *vc1 = [[OneViewController alloc]init];
//    [controlArray addObject:vc1];
//
//    TwoViewController *vc2 = [[TwoViewController alloc]init];
//    [controlArray addObject:vc2];
//
//    ThreeViewController *vc3 = [[ThreeViewController alloc]init];
//    [controlArray addObject:vc3];
//    vc.source = @"me";
//    vc.titleSelectedColor = [UIColor redColor];
//    vc.subViewControllers = controlArray;
//    vc.buttonWidth = self.view.frame.size.width/3;
//    vc.buttonHeight = ButtonHeight;
//    vc.bottomCount = titleArray.count;
//    [vc initSegment];
//    [vc addParentController:self];
    
    NSArray *titleArray = @[@"订单",@"合买",@"跟单"];
    vc.titleArray = titleArray;
    NSMutableArray *controlArray = [[NSMutableArray alloc]init];
    
    BettingViewController *vc1 = [[BettingViewController alloc]init];
    [controlArray addObject:vc1];
    
    ChippedViewController *vc2 = [[ChippedViewController alloc]init];
    [controlArray addObject:vc2];
    
    DocumentaryViewController *vc3 = [[DocumentaryViewController alloc]init];
    [controlArray addObject:vc3];
    vc.source = @"me";
    vc.titleSelectedColor = [UIColor redColor];
    vc.subViewControllers = controlArray;
    vc.buttonWidth = self.view.frame.size.width/3;
    vc.buttonHeight = ButtonHeight;
    vc.bottomCount = titleArray.count;
    [vc initSegment];
    [vc addParentController:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
