//
//  OneViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/9.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OneViewController.h"
#import "BettingCell.h"
#import "BigLotteryViewController.h"
#import "RaceColorViewController.h"
@interface OneViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>
{
    
  
    NSString *total;
    NSString *typeStr;
}
@property (nonatomic,strong) UIButton *rightBtn;

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *listArr;
@property (nonatomic,assign)NSInteger page;

@end

@implementation OneViewController
static NSString *cellID = @"cell";
-(NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutView];
    [self setupRefresh];
    
 
    self.view.backgroundColor = RGB(249, 249, 249, 1);
}
#pragma mark --创建上拉加载和下拉刷新
- (void)setupRefresh {
    __block OneViewController *weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadTopics];
    }];
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadNewTopics];
    }];
}
-(void)loadTopics
{
    self.page = 1;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"state"] = @"1";
    dic[@"pageNum"] = pageStr;
    dic[@"type"] = typeStr;
    //    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser]shopId ],@"state":@"1",@"pageNum":pageStr,@"type":typeStr};
    NSLog(@"%@",dic);
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            NSMutableArray *arr = [solveJsonData changeType:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
            if (arr.count > 0) {
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str] ) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData ];
                }else
                {
                    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        [self loadNewTopics];
                    }];
                }
                
                self.listArr = [AllModel mj_objectArrayWithKeyValuesArray:arr];
                
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }else
            {
                [self.listArr removeAllObjects];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
//                [STTextHudTool showText:@"暂无数据!"];
            }
            
        }else
        {
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_header endRefreshing];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)loadNewTopics
{
    self.page ++;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"state"] = @"1";
    dic[@"pageNum"] = pageStr;
    dic[@"type"] = typeStr;
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            NSArray  *arr =[AllModel mj_objectArrayWithKeyValuesArray:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
            if (arr == nil) {
                [STTextHudTool showText:@"暂无更多数据!"];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
                //                [self.view showError:@"暂无信息"];
            }else
            {
                [self.listArr addObjectsFromArray:arr];
                
                [self.tableView reloadData];
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                //                GFLog(@"total=%@",total);
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str]) {
                    //                    GFLog(@"2233");
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else
                {
                    [self.tableView.mj_footer endRefreshing];
                }
            }
            
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
    self.tableView.dataSource =self;
    self.tableView.delegate = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.backgroundColor = RGB(249, 249, 249, 1);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[BettingCell class] forCellReuseIdentifier:cellID];
    [self.view addSubview:self.tableView];
    
    [self.tableView makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0)];
    v.backgroundColor = RGB(249, 249, 249, 1);
    return v;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 10)];
    v.backgroundColor = RGB(249, 249, 249, 1);
    return v;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BettingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[BettingCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellID];
    }
    [cell.lookBtn addTarget:self action:@selector(lookAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [cell.cancleBtn addTarget:self action:@selector(cancleAction:) forControlEvents:(UIControlEventTouchUpInside)];
    cell.model = self.listArr[indexPath.section];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中颜色消失
    NSLog(@"--%ld",indexPath.section);
    AllModel *model = self.listArr[indexPath.section];
    NSString *str = model.type;
    NSString *orderId = model.orderTotalId;
    NSLog(@"--%@-%@",str,orderId);
    if ([str isEqualToString:@"3"] || [str isEqualToString:@"4"] || [str  isEqualToString:@"5"] || [str isEqualToString:@"6"]) {
        self.hidesBottomBarWhenPushed = YES;
        BigLotteryViewController *bigV = [[BigLotteryViewController alloc]init];
        bigV.strType = str;
        bigV.orderId = orderId;
        bigV.NextViewControllerBlock = ^(NSInteger tfText) {
            
            [self.tableView beginUpdates];
            [_listArr removeObjectAtIndex:indexPath.section];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
        };
        [self.navigationController pushViewController:bigV animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }else
    {
        self.hidesBottomBarWhenPushed = YES;
        RaceColorViewController *raV = [[RaceColorViewController alloc]init];
        raV.strType = str;
        raV.orderId = orderId;
        raV.NextViewControllerBlock = ^(NSInteger tfText) {
            [self.tableView beginUpdates];
            [_listArr removeObjectAtIndex:indexPath.section];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
        };
        [self.navigationController pushViewController:raV animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }
    
}
#pragma mark 查看详情
-(void)lookAction:(UIButton *)button
{
    BettingCell *cell = (BettingCell *)button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AllModel *model = self.listArr[indexPath.section];
    NSString *str = model.type;
    NSString *orderId = model.orderTotalId;
    NSLog(@"--%@-%@",str,orderId);
    if ([str isEqualToString:@"3"] || [str isEqualToString:@"4"] || [str  isEqualToString:@"5"] || [str isEqualToString:@"6"]) {
        self.hidesBottomBarWhenPushed = YES;
        BigLotteryViewController *bigV = [[BigLotteryViewController alloc]init];
        bigV.strType = str;
        bigV.orderId = orderId;
        bigV.NextViewControllerBlock = ^(NSInteger tfText) {
            
            [self.tableView beginUpdates];
            [_listArr removeObjectAtIndex:indexPath.section];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
        };
        [self.navigationController pushViewController:bigV animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }else
    {
        self.hidesBottomBarWhenPushed = YES;
        RaceColorViewController *raV = [[RaceColorViewController alloc]init];
        raV.strType = str;
        raV.orderId = orderId;
        raV.NextViewControllerBlock = ^(NSInteger tfText) {
            [self.tableView beginUpdates];
            [_listArr removeObjectAtIndex:indexPath.section];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
        };
        [self.navigationController pushViewController:raV animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }
}
-(void)cancleAction:(UIButton *)button
{
    BettingCell *cell = (BettingCell *)button.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AllModel *model = self.listArr[indexPath.section];
    
    NSDictionary *dic = @{@"orderTotalId":model.orderTotalId};
    [PPNetworkHelper POST:PostCancleOrder parameters:dic success:^(id responseObject) {
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            [self.tableView beginUpdates];
            [_listArr removeObjectAtIndex:indexPath.section];
            
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            //            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationRight)];
            //            [_tableView reloadData];
            [self.tableView endUpdates];
            [STTextHudTool showText:@"订单撤销成功"];
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSError *error) {
        [STTextHudTool showText:@"订单撤销失败"];
    }];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kWidth/2.8846;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0001;
}
#pragma mark DZ
-(CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 25;
}
-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"meiyouOrder"];
}
-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    return YES;
}
-(BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
