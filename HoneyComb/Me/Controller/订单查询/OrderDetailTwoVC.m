//
//  OrderDetailTwoVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderDetailTwoVC.h"
#import "OrderDetailTwoCell.h"
#import "OSSImageUploader.h"

#import "AF_OddsNoteView.h"

///弱引用/强引用
#define CCWeakSelf __weak typeof(self) weakSelf = self;
@interface OrderDetailTwoVC ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,UINavigationControllerDelegate,PYPhotosViewDelegate>
{
       NSDictionary *mDic;
       CGFloat  noteHeight;
}
@property (nonatomic, weak) PYPhotosView *publishPhotosView;//属性 保存选择的图片
@property(nonatomic,strong)NSMutableArray *photos;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UILabel *nameLabel;//彩种名称
@property (nonatomic,strong) UILabel *expectLabel;//预计奖金
@property (nonatomic,strong) UILabel *stateLabel;;//状态
@property (nonatomic,strong) UILabel *priceLabel;//投注金额
@property (nonatomic,strong) UILabel *nickNameLabel;// 昵称
@property (nonatomic,strong) UILabel *phoneLabel;//手机号码
@property (nonatomic,strong) UILabel *numberLabel;//编号
@property (nonatomic,strong) UILabel *payDateLabel;//购买时间
@property (nonatomic,strong) UILabel *outDateLabel;//出票时间
@property (nonatomic,strong) UILabel *playLabel;//玩法
@property (nonatomic,strong) UIButton *takeBtn;//
@property (nonatomic,strong) UIButton *hairBtn;//
@property (nonatomic,strong) UIButton *outBtn;//
@property (nonatomic,strong) NSMutableArray *listArr;
@property (nonatomic,strong) UIImageView *photoImage;
@property (nonatomic,assign) CGFloat floatHeight;
@property (nonatomic,strong) UIButton *doneBtn;//完成
@property (nonatomic,strong) UIButton *editBtn;//编辑
@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;
@property (nonatomic,strong) NSMutableArray *matchList;

@property (nonatomic,strong) UIView *tableHeadView;

@property (nonatomic,assign) BOOL isClicking;

@property (nonatomic,strong) AF_OddsNoteView * noteView;

@end

@implementation OrderDetailTwoVC
static NSString *kTempFolder = @"shop/order";
-(NSMutableArray *)matchList
{
    if (!_matchList) {
        _matchList = [NSMutableArray array];
    }
    return _matchList;
}
-(NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"订单查询";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    
    noteHeight = 40;

    self.isClicking = NO;
    
    [self layoutView];
    [self creatData];
    [self creatToken];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(deleteAction:) name:@"deleteTongzhi" object:nil];
}
-(void)deleteAction:(NSNotification *)dic
{
    self.doneBtn.hidden = YES;
}
-(void)creatToken
{
    
    [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {
        //        NSLog(@"--%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
            self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
            self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
        }
        
    } failure:^(NSError *error) {
        //        NSLog(@"--%@",error);
    }];
    
    
}
-(void)getState
{
    if ([self.state isEqualToString:@"0"]) {
       
          [self.tableView setFrame:CGRectMake(0, 0, kWidth, kHeight)];
     
    }else if([self.state isEqualToString:@"1"]){
      
        self.takeBtn.hidden = NO;
        self.hairBtn.hidden = NO;
        [self.takeBtn setTitle:@"撤单" forState:(UIControlStateNormal)];
        [self.hairBtn setTitle:@"出票" forState:(UIControlStateNormal)];
        
    }else if([self.state isEqualToString:@"2"]){
      
         [self.tableView setFrame:CGRectMake(0, 0, kWidth, kHeight)];
    }
    else if([self.state isEqualToString:@"3"]){
       
        self.takeBtn.hidden = NO;
        self.hairBtn.hidden = NO;
        [self.takeBtn setTitle:@"取票" forState:(UIControlStateNormal)];
        [self.hairBtn setTitle:@"派奖" forState:(UIControlStateNormal)];
       
    }
    else if([self.state isEqualToString:@"4"]){
      
        self.outBtn.hidden = NO;
        
    }
    else if([self.state isEqualToString:@"5"]){
         [self.tableView setFrame:CGRectMake(0, 0, kWidth, kHeight)];
    }else{
          [self.tableView setFrame:CGRectMake(0, 0, kWidth, kHeight)];
    }
}
-(void)creatData
{
     NSDictionary *dic = @{@"type":self.typeStr,@"orderTotalId":self.orderId};
    
    NSString * urlStr = [self.typeStr integerValue] == 8?PostNineOrder:[self.typeStr integerValue] == 7?PostSuccessOrder:PostAthleticeOrder;

    [PPNetworkHelper POST:urlStr parameters:dic success:^(id responseObject) {
        
         NSLog(@"竞彩=%@",responseObject);
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            mDic = [solveJsonData changeType:[responseObject objectForKey:@"data"]];
            self.matchList =[mDic objectForKey:@"detailList"] ;
            self.listArr = [AllModel mj_objectArrayWithKeyValuesArray:[mDic objectForKey:@"detailList"]];
            
            [self.tableView reloadData];
            
            [self reloadHeadView];
            
            [self assignmentAction];
            [self getState];
        }else{
            [STTextHudTool showErrorText:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        [STTextHudTool showErrorText:@"网络连接错误,请稍后重试"];
    }];
}
-(void)layoutView
{
   
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight) style:(UITableViewStyleGrouped)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[OrderDetailTwoCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
    
    CGFloat  leftMar = 12,centerMar = 12,height = 40 , width = (kWidth - leftMar * 2 - centerMar)/2,bottom = IPHONE_X?TAB_BAR_SAFE_HEIGHT:10;
    
    self.takeBtn = [[UIButton alloc]init];
    [self.view addSubview:self.takeBtn];
    self.takeBtn.hidden = YES;
    [self.takeBtn setTitleColor:kRedColor forState:(UIControlStateNormal)];
    [self.takeBtn setTitle:@"取票" forState:(UIControlStateNormal)];
    [self.takeBtn addTarget:self action:@selector(takeAction:) forControlEvents:(UIControlEventTouchUpInside)];
    self.takeBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    self.takeBtn.layer.cornerRadius = 4;
    self.takeBtn.layer.masksToBounds = YES;
    //边框宽度
    [self.takeBtn.layer setBorderWidth:1.0];
    self.takeBtn.layer.borderColor = RGB(231, 31, 25, 0.5).CGColor;
    [self.takeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
        make.left.mas_equalTo(leftMar);
        make.bottom.mas_equalTo(-bottom);
    }];
    
    self.hairBtn = [[UIButton alloc]init];
    [self.view addSubview:self.hairBtn];
    self.hairBtn.hidden = YES;
    [self.hairBtn addTarget:self action:@selector(hairAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.hairBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.hairBtn setTitle:@"派奖" forState:(UIControlStateNormal)];
    self.hairBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    self.hairBtn.layer.cornerRadius = 4;
    self.hairBtn.layer.masksToBounds = YES;
    self.hairBtn.backgroundColor = kRedColor;
    [self.hairBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
        make.right.mas_equalTo(-leftMar);
        make.bottom.mas_equalTo(-bottom);
    }];
    
    self.outBtn = [[UIButton alloc]init];
    [self.view addSubview:self.outBtn];
    self.outBtn.hidden = YES;
    [self.outBtn addTarget:self action:@selector(outAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.outBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.outBtn setTitle:@"取票" forState:(UIControlStateNormal)];
    self.outBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    self.outBtn.layer.cornerRadius = 4;
    self.outBtn.layer.masksToBounds = YES;
    self.outBtn.backgroundColor = kRedColor;
    [self.outBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-leftMar);
        make.height.mas_equalTo(height);
        make.left.mas_equalTo(leftMar);
        make.bottom.mas_equalTo(-bottom);
    }];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.tableHeadView;
    
}

-(UIView *)tableHeadView{
    
    if (!_tableHeadView) {
        
        _tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kWidth/1.019 + noteHeight)];
        _tableHeadView.backgroundColor = [UIColor whiteColor];
        
        _noteView = [[AF_OddsNoteView alloc]initWithFrame:CGRectMake(0, 0, kWidth, noteHeight)];
        _noteView.backgroundColor = [UIColor colorWithHexString:@"#FFEEEC"];
        [_tableHeadView addSubview:_noteView];

        
        CGFloat leftMargin = kWidth/31.25;
        
        self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, kWidth/25 + noteHeight, kWidth-kWidth/15.625, kWidth/22.0588)];
        self.nameLabel.text = @"-";
        self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
        self.nameLabel.textColor = kBlackColor;
        [_tableHeadView addSubview:self.nameLabel];
        
        self.expectLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.nameLabel.frame)+kWidth/13.3928, kWidth/3, kWidth/22.0588)];
        self.expectLabel.text = @"0.00";
        self.expectLabel.textAlignment = NSTextAlignmentCenter;
        self.expectLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
        self.expectLabel.textColor = kRedColor;
        [_tableHeadView addSubview:self.expectLabel];
        
        self.stateLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/3, CGRectGetMaxY(self.nameLabel.frame)+kWidth/13.3928, kWidth/3, kWidth/22.0588)];
        self.stateLabel.text = @"-";
        self.stateLabel.textAlignment = NSTextAlignmentCenter;
        self.stateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
        self.stateLabel.textColor = kBlackColor;
        [_tableHeadView addSubview:self.stateLabel];
        
        self.priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.stateLabel.frame), CGRectGetMaxY(self.nameLabel.frame)+kWidth/13.3928, kWidth/3, kWidth/22.0588)];
        self.priceLabel.text = @"0.00";
        self.priceLabel.textAlignment = NSTextAlignmentCenter;
        self.priceLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
        self.priceLabel.textColor = kRedColor;
        [_tableHeadView addSubview:self.priceLabel];
        
        NSArray *arrPrice = @[@"预计奖金(元)",@"订单状态",@"投注金额(元)"];
        for (int i = 0; i<arrPrice.count; i++) {
            UILabel *titleL = [[UILabel alloc]initWithFrame:
                               CGRectMake((kWidth/3)*(i%3), CGRectGetMaxY(self.stateLabel.frame)+kWidth/31.25, kWidth/3, kWidth/28.8461)];
            titleL.text = arrPrice[i];
            titleL.textColor = kGrayColor;
            titleL.textAlignment = NSTextAlignmentCenter;
            titleL.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
            [_tableHeadView addSubview:titleL];
        }
        
        CGFloat  grayViewHeight = 15,infoViewHeight = 35,originY = kWidth/8.7209,itemHeight = 25;
        
        UIView *grayView = [[UIView alloc]init];
        grayView.backgroundColor = RGB(0, 0, 0, 0.1);
        [_tableHeadView addSubview:grayView];
        
        [grayView makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(CGRectGetMaxY(self.stateLabel.frame) + originY);
            make.height.mas_equalTo(grayViewHeight);
        }];
        
        UILabel *orderLabel = [[UILabel alloc]init];
        orderLabel.textAlignment = NSTextAlignmentLeft;
        orderLabel.text = @"订单信息";
        orderLabel.textColor = kBlackColor;
        orderLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
        [_tableHeadView addSubview:orderLabel];
        
        [orderLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(grayView.mas_bottom);
            make.height.mas_equalTo(infoViewHeight);
        }];
        
        self.nickNameLabel = [[UILabel alloc]init];
        self.nickNameLabel.textColor = kGrayColor;
        self.nickNameLabel.text = @"用  户 名: -";
        self.nickNameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        self.nickNameLabel.userInteractionEnabled = YES;
        [_tableHeadView addSubview:self.nickNameLabel];
        
        [self.nickNameLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(orderLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        self.phoneLabel = [[UILabel alloc]init];
        self.phoneLabel.textColor = kGrayColor;
        self.phoneLabel.text = @"手  机 号: -";
        self.phoneLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        self.phoneLabel.userInteractionEnabled = YES;
        
        //长按拨打
        UILongPressGestureRecognizer * longGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(lognGestureAction:)];
        [self.phoneLabel addGestureRecognizer:longGesture];
        
        [_tableHeadView addSubview:self.phoneLabel];
        
        [self.phoneLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(self.nickNameLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        
        self.numberLabel = [[UILabel alloc]init];
        self.numberLabel.textColor = kGrayColor;
        self.numberLabel.text = @"订单编号: -";
        self.numberLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        [_tableHeadView addSubview:self.numberLabel];
        
        [self.numberLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(self.phoneLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        self.payDateLabel = [[UILabel alloc]init];
        self.payDateLabel.textColor = kGrayColor;
        self.payDateLabel.text = @"购买时间: -";
        self.payDateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        [_tableHeadView addSubview:self.payDateLabel];
        
        [self.payDateLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(self.numberLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        self.outDateLabel = [[UILabel alloc]init];
        self.outDateLabel.textColor = kGrayColor;
        self.outDateLabel.text = @"出票时间: -";
        self.outDateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        [_tableHeadView addSubview:self.outDateLabel];
        
        [self.outDateLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(self.payDateLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];

        self.playLabel = [[UILabel alloc]init];
        self.playLabel.textColor = kGrayColor;
        self.playLabel.text = @"玩      法: -";
        self.playLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        [_tableHeadView addSubview:self.playLabel];
        
        [self.playLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(self.outDateLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        UIView *secondGrayView = [[UIView alloc]init];
        secondGrayView.backgroundColor = RGB(0, 0, 0, 0.1);
        [_tableHeadView addSubview:secondGrayView];
        
        [secondGrayView makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(self.playLabel.mas_bottom);
            make.height.mas_equalTo(grayViewHeight);
        }];
        
        UILabel *conLabel = [[UILabel alloc]init];
        conLabel.textAlignment = NSTextAlignmentLeft;
        conLabel.text = @"订单内容";
        conLabel.textColor = kBlackColor;
        conLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
        [_tableHeadView addSubview:conLabel];
        
        [conLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
            make.top.mas_equalTo(secondGrayView.mas_bottom);
            make.height.mas_equalTo(infoViewHeight);
        }];
        
    }
    return _tableHeadView;
}

-(void)reloadHeadView{
    
    if ([self.typeStr integerValue] < 3) {
        self.nameLabel.text = [NSString stringWithFormat:@"%@",[mDic objectForKey:@"playName"]];
    }else{
        self.nameLabel.text = [NSString stringWithFormat:@"%@ | 第%@期",[mDic objectForKey:@"playName"],[mDic objectForKey:@"issue"]];
    }
    
    self.expectLabel.text = [mDic objectForKey:@"award"];
    self.priceLabel.text = [mDic objectForKey:@"price"];
    if ([mDic valueForKey:@"nickName"]) {
        self.nickNameLabel.text = [NSString stringWithFormat:@"用  户 名: %@",[mDic valueForKey:@"nickName"]];
    }
    if ([mDic valueForKey:@"mobile"]) {
        self.phoneLabel.text= [NSString stringWithFormat:@"手  机 号: %@(长按拨打)",[mDic objectForKey:@"mobile"]];
    }
    if ([mDic valueForKey:@"orderNumber"]) {
        self.numberLabel.text = [NSString stringWithFormat:@"订单编号: %@",[mDic objectForKey:@"orderNumber"]];
    }
    if ([mDic valueForKey:@"createDate"]) {
        self.payDateLabel.text = [NSString stringWithFormat:@"购买时间: %@",[mDic objectForKey:@"createDate"]];
    }
    if ([mDic valueForKey:@"printDate"]) {
        self.outDateLabel.text = [NSString stringWithFormat:@"出票时间: %@",[mDic objectForKey:@"printDate"]];
    }
    if ([mDic valueForKey:@"account"] && [mDic valueForKey:@"times"]) {
        self.playLabel.text = [NSString stringWithFormat:@"玩      法: %@ 注 %@ 倍",[mDic objectForKey:@"account"],[mDic objectForKey:@"times"]];
    }
    
    CGFloat  margin = IPHONE_X?TAB_BAR_SAFE_HEIGHT:0;

    NSInteger stateNumber = [[mDic objectForKey:@"state"] integerValue];
    NSString * stateString = [NSString stateStringWithType:stateNumber];
    
    switch (stateNumber) {
        case 1:
            self.takeBtn.hidden = NO;
            self.hairBtn.hidden = NO;
            [self.takeBtn setTitle:@"撤单" forState:(UIControlStateNormal)];
            [self.hairBtn setTitle:@"出票" forState:(UIControlStateNormal)];
            margin = IPHONE_X?TabBarHeight:60;
            break;
        case 2:
            self.outBtn.hidden = NO;
            margin = IPHONE_X?TabBarHeight:60;
            break;
        case 3:
            self.takeBtn.hidden = NO;
            self.hairBtn.hidden = NO;
            [self.takeBtn setTitle:@"取票" forState:(UIControlStateNormal)];
            [self.hairBtn setTitle:@"派奖" forState:(UIControlStateNormal)];
            margin = IPHONE_X?TabBarHeight:60;
            break;
        case 4:
            self.outBtn.hidden = NO;
            margin = IPHONE_X?TabBarHeight:60;
            break;
        default:
            break;
    }

    self.stateLabel.text = stateString;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, NAVIGATION_HEIGHT + margin, 0);
    
}

-(void)lognGestureAction:(UILongPressGestureRecognizer *)longGesture{
    
    if (longGesture.state == UIGestureRecognizerStateBegan) {
        
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",[mDic objectForKey:@"mobile"]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kWidth/2.7777)];
    view.backgroundColor = [UIColor whiteColor];
    UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 2, kWidth, kWidth/25)];
    grayView.backgroundColor = RGB(0, 0, 0, 0.1);
    [self.tableView bringSubviewToFront:grayView];
    [view addSubview:grayView];
    
    UILabel *conLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/31.25, CGRectGetMaxY(grayView.frame)+kWidth/25, kWidth/2, kWidth/22.0588)];
    conLabel.textAlignment = NSTextAlignmentLeft;
    conLabel.text = @"彩票照片";
    conLabel.textColor = kBlackColor;
    conLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
    [view addSubview:conLabel];
    
    CGFloat  topMargin = 40;
    self.doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(kWidth-kWidth/7.2115, topMargin, kWidth/9.375, kWidth/25)];
    [self.doneBtn setTitle:@"完成" forState:(UIControlStateNormal)];
    self.doneBtn.hidden = YES;
    self.doneBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
    [self.doneBtn setTitleColor:[UIColor blueColor] forState:(UIControlStateNormal)];
    [self.doneBtn addTarget:self action:@selector(doneAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [view addSubview:self.doneBtn];
    
    self.editBtn = [[UIButton alloc]initWithFrame:CGRectMake(kWidth-kWidth/7.2115, topMargin, kWidth/9.375, kWidth/25)];
    [self.editBtn setTitle:@"编辑" forState:(UIControlStateNormal)];
    self.editBtn.hidden = YES;
    self.editBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
    [self.editBtn setTitleColor:[UIColor blueColor] forState:(UIControlStateNormal)];
    [self.editBtn addTarget:self action:@selector(editAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [view addSubview:self.editBtn];
    
    // 1. 常见一个发布图片时的photosView
    PYPhotosView *publishPhotosView = [PYPhotosView photosView];
    publishPhotosView.py_x = kWidth/34.0909;
    publishPhotosView.py_y = kWidth/5.9523;
    // 2.1 设置本地图片
    publishPhotosView.images = nil;
    // 3. 设置代理
    publishPhotosView.delegate = self;
    publishPhotosView.photosMaxCol = 5;//每行显示最大图片个数
    publishPhotosView.imagesMaxCountWhenWillCompose = 3;//最多选择图片的个数
    // 4. 添加photosView
    self.publishPhotosView = publishPhotosView;
    [view addSubview:publishPhotosView];
    
    if ([[mDic objectForKey:@"image"] isEqualToString:@""]) {
        self.doneBtn.hidden = YES;
    }else
    {
        self.editBtn.hidden = YES;
        self.doneBtn.hidden = YES;
        NSString * imageStr = [mDic objectForKey:@"image"];
        if ([imageStr hasSuffix:@","]) {
            imageStr = [imageStr substringToIndex:imageStr.length - 1];
        }
        NSArray *arr = [imageStr componentsSeparatedByString:@","];
        NSMutableArray *arrAy = [NSMutableArray arrayWithArray:arr];
//        NSLog(@"--%@",arrAy);
        self.publishPhotosView.originalUrls = arrAy;    //  加载网络图片
        
    }
//    self.photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(conLabel.frame)+15, 115, 73)];
//    self.photoImage.backgroundColor = kRedColor;
//    [view addSubview:self.photoImage];
    return view;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[OrderDetailTwoCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.model = self.listArr[indexPath.row];
  int i =self.listArr.count-1;
    if (indexPath.row == i) {
        cell.separatorView.hidden = YES;
    }
    NSLog(@"---%d",i);
    
    cell.selectedBackgroundView.backgroundColor =RGB(0, 0, 0, 0.5);
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kWidth/1.019 + noteHeight;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return kWidth/2.4193;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kWidth/5;
}
-(void)assignmentAction
{
    
}
#pragma mark 撤单/取票
-(void)takeAction:(UIButton *)btn
{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        @WeakObj(self)
        if ([btn.titleLabel.text isEqualToString:@"撤单"]) {
            
            NSDictionary *dic = @{
                                  @"orderTotalId":self.orderId
                                  };
            
            [PPNetworkHelper POST:PostCancleOrder parameters:dic success:^(id responseObject) {
                
                @StrongObj(self)
                if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                    
                    if (self.PopBlock) {
                        self.PopBlock();
                    }
                    [STTextHudTool showText:@"订单撤销成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else
                {
                    [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                }
                self.isClicking = NO;
            } failure:^(NSError *error) {
                @StrongObj(self)
                [STTextHudTool showText:@"订单撤销失败"];
                self.isClicking = NO;
            }];
        }else if ([btn.titleLabel.text isEqualToString:@"取票"]){
            
            [self showAlertViewController];
        }
        
    }

}

-(void)showAlertViewController{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确认取票" message:@"确认彩民已到店,取走彩票!" preferredStyle:UIAlertControllerStyleAlert];
    @WeakObj(self)
    UIAlertAction *savePhotoAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @StrongObj(self)
        
        [self takeOutTicketRequest];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:savePhotoAction];
    
    
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    
}
#pragma mark --------  派奖/出票  --------

-(void)hairAction:(UIButton *)btn
{

    if (!self.isClicking) {
        
        self.isClicking = YES;
        @WeakObj(self)
        if ([btn.titleLabel.text isEqualToString:@"派奖"]) {
            
            NSDictionary *dic = @{
                                  @"orderTotalId":self.orderId,@"type":@"5"
                                  };
            [PPNetworkHelper POST:PostSendPrice parameters:dic success:^(id responseObject) {
                
                if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                    
                    if (self.PopBlock) {
                        self.PopBlock();
                    }
                    [STTextHudTool showText:@"派奖成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else
                {
                    [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                }
                self.isClicking = NO;
            } failure:^(NSError *error) {
                @StrongObj(self)
                self.isClicking = NO;
                [STTextHudTool showText:@"派奖失败"];
            }];
        }
        else
        {
            NSMutableDictionary * dic = [NSMutableDictionary new];
            [dic setValue:self.typeStr forKey:@"type"];
            [dic setValue:self.orderId forKey:@"orderTotalId"];
            if ([self.typeStr integerValue] < 3) {
                [dic setValue:self.matchList forKey:@"matchList"];
            }
            
            [PPNetworkHelper POST:PostPrintOrder parameters:dic success:^(id responseObject) {
                
                @StrongObj(self)
                if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                    //                if (self.NextViewControllerBlock) {
                    //                    self.NextViewControllerBlock(1);
                    //                }
                    if (self.PopBlock) {
                        self.PopBlock();
                    }
                    [STTextHudTool showText:@"出票成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else
                {
                    [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                }
                self.isClicking = NO;
            } failure:^(NSError *error) {
                @StrongObj(self)
                [STTextHudTool showText:@"出票失败"];
                 self.isClicking = NO;
            }];
        }
        
    }

}
#pragma mark 取票
-(void)outAction:(UIButton *)btn
{
    [self showAlertViewController];
}

-(void)takeOutTicketRequest{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        @WeakObj(self)
        NSDictionary *dic = @{
                              @"orderTotalId":self.orderId
                              };
        
        [PPNetworkHelper POST:PostTakeOrder parameters:dic success:^(id responseObject) {
            
            @StrongObj(self)
            if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                
                if (self.PopBlock) {
                    self.PopBlock();
                }
                [STTextHudTool showText:@"取票成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else
            {
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
            self.isClicking = NO;
        } failure:^(NSError *error) {
            @StrongObj(self)
            [STTextHudTool showText:@"取票失败"];
            self.isClicking = NO;
        }];
        
    }
    
}

#pragma mark 完成
-(void)doneAction:(UIButton *)btn
{
    self.publishPhotosView.py_x = kWidth/34.0909;
    self.publishPhotosView.py_y = kWidth/5.9523;
   
    NSArray *arrAll = self.photos;
    NSLog(@"-%@",arrAll);
    
    [OSSImageUploader asyncUploadImages:arrAll folder:kTempFolder accessKey:self.accessKey secretKey:self.accessKeySecret token:self.tokenStr complete:^(NSArray<NSString *> *names, UploadImageState state) {
        NSLog(@"彩票照片-%@",names);
        
        NSMutableArray *arraY = [NSMutableArray array];
        for (NSString *str in names) {
            [arraY addObject:[NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",str]];
        }
        NSString *string =[arraY componentsJoinedByString:@","];
        NSLog(@"转换-%@",string);
        NSDictionary *dic = @{@"img":string,@"orderTotalId":self.orderId};
        [PPNetworkHelper POST:PostSetOrderImage parameters:dic success:^(id responseObject) {
            NSLog(@"%@",responseObject);
        } failure:^(NSError *error) {
            
        }];
        
    }];
    self.publishPhotosView.imagesMaxCountWhenWillCompose = self.photos.count;
    NSDictionary *dic = @{@"hiden":@"1"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.doneBtn.hidden = YES;
    self.editBtn.hidden =NO;
    
    
    //    self.publishPhotosView.imagesMaxCountWhenWillCompose = self.photos.count;
}
#pragma mark 编辑
-(void)editAction:(UIButton *)btn
{
    self.publishPhotosView.py_x = kWidth/34.0909;
    self.publishPhotosView.py_y = kWidth/5.9523;
    self.publishPhotosView.imagesMaxCountWhenWillCompose = 3;
    
    NSDictionary *dic = @{@"hiden":@"2"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.doneBtn.hidden = NO;
    self.editBtn.hidden =YES;
    
}
#pragma mark - PYPhotosViewDelegate
- (void)photosView:(PYPhotosView *)photosView didAddImageClickedWithImages:(NSMutableArray *)images{
    // 在这里做当点击添加图片按钮时，你想做的事。
    
    [self getPhotos];
}
// 进入预览图片时调用, 可以在此获得预览控制器，实现对导航栏的自定义
- (void)photosView:(PYPhotosView *)photosView didPreviewImagesWithPreviewControlelr:(PYPhotosPreviewController *)previewControlelr{
    NSLog(@"进入预览图片");
}
//进入相册的方法:
-(void)getPhotos{
    CCWeakSelf;
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3-weakSelf.photos.count delegate:weakSelf];
    imagePickerVc.maxImagesCount = 3;//最大照片张数,默认是0
    imagePickerVc.sortAscendingByModificationDate = NO;// 对照片排序，按修改时间升序，默认是YES。如果设置为NO,最新的照片会显示在最前面，内部的拍照按钮会排在第一个
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets,BOOL isSelectOriginalPhoto){
        NSLog(@"选中图片photos === %@",photos);
        //        for (UIImage *image in photos) {
        //            [weakSelf requestData:image];//requestData:图片上传方法 在这里就不贴出来了
        //        }
        [weakSelf.photos addObjectsFromArray:photos];
        
        NSLog(@"%ld",weakSelf.photos.count);
        [self.publishPhotosView reloadDataWithImages:weakSelf.photos];
        
        if (weakSelf.photos.count == 0) {
            self.doneBtn.hidden = YES;
        }else
        {
            self.doneBtn.hidden = NO;
        }
    }];
    [weakSelf presentViewController:imagePickerVc animated:YES completion:nil];
}
-(NSMutableArray *)photos{
    if (_photos == nil) {
        _photos = [[NSMutableArray alloc]init];
        
    }
    return _photos;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
