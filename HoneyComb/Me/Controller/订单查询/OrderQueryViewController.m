//
//  OrderQueryViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderQueryViewController.h"
#import "OrderQueryCell.h"
#import "OrderQueryDataSource.h"
#import "OrderQueryItem.h"
#import "OrderDetailOneVC.h"
#import "OrderDetailTwoVC.h"
#import "OrderDetailThreeVC.h"
@interface OrderQueryViewController ()<UITableViewDelegate>
{
    NSString *total;
}
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *timeArr;
@property (nonatomic,strong) OrderQueryDataSource *dataSource;
@property (nonatomic,assign)NSInteger page;
@end

@implementation OrderQueryViewController
-(NSMutableArray *)timeArr
{
    if (!_timeArr) {
        _timeArr = [NSMutableArray array];
    }
    return _timeArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"订单查询";
    [self setupRefresh];
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self.view addSubview:self.tableView];
   
}
#pragma mark --创建上拉加载和下拉刷新
- (void)setupRefresh {
    __block OrderQueryViewController *weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadTopics];
    }];
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadNewTopics];
    }];
}
-(void)loadTopics
{
    NSString *urlStr ;
    NSString *listName;
    if ([self.styleId isEqualToString:@"1"]) {
        urlStr =PostUserOrder ;
        listName = @"userOrderList";
    }else
    {
        urlStr = PostHomeOrder;
         listName = @"orderList";
    }
    self.page = 1;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"pageNum"] = pageStr;
    dic[@"userId"] =self.strId;
    [PPNetworkHelper POST:urlStr parameters:dic success:^(id responseObject) {
        DYLog(@"订单管理~~%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            NSMutableArray *arr = [solveJsonData changeType:[[responseObject objectForKey:@"data"]objectForKey:listName]];
            if (arr.count > 0) {
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str] ) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData ];
                }else
                {
                    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        [self loadNewTopics];
                    }];
                }
                NSMutableArray *oldArray =[NSMutableArray new];
                NSArray *array = [[responseObject objectForKey:@"data"] objectForKey:listName];
                oldArray = [array mutableCopy];
                NSMutableArray *newArr = [NSMutableArray new];
                for (NSDictionary *dd in oldArray) {
                    if ([[dd allKeys] containsObject:@"issue"]) {
                        if ([dd[@"state"] isEqualToString:@"5"]) {
                            //已派奖
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"0" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }else
                        {
                            //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"1" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }
                    }else
                    {
                        if ([dd[@"state"]isEqualToString:@"5"]) {
                            //已派奖
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"2" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }else
                        {
                            //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"3" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }
                    }
                    
                }
                self.dataSource.itemArray = [AllModel mj_objectArrayWithKeyValuesArray:newArr];
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }else
            {
                [self.dataSource.itemArray removeAllObjects];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
                [STTextHudTool showText:@"暂无数据!"];
            }
            
        }else
        {
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_header endRefreshing];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"订单管理--%@",error);
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)loadNewTopics
{  NSString *urlStr ;
    NSString *listName;
    if ([self.styleId isEqualToString:@"1"]) {
        urlStr =PostUserOrder ;
        listName = @"userOrderList";
    }else
    {
        urlStr = PostHomeOrder;
        listName = @"orderList";
    }
    self.page ++;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"pageNum"] = pageStr;
      dic[@"userId"] =self.strId;
    [PPNetworkHelper POST:urlStr parameters:dic success:^(id responseObject) {
        DYLog(@"订单管理分页~~%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            NSArray  *arr =[AllModel mj_objectArrayWithKeyValuesArray:[[responseObject objectForKey:@"data"]objectForKey:listName]];
            if (arr == nil) {
                [STTextHudTool showText:@"暂无更多数据!"];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
                
            }else
            {
                NSMutableArray *oldArray =[NSMutableArray new];
                NSArray *array = [[responseObject objectForKey:@"data"] objectForKey:listName];
                oldArray = [array mutableCopy];
                NSMutableArray *newArr = [NSMutableArray new];
                for (NSDictionary *dd in oldArray) {
                    if ([[dd allKeys] containsObject:@"issue"]) {
                        if ([dd[@"state"] isEqualToString:@"5"]) {
                            //已派奖
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"0" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }else
                        {
                            //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"1" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }
                    }else
                    {
                        if ([dd[@"state"]isEqualToString:@"5"]) {
                            //已派奖
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"2" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }else
                        {
                            //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"3" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }
                    }
                    
                }
                
                [self.dataSource.itemArray addObjectsFromArray:[AllModel mj_objectArrayWithKeyValuesArray:newArr]];
                
                [self.tableView reloadData];
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str]) {
                    
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else
                {
                    [self.tableView.mj_footer endRefreshing];
                }
            }
            
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-NavgationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self.dataSource;
    }
    return _tableView;
}
- (OrderQueryDataSource *)dataSource{
    if (!_dataSource) {
        _dataSource = [OrderQueryDataSource new];
//        _dataSource.itemArray = [self items];
    }
    return _dataSource;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AllModel *model = self.dataSource.itemArray[indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中颜色消失
    
    if (model.issue == nil || [model.type integerValue] > 6) {
        [self pushToAthDetailWithModel:model];
    }else{
        [self pushToNumberDetailWithModel:model];
    }

}

-(void)pushToAthDetailWithModel:(AllModel *)model{
    
    self.hidesBottomBarWhenPushed = YES;
    OrderDetailTwoVC *oneVC = [[OrderDetailTwoVC alloc]init];
    oneVC.orderId = model.orderTotalId;
    oneVC.typeStr = model.type;
    oneVC.state = model.state;
    
    oneVC.PopBlock = ^{
        [self loadTopics];
    };
    
    [self.navigationController pushViewController:oneVC animated:YES];
    
}

-(void)pushToNumberDetailWithModel:(AllModel *)model{
    
    self.hidesBottomBarWhenPushed = YES;
    OrderDetailOneVC *oneVC = [[OrderDetailOneVC alloc]init];
    oneVC.orderId = model.orderTotalId;
    oneVC.typeStr = model.type;
    
    @WeakObj(self)
    oneVC.popBlock = ^{
        @StrongObj(self)
        [self loadTopics];
    };
    [self.navigationController pushViewController:oneVC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class cls = [self.dataSource cellClassAtIndexPath:indexPath];
    return [cls tableView:tableView rowHeightForObject:self.dataSource.itemArray[indexPath.row]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
