//
//  OrderDetailOneVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderDetailOneVC.h"
#import "OrderNumberCell.h"
#import "OSSImageUploader.h"

#import "FC_NumberLottoryDetailModel.h"
#import "FC_OrderQueryHeadView.h"
#import "FC_OrderQueryOrderinfoView.h"
#import "FC_NumberLottoryBettingView.h"
#import "FC_NumberLottoryDetailPhotoView.h"

#import "AF_OddsNoteView.h"

///弱引用/强引用
#define CCWeakSelf __weak typeof(self) weakSelf = self;

@interface OrderDetailOneVC ()<TZImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PYPhotosViewDelegate,FC_DetailPhotoViewDelegate>
{
    NSDictionary *mDic;
    CGFloat   headHeight;
    CGFloat   orderInfoHeight;
    CGFloat   orderContentHeight;
    CGFloat   orderPhotoHeight;
    
    CGFloat   contentScrollBottom;
    
    CGFloat   noteHeight;
}

@property(nonatomic,strong)NSMutableArray *photos;

@property (nonatomic,strong) UIButton *takeBtn;//
@property (nonatomic,strong) UIButton *hairBtn;//
@property (nonatomic,strong) UIButton *outBtn;//

@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;

@property (nonatomic,strong) FC_NumberLottoryDetailModel * detailModel;
@property (nonatomic,strong) UIScrollView * contentScrollView;
@property (nonatomic,strong) FC_OrderQueryHeadView * headView;
@property (nonatomic,strong) FC_OrderQueryOrderinfoView * orderInfoView;
@property (nonatomic,strong) FC_NumberLottoryBettingView *orderContentView;
@property (nonatomic,strong) FC_NumberLottoryDetailPhotoView *orderPhotoView;

@property (nonatomic,assign) BOOL isClicking;

@property (nonatomic,strong) AF_OddsNoteView * noteView;

@end

@implementation OrderDetailOneVC
static NSString *kTempFolder = @"shop/order";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabe.text = @"订单查询";
    
    noteHeight = 40;
    headHeight = 115;
    orderInfoHeight = 40;
    orderContentHeight = 40;
    orderPhotoHeight = 130;
    contentScrollBottom = IPHONE_X?TAB_BAR_SAFE_HEIGHT:0;
    
    self.isClicking = NO;
    
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
    
    [self creatToken];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(deleteAction:) name:@"deleteTongzhi" object:nil];
    [self creatData];
}
-(void)deleteAction:(NSNotification *)dic
{
    self.orderPhotoView.doneButton.hidden = YES;
}

#pragma mark  --  lazy  --

-(UIScrollView *)contentScrollView{
    
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc]init];
        [self.view addSubview:_contentScrollView];
        
        [_contentScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(-contentScrollBottom);
        }];
        
        self.noteView.backgroundColor = [UIColor colorWithHexString:@"#FFEEEC"];
        
    }
    return _contentScrollView;
}

-(AF_OddsNoteView *)noteView{
    
    if (!_noteView) {
        _noteView = [[AF_OddsNoteView alloc]init];
        [self.contentScrollView addSubview:_noteView];
        
        [_noteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kWidth);
            make.height.mas_equalTo(noteHeight);
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(0);
            
        }];
        
    }
    return _noteView;
}

-(FC_OrderQueryHeadView *)headView{
    
    if (!_headView) {
        _headView = [[FC_OrderQueryHeadView alloc]init];
        [self.contentScrollView addSubview:_headView];
        
        [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kWidth, headHeight));
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(noteHeight);
        }];
    }
    return _headView;
}

-(FC_OrderQueryOrderinfoView *)orderInfoView{
    
    if (!_orderInfoView) {
        _orderInfoView = [[FC_OrderQueryOrderinfoView alloc]init];
        [self.contentScrollView addSubview:_orderInfoView];
        
        [_orderInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kWidth, orderInfoHeight));
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(self.headView.mas_bottom).offset(kTopMar);
        }];
    }
    return _orderInfoView;
    
}

-(FC_NumberLottoryBettingView *)orderContentView{
    
    if (!_orderContentView) {
        _orderContentView = [[FC_NumberLottoryBettingView alloc]initWithFrame:CGRectZero type:[self.typeStr integerValue]];
        [self.contentScrollView addSubview:_orderContentView];
        
        [_orderContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kWidth, orderContentHeight));
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(self.orderInfoView.mas_bottom).offset(kTopMar);
        }];
    }
    return _orderContentView;
}

-(FC_NumberLottoryDetailPhotoView *)orderPhotoView{
    
    if (!_orderPhotoView) {
        _orderPhotoView = [[FC_NumberLottoryDetailPhotoView alloc]init];
        _orderPhotoView.delegate = self;
        _orderPhotoView.photoView.delegate = self;
        [self.contentScrollView addSubview:_orderPhotoView];
        
        [_orderPhotoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kWidth, orderPhotoHeight));
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(self.orderContentView.mas_bottom).offset(kTopMar);
        }];
    }
    return _orderPhotoView;
}

#pragma mark  -- detail photo view delegate  --

-(void)done{
    
    CGFloat  leftMar = 12,topMar = 40;
    
    self.orderPhotoView.photoView.py_x = leftMar;
    self.orderPhotoView.photoView.py_y = topMar;
    
    NSArray *arrAll = self.photos;
    NSLog(@"-%@",arrAll);
    
    [OSSImageUploader asyncUploadImages:arrAll folder:kTempFolder accessKey:self.accessKey secretKey:self.accessKeySecret token:self.tokenStr complete:^(NSArray<NSString *> *names, UploadImageState state) {
        NSLog(@"彩票照片-%@",names);
        
        NSMutableArray *arraY = [NSMutableArray array];
        for (NSString *str in names) {
            [arraY addObject:[NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",str]];
        }
        NSString *string =[arraY componentsJoinedByString:@","];
        NSLog(@"转换-%@",string);
        NSDictionary *dic = @{@"img":string,@"orderTotalId":self.orderId};
        [PPNetworkHelper POST:PostSetOrderImage parameters:dic success:^(id responseObject) {
            NSLog(@"%@",responseObject);
        } failure:^(NSError *error) {
            
        }];
        
    }];
    self.orderPhotoView.photoView.imagesMaxCountWhenWillCompose = self.photos.count;
    NSDictionary *dic = @{@"hiden":@"1"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.orderPhotoView.doneButton.hidden = YES;
    self.orderPhotoView.editButton.hidden =NO;
    
}

-(void)edit{
    
    CGFloat  leftMar = 12,topMar = 40;
    
    self.orderPhotoView.photoView.py_x = leftMar;
    self.orderPhotoView.photoView.py_y = topMar;
    self.orderPhotoView.photoView.imagesMaxCountWhenWillCompose = 3;
    
    NSDictionary *dic = @{@"hiden":@"2"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.orderPhotoView.doneButton.hidden = NO;
    self.orderPhotoView.editButton.hidden = YES;
    
}

#pragma mark --- PYPhotosViewDelegate  ---

- (void)photosView:(PYPhotosView *)photosView didAddImageClickedWithImages:(NSMutableArray *)images{
    // 在这里做当点击添加图片按钮时，你想做的事。
    
    [self getPhotos];
}
// 进入预览图片时调用, 可以在此获得预览控制器，实现对导航栏的自定义
- (void)photosView:(PYPhotosView *)photosView didPreviewImagesWithPreviewControlelr:(PYPhotosPreviewController *)previewControlelr{
    NSLog(@"进入预览图片");
}
//进入相册的方法:
-(void)getPhotos{
    CCWeakSelf;
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3-weakSelf.photos.count delegate:weakSelf];
    imagePickerVc.maxImagesCount = 3;//最大照片张数,默认是0
    imagePickerVc.sortAscendingByModificationDate = NO;// 对照片排序，按修改时间升序，默认是YES。如果设置为NO,最新的照片会显示在最前面，内部的拍照按钮会排在第一个
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets,BOOL isSelectOriginalPhoto){
        NSLog(@"选中图片photos === %@",photos);
        //        for (UIImage *image in photos) {
        //            [weakSelf requestData:image];//requestData:图片上传方法 在这里就不贴出来了
        //        }
        [weakSelf.photos addObjectsFromArray:photos];
        if (weakSelf.photos.count == 0) {
            self.orderPhotoView.doneButton.hidden = YES;
        }else{
            self.orderPhotoView.doneButton.hidden = NO;
        }
        NSLog(@"%ld",weakSelf.photos.count);
        [self.orderPhotoView.photoView reloadDataWithImages:weakSelf.photos];
    }];
    [weakSelf presentViewController:imagePickerVc animated:YES completion:nil];
}

-(NSMutableArray *)photos{
    if (_photos == nil) {
        _photos = [[NSMutableArray alloc]init];
        
    }
    return _photos;
}

-(void)creatToken
{
    [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {
        //        NSLog(@"--%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
            self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
            self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
        }
        
    } failure:^(NSError *error) {
    }];
    
}
-(void)creatData
{
    NSDictionary *dic = @{@"type":self.typeStr,@"orderTotalId":self.orderId};
    [PPNetworkHelper POST:PostNumberOrder parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            self.detailModel = [FC_NumberLottoryDetailModel mj_objectWithKeyValues:responseObject[@"data"]];
            
            [self showButton];
            
            // head view
            [self.headView reloadWithModel:self.detailModel];
            
            // orderInfoView
            [self reloadOrderView];
            
            // orderContentView
            orderContentHeight = (self.detailModel.detailList.count + 1) * 40;
            [self.orderContentView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(orderContentHeight);
            }];
            [self.orderContentView reloadBettingWithModel:self.detailModel];
            
            // photo view
            
            if ([self.detailModel.image isEqualToString:@""] || [self.detailModel.image isEqualToString:@"-"]) {
                self.orderPhotoView.doneButton.hidden = YES;
            }else{
                self.orderPhotoView.editButton.hidden = YES;
                self.orderPhotoView.doneButton.hidden = YES;
                NSString * imageStr = self.detailModel.image;
                if ([self.detailModel.image hasSuffix:@","]) {
                    imageStr = [self.detailModel.image substringToIndex:self.detailModel.image.length - 1];
                }
                NSArray *arr = [imageStr componentsSeparatedByString:@","];
                self.photos = [NSMutableArray arrayWithArray:arr];
                self.orderPhotoView.photoView.originalUrls = self.photos;    //  加载网络图片
            }
            
            
            CGFloat  contentHeight = kHeight - (NAVIGATION_HEIGHT + TabBarHeight);
            if (contentHeight < noteHeight + headHeight + orderInfoHeight + orderContentHeight + orderPhotoHeight + kTopMar * 3) {
                contentHeight = noteHeight + headHeight + orderInfoHeight + orderContentHeight + orderPhotoHeight + kTopMar * 3;
            }
            [self.contentScrollView setContentSize:CGSizeMake(kWidth, contentHeight)];
            
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)showButton{

    contentScrollBottom = 0;

    switch ([self.detailModel.state integerValue]) {
        case 1:
            contentScrollBottom = IPHONE_X?TabBarHeight:60;
            self.takeBtn.hidden = NO;
            self.hairBtn.hidden = NO;
            [self.takeBtn setTitle:@"撤单" forState:(UIControlStateNormal)];
            [self.hairBtn setTitle:@"出票" forState:(UIControlStateNormal)];
            break;
        case 3:
            contentScrollBottom = IPHONE_X?TabBarHeight:60;
            self.takeBtn.hidden = NO;
            self.hairBtn.hidden = NO;
            [self.takeBtn setTitle:@"取票" forState:(UIControlStateNormal)];
            [self.hairBtn setTitle:@"派奖" forState:(UIControlStateNormal)];
            break;
        case 2:case 4:case 7:
            contentScrollBottom = IPHONE_X?TabBarHeight:60;
            self.outBtn.hidden = NO;
            break;
        default:
            break;
     
    }
    
    [self.contentScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-contentScrollBottom);
    }];

}

-(void)reloadOrderView{
    
    NSMutableDictionary * items = [NSMutableDictionary new];
    
    if ([self isItemWith:self.detailModel.nickName]) {
        [items setValue:self.detailModel.nickName forKey:@"用 户 名  : "];
    }
    
    if ([self isItemWith:self.detailModel.mobile]) {
        [items setValue:self.detailModel.mobile forKey:@"手 机 号  : "];
    }
    
    if ([self isItemWith:self.detailModel.orderNumber]) {
        [items setValue:self.detailModel.orderNumber forKey:@"订单编号 : "];
    }
    
    if ([self isItemWith:self.detailModel.createDate]) {
        [items setValue:self.detailModel.createDate forKey:@"购买时间 : "];
    }
    if ([self isItemWith:self.detailModel.printDate]) {
        [items setValue:self.detailModel.printDate forKey:@"出票时间 : "];
    }
    if ([self isItemWith:self.detailModel.times] && [self isItemWith:self.detailModel.account]) {
        [items setValue:[NSString stringWithFormat:@"%@注%@倍",self.detailModel.account,self.detailModel.times] forKey:@"玩      法 : "];
    }
    if ([self isItemWith:self.detailModel.result]) {
        [items setValue:self.detailModel.result forKey:@"开奖号码 : "];
    }
    
    orderInfoHeight = 45 + items.count * 30;
    [self.orderInfoView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(orderInfoHeight);
    }];
    [self.orderInfoView reloadOrderQueryInfoWithModel:self.detailModel];
    
}

-(BOOL)isItemWith:(NSString *)item{
    
    if ([item isEqualToString:@""] || !item) {
        return NO;
    }
    if ([item isEqualToString:@"-"]) {
        return  NO;
    }
    
    return  YES;
}

-(UIButton *)takeBtn{
    
    CGFloat  leftMar = 12,centerMar = 15,bottom = IPHONE_X?TAB_BAR_SAFE_HEIGHT:10,height = 40,width = (kWidth - leftMar * 2 - centerMar)/2;
    
    if (!_takeBtn) {
        _takeBtn = [[UIButton alloc]init];
        [self.view addSubview:_takeBtn];
        [self.takeBtn setTitle:@"取票" forState:(UIControlStateNormal)];
        [self.takeBtn addTarget:self action:@selector(takeAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self updateButton:_takeBtn];
        [self.takeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.left.mas_equalTo(leftMar);
            make.bottom.mas_equalTo(-bottom);
        }];
    }
    return _takeBtn;
}

-(UIButton *)hairBtn{
    
    CGFloat  leftMar = 12,centerMar = 15,bottom = IPHONE_X?TAB_BAR_SAFE_HEIGHT:10,height = 40,width = (kWidth - leftMar * 2 - centerMar)/2;
    
    if (!_hairBtn) {
        _hairBtn = [[UIButton alloc]init];
        [self.view addSubview:_hairBtn];
        [_hairBtn setTitle:@"派奖" forState:(UIControlStateNormal)];
        
        [_hairBtn setBackgroundColor:kRedColor];
        [_hairBtn addTarget:self action:@selector(hairAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self updateButton:_hairBtn];
        [_hairBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [_hairBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.right.mas_equalTo(-leftMar);
            make.bottom.mas_equalTo(-bottom);
        }];
    }
    return _hairBtn;
}

-(void)updateButton:(UIButton *)button{
    
    [button setTitleColor:kRedColor forState:(UIControlStateNormal)];
    button.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    button.layer.cornerRadius =4;
    button.layer.masksToBounds = YES;
    
    //边框宽度
    [button.layer setBorderWidth:1.0];
    button.layer.borderColor = RGB(231, 31, 25, 0.5).CGColor;
}

-(void)layoutView
{
    
    CGFloat  leftMar = 12,width = kWidth - leftMar * 2,height = 40,bottom = IPHONE_X?TAB_BAR_SAFE_HEIGHT:10;
    self.outBtn = [[UIButton alloc]init];
    [self.view addSubview:self.outBtn];
    self.outBtn.hidden = YES;
    [self.outBtn addTarget:self action:@selector(outAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.outBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.outBtn setTitle:@"取票" forState:(UIControlStateNormal)];
    self.outBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    self.outBtn.layer.cornerRadius =4;
    self.outBtn.layer.masksToBounds = YES;
    self.outBtn.backgroundColor = kRedColor;
    [self.outBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
        make.height.mas_equalTo(height);
        make.left.mas_equalTo(leftMar);
        make.bottom.mas_equalTo(-bottom);
    }];
}

#pragma mark    --  state button action  --

-(void)takeAction:(UIButton *)btn
{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        @WeakObj(self)
        if ([btn.titleLabel.text isEqualToString:@"撤单"]) {
            
            NSDictionary *dic = @{
                                  @"orderTotalId":self.orderId
                                  };
            
            [PPNetworkHelper POST:PostCancleOrder parameters:dic success:^(id responseObject) {
                
                @StrongObj(self)
                if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                    
                    if (self.popBlock) {
                        self.popBlock();
                    }
                    [STTextHudTool showText:@"订单撤销成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else
                {
                    [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                }
                self.isClicking = NO;
            } failure:^(NSError *error) {
                @StrongObj(self)
                [STTextHudTool showText:@"订单撤销失败"];
                self.isClicking = NO;
            }];
        }else if ([btn.titleLabel.text isEqualToString:@"取票"]){
            [self showAlertViewController];
        }
    }
    
}

-(void)showAlertViewController{
        
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确认取票" message:@"确认彩民已到店,取走彩票!" preferredStyle:UIAlertControllerStyleAlert];
    @WeakObj(self)
    UIAlertAction *savePhotoAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @StrongObj(self)
        
        [self takeOutTicketRequest];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:savePhotoAction];
    
    
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
 
}

#pragma mark 派奖/出票
-(void)hairAction:(UIButton *)btn
{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        @WeakObj(self)
        if ([btn.titleLabel.text isEqualToString:@"派奖"]) {
            
            NSDictionary *diccc = @{
                                    @"orderTotalId":self.orderId,@"type":@"5"
                                    };
            
            [PPNetworkHelper POST:PostSendPrice parameters:diccc success:^(id responseObject) {
                
                @StrongObj(self)
                if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                    if (self.popBlock) {
                        self.popBlock();
                    }
                    [STTextHudTool showText:@"派奖成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else
                {
                    [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                }
                self.isClicking = NO;
            } failure:^(NSError *error) {
                @StrongObj(self)
                self.isClicking = NO;
                [STTextHudTool showText:@"派奖失败"];
            }];
        }
        else
        {
            NSDictionary *dic = @{
                                  @"type":self.typeStr,@"orderTotalId":self.orderId
                                  };
            
            [PPNetworkHelper POST:PostPrintOrder parameters:dic success:^(id responseObject) {
                
                @StrongObj(self)
                if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                    if (self.popBlock) {
                        self.popBlock();
                    }
                    [STTextHudTool showText:@"出票成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else
                {
                    [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                }
                self.isClicking = NO;
            } failure:^(NSError *error) {
                @StrongObj(self)
                [STTextHudTool showText:@"出票失败"];
                self.isClicking = NO;
            }];
        }
        
    }
}
#pragma mark 取票
-(void)outAction:(UIButton *)btn
{
    
    [self showAlertViewController];
    
}

-(void)takeOutTicketRequest{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        @WeakObj(self)
        
        NSDictionary *dic = @{
                              @"orderTotalId":self.orderId
                              };
        [PPNetworkHelper POST:PostTakeOrder parameters:dic success:^(id responseObject) {
            NSLog(@"取票-%@",responseObject);
            
            @StrongObj(self)
            if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                if (self.popBlock) {
                    self.popBlock();
                }
                [STTextHudTool showText:@"取票成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else
            {
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
            self.isClicking = NO;
        } failure:^(NSError *error) {
            @StrongObj(self)
            [STTextHudTool showText:@"取票失败"];
            self.isClicking = NO;
        }];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
