//
//  OrderDetailThreeVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderDetailThreeVC.h"
#import "OrderDetailThreeCell.h"
@interface OrderDetailThreeVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UILabel *nameLabel;//彩种名称
@property (nonatomic,strong) UILabel *expectLabel;//预计奖金
@property (nonatomic,strong) UILabel *stateLabel;;//状态
@property (nonatomic,strong) UILabel *priceLabel;//投注金额
@property (nonatomic,strong) UILabel *phoneLabel;//手机号码
@property (nonatomic,strong) UILabel *numberLabel;//编号
@property (nonatomic,strong) UILabel *payDateLabel;//购买时间
@property (nonatomic,strong) UILabel *outDateLabel;//出票时间
@property (nonatomic,strong) UILabel *playLabel;//玩法
@property (nonatomic,strong) UIButton *takeBtn;//取票
@property (nonatomic,strong) UIButton *hairBtn;//派奖
@property (nonatomic,strong) UIImageView *photoImage;
@end

@implementation OrderDetailThreeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"订单查询";
    self.view.backgroundColor =[UIColor whiteColor];
    [self layoutView];
}
-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-NavgationBarHeight-60) style:(UITableViewStyleGrouped)];
    self.tableView.dataSource =self;
    self.tableView.delegate =self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[OrderDetailThreeCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
    
    self.takeBtn = [[UIButton alloc]init];
    [self.view addSubview:self.takeBtn];
    [self.takeBtn setTitleColor:kRedColor forState:(UIControlStateNormal)];
    [self.takeBtn setTitle:@"取票" forState:(UIControlStateNormal)];
    self.takeBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    self.takeBtn.layer.cornerRadius =4;
    self.takeBtn.layer.masksToBounds = YES;
    //边框宽度
    [self.takeBtn.layer setBorderWidth:1.0];
    self.takeBtn.layer.borderColor=RGB(231, 31, 25, 0.5).CGColor;
    [self.takeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(168);
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(12);
        make.bottom.mas_equalTo(-15);
    }];

    self.hairBtn = [[UIButton alloc]init];
    [self.view addSubview:self.hairBtn];
    [self.hairBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.hairBtn setTitle:@"派奖" forState:(UIControlStateNormal)];
    self.hairBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    self.hairBtn.layer.cornerRadius =4;
    self.hairBtn.layer.masksToBounds = YES;
    self.hairBtn.backgroundColor = kRedColor;
    [self.hairBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(168);
        make.height.mas_equalTo(40);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(-15);
    }];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 368)];
    view.backgroundColor = [UIColor whiteColor];
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, 15, kWidth-24, 17)];
    self.nameLabel.text = @"排列三 | 第2018123期";
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    self.nameLabel.textColor = kBlackColor;
    [view addSubview:self.nameLabel];
    
    self.expectLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.nameLabel.frame)+28, kWidth/3, 17)];
    self.expectLabel.text = @"0";
    self.expectLabel.textAlignment = NSTextAlignmentCenter;
    self.expectLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    self.expectLabel.textColor = kRedColor;
    [view addSubview:self.expectLabel];
    
    self.stateLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/3, CGRectGetMaxY(self.nameLabel.frame)+28, kWidth/3, 17)];
    self.stateLabel.text = @"未中奖";
    self.stateLabel.textAlignment = NSTextAlignmentCenter;
    self.stateLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    self.stateLabel.textColor = kBlackColor;
    [view addSubview:self.stateLabel];
    
    self.priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.stateLabel.frame), CGRectGetMaxY(self.nameLabel.frame)+28, kWidth/3, 17)];
    self.priceLabel.text = @"2.00";
    self.priceLabel.textAlignment = NSTextAlignmentCenter;
    self.priceLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    self.priceLabel.textColor = kRedColor;
    [view addSubview:self.priceLabel];
    
    NSArray *arrPrice = @[@"预计奖金(元)",@"订单状态",@"投注金额(元)"];
    for (int i = 0; i<arrPrice.count; i++) {
        UILabel *titleL = [[UILabel alloc]initWithFrame:
                           CGRectMake((kWidth/3)*(i%3), CGRectGetMaxY(self.stateLabel.frame)+12, kWidth/3, 13)];
        titleL.text = arrPrice[i];
        titleL.textColor = kGrayColor;
        titleL.textAlignment = NSTextAlignmentCenter;
        titleL.font = [UIFont fontWithName:kPingFangRegular size:12];
        [view addSubview:titleL];
    }
    UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.stateLabel.frame)+43, kWidth, 15)];
    grayView.backgroundColor = RGB(0, 0, 0, 0.1);
    [view addSubview:grayView];
    
    UILabel *orderLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(grayView.frame)+15, kWidth/2, 17)];
    orderLabel.textAlignment = NSTextAlignmentLeft;
    orderLabel.text = @"订单信息";
    orderLabel.textColor = kBlackColor;
    orderLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    [view addSubview:orderLabel];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(orderLabel.frame)+12, 60, 15)];
    name.textAlignment = NSTextAlignmentCenter;
    name.text = @"用  户 名:";
    name.textColor = kGrayColor;
    name.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:name];
    
    self.phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(name.frame)+10, CGRectGetMaxY(orderLabel.frame)+12, kWidth-10-24-60, 15)];
    self.phoneLabel.textColor = kGrayColor;
    self.phoneLabel.text = @"13012345678 (长按拨打)";
    self.phoneLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:self.phoneLabel];
    
    UILabel *num = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(name.frame)+12, 60, 15)];
    num.textAlignment = NSTextAlignmentCenter;
    num.text = @"订单编号:";
    num.textColor = kGrayColor;
    num.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:num];
    
    self.numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(num.frame)+10, CGRectGetMaxY(name.frame)+12, kWidth-10-24-60, 15)];
    self.numberLabel.textColor = kGrayColor;
    self.numberLabel.text = @"TE201812931929418521541251";
    self.numberLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:self.numberLabel];
    
    UILabel *payDate = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(num.frame)+12, 60, 15)];
    payDate.textAlignment = NSTextAlignmentCenter;
    payDate.text = @"购买时间:";
    payDate.textColor = kGrayColor;
    payDate.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:payDate];
    
    self.payDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(payDate.frame)+10, CGRectGetMaxY(num.frame)+12, kWidth-10-24-60, 15)];
    self.payDateLabel.textColor = kGrayColor;
    self.payDateLabel.text = @"2018-08-01 19:20:22";
    self.payDateLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:self.payDateLabel];
    
    UILabel *outDate = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(payDate.frame)+12, 60, 15)];
    outDate.textAlignment = NSTextAlignmentCenter;
    outDate.text = @"出票时间:";
    outDate.textColor = kGrayColor;
    outDate.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:outDate];
    
    self.outDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(outDate.frame)+10, CGRectGetMaxY(payDate.frame)+12, kWidth-10-24-60, 15)];
    self.outDateLabel.textColor = kGrayColor;
    self.outDateLabel.text = @"2018-08-01 19:20:22";
    self.outDateLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:self.outDateLabel];
    
    UILabel *playStr = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(outDate.frame)+12, 60, 15)];
    playStr.textAlignment = NSTextAlignmentCenter;
    playStr.text = @"玩      法:";
    playStr.textColor = kGrayColor;
    playStr.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:playStr];
    
    self.playLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(playStr.frame)+10, CGRectGetMaxY(outDate.frame)+12, kWidth-10-24-60, 15)];
    self.playLabel.textColor = kGrayColor;
    self.playLabel.text = @"1注1倍";
    self.playLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [view addSubview:self.playLabel];
    
    
    
    UIView *grayView1 = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.playLabel.frame)+13, kWidth, 15)];
    grayView1.backgroundColor = RGB(0, 0, 0, 0.1);
    [view addSubview:grayView1];
    
    UILabel *conLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(grayView1.frame)+15, kWidth/2, 17)];
    conLabel.textAlignment = NSTextAlignmentLeft;
    conLabel.text = @"订单内容";
    conLabel.textColor = kBlackColor;
    conLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    [view addSubview:conLabel];
    
    return view;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 135)];
    view.backgroundColor = [UIColor whiteColor];
    UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 2, kWidth, 15)];
    grayView.backgroundColor = RGB(0, 0, 0, 0.1);
    [self.tableView bringSubviewToFront:grayView];
    [view addSubview:grayView];
    
    UILabel *conLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(grayView.frame)+15, kWidth/2, 17)];
    conLabel.textAlignment = NSTextAlignmentLeft;
    conLabel.text = @"彩票照片";
    conLabel.textColor = kBlackColor;
    conLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    [view addSubview:conLabel];
    
    self.photoImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, CGRectGetMaxY(conLabel.frame)+15, 115, 73)];
    self.photoImage.backgroundColor = kRedColor;
    [view addSubview:self.photoImage];
    return view;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSLog(@"---%ld",(long)indexPath.row);
    
    cell.selectedBackgroundView.backgroundColor =RGB(0, 0, 0, 0.5);
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 368;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 155;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
