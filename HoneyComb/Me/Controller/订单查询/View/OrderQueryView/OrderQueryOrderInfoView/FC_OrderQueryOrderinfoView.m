//
//  FC_OrderQueryOrderinfoView.m
//  HoneyComb
//
//  Created by afc on 2018/11/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_OrderQueryOrderinfoView.h"

#import "FC_NumberLottoryDetailModel.h"

@interface FC_OrderQueryOrderinfoView ()
{
    CGFloat  titleLabelHeight;
    NSString * phoneNumber;
}

@property (nonatomic,copy) NSArray * titles;

@end

@implementation FC_OrderQueryOrderinfoView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self == [super initWithFrame:frame]) {
        
        titleLabelHeight = 40;
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self noteLabelSetup];
    
    phoneNumber = nil;
    
}

#pragma mark  --  lazy  --

-(UIView *)backView{
    
    if (!_backView) {
        
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 8.0f;
        [self addSubview:_backView];
        
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
        
    }
    return _backView;
}

-(void)reloadLabel:(UILabel *)label withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    
    label.textColor = textColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:fontSize];
    label.text = @"-";
    label.textAlignment = NSTextAlignmentCenter;
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"用 户 名  : ",@"手 机 号  : ",@"订单编号 : ",@"购买时间 : ",@"出票时间 : ",@"玩      法 : ",@"开奖号码 : "];
    }
    return _titles;
}

-(void)noteLabelSetup{
    
    CGFloat  labelHeight = 45,leftMar = 12;
    
    UILabel * label = [[UILabel alloc]init];
    [self reloadLabel:label withFontSize:17 textColor:RGB(51, 51, 51, 1.0f)];
    label.text = @"订单信息";
    label.textAlignment = NSTextAlignmentLeft;
    label.userInteractionEnabled = YES;
    [self.backView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(labelHeight);
        make.left.mas_equalTo(leftMar);
        make.top.mas_equalTo(0);
    }];
    
}

#pragma mark  --  reload info  --

-(void)reloadOrderQueryInfoWithModel:(FC_NumberLottoryDetailModel *)model{
    
    UIView * view = [self viewWithTag:-1234];
    [view removeFromSuperview];
    
    NSMutableArray * itemArr = [self getItemArrayWithModel:model];
    
    CGFloat  itemHeight = 30,leftMar = 12;
    
    for (int i = 0; i < itemArr.count; i ++) {
        UILabel * label = [[UILabel alloc]init];
        [self reloadLabel:label withFontSize:14 textColor:RGB(153, 153, 153, 1.0f)];
        label.tag = -1234;

        NSMutableDictionary * dic = [itemArr objectAtIndex:i];
        NSString * key = [dic.allKeys firstObject];
        NSString * value = [dic valueForKey:key];
        
        if ([key isEqualToString:@"手 机 号  : "]) {
            label.text = [NSString stringWithFormat:@"%@ %@(长按拨打)",key,value];
            
            label.userInteractionEnabled = YES;
            phoneNumber = value;
            UILongPressGestureRecognizer * longGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(callAction:)];
            [label addGestureRecognizer:longGesture];
            
        }else{
            label.text = [NSString stringWithFormat:@"%@ %@",key,value];
            if ([key isEqualToString:@"开奖号码 : "]) {
                NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc]initWithString:label.text];
                NSRange subRange = [label.text rangeOfString:value];
                [attributeString addAttribute:NSForegroundColorAttributeName value:RGB(253, 37, 37, 1.0f) range:subRange];
                
                if ([model.playName containsString:@"大乐透"]) {
                    NSArray * array = [model.result componentsSeparatedByString:@"|"];
                    NSRange blueRange = [label.text rangeOfString:[array lastObject]];
                    [attributeString addAttribute:NSForegroundColorAttributeName value:RGB(17, 58, 235, 1.0f) range:blueRange];
                }
                
                label.attributedText = attributeString;
            }
        }
         label.userInteractionEnabled = YES;
        label.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(itemHeight);
            make.left.mas_equalTo(leftMar);
            make.top.mas_equalTo(titleLabelHeight + itemHeight * i);
        }];
    }
    
}

-(void)callAction:(UILongPressGestureRecognizer *)longGesture{
    
    if (longGesture.state == UIGestureRecognizerStateBegan) {
        
        if (phoneNumber) {
            NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",phoneNumber];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
       
    }
    
}

-(void)tapAction{
    
    NSLog(@"tapAction");
    
}

-(NSMutableArray *)getItemArrayWithModel:(FC_NumberLottoryDetailModel *)model{
    
    NSMutableArray * itemArr = [NSMutableArray new];
    
    if ([self isItemWith:model.nickName]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.nickName forKey:@"用 户 名  : "];
        [itemArr addObject:dic];
    }
    
    if ([self isItemWith:model.mobile]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.mobile forKey:@"手 机 号  : "];
        [itemArr addObject:dic];
    }
    
    if ([self isItemWith:model.orderNumber]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.orderNumber forKey:@"订单编号 : "];
        [itemArr addObject:dic];
    }
    
    if ([self isItemWith:model.createDate]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.createDate forKey:@"购买时间 : "];
        [itemArr addObject:dic];
    }
    if ([self isItemWith:model.printDate]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.printDate forKey:@"出票时间 : "];
        [itemArr addObject:dic];
    }
    if ([self isItemWith:model.times] && [self isItemWith:model.account]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:[NSString stringWithFormat:@"%@ 注 %@ 倍",model.account,model.times] forKey:@"玩      法 : "];
        [itemArr addObject:dic];
    }
    if ([self isItemWith:model.result]) {
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:model.result forKey:@"开奖号码 : "];
        [itemArr addObject:dic];
    }
    
    return itemArr;
    
}

-(BOOL)isItemWith:(NSString *)item{
    
    if ([item isEqualToString:@""] || !item) {
        return NO;
    }
    if ([item isEqualToString:@"-"]) {
        return  NO;
    }
    
    return  YES;
}


@end
