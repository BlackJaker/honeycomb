//
//  FC_OrderQueryHeadView.m
//  HoneyComb
//
//  Created by afc on 2018/11/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_OrderQueryHeadView.h"

#import "FC_NumberLottoryDetailModel.h"

@interface FC_OrderQueryHeadView ()

@property (nonatomic,copy) NSMutableArray * titleLabels;

@end

@implementation FC_OrderQueryHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(NSMutableArray *)titleLabels{
    
    if (!_titleLabels) {
        _titleLabels = [NSMutableArray new];
    }
    return _titleLabels;
}

-(void)contentSetup{
    
    for (UILabel * label in self.titleLabels) {
        [label removeFromSuperview];
    }
    [self.titleLabels removeAllObjects];
    
    NSArray * notes = @[@"预计奖金(元)",@"订单状态",@"投注金额(元)"];
    CGFloat  width = kWidth/notes.count,height = 14,bottom = 15;
    
    for (int i = 0; i < notes.count; i ++) {
        UILabel * label = [[UILabel alloc]init];
        label.tag = 111;
        label.text = [notes objectAtIndex:i];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor =RGB(153, 153, 153, 1.0f);
        label.font = [UIFont fontWithName:kPingFangRegular size:height];
        [self addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(width * i);
            make.bottom.mas_equalTo(-bottom);
        }];
        
        [self.titleLabels addObject:label];
    }
    
    self.expectBonusesLabel.text = @"0.00";
    self.orderStateLabel.text = @"-";
    self.betMoneyLabel.text = @"0.00";
    
}

#pragma mark  --  lazy  --

-(UILabel *)nameLabel{
    
    CGFloat  topMar = 15,leftMar = 12,height = 17;
    
    if (!_nameLabel) {
        
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = RGB(51, 51, 51, 1.0f);
        _nameLabel.font = [UIFont fontWithName:kPingFangRegular size:height];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(DEFAULT_WIDTH, height));
            make.left.mas_equalTo(leftMar);
            make.top.mas_equalTo(topMar);
        }];
        
    }
    return _nameLabel;
}

-(UILabel *)expectBonusesLabel{
    
    CGFloat  bottomMar = 40,width = kWidth/3,height = 15;
    
    if (!_expectBonusesLabel) {
        
        _expectBonusesLabel = [[UILabel alloc]init];
        _expectBonusesLabel.textColor = RGB(253, 37, 37, 1.0f);
        _expectBonusesLabel.textAlignment = NSTextAlignmentCenter;
        _expectBonusesLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
        [self addSubview:_expectBonusesLabel];
        
        [_expectBonusesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(-bottomMar);
        }];
        
    }
    return _expectBonusesLabel;
}

-(UILabel *)orderStateLabel{
    
    CGFloat  bottomMar = 40,width = kWidth/3,height = 15;
    
    if (!_orderStateLabel) {
        
        _orderStateLabel = [[UILabel alloc]init];
        _orderStateLabel.textColor = RGB(51, 51, 51, 1.0f);
        _orderStateLabel.textAlignment = NSTextAlignmentCenter;
        _orderStateLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
        [self addSubview:_orderStateLabel];
        
        [_orderStateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(width);
            make.bottom.mas_equalTo(-bottomMar);
        }];
        
    }
    return _orderStateLabel;
}

-(UILabel *)betMoneyLabel{
    
    CGFloat  bottomMar = 40,width = kWidth/3,height = 15;
    
    if (!_betMoneyLabel) {
        
        _betMoneyLabel = [[UILabel alloc]init];
        _betMoneyLabel.textColor = RGB(253, 37, 37, 1.0f);
        _betMoneyLabel.textAlignment = NSTextAlignmentCenter;
        _betMoneyLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
        [self addSubview:_betMoneyLabel];
        
        [_betMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(width * 2);
            make.bottom.mas_equalTo(-bottomMar);
        }];
        
    }
    return _betMoneyLabel;
}

#pragma mark  --  reload  --

-(void)reloadWithModel:(FC_NumberLottoryDetailModel *)model{
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@ | 第%@期",model.playName,model.issue];
    
    self.expectBonusesLabel.text = model.award;
    self.betMoneyLabel.text = model.price;
    
    self.orderStateLabel.text = [NSString stateStringWithType:[model.state integerValue]];

    UILabel * label = [self.titleLabels firstObject];
    
    if ([self.orderStateLabel.text isEqualToString:@"已中奖"] || [self.orderStateLabel.text isEqualToString:@"已派奖"]) {
        label.text = @"中奖金额(元)";
    }
    
}

@end
