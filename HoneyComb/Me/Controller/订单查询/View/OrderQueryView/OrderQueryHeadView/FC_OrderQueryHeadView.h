//
//  FC_OrderQueryHeadView.h
//  HoneyComb
//
//  Created by afc on 2018/11/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_NumberLottoryDetailModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_OrderQueryHeadView : UIView

@property (nonatomic,strong) UILabel * nameLabel;
@property (nonatomic,strong) UILabel * expectBonusesLabel;
@property (nonatomic,strong) UILabel * orderStateLabel;
@property (nonatomic,strong) UILabel * betMoneyLabel;

-(void)reloadWithModel:(FC_NumberLottoryDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
