//
//  OrderDetailFourCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailFourCell : UITableViewCell
@property (nonatomic,strong) UILabel *dateLabel;//日期
@property (nonatomic,strong) UILabel *gameNameLabel;//比赛名称
@property (nonatomic,strong) UILabel *dateEndLabel;//截止时间
@property (nonatomic,strong) UILabel *leftCity;//左侧国家
@property (nonatomic,strong) UILabel *scoreLabel;//比分
@property (nonatomic,strong) UILabel *rightCity;//右侧国家
@property (nonatomic,strong) UILabel *bettingLabel;//投注详情
@property(nonatomic,weak) UIView *separatorView;
@end
