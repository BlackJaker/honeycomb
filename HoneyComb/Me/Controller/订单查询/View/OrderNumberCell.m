//
//  OrderNumberCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/9/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderNumberCell.h"

@implementation OrderNumberCell

-(void)setModel:(AllModel *)model
{
    if ([self.selectStr isEqualToString:@"3"]) {
        if ([model.type isEqualToString:@"0"]) {
            self.nameLabel.text = @"普通";
        }else
        {
            self.nameLabel.text = @"胆拖";
        }
        if ([model.redBile isEqualToString:@""]) {
            NSString *str =  [model.red stringByReplacingOccurrencesOfString:@"," withString:@" "];
            NSString *str2 =  [model.blue stringByReplacingOccurrencesOfString:@"," withString:@" "];
            self.contentLabel.text = [NSString stringWithFormat:@"%@ | %@",str,str2];
        }else
        {
            NSString *str1 =  [model.red stringByReplacingOccurrencesOfString:@"," withString:@" "];
            NSString *str2 =  [model.redBile stringByReplacingOccurrencesOfString:@"," withString:@" "];
            NSString *str3 =  [model.blue stringByReplacingOccurrencesOfString:@"," withString:@" "];
            NSString *str4 =  [model.blueBile stringByReplacingOccurrencesOfString:@"," withString:@" "];
            self.contentLabel.text = [NSString stringWithFormat:@"%@ # %@ | %@ # %@",str1,str2,str3,str4];
        }
    }else if ([self.selectStr isEqualToString:@"4"]){
        if ([model.type isEqualToString:@"1"]) {
            self.nameLabel.text = @"直选";
        }else if([model.type isEqualToString:@"2"]){
             self.nameLabel.text = @"组三";
        }else
        {
            self.nameLabel.text = @"组六";
        }
        
        self.contentLabel.text =model.content;
        
    }else
    {
        
    }
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}
-(void)layoutView
{
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/31.25, 2, kWidth/12.5, kWidth/26.7857)];
    //    self.contentLabel.text = @"1 普通直选 |0|3|5";
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.textColor = kGrayColor;
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [self.contentView addSubview:self.nameLabel];
    
    
    self.contentLabel = [[UILabel alloc]init];
    //    self.contentLabel.text = @"1 普通直选 |0|3|5";
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = kGrayColor;
    self.contentLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-(kWidth/31.25)*2-kWidth/12.5);
        make.height.mas_equalTo(kWidth/26.7857);
        make.top.mas_equalTo(2);
        make.left.mas_equalTo(self.nameLabel.mas_right).offset(10);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
