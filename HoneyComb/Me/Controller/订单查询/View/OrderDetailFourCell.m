//
//  OrderDetailFourCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderDetailFourCell.h"

@implementation OrderDetailFourCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}
-(void)layoutView
{
    /*
     @property (nonatomic,strong) UILabel *dateLabel;//日期
     @property (nonatomic,strong) UILabel *gameNameLabel;//比赛名称
     @property (nonatomic,strong) UILabel *dateEndLabel;//截止时间
     @property (nonatomic,strong) UILabel *leftCity;//左侧国家
     @property (nonatomic,strong) UILabel *scoreLabel;//比分
     @property (nonatomic,strong) UILabel *rightCity;//右侧国家
     @property (nonatomic,strong) UILabel *bettingLabel;//投注详情
     */
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.text = @"周二  061  世界杯  01:00截止";
    self.dateLabel.textColor = kGrayColor;
    self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [self.contentView addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-20);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(2);
        make.left.mas_equalTo(12);
    }];
    self.leftCity = [[UILabel alloc]init];
    self.leftCity.text = @"法国   VS   比利时";
    self.leftCity.textColor = kGrayColor;
    self.leftCity.font = [UIFont fontWithName:kPingFangRegular size:14];
    [self.contentView addSubview:self.leftCity];
    [self.leftCity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-20);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.dateLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(12);
    }];
    
    UILabel *personLabel = [[UILabel alloc]init];
    personLabel.text = @"彩民投注";
    personLabel.textColor = kGrayColor;
    personLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [self.contentView addSubview:personLabel];
    [personLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.leftCity.mas_bottom).offset(10);
        make.left.mas_equalTo(12);
    }];
    
    
    self.bettingLabel = [[UILabel alloc]init];
    self.bettingLabel.text = @"胜";
    self.bettingLabel.textColor = kRedColor;
    self.bettingLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [self.contentView addSubview:self.bettingLabel];
    [self.bettingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-20);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(self.leftCity.mas_bottom).offset(10);
        make.left.mas_equalTo(personLabel.mas_right).offset(10);
    }];
    
    
    
}
-(UIView *)separatorView
{
    if (_separatorView == nil) {
        UIView *separatorView = [[UIView alloc]init];
        self.separatorView = separatorView;
        separatorView.backgroundColor = RGB(0, 0, 0, 0.1);
        [self addSubview:separatorView];
    }
    return _separatorView;
}

//重写layoutSubViews方法，设置位置及尺寸
-(void)layoutSubviews
{
    [super layoutSubviews];
    self.separatorView.frame = CGRectMake(12, self.bounds.size.height-1,     self.bounds.size.width-24, 1);
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
