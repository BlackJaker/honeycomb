//
//  OrderTypeDCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderTypeDCell.h"

@implementation OrderTypeDCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}
+ (CGFloat)tableView:(UITableView *)tableView rowHeightForObject:(AllModel *)object{
    return kWidth/4.2613;
}
-(void)layoutView
{
    self.nameLabel = [[UILabel alloc]init];
//    self.nameLabel.text = @"排列三";
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
    self.nameLabel.textColor = kBlackColor;
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-kWidth/31.25);
        make.height.mas_equalTo(kWidth/20.8333);
        make.left.mas_equalTo(kWidth/31.25);
        make.top.mas_equalTo(kWidth/25);
    }];
    
    self.dateLabel = [[UILabel alloc]init];
//    self.dateLabel.text = @"2018-07-11 14:40:10";
    self.dateLabel.textAlignment = NSTextAlignmentRight;
    self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/28.8461];
    self.dateLabel.textColor = kGrayColor;
    [self.contentView addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-kWidth/31.25);
        make.height.mas_equalTo(kWidth/26.7857);
        make.right.mas_equalTo(-kWidth/31.25);
        make.top.mas_equalTo(kWidth/20.8333);
    }];
    UILabel *num = [[UILabel alloc]init];
    num.text = @"期号:";
    num.textColor = kBlackColor;
    num.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    [self.contentView addSubview:num];
    [num mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/12.5);
        make.height.mas_equalTo(kWidth/28.8461);
        make.left.mas_equalTo(kWidth/31.25);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(kWidth/37.5);
    }];
    self.numberLabel = [[UILabel alloc]init];
//    self.numberLabel.text = @"2018123";
    self.numberLabel.textAlignment = NSTextAlignmentLeft;
    self.numberLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    self.numberLabel.textColor = kGrayColor;
    [self.contentView addSubview:self.numberLabel];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-kWidth/8.5227);
        make.height.mas_equalTo(kWidth/28.8461);
        make.left.mas_equalTo(num.mas_right);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(kWidth/37.5);
    }];
    
    self.priceLabel = [[UILabel alloc]init];
//    self.priceLabel.text = @"2.00元";
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
    self.priceLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    self.priceLabel.textColor = kBlackColor;
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-kWidth/31.25);
        make.height.mas_equalTo(kWidth/28.8461);
        make.left.mas_equalTo(kWidth/31.25);
        make.top.mas_equalTo(num.mas_bottom).offset(kWidth/37.5);
    }];
    
    self.stateBtn = [[UIButton alloc]init];
    [self.stateBtn setImage:[UIImage imageNamed:@"组2"] forState:(UIControlStateNormal)];
    [self.contentView addSubview:self.stateBtn];
    [self.stateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/11.7187);
        make.height.mas_equalTo(kWidth/31.25);
        make.right.mas_equalTo(-kWidth/31.25);
        make.bottom.mas_equalTo(-kWidth/22.0588);
    }];
    
    self.bounsLabel = [[UILabel alloc]init];
//    self.bounsLabel.text = @"奖金5.00元";
    self.bounsLabel.textAlignment = NSTextAlignmentRight;
    self.bounsLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    self.bounsLabel.textColor = kRedColor;
    [self.contentView addSubview:self.bounsLabel];
    [self.bounsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-kWidth/7.9787);
        make.height.mas_equalTo(kWidth/25);
        make.right.mas_equalTo(self.stateBtn.mas_left).offset(-3);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-kWidth/25);
    }];
}
-(void)setPerson:(AllModel *)p
{
    _nameLabel.text = p.playName;
    _dateLabel.text = p.createDate;
    self.numberLabel.text = p.issue;
    self.priceLabel.text = [NSString stringWithFormat:@"%@元",p.price];
    self.bounsLabel.text = [NSString stringWithFormat:@"奖金: %@元",p.award];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
