//
//  OrderQueryDataSource.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderQueryDataSource.h"
#import "OrderQueryItem.h"
#import "OrderTypeACell.h"
#import "OrderTypeBCell.h"
#import "OrderTypeCCell.h"
#import "OrderTypeDCell.h"
#import "AllModel.h"
@implementation OrderQueryDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.itemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderQueryCell *cell = [tableView dequeueReusableCellWithIdentifier:[self cellIdentiferAtIndexPath:indexPath]];
    if (!cell) {
        Class cls = [self cellClassAtIndexPath:indexPath];
        cell = [[cls alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[self cellIdentiferAtIndexPath:indexPath]];
        
    }
    
    if ([cell isKindOfClass:[OrderTypeACell class]]) {
        [(OrderTypeACell *)cell setPerson:[self itemAtIndexPath:indexPath]];
    }
    if ([cell isKindOfClass:[OrderTypeBCell class]]) {
        [(OrderTypeBCell *)cell setPerson:[self itemAtIndexPath:indexPath]];
    }
    if ([cell isKindOfClass:[OrderTypeCCell class]]) {
        [(OrderTypeCCell *)cell setPerson:[self itemAtIndexPath:indexPath]];
    }
    if ([cell isKindOfClass:[OrderTypeDCell class]]) {
        [(OrderTypeDCell *)cell setPerson:[self itemAtIndexPath:indexPath]];
    }
    
//    cell.item = [self itemAtIndexPath:indexPath];
    return cell;
}

- (Class)cellClassAtIndexPath:(NSIndexPath *)indexPath{
    AllModel *item = [self itemAtIndexPath:indexPath];
//    switch (item.type) {
//        case typeA:{
//            return [OrderTypeACell class];
//        }
//            break;
//        case typeB:{
//            return [OrderTypeBCell class];
//        }
//            break;
//        case typeC:{
//            return [OrderTypeCCell class];
//        }
//            break;
//        case typeD:{
//            return [OrderTypeDCell class];
//        }
//
//            break;
//    }
    int type = [item.orderCellType intValue];
    if (type==0) {
        return [OrderTypeDCell class];
    }
    else if (type==1) {
        return [OrderTypeACell class];
    }
    else if (type==2) {
        return [OrderTypeBCell class];
    }
    else {
        return [OrderTypeCCell class];
    }
}

- (AllModel *)itemAtIndexPath:(NSIndexPath *)indexPath{

    return self.itemArray[indexPath.row];
}

- (NSString *)cellIdentiferAtIndexPath:(NSIndexPath *)indexPath{
    return NSStringFromClass([self cellClassAtIndexPath:indexPath]);
}



@end
