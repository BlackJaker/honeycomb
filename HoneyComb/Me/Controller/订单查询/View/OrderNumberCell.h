//
//  OrderNumberCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/9/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderNumberCell : UITableViewCell
@property (nonatomic,strong) AllModel *model;
@property (nonatomic,strong) UILabel *nameLabel;// 普通 或  胆拖
@property (nonatomic,strong) UILabel *contentLabel;//投注内容
@property (nonatomic,copy) NSString *selectStr;
@end
