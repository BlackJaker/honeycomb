//
//  OrderQueryCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AllModel;
@interface OrderQueryCell : UITableViewCell
@property (nonatomic,strong) AllModel *item;

+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(AllModel *)object;

@end
