//
//  OrderQueryDataSource.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class AllModel;

@interface OrderQueryDataSource : NSObject<UITableViewDataSource>

@property (nonatomic,strong) NSMutableArray *itemArray;

-(AllModel *)itemAtIndexPath:(NSIndexPath *)indexPath;
-(Class)cellClassAtIndexPath:(NSIndexPath *)indexPath;


@end
