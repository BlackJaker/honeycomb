//
//  FC_OrderFiltrateView.h
//  HoneyComb
//
//  Created by afc on 2019/1/25.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^OrderStatusChoosedBlock)(NSString * kind,NSString * state,NSString * day);

@interface FC_OrderFiltrateView : UIView

@property (nonatomic,copy) OrderStatusChoosedBlock  orderStatusChoosedBlock;

-(void)showViewWithKind:(NSString *)kind state:(NSString *)state day:(NSString *)day;

-(void)dismissView;

@end

NS_ASSUME_NONNULL_END
