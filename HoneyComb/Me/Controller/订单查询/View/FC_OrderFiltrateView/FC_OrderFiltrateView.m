//
//  FC_OrderFiltrateView.m
//  HoneyComb
//
//  Created by afc on 2019/1/25.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import "FC_OrderFiltrateView.h"

@interface FC_OrderFiltrateView()
{
    NSString *dayStr;
    NSString *orderMoney;
    CGFloat  infoBackLeftMargin;
    CGFloat  leftMargin;
}
@property (nonatomic,strong) UIView   *bgView;
@property (nonatomic,strong) UIWindow *window;
@property (nonatomic,strong) UIView   *infoBackView;
@property (nonatomic,strong) UIView   *kindBackView;
@property (nonatomic,strong) UIView   *stateBackView;
@property (nonatomic,strong) UIView   *timeBackView;

@property (nonatomic,copy) NSArray *kinds;
@property (nonatomic,copy) NSArray *kindTypes;
@property (nonatomic,copy) NSArray *states;
@property (nonatomic,copy) NSArray *days;
@property (nonatomic,copy) NSArray *types;

@property (nonatomic,copy) NSMutableArray *kindButtons;
@property (nonatomic,copy) NSMutableArray *recentlies;
@property (nonatomic,copy) NSMutableArray *daies;

@property (nonatomic,strong) UIButton * currentKindSelectedButton;
@property (nonatomic,strong) UIButton * currentStateSelectedButton;
@property (nonatomic,strong) UIButton * currentTimeSelectedButton;

@property (nonatomic,assign) NSInteger selectedStateIndex;
@property (nonatomic,assign) NSInteger selectedDayIndex;

@property (nonatomic,strong) NSMutableArray *clickArrTwo;
@property (nonatomic,strong)UIButton *priceBtn;

@end

@implementation FC_OrderFiltrateView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.selectedStateIndex = -1;
        self.selectedDayIndex = -1;
        
        infoBackLeftMargin = 60;
        leftMargin = 13;
        
        [self layoutView];

        _window = [UIApplication sharedApplication].keyWindow;
        [_window addSubview:self];
    }
    return self;
    
}

-(void)layoutView
{
    
    self.bgView.backgroundColor = [UIColor blackColor];
    
    [self kindViewSetup];
    
    [self stateButtonSetup];
    
    [self dayButtonSetup];
    
    [self bottomViewSetup];
    
}

-(UIView *)bgView{
    
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.alpha = 0.4;
        [self addSubview:_bgView];
        
        [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
        }];
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissContactView:)];
        [_bgView addGestureRecognizer:tapGes];
    }
    return _bgView;
}

-(UIView *)infoBackView{
    
    if (!_infoBackView) {
        _infoBackView = [[UIView alloc]init];
        _infoBackView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_infoBackView];
        
        [_infoBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(infoBackLeftMargin);
            make.right.mas_equalTo(0);
        }];
    }
    return _infoBackView;
}

-(UIView *)kindBackView{
    
    if (!_kindBackView) {
        
         CGFloat  kindBackHeight = 220;
        
        CGFloat topMargin = 41 + NAVI_SAFE_HEIGHT;
        
        _kindBackView = [[UIView alloc]init];
        [self.infoBackView addSubview:_kindBackView];
        
        [_kindBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(topMargin);
            make.height.mas_equalTo(kindBackHeight);
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
        }];
        
    }
    return _kindBackView;
}

-(void)kindViewSetup{
    
    CGFloat kindNoteHeight = 13;
    
    UILabel *kindNoteLabel = [[UILabel alloc]init];
    [self updateLabel:kindNoteLabel withText:@"彩种选择"];
    [self.kindBackView addSubview:kindNoteLabel];
    
    [kindNoteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(kindNoteHeight);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    CGFloat recenOrigin = 33,centerMar = 12,yMar = 10,recenButtonHeight = 30,recenButtonWidth = (kWidth - infoBackLeftMargin - centerMar * 2 - leftMargin * 2)/3;
    
    for (int i = 0; i < self.kinds.count; i++) {
        
        UIButton * button = [[UIButton alloc]init];
        
        button.tag = [[self.kindTypes objectAtIndex:i] integerValue];
        if (!self.currentKindSelectedButton) {
            button.selected = YES;
            self.currentKindSelectedButton = button;
        }else{
            button.selected = button.tag == self.currentKindSelectedButton.tag?YES:NO;
        }
        
        [self updatePropertyButton:button];
        [button setTitle:self.kinds[i] forState:(UIControlStateNormal)];
        [button addTarget:self action:@selector(kindSelectedAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.kindBackView addSubview:button];
        
        [self.kindButtons addObject:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo((recenButtonWidth + centerMar)*(i%3));
            make.width.mas_equalTo(recenButtonWidth);
            make.height.mas_equalTo(recenButtonHeight);
            make.top.mas_equalTo(recenOrigin + (recenButtonHeight + yMar)*(i/3));
        }];
        
    }
    
}

-(void)updateLabel:(UILabel *) lable withText:(NSString *)text{
    lable.text = text;
    lable.font = [UIFont fontWithName:kMedium size:13];
    lable.textColor = [UIColor colorWithHexString:@"#A2A9B8"];
    lable.textAlignment = NSTextAlignmentLeft;
}

-(void)updatePropertyButton:(UIButton *)button{
    
    [button.layer setCornerRadius:5];
    button.layer.masksToBounds = YES;
    [button setBackGroundColor:RGB(246, 246, 246, 1.0f) controlState:UIControlStateNormal];
    [button setBackGroundColor:RGB(255, 244, 240, 1.0f) controlState:UIControlStateSelected];
    [button setBackGroundColor:[UIColor clearColor] controlState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithHexString:@"#3A4150"] forState:(UIControlStateNormal)];
    [button setTitleColor:[UIColor colorWithHexString:@"#F54F58"] forState:UIControlStateSelected];
    if (button.selected) {
        button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    }else{
        button.titleLabel.font = [UIFont fontWithName:kMedium size:14];
    }
    
}


-(void)kindSelectedAction:(UIButton *)btn{
    
    if (btn.selected) {
        return;
    }
    btn.selected = YES;
    self.currentKindSelectedButton.selected = NO;
    self.currentKindSelectedButton = btn;
    
}


-(UIView *)stateBackView{
    
    if (!_stateBackView) {
        
        CGFloat  stateBackHeight = 180;
        
        _stateBackView = [[UIView alloc]init];
        [self.infoBackView addSubview:_stateBackView];
        
        [_stateBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.kindBackView.mas_bottom);
            make.height.mas_equalTo(stateBackHeight);
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
        }];
        
    }
    return _stateBackView;
}

-(void)stateButtonSetup{
    
    CGFloat recenHeight = 13;

    UILabel *recenLabel = [[UILabel alloc]init];
    [self updateLabel:recenLabel withText:@"状态选择"];
    [self.stateBackView addSubview:recenLabel];
    
    [recenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(recenHeight);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];

   CGFloat recenOrigin = 33,centerMar = 10,yMar = 10,recenButtonHeight = 30,recenButtonWidth = (kWidth - infoBackLeftMargin - centerMar * 3 - leftMargin * 2)/4;

    for (int i = 0; i < self.states.count; i++) {

        UIButton * button = [[UIButton alloc]init];

        button.tag = [[self.types objectAtIndex:i] integerValue];

        if (!self.currentStateSelectedButton) {
            button.selected = YES;
            self.currentStateSelectedButton = button;
        }else{
            button.selected = button.tag == self.currentStateSelectedButton.tag?YES:NO;
        }
        [self updatePropertyButton:button];
        [button setTitle:self.states[i] forState:(UIControlStateNormal)];
        [button addTarget:self action:@selector(stateSelectedAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.stateBackView addSubview:button];

        [self.recentlies addObject:button];

        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo((recenButtonWidth + centerMar)*(i%4));
            make.width.mas_equalTo(recenButtonWidth);
            make.height.mas_equalTo(recenButtonHeight);
            make.top.mas_equalTo(recenOrigin + (recenButtonHeight + yMar)*(i/4));
        }];
    }

}

-(void)stateSelectedAction:(UIButton *)btn
{
    
    if (btn.selected) {
        return;
    }
    btn.selected = YES;
    self.currentStateSelectedButton.selected = NO;
    self.currentStateSelectedButton = btn;
    
}

-(UIView *)timeBackView{
    
    if (!_timeBackView) {
        
        CGFloat  timeBackHeight = 160;
        
        _timeBackView = [[UIView alloc]init];
        [self.infoBackView addSubview:_timeBackView];
        
        [_timeBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.stateBackView.mas_bottom);
            make.height.mas_equalTo(timeBackHeight);
            make.left.mas_equalTo(leftMargin);
            make.right.mas_equalTo(-leftMargin);
        }];
        
    }
    return _timeBackView;
}

-(void)dayButtonSetup{
    
    
    CGFloat timeHeight = 13;
    
    UILabel *dayLabel = [[UILabel alloc]init];
    [self updateLabel:dayLabel withText:@"时间选择"];
    [self.timeBackView addSubview:dayLabel];
    
    [dayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(timeHeight);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    CGFloat recenOrigin = 33,centerMar = 10,yMar = 10,recenButtonHeight = 30,recenButtonWidth = (kWidth - infoBackLeftMargin - centerMar * 2 - leftMargin * 2)/3;
    
    for (int i = 0; i < self.days.count; i++) {
        
        UIButton * button = [[UIButton alloc]init];
        
        button.tag = 200 + i;
        
        if (!self.currentTimeSelectedButton) {
            button.selected = YES;
            self.currentTimeSelectedButton = button;
        }else{
            button.selected = button.tag == self.currentTimeSelectedButton.tag?YES:NO;
        }
        [self updatePropertyButton:button];
        [button setTitle:self.days[i] forState:(UIControlStateNormal)];
        [button addTarget:self action:@selector(dayButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.timeBackView addSubview:button];
        
        [self.daies addObject:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(recenOrigin + (recenButtonHeight + yMar)*(i/3));
            make.left.mas_equalTo((recenButtonWidth + centerMar)*(i%3));
            make.width.mas_equalTo(recenButtonWidth);
            make.height.mas_equalTo(recenButtonHeight);
            
        }];
    }

}



-(void)dayButtonAction:(UIButton *)btn
{
    if (btn.selected) {
        return;
    }

    btn.selected = YES;
    
    self.currentTimeSelectedButton.selected = NO;
    self.currentTimeSelectedButton = btn;

}


-(void)bottomViewSetup{
    
    CGFloat  btnWidth = (kWidth - infoBackLeftMargin)/2;
    
    UIButton *chippedBtn =[[UIButton alloc]init];
    [chippedBtn setTitle:@"取消" forState:(UIControlStateNormal)];
    [chippedBtn setTitleColor:RGB(51, 51, 51, 1) forState:(UIControlStateNormal)];
    [chippedBtn addTarget:self action:@selector(chippedAction:) forControlEvents:(UIControlEventTouchUpInside)];
    chippedBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    [self.infoBackView addSubview:chippedBtn];
    
    [chippedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(btnWidth);
        make.height.mas_equalTo(TAB_BAR_HEIGHT);
        make.bottom.mas_equalTo(0);
        
    }];
    
    
    UIButton *caseBtn =[[UIButton alloc]init];
    [caseBtn setTitle:@"确定" forState:(UIControlStateNormal)];
    caseBtn.backgroundColor = RGB(237, 37, 37, 1);
    [caseBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [caseBtn addTarget:self action:@selector(caseAction:) forControlEvents:(UIControlEventTouchUpInside)];
    caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    [self.infoBackView addSubview:caseBtn];
    
    [caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(TAB_BAR_HEIGHT);
        
    }];
    
    UIView *vvv = [[UIView alloc]init];
    vvv.backgroundColor = RGB(237, 237, 237, 1);
    [self.infoBackView addSubview:vvv];

    [vvv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(caseBtn.mas_top).offset(-1);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
}
-(void)caseAction:(UIButton *)btn
{

    NSString * kind = self.currentKindSelectedButton.tag == 0?nil:[NSString stringWithFormat:@"%ld",self.currentKindSelectedButton.tag];
    NSString * state = self.currentStateSelectedButton.tag == 0?nil: [NSString stringWithFormat:@"%ld",self.currentStateSelectedButton.tag];
    NSString * day = [self dayString];
    
    if (self.orderStatusChoosedBlock) {
        self.orderStatusChoosedBlock(kind,state,day);
    }
    
    [self dismissView];
}

-(NSString *)dayString{
    
    NSString * dayStr = nil;
    
    switch (self.currentTimeSelectedButton.tag) {
        case 201:
            dayStr = @"7";
            break;
        case 202:
            dayStr = @"30";
            break;
        case 203:
            dayStr = @"60";
            break;
        default:
            break;
    }
    return dayStr;
}

-(void)chippedAction:(UIButton *)btn
{
    [self dismissView];
}
-(void)dismissContactView:(UITapGestureRecognizer *)tagGes
{
    [self dismissView];
}
-(void)showViewWithKind:(NSString *)kind state:(NSString *)state day:(NSString *)day
{
    @WeakObj(self)
    [UIView animateWithDuration:0.2 animations:^{
        @StrongObj(self)
        self.hidden = NO;
        self.bgView.alpha = 0.4;
        self.frame = CGRectMake(0, 0, kWidth, kHeight);
        
        [self reloadCurrentChoosedWithKind:kind state:state day:day];
        
    }];
}

-(void)reloadCurrentChoosedWithKind:(NSString *)kind state:(NSString *)state day:(NSString *)day{
    
    if (kind) {
        self.currentKindSelectedButton.selected = NO;
        for (UIButton * button in self.kindButtons) {
            if (button.tag == [kind integerValue]) {
                button.selected = YES;
                self.currentKindSelectedButton = button;
                break;
            }
        }

    }else{
        self.currentKindSelectedButton.selected = NO;
        self.currentKindSelectedButton = [self.kindButtons firstObject];
        self.currentKindSelectedButton.selected = YES;
    }
    
    if (state) {
        self.currentStateSelectedButton.selected = NO;
        for (UIButton * button in self.recentlies) {
            if (button.tag == [state integerValue]) {
                button.selected = YES;
                self.currentStateSelectedButton = button;
                break;
            }
        }
        
    }else{
        self.currentStateSelectedButton.selected = NO;
        self.currentStateSelectedButton = [self.recentlies firstObject];
        self.currentStateSelectedButton.selected = YES;
    }
    
    if (day) {
        self.currentTimeSelectedButton.selected = NO;
        for (UIButton * button in self.daies) {
            if (button.tag == [self tagWithDay:day]) {
                button.selected = YES;
                self.currentTimeSelectedButton = button;
                break;
            }
        }
        
    }else{
        self.currentTimeSelectedButton.selected = NO;
        self.currentTimeSelectedButton = [self.daies firstObject];
        self.currentTimeSelectedButton.selected = YES;
    }
    
}

-(NSInteger)tagWithDay:(NSString *)day{
    
    NSInteger tag = 200;
    
    if ([day isEqualToString:@"7"]) {
        tag = 201;
    }
    if ([day isEqualToString:@"30"]) {
        tag = 202;
    }
    
    if ([day isEqualToString:@"60"]) {
        tag = 203;
    }
    return tag;
}

-(void)dismissView
{
    
    @WeakObj(self)
    [UIView animateWithDuration:0.2 animations:^{
        @StrongObj(self)
        self.bgView.alpha = 0;
        self.hidden = YES;
        self.frame = CGRectMake(kWidth, 0, kWidth, kHeight);
    } completion:^(BOOL finished) {
//        @StrongObj(self)
//        self.bgView.alpha = 0;
//        self.hidden = YES;
    }];
}

#pragma mark  --  array new  --

-(NSArray *)kinds{
    
    if (!_kinds) {
        _kinds = @[@"全部彩种",@"竞彩足球",@"大乐透",@"竞彩篮球",@"排列三",@"排列五",@"七星彩",@"任选九",@"胜负彩"];
    }
    return _kinds;
}

-(NSArray *)kindTypes{
    
    if (!_kindTypes) {
        _kindTypes = @[@"0",@"1",@"3",@"2",@"4",@"5",@"6",@"8",@"7"];
    }
    return _kindTypes;
}


-(NSArray *)states{
    
    if (!_states) {
        _states = @[@"全部",@"待出票",@"已出票",@"已中奖",@"未中奖",@"已派奖",@"已撤单",@"已取票"];
    }
    return _states;
}

-(NSArray *)types{
    
    if (!_types) {
        _types = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"8"];
    }
    return _types;
}

-(NSArray *)days{
    
    if (!_days) {
        _days = @[@"全部",@"近一周",@"近一个月",@"近两个月"];
    }
    return _days;
}

-(NSMutableArray *)kindButtons{
    
    if (!_kindButtons) {
        _kindButtons = [NSMutableArray new];
    }
    return _kindButtons;
}

-(NSMutableArray *)recentlies{
    
    if (!_recentlies) {
        _recentlies = [NSMutableArray new];
    }
    return _recentlies;
}

-(NSMutableArray *)daies{
    
    if (!_daies) {
        _daies = [NSMutableArray new];
    }
    return _daies;
}


@end
