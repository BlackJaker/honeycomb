//
//  OrderTypeDCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderQueryCell.h"

@interface OrderTypeDCell : OrderQueryCell
@property (nonatomic,strong) UILabel *nameLabel;//彩种名称
@property (nonatomic,strong) UILabel  *dateLabel;//时间
@property (nonatomic,strong) UILabel *numberLabel;//期号
@property (nonatomic,strong) UILabel *priceLabel;//消费钱数
@property (nonatomic,strong) UILabel *bounsLabel;//奖金
@property (nonatomic,strong) UIButton *stateBtn;//派奖情况
- (void)setPerson:(AllModel *)p;
@end
