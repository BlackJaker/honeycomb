//
//  OrderLookViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/7.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OrderLookViewController.h"
#import "OrderQueryCell.h"
#import "OrderQueryDataSource.h"
#import "OrderQueryItem.h"
#import "OrderDetailOneVC.h"
#import "OrderDetailTwoVC.h"
#import "OrderDetailThreeVC.h"

#import "FC_OrderFiltrateView.h"

@interface OrderLookViewController ()<UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
{
    NSString *total;
    NSString *typeStr;
}
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) OrderQueryDataSource *dataSource;
@property (nonatomic,assign)NSInteger page;

@property (nonatomic,strong)FC_OrderFiltrateView * orderFiltrateView;

@property (nonatomic,copy)NSString * kind;
@property (nonatomic,copy)NSString * state;
@property (nonatomic,copy)NSString * day;

@end

@implementation OrderLookViewController
static NSString *cellID = @"cell";

-(void)viewWillAppear:(BOOL)animated
{
   
    self.tabBarController.tabBar.hidden = YES;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupRefresh];
    self.titleLabe.text = @"订单管理";
    [self rightButtonSetup];
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self.view addSubview:self.tableView];
}

#pragma mark  --  setup right button  --

-(void)rightButtonSetup{
    
   UIButton * rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setImage:[UIImage imageNamed:@"分类图标"] forState:(UIControlStateNormal)];
    [rightButton addTarget:self action:@selector(filtrateAction) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

-(void)filtrateAction{
    
    [self.orderFiltrateView showViewWithKind:self.kind state:self.state day:self.day];
    
}

-(FC_OrderFiltrateView * )orderFiltrateView{
    
    if (!_orderFiltrateView) {
        _orderFiltrateView = [[FC_OrderFiltrateView alloc]initWithFrame:CGRectMake(kWidth, 0, 0, kHeight)];
        
        @WeakObj(self)
        _orderFiltrateView.orderStatusChoosedBlock = ^(NSString* kind, NSString *state, NSString *day) {
            @StrongObj(self)
            self.kind = kind;
            self.state = state;
            self.day = day;
            
            [self loadTopics];
            
            self.tableView.scrollsToTop = YES;
            
        };
        
    }
    return _orderFiltrateView;
}


#pragma mark --创建上拉加载和下拉刷新
- (void)setupRefresh {
    __block OrderLookViewController *weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadTopics];
    }];
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadNewTopics];
    }];
}
-(void)loadTopics
{
    self.page = 1;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"pageNum"] = pageStr;
    if (self.kind) {
        dic[@"type"] = self.kind;
    }
    if (self.state) {
        dic[@"state"] = self.state;
    }
    
    if (self.day) {
        dic[@"day"] = self.day;
    }
  
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
       DYLog(@"订单管理~~%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
             NSMutableArray *arr = [solveJsonData changeType:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
            if (arr.count > 0) {
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str] ) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData ];
                }else
                {
                    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        [self loadNewTopics];
                    }];
                }
                NSMutableArray *oldArray =[NSMutableArray new];
                NSArray *array = [[responseObject objectForKey:@"data"] objectForKey:@"orderList"];
                oldArray = [array mutableCopy];
                NSMutableArray *newArr = [NSMutableArray new];
                for (NSDictionary *dd in oldArray) {
                    if ([[dd allKeys] containsObject:@"issue"]) {
                        if ([dd[@"state"] isEqualToString:@"5"]) {
                            //已派奖
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"0" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }else
                        {
                            //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"1" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }
                    }else
                    {
                        if ([dd[@"state"]isEqualToString:@"5"]) {
                            //已派奖
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"2" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }else
                        {
                            //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                            NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                            mDic = [dd mutableCopy];
                            [mDic setObject:@"3" forKey:@"orderCellType"];
                            [newArr addObject:mDic];
                        }
                    }
             
                }
                self.dataSource.itemArray = [AllModel mj_objectArrayWithKeyValuesArray:newArr];
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }else
            {
                [self.dataSource.itemArray removeAllObjects];
                [self.tableView.mj_header endRefreshing];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
                [STTextHudTool showText:@"暂无数据!"];
            }
     
        }else
        {
            [self.tableView.mj_footer endRefreshingWithNoMoreData ];
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_header endRefreshing];
        }
    
    } failure:^(NSError *error) {
         NSLog(@"订单管理--%@",error);
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)loadNewTopics
{
    self.page ++;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"pageNum"] = pageStr;
    if (self.kind) {
        dic[@"type"] = self.kind;
    }
    if (self.state) {
        dic[@"state"] = self.state;
    }
    if (self.day) {
        dic[@"day"] = self.day;
    }
    [PPNetworkHelper POST:PostHomeOrder parameters:dic success:^(id responseObject) {
          DYLog(@"订单管理分页~~%@",responseObject);
         if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
             NSArray  *arr =[AllModel mj_objectArrayWithKeyValuesArray:[[responseObject objectForKey:@"data"]objectForKey:@"orderList"]];
             if (arr == nil) {
                 [STTextHudTool showText:@"暂无更多数据!"];
                 [self.tableView.mj_header endRefreshing];
                 self.tableView.mj_footer.hidden = YES;
                 [self.tableView reloadData];
               
             }else
             {
                 NSMutableArray *oldArray =[NSMutableArray new];
                 NSArray *array = [[responseObject objectForKey:@"data"] objectForKey:@"orderList"];
                 oldArray = [array mutableCopy];
                 NSMutableArray *newArr = [NSMutableArray new];
                 for (NSDictionary *dd in oldArray) {
                     if ([[dd allKeys] containsObject:@"issue"]) {
                         if ([dd[@"state"] isEqualToString:@"5"]) {
                             //已派奖
                             NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                             mDic = [dd mutableCopy];
                             [mDic setObject:@"0" forKey:@"orderCellType"];
                             [newArr addObject:mDic];
                         }else
                         {
                             //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                             NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                             mDic = [dd mutableCopy];
                             [mDic setObject:@"1" forKey:@"orderCellType"];
                             [newArr addObject:mDic];
                         }
                     }else
                     {
                         if ([dd[@"state"]isEqualToString:@"5"]) {
                             //已派奖
                             NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                             mDic = [dd mutableCopy];
                             [mDic setObject:@"2" forKey:@"orderCellType"];
                             [newArr addObject:mDic];
                         }else
                         {
                             //0未付款，1已付款，2已出票，3已中奖，4未中奖,,6已撤单
                             NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
                             mDic = [dd mutableCopy];
                             [mDic setObject:@"3" forKey:@"orderCellType"];
                             [newArr addObject:mDic];
                         }
                     }
                     
                 }
                 
                 [self.dataSource.itemArray addObjectsFromArray:[AllModel mj_objectArrayWithKeyValuesArray:newArr]];
                 
                 [self.tableView reloadData];
                 total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
            
                 NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                 if ([total isEqualToString:str]) {
                     [self.tableView.mj_footer endRefreshingWithNoMoreData];
                 }else{
                     [self.tableView.mj_footer endRefreshing];
                 }
             }
             
         }else
         {
             [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
             [self.tableView.mj_footer endRefreshingWithNoMoreData ];
         }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-NavgationBarHeight) style:UITableViewStylePlain];
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
        self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        _tableView.dataSource = self.dataSource;
    }
    return _tableView;
}
- (OrderQueryDataSource *)dataSource{
    if (!_dataSource) {
        _dataSource = [OrderQueryDataSource new];
        //        _dataSource.itemArray = [self items];
    }
    return _dataSource;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AllModel *model = self.dataSource.itemArray[indexPath.row];
    
    NSLog(@"%@",model.issue);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中颜色消失
    
    if (model.issue == nil || [model.type integerValue] > 6) {
        [self pushToAthDetailWithModel:model];
    }else{
        [self pushToNumberDetailWithModel:model];
    }
    
}

-(void)pushToAthDetailWithModel:(AllModel *)model{
    
    self.hidesBottomBarWhenPushed = YES;
    OrderDetailTwoVC *oneVC = [[OrderDetailTwoVC alloc]init];
    oneVC.orderId = model.orderTotalId;
    oneVC.typeStr = model.type;
    oneVC.state = model.state;
    
    oneVC.PopBlock = ^{
        [self loadTopics];
    };
    
    [self.navigationController pushViewController:oneVC animated:YES];
    
}

-(void)pushToNumberDetailWithModel:(AllModel *)model{
    
    self.hidesBottomBarWhenPushed = YES;
    OrderDetailOneVC *oneVC = [[OrderDetailOneVC alloc]init];
    oneVC.orderId = model.orderTotalId;
    oneVC.typeStr = model.type;
    
    @WeakObj(self)
    oneVC.popBlock = ^{
        @StrongObj(self)
        [self loadTopics];
    };
    [self.navigationController pushViewController:oneVC animated:YES];
    
}

#pragma mark DZ
-(CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 25;
}
-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"meiyouOrder"];
}
-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    return YES;
}
-(BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class cls = [self.dataSource cellClassAtIndexPath:indexPath];
    return [cls tableView:tableView rowHeightForObject:self.dataSource.itemArray[indexPath.row]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
