//
//  OrderDetailOneVC.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/27.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PopBlock)(void);

@interface OrderDetailOneVC : AllViewController

@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *typeStr;
@property (nonatomic,copy) void (^NextViewControllerBlock)(NSInteger tfText);

@property (nonatomic,copy) PopBlock popBlock;

@end
