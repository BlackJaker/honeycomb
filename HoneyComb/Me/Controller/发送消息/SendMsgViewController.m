//
//  SendMsgViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "SendMsgViewController.h"
#import "XMTextView.h"
#import "UITextView+XMExtension.h"
#import "ChooseUserVC.h"
#define WEAKSELF typeof(self) __weak weakSelf = self;
@interface SendMsgViewController ()
@property (nonatomic,strong) UIButton *caseBtn;
@property (nonatomic,strong) UISwitch *switchOne;
@property (nonatomic,strong) UIView *chooseView;
@property (nonatomic,strong) UIView *backView ;
@property (nonatomic,assign) BOOL  isSelect;
@end

@implementation SendMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"发送消息";
    [self layoutView];
    _isSelect = YES;
    self.view.backgroundColor = RGB(249, 249, 249, 1);
}
-(void)layoutView
{
    XMTextView *tv = [[XMTextView alloc] initWithFrame:CGRectMake(0, 0, kWidth, 200)];
    tv.tvFont = [UIFont systemFontOfSize:14];
    tv.textMaxNum = 200;
    [self.view addSubview:tv];
    
    WEAKSELF
    tv.textViewListening = ^(NSString *textViewStr) {
        NSLog(@"tv监听输入的内容：%@",textViewStr);
        //        weakSelf.
    };
    
   self.backView = [[UIView alloc]init];
    self.backView.userInteractionEnabled = YES;
    self.backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(50);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(tv.mas_bottom).offset(10);
    }];
    UILabel *allLabel = [[UILabel alloc]init];
    allLabel.text =@"向所有用户发送";
    allLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    allLabel.textColor = [UIColor blackColor];
    [self.backView addSubview:allLabel];
    [allLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
    }];
    self.switchOne = [[UISwitch alloc]init];
    _switchOne.on = YES;
    [_switchOne addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];   // 开关事件切换通知
    [self.backView addSubview: _switchOne];
    [self.switchOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(-30);
        make.top.mas_equalTo(10);
    }];
  
    self.chooseView = [[UIView alloc]initWithFrame:CGRectMake(0, 270, kWidth, 50)];
    self.chooseView.hidden = YES;
    self.chooseView.userInteractionEnabled = YES;
    self.chooseView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.chooseView];
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(pushContactView:)];
    [_chooseView addGestureRecognizer:tapGes];
    UILabel *chooseLabel = [[UILabel alloc]init];
    chooseLabel.text =@"选择发布对象";
    chooseLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    chooseLabel.textColor = [UIColor blackColor];
    [self.chooseView addSubview:chooseLabel];
    [chooseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
    }];
    
    UIImageView *chooseImage = [[UIImageView alloc]init];
    chooseImage.image = [UIImage imageNamed:@"spread"];
    [self.chooseView addSubview:chooseImage];
    [chooseImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(10);
        make.height.mas_equalTo(16);
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(17);
    }];
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"保存" forState:(UIControlStateNormal)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(self.backView.mas_bottom).offset(20);
        make.left.mas_equalTo(12);
    }];
}
-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch*)sender;
    BOOL isButtonOn = [switchButton isOn];
    if (isButtonOn) {
        NSLog(@"开");
         _isSelect = YES;
        self.chooseView.hidden = YES;
       
        [self.caseBtn setFrame:CGRectMake(12, CGRectGetMaxY(self.backView.frame)+20, kWidth-24, 43)];
      
    }else {
        NSLog(@"关");
        _isSelect = NO;
         self.chooseView.hidden = NO;
         [self.caseBtn setFrame:CGRectMake(12, CGRectGetMaxY(self.chooseView.frame)+20, kWidth-24, 43)];
      
    }
}
-(void)pushContactView:(UITapGestureRecognizer *)tapGes
{
    self.hidesBottomBarWhenPushed=YES;
    
    [self.navigationController pushViewController:[[ChooseUserVC alloc]init] animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
