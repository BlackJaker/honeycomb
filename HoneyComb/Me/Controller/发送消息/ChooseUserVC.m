//
//  ChooseUserVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "ChooseUserVC.h"
#import "UserCell.h"
@interface ChooseUserVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UILabel *numberLabel;
@property (nonatomic,strong) UIButton *confirmBtn;
@end

@implementation ChooseUserVC
static NSString *cellID = @"cell";
- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"用户列表";
    [self layoutView];

    self.view.backgroundColor = [UIColor  whiteColor];
    
}
-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-TabBarHeight-NavgationBarHeight) style:(UITableViewStylePlain)];
    self.tableView.dataSource = self;
    self.tableView.delegate =self;
    self.tableView.backgroundColor =RGB(249, 249, 249, 1);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[UserCell class] forCellReuseIdentifier:cellID];
    [self.view addSubview:self.tableView];
    
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.text = @"共1人";
    self.numberLabel.textColor = kBlackColor;
    self.numberLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    [self.view addSubview:self.numberLabel];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-156-12);
        make.height.mas_equalTo(49);
        make.left.mas_equalTo(12);
        make.bottom.mas_equalTo(0);
    }];
    
    self.confirmBtn = [[UIButton alloc]init];
    [self.view addSubview:self.confirmBtn];
    [self.confirmBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.confirmBtn setTitle:@"确认(1)" forState:(UIControlStateNormal)];
    self.confirmBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    self.confirmBtn.backgroundColor = kRedColor;
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(156);
        make.height.mas_equalTo(49);
        make.left.mas_equalTo(self.numberLabel.mas_right);
       make.bottom.mas_equalTo(0);
    }];
  
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 12;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0)];
        v.backgroundColor = RGB(249, 249, 249, 1);
        return v;
    }else
    {
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0)];
        v.backgroundColor=RGB(249, 249, 249, 1);
        return v;
    }
}
//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//
//        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 5)];
//    v.backgroundColor  =RGB(249, 249, 249, 1);
//        return v;
//
//}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserCell *cell =[tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath ];
    if (cell == nil) {
        cell = [[UserCell alloc]initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = RGB(249, 249, 249, 1);
    
    return cell;
    
}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 76;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 5;
//}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.0001;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
