//
//  FC_PushInfoModel.h
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_PushInfoModel : NSObject

@property (nonatomic,copy) NSString *cancel;  // 撤销

@property (nonatomic,copy) NSString *sign;  

@property (nonatomic,copy) NSString *createDate;

@property (nonatomic,copy) NSString *draw;  // 提现

@property (nonatomic,copy) NSString *ID;

@property (nonatomic,copy) NSString *print; // 出票

@property (nonatomic,copy) NSString *prize; // 中奖

@property (nonatomic,copy) NSString *send;  // 派奖

@property (nonatomic,copy) NSString *take;  // 取票

@property (nonatomic,copy) NSString *shopId;

@end

NS_ASSUME_NONNULL_END
