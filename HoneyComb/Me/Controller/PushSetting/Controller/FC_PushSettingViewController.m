//
//  FC_PushSettingViewController.m
//  HoneyComb
//
//  Created by afc on 2018/11/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_PushSettingViewController.h"

#import "FC_PushInfoModel.h"

@interface FC_PushSettingViewController ()
{
    CGFloat  backHeight;
    CGFloat  switchWidth;
}

@property (nonatomic,copy) NSMutableArray *switches;

@property (nonatomic,copy) NSString *currentSwitchStr;

@end

@implementation FC_PushSettingViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    backHeight = 213;
    switchWidth = 70;
    _currentSwitchStr = nil;
    
    self.titleLabe.text = @"推送设置";
    [self layoutView];
    
    [self getPushSwitchStateRequest];  //
    
}

-(void)layoutView
{
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMar, kTopMar, DEFAULT_WIDTH, backHeight)];
    backView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backView];
    
    NSArray * titles = @[@"订单通知",@"中奖通知",@"提现通知",@"新用户注册通知"];
    
    CGFloat  leftMargin = DEFAULT_WIDTH - switchWidth,centerMargin = 11,height = (backHeight - centerMargin)/titles.count,labelWidth = 300;
    
    for (int i = 0; i < titles.count; i ++) {
        
        CGFloat  num = i < 2?0:1;
        
        if (i % 2 == 0) {
            UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, height * 2 * (i / 2) + num * centerMargin, DEFAULT_WIDTH, height * 2)];
            view.layer.cornerRadius = 5.0f;
            [view addProjectionWithShadowOpacity:1];
            view.backgroundColor= [UIColor whiteColor];
            [backView addSubview:view];
        }
        
        UISwitch * sw = [[UISwitch alloc]initWithFrame:CGRectMake(leftMargin, height * i + num * centerMargin, 0, 0)];
        sw.on = YES;
        sw.tag = 1000 + i;
        sw.transform = CGAffineTransformMakeScale(0.7, 0.7);
        sw.layer.anchorPoint = CGPointMake(0, 0.3);
        [sw addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        [backView addSubview:sw];
        
        [self.switches addObject:sw];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(kLeftMar, height * i + num * centerMargin, labelWidth, height)];
        label.text = titles[i];
        label.textColor = [UIColor colorWithHexString:@"#333333"];
        label.font = [UIFont fontWithName:kMedium size:15];
        [backView addSubview:label];
    }

    CGFloat  noteTopMar = 25,noteLeftMar = 27,noteHeight = 16;
    UILabel *noteLabel = [[UILabel alloc]init];
    noteLabel.text = @"温馨提示";
    noteLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    noteLabel.font = [UIFont fontWithName:kBold size:noteHeight];
    [self.view addSubview:noteLabel];
    [noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-noteLeftMar);
        make.height.mas_equalTo(noteHeight);
        make.left.mas_equalTo(noteLeftMar);
        make.top.mas_equalTo(backView.mas_bottom).offset(noteTopMar);
    }];
    
    NSArray *arrT = @[@"1.网络未连接,无法接收消息提醒",@"2.未开通提醒,无法接收消息提醒",@"3.未登陆或更换用户名,无法接收消息提醒"];
    height = 13;
    
    for (int i = 0; i < arrT.count; i++) {
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(27, 350 + NAVIGATION_HEIGHT+21*(i%3), 200, 11)];
        label.text = arrT[i];
        label.textColor = [UIColor colorWithHexString:@"#999999"];
        label.font = [UIFont fontWithName:kPingFangRegular size:11];
        [self.view addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-noteLeftMar);
            make.height.mas_equalTo(height);
            make.left.mas_equalTo(noteLeftMar);
            make.top.mas_equalTo(noteLabel.mas_bottom).offset(kTopMar + (height + kTopMar/2) * i);
        }];
        
    }
}

#pragma mark  --   switch action  --

-(void)switchAction:(UISwitch *)sender{
    
    switch (sender.tag) {
        case 1000:
            _currentSwitchStr = @"print";
            break;
        case 1001:
            _currentSwitchStr = @"prize";
            break;
        case 1002:
            _currentSwitchStr = @"draw";
            break;
        case 1003:
            _currentSwitchStr = @"sign";
            break;
        default:
            break;
    }
    
    [self setPushSwitchStateRequest];
}

#pragma mark  --  lazy  --

-(NSMutableArray *)switches{
    
    if (!_switches) {
        _switches = [NSMutableArray new];
    }
    return _switches;
}

#pragma mark  --  获取推送状态  --

-(void)getPushSwitchStateRequest{
    
    NSDictionary * pushStateDic = @{
                                    @"shopId":[LoginUser shareLoginUser].shopId
                                    };
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:PushView
               parameters:pushStateDic
                  success:^(id responseObject) {

                      NSLog(@"%@",responseObject);

                      @StrongObj(self)

                      [STTextHudTool hideSTHud];
                      if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {

                          FC_PushInfoModel * pushInfoModel =[FC_PushInfoModel mj_objectWithKeyValues:responseObject[@"data"][@"shopPush"]];
                          
                          [self reloadDataWithModel:pushInfoModel];

                      }else{
                          [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                      }
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
}

-(void)reloadDataWithModel:(FC_PushInfoModel *)model{
    
    if (!model) {
        return;
    }
    
    NSArray * switchOns = @[model.print,model.prize,model.draw,model.sign];
    
    for (int i = 0; i < self.switches.count; i ++) {
        UISwitch * sw = [self.switches objectAtIndex:i];
        sw.on = [[switchOns objectAtIndex:i] boolValue];
    }
    
}

#pragma mark  --  设置推送状态  --

-(void)setPushSwitchStateRequest{
    
    NSDictionary * pushSetStateDic = @{
                                       @"shopId":[LoginUser shareLoginUser].shopId,
                                       @"type":_currentSwitchStr
                                       };
    
    [PPNetworkHelper POST:SetPush
               parameters:pushSetStateDic
                  success:^(id responseObject) {

                      [STTextHudTool hideSTHud];
                      if (![[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                          
                          [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                      }
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
