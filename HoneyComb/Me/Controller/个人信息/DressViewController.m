//
//  DressViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "DressViewController.h"
#import "ChooseLocationView.h"
#import "CitiesDataTool.h"
@interface DressViewController ()<NSURLSessionDelegate,UIGestureRecognizerDelegate>
@property (nonatomic,strong) ChooseLocationView *chooseLocationView;
@property (nonatomic,strong) UIView  *cover;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UITextField *numTf;
@property (nonatomic,strong) UIButton *caseBtn;
@end

@implementation DressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[CitiesDataTool sharedManager] requestGetData];
    [self.view addSubview:self.cover];
    self.titleLabe.text = @"编辑店铺地址";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
}
-(void)layoutView
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
    view.layer.shadowColor = RGB(235, 235, 235, 1).CGColor;
    view.layer.shadowOpacity = 1;
    
    view.layer.shadowOffset = CGSizeZero;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(91);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    view.userInteractionEnabled = YES;
    UIImageView *imageV  = [[UIImageView alloc]init];
    imageV.image = [UIImage imageNamed:@"spread"];
    [view addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(8);
        make.height.mas_equalTo(14);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(16);
    }];
    UILabel *label = [[UILabel alloc]init];
    label.text = @"所在地区";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = kBlackColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.nameLabel = [[UILabel alloc]init];
  
    if (self.textStr.length == 0) {
         self.nameLabel.text = @"";
    }else
    {
          self.nameLabel.text = self.textStr;
    }
    self.nameLabel.textColor = kBlackColor;
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
//    self.nameTf.textColor = kBlackColor;
//    self.nameTf.font = [UIFont fontWithName:kPingFangRegular size:13];
//    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请输入6-12位数字/字母或组合" attributes:
//                                      @{NSForegroundColorAttributeName:kGrayColor,
//                                        NSFontAttributeName:self.nameTf.font
//                                        }];
//    self.nameTf.attributedPlaceholder = attrString;
    self.nameLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissContactView)];
    [self.nameLabel addGestureRecognizer:tapGes];
    [view addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(0);
    }];
    UIView *backView= [[UIView alloc]init];
    backView.backgroundColor =RGB(0, 0, 0, 0.15);
    [view addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(label.mas_bottom).offset(0);
    }];
    
    UILabel *numLabel = [[UILabel alloc]init];
    numLabel.text = @"详细地址";
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.textColor = kBlackColor;
    numLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:numLabel];
    [numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(backView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
    }];
    self.numTf = [[UITextField alloc]init];
    if (self.dressStr.length == 0) {
        self.numTf.font = [UIFont fontWithName:kPingFangRegular size:13];
        NSAttributedString *numString = [[NSAttributedString alloc] initWithString:@"街道、楼牌号等" attributes:
                                         @{NSForegroundColorAttributeName:kGrayColor,
                                           NSFontAttributeName:self.numTf.font
                                           }];
        self.numTf.attributedPlaceholder = numString;
    }else
    {
          self.numTf.font = [UIFont fontWithName:kPingFangRegular size:13];
        self.numTf.text = self.dressStr;
    }
    
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.numTf.textColor = kBlackColor;
   
    [view addSubview:self.numTf];
    [self.numTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(backView.mas_bottom);
    }];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"注意"];
    [self.view addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo (10);
        make.left.mas_equalTo(80);
        make.top.mas_equalTo(view.mas_bottom).offset(15);
    }];
    
    
    UILabel *alertLabel = [[UILabel alloc]init];
    alertLabel.text = @"地址提交后不可更改,请谨慎填写";
    alertLabel.textAlignment = NSTextAlignmentLeft;
    alertLabel.textColor = kBlackColor;
    alertLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    [self.view addSubview:alertLabel];
    [alertLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(12);
        make.top.mas_equalTo(view.mas_bottom).offset(14);
        make.left.mas_equalTo(image.mas_right).offset(11);
    }];
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"保存" forState:(UIControlStateNormal)];
    [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(alertLabel.mas_bottom).offset(15);
        make.left.mas_equalTo(12);
    }];
}
-(void)caseAction
{
    NSLog(@"%@",self.nameLabel.text);
    if (self.nameLabel.text.length == 0) {
        [STTextHudTool showText:@"地址不能为空"];
        return;
    }
    if (self.numTf.text.length == 0) {
        [STTextHudTool showText:@"街道或楼牌号不能为空"];
        return;
    }
   
    NSString *str = [NSString stringWithFormat:@"%@%@",self.nameLabel.text,self.numTf.text];
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser]shopId ],@"address":str};
    [PPNetworkHelper POST:PostChangeStore parameters:dic   success:^(id responseObject) {
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
        
            if (self.NextViewControllerBlock) {
                self.NextViewControllerBlock(str);
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
            
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)dismissContactView
{
    self.cover.hidden = !self.cover.hidden;
    self.chooseLocationView.hidden = self.cover.hidden;
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_chooseLocationView.frame, point)){
        return NO;
    }
    return YES;
}


- (void)tapCover:(UITapGestureRecognizer *)tap{
    
    if (_chooseLocationView.chooseFinish) {
        _chooseLocationView.chooseFinish();
    }
}

- (ChooseLocationView *)chooseLocationView{
    
    if (!_chooseLocationView) {
        _chooseLocationView = [[ChooseLocationView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 350, [UIScreen mainScreen].bounds.size.width, 350)];
        
    }
    return _chooseLocationView;
}

- (UIView *)cover{
    
    if (!_cover) {
        _cover = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _cover.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
        [_cover addSubview:self.chooseLocationView];
        __weak typeof (self) weakSelf = self;
        _chooseLocationView.chooseFinish = ^{
            [UIView animateWithDuration:0.25 animations:^{
                weakSelf.nameLabel.text = weakSelf.chooseLocationView.address;
                weakSelf.view.transform = CGAffineTransformIdentity;
                weakSelf.cover.hidden = YES;
            }];
        };
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapCover:)];
        [_cover addGestureRecognizer:tap];
        tap.delegate = self;
        _cover.hidden = YES;
    }
    return _cover;
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.numTf resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
