//
//  AF_AlipayIdentViewController.m
//  HoneyComb
//
//  Created by afc on 2018/11/13.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AF_AlipayIdentViewController.h"

#import "AF_AlipayInfoCell.h"

#import "YGDatePickerView.h"

@interface AF_AlipayIdentViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    CGFloat  noteLabelHeight;
    
    CGFloat  rechangedViewHeight;
    
    CGFloat  originY;
    CGFloat  leftMar;
    CGFloat  rightMar;
    CGFloat  headHeight;
    CGFloat  cellHeight;
    
    CGFloat  photoBackHeight;
}
@property (nonatomic,strong) YYLabel * noteLabel;

@property (nonatomic,strong) UIScrollView * contentScrollView;

@property (nonatomic,strong) UITableView * tableView;

@property (nonatomic,copy) NSArray * names;

@property (nonatomic,copy) NSArray * placeholdes;

@property (nonatomic,copy) YGDatePickerView * datePicker;

@property (nonatomic,strong) UIView * saleCardView;

@property (nonatomic,strong) UIImageView * saleCardImgView;

@property (nonatomic,strong) UIButton * saleCardButton;

@property (nonatomic,strong) UIView * photoView;

@property (nonatomic,strong) UIImageView * photoImgView;

@property (nonatomic,strong) UIButton * photoButton;

@end

@implementation AF_AlipayIdentViewController

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self propertySetup];
    
    [self contentSetup];
    
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    self.titleLabe.text = @"支付宝认证";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f7f7f7"];
    
    originY = 10;
    leftMar = 16;
    rightMar = 20;
    headHeight = 39;
    cellHeight = 52;
    
    noteLabelHeight = 62;
    
    rechangedViewHeight = 405;
    
    photoBackHeight = 185;

}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self.contentScrollView addSubview:self.noteLabel];
    
    [self.tableView reloadData];
    
    [self saleCardAndPhotoViewSetup];
    
    [_contentScrollView setContentSize:CGSizeMake(kWidth, noteLabelHeight + self.tableView.height + (originY + photoBackHeight) * 2)];
    
}

-(void)saleCardAndPhotoViewSetup{
    
    [self.saleCardButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
    [self.photoButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
    self.saleCardImgView.backgroundColor = [UIColor clearColor];
    self.photoImgView.backgroundColor = [UIColor clearColor];
    
}

#pragma mark  --  lazy  --

-(UIScrollView *)contentScrollView{
    
    if (!_contentScrollView) {
        
        _contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight - NavgationBarHeight - TAB_BAR_SAFE_HEIGHT)];
        _contentScrollView.backgroundColor = [UIColor clearColor];
        
        [self.view addSubview:_contentScrollView];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignResponderTap)];
        [_contentScrollView addGestureRecognizer:tap];
    }
    return _contentScrollView;
}

-(void)resignResponderTap{
    for (int i = 0; i < 6; i ++) {
        UITextField * tf = [self.view viewWithTag:1000 + i];
        [tf resignFirstResponder];
    }
}

-(YYLabel *)noteLabel{
    
    NSMutableAttributedString *text  = [[NSMutableAttributedString alloc] initWithString:@"为了给您和彩民持续提供安全便捷的服务,请您认真上传认证资料,我们会在24小时内电话通知您审核结果。"];
    text.lineSpacing = 10;
    text.font = [UIFont systemFontOfSize:13];
    text.color = [UIColor colorWithHexString:@"#666666"];
    
    if (!_noteLabel) {
        _noteLabel = [[YYLabel alloc]initWithFrame:CGRectMake(leftMar, 0,kWidth - (leftMar + rightMar) , noteLabelHeight)];
        _noteLabel.numberOfLines = 0;
        _noteLabel.attributedText = text;
    }
    return _noteLabel;
}

-(UITableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, noteLabelHeight, kWidth, headHeight + self.names.count * cellHeight)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.scrollEnabled = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.contentScrollView addSubview:_tableView];
    }
    return _tableView;
}

-(NSArray *)names{
    
    if (!_names) {
        _names = @[@"支付宝账号:",@"真实姓名:",@"联系人电话:",@"联系人邮箱:",@"店铺地址:",@"代销证编号:",@"代销证截止日期:"];
        
    }
    return _names;
}
-(NSArray *)placeholdes{
    
    if (!_placeholdes) {
        _placeholdes = @[@"请输入支付宝账号",@"请输入真实姓名",@"请输入联系人电话",@"请输入联系人邮箱",@"请输入店铺地址",@"请输入代销证号",@"请选择代销证截止日期"];
        
    }
    return _placeholdes;
}

// 代销证

-(UIView *)saleCardView{
    
    if (!_saleCardView) {
        _saleCardView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + originY, kWidth, photoBackHeight)];
        _saleCardView.backgroundColor = [UIColor whiteColor];
        [self.contentScrollView addSubview:_saleCardView];
    }
    return _saleCardView;
}

-(UIButton *)saleCardButton{
    
    CGFloat  top = 26,width = 88,height = 113,left = 50,yOrigin = 11,nHeight = 13;
    
    if (!_saleCardButton) {
        _saleCardButton = [[UIButton alloc]initWithFrame:CGRectMake(left, top, width, height)];
        [_saleCardButton addTarget:self action:@selector(addSaleCardAction) forControlEvents:UIControlEventTouchUpInside];
        [self.saleCardView addSubview:_saleCardButton];
        
        UILabel * noteLabel = [[UILabel alloc]initWithFrame:CGRectMake(left, top + height + yOrigin, width, nHeight)];
        noteLabel.text = @"上传代销证";
        noteLabel.textColor = [UIColor colorWithHexString:@"#5c6272"];
        noteLabel.font = [UIFont fontWithName:kMedium size:13];
        noteLabel.textAlignment = NSTextAlignmentCenter;
        [self.saleCardView addSubview:noteLabel];
        
        UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, height)];
        line.center = CGPointMake(self.saleCardView.centerx, top + height/2);
        line.backgroundColor = [UIColor colorWithHexString:@"#e0e0e0"];
        [self.saleCardView addSubview:line];
    }
    return _saleCardButton;
}

-(void)addSaleCardAction{
    
}

-(UIImageView *)saleCardImgView{
    
    CGFloat  top = 26,width = 88,height = 113,right = 50;
    
    if (!_saleCardImgView) {
        _saleCardImgView = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth - (width + right), top, width, height)];
        _saleCardImgView.image = [UIImage imageNamed:@"shili1"];
        [self.saleCardView addSubview:_saleCardImgView];
    }
    return _saleCardImgView;
}

// 门头照
-(UIView *)photoView{
    
    if (!_photoView) {
        _photoView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.saleCardView.frame) + originY, kWidth, photoBackHeight)];
        _photoView.backgroundColor = [UIColor whiteColor];
        [self.contentScrollView addSubview:_photoView];

    }
    return _photoView;
}

-(UIButton *)photoButton{
    
    CGFloat  top = 26,width = 88,height = 113,left = 50,yOrigin = 11,nHeight = 13;
    
    if (!_photoButton) {
        _photoButton = [[UIButton alloc]initWithFrame:CGRectMake(left, top, width, height)];
        [_photoButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
        [_photoButton addTarget:self action:@selector(addPhotoButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.photoView addSubview:_photoButton];
        
        UILabel * noteLabel = [[UILabel alloc]initWithFrame:CGRectMake(left, top + height + yOrigin, width, nHeight)];
        noteLabel.text = @"上传门头照";
        noteLabel.textColor = [UIColor colorWithHexString:@"#5c6272"];
        noteLabel.font = [UIFont fontWithName:kMedium size:13];
        noteLabel.textAlignment = NSTextAlignmentCenter;
        [self.photoView addSubview:noteLabel];
        
        UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, height)];
        line.center = CGPointMake(self.photoView.centerx, top + height/2);
        line.backgroundColor = [UIColor colorWithHexString:@"#e0e0e0"];
        [self.photoView addSubview:line];
    }
    return _photoButton;
}

-(void)addPhotoButtonAction{
    
}

-(UIImageView *)photoImgView{
    
    CGFloat  top = 26,width = 156,height = 113,right = 16;
    
    if (!_photoImgView) {
        _photoImgView = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth - (width + right), top, width, height)];
        _photoImgView.image = [UIImage imageNamed:@"shili2"];
        [self.photoView addSubview:_photoImgView];
    }
    return _photoImgView;
}


#pragma mark  --  table view datasource  --

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.names.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString * alipayCell = @"alipayCell";
    
    AF_AlipayInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:alipayCell];
    
    if (cell == nil) {
        cell = [[AF_AlipayInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:alipayCell];
    }
    cell.nameLabel.text = [self.names objectAtIndex:indexPath.row];
    cell.contentTextField.placeholder = [self.placeholdes objectAtIndex:indexPath.row];
    cell.contentTextField.delegate = self;
    cell.contentTextField.tag = 1000 + indexPath.row;
    cell.indexPath = indexPath;
    [cell reloadCell];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark  --  table view delegate  --

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, headHeight)];
    
    CGFloat  top = 21;
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(leftMar, top, kWidth - (leftMar + rightMar), headHeight - top)];
    label.text = @"添加信息";
    label.font = [UIFont fontWithName:kBold size:18];
    label.textColor = [UIColor colorWithHexString:@"#333333"];
    [view addSubview:label];
    
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return cellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return  headHeight;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    for (int i = 0; i < 6; i ++) {
        UITextField * tf = [self.view viewWithTag:1000 + i];
        [tf resignFirstResponder];
    }
    
}

#pragma mark  --  textfield  delegate  --

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 1006) {
        
        [textField resignFirstResponder];
        
        self.datePicker.currentDateStr = textField.text;
        if (self.datePicker.currentDateStr.length == 0) {
            self.datePicker.currentDateStr = [self dateStringWithDate:[NSDate date]];
        }
        
        [self.datePicker show];
    }
    
    if (textField.tag == 1002 || textField.tag == 1005) {
        textField.keyboardType = UIKeyboardTypePhonePad;
    }else if(textField.tag == 1003 || textField.tag == 1004){
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    }else{
        textField.keyboardType = UIKeyboardTypeDefault;
    }
    
}

#pragma mark  -- date picker setup  --

-(YGDatePickerView *)datePicker{
    
    UITextField * textField = [self.view viewWithTag:1006];
    
    if (!_datePicker) {
        
        @WeakObj(self)
        
        _datePicker = [YGDatePickerView showWithStartDate:nil endDate:nil titleString:@"" datePickerMode:UIDatePickerModeDate handler:^(NSDate *selectedDate) {
            
            @StrongObj(self)
            
            textField.text = [self dateStringWithDate:selectedDate];

            
        }];

    }
    
    return _datePicker;
}

-(NSString *)dateStringWithDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
    if (range.length == 1 && string.length == 0) {
        return YES;
    }else if(textField.tag == 1000) {
        return textField.text.length < 18;
    }
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
