//
//  AF_AlipayInfoCell.m
//  HoneyComb
//
//  Created by afc on 2018/11/13.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AF_AlipayInfoCell.h"

@implementation AF_AlipayInfoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    
    
}

// lazy

-(UILabel *)nameLabel{
    
    CGFloat  leftMar = 16,width = 95,height = 15,top = 21;
    
    if (!_nameLabel) {
        _nameLabel = [self createLabel];
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(top);
            make.left.mas_equalTo(leftMar);
        }];
    }
    return _nameLabel;
}

-(UITextField *)contentTextField{
    
    CGFloat  leftMar = 115,rightMar = 20,width = kWidth - (leftMar + rightMar),height = 19,top = 19;
    
    if (!_contentTextField) {
        
        _contentTextField = [[UITextField alloc]init];
        _contentTextField.textColor = [UIColor colorWithHexString:@"#464e5f"];
        _contentTextField.font = [UIFont fontWithName:kMedium size:15];
        [self addSubview:_contentTextField];
        
        [_contentTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(top);
            make.left.mas_equalTo(leftMar);
        }];
        
    }
    return _contentTextField;
}

-(UILabel *)createLabel{
    UILabel * label = [[UILabel alloc]init];
    label.textColor = [UIColor colorWithHexString:@"#464e5f"];
    label.font = [UIFont fontWithName:kBold size:15];
    label.userInteractionEnabled = YES;
    return label;
}

-(UILabel *)line{
    
    CGFloat  leftMar = 16,right = 20;
    
    if (!_line) {
        _line = [[UILabel alloc]init];
        _line.hidden = NO;
        _line.backgroundColor = [UIColor colorWithHexString:@"#f3f3f3"];
        [self addSubview:_line];
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar);
            make.height.mas_equalTo(1);
            make.bottom.mas_equalTo(-1);
            make.right.mas_equalTo(-right);
        }];
    }
    return _line;
}

-(void)reloadCell{
    
    self.line.hidden = NO;
    if (self.indexPath.row == 6) {
        [_nameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(120);
        }];
        [_contentTextField mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(135);
        }];
        self.line.hidden = YES;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
