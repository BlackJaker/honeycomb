//
//  DressViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DressViewController : AllViewController
@property (nonatomic,copy) void (^NextViewControllerBlock)(NSString *tfText);
@property (nonatomic,copy) NSString *textStr;
@property (nonatomic,copy) NSString *dressStr;
@end
