//
//  ChangeMeViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "ChangeMeViewController.h"
#import "WechatViewController.h"
#import "StoreAnnouncementVC.h"
#import "AppNameViewController.h"
#import "IdentityViewController.h"
#import "StoreCertificationVC.h"
#import "SetPassWordViewController.h"
#import "WithdrawalViewController.h"
#import "DressViewController.h"
#import "MeHeadViewController.h"
#import "ChangeWithdrawalVC.h"
#import "OSSImageUploader.h"

#import "AF_StoreCertificationVC.h"

#import "FC_CloseShopNoteView.h"

@interface ChangeMeViewController ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,MeImageCropperDelegate,UIPickerViewAccessibilityDelegate,UINavigationControllerDelegate>
{
    OSSClient *_client;
}

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) UILabel *changeHeader;//更换头像
@property (nonatomic,strong) UILabel *wechatLabel;//微信号
@property (nonatomic,strong) UILabel *announcementLabel;//店铺公告
@property (nonatomic,strong) UILabel *nameLabel;//APP名称

@property (nonatomic,strong) UILabel *storeLabel;//店铺认证

@property (nonatomic,strong) UIButton *switchButton;//店铺开店中

@property (nonatomic,strong) UILabel *passwordLabel;//密码
@property (nonatomic,strong) UILabel *withdrawalLabel;//取现密码

@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;

@property (nonatomic,copy) NSArray * titles;

@property (nonatomic,assign) BOOL  isSwitching;

@end

@implementation ChangeMeViewController

NSString * const BUCKET_NAME = @"afcplay-app";
NSString * const DOWNLOAD_OBJECT_KEY = @"object-key";
NSString * const endPoint = @"oss-cn-huhehaote.aliyuncs.com";
static NSString *kTempFolder = @"shop/headImg";
static NSString *cellID = @"cell";

-(void)viewWillAppear:(BOOL)animated
{
     [self creatData];
    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"payPassword"];
    if ([str isEqualToString:@"1"]) {
        self.withdrawalLabel.text = @"******";
    }
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.titleLabe.text = @"个人信息";
    self.view.backgroundColor = RGB(235, 235, 235, 1.0f);

    [self layoutView];
    
    [self userInfoRequest];
  
    self.isSwitching = NO;
    
}
-(void)creatData
{
  
    [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {

        self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
        self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
        self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
        
    } failure:^(NSError *error) {
        
    }];

}
-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight - NAVIGATION_HEIGHT)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorColor = RGB(0, 0, 0, 0.15);
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellID];
    [self.view addSubview:self.tableView];
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@[@"",@"微      信",@"店铺认证",@"店铺公告",@"设置密码",@"设置提现密码"],@[@"店铺名称"],@[@"开 店 中"]];
    }
    return _titles;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titles.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.titles objectAtIndex:section] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.textLabel.textColor = RGB(51, 51, 51, 1);
    cell.textLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    cell.textLabel.text = self.titles[indexPath.section][indexPath.row];
    
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];//分割线延长

    cell.backgroundColor = [UIColor whiteColor];
    
    if (indexPath.section != self.titles.count - 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;//右侧箭头
    }
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            [self.imageView removeFromSuperview];
            self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 3, 39, 39)];
            self.imageView.layer.cornerRadius = 39/2;
            self.imageView.layer.masksToBounds = YES;
            self.imageView.contentMode = UIViewContentModeScaleAspectFit;
            NSURL *url = [NSURL URLWithString:[self.modeDic objectForKey:@"img"]];
            [self.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"默认头像"]];
            [cell.contentView addSubview:self.imageView];
            
            [self.changeHeader removeFromSuperview];
            self.changeHeader = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, kWidth/2, 45)];
            self.changeHeader.text = @"更换头像";
            self.changeHeader.font = [UIFont fontWithName:kPingFangRegular size:13];
            self.changeHeader.textColor = RGB(153, 153, 153, 1);
            [cell.contentView addSubview:self.changeHeader];
        }
        if (indexPath.row == 1) {
            [self.wechatLabel removeFromSuperview];
            self.wechatLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, kWidth/2, 45)];
            self.wechatLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
            self.wechatLabel.textColor = RGB(153, 153, 153, 1);
            [cell.contentView addSubview:self.wechatLabel];
            
            if ([self.modeDic[@"wechat"] isEqualToString:@""]) {
                self.wechatLabel.text = @"方便彩民去联系您,请填写";
            }else{
                self.wechatLabel.text =[self.modeDic objectForKey:@"wechat"] ;
            }
            
        }
        
        if (indexPath.row == 2) {
            
            [self.storeLabel removeFromSuperview];
            self.storeLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, kWidth/2, 45)];
            
            self.storeLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
            self.storeLabel.textColor = RGB(153, 153, 153, 1);
            [cell.contentView addSubview:self.storeLabel];
            
            NSString *str ;
            if ([[self.modeDic objectForKey:@"shopState"] isEqualToString:@"NORMAL"]) {
                str = @"已认证";
            }else if ([[self.modeDic objectForKey:@"shopState"] isEqualToString:@"LOCKED"]){
                str = @"已锁定";
            }else if ([[self.modeDic objectForKey:@"shopState"] isEqualToString:@"AUDIT"]){
                str = @"审核中";
            }else{
                str = [[self.modeDic objectForKey:@"shopState"] isEqualToString:@"UNVERIFIED"]?@"未认证":@"被驳回";
            }
            self.storeLabel.text = str;
            
        }
        if (indexPath.row == 3) {
            
            [self.announcementLabel removeFromSuperview];
            self.announcementLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, kWidth/2, 45)];
            
            self.announcementLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
            self.announcementLabel.textColor = RGB(153, 153, 153, 1);
            [cell.contentView addSubview:self.announcementLabel];
            
            if ([self.modeDic[@"notice"] isEqualToString:@""]) {
                self.announcementLabel.text = @"未填写";
            }else{
                self.announcementLabel.text =[self.modeDic objectForKey:@"notice"];
            }
            
        }
        
        if (indexPath.row == 4) {
            
            [self.passwordLabel removeFromSuperview];
            self.passwordLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, kWidth/2, 45)];
            self.passwordLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
            self.passwordLabel.textColor = RGB(153, 153, 153, 1);
            [cell.contentView addSubview:self.passwordLabel];
        }
        if (indexPath.row == 5) {
            
            [self.withdrawalLabel removeFromSuperview];
            self.withdrawalLabel = [[UILabel alloc]initWithFrame:CGRectMake(120, 0, kWidth/2, 45)];
            
            self.withdrawalLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
            self.withdrawalLabel.textColor = RGB(153, 153, 153, 1);
            [cell.contentView addSubview:self.withdrawalLabel];
            
            if ([[self.modeDic objectForKey:@"isPay"] isEqualToString:@"0"]) {
                self.withdrawalLabel.text = @"";
            }else{
                cell.textLabel.text = @"修改提现密码";
                self.withdrawalLabel.text = @"******";
            }
            
        }
    }else if(indexPath.section == 1){
        if (indexPath.row ==0) {
            
            [self.nameLabel removeFromSuperview];
            self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 0, kWidth/2, 45)];
            self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
            self.nameLabel.textColor = RGB(153, 153, 153, 1);
            [cell.contentView addSubview:self.nameLabel];
            
            if ([self.modeDic[@"appName"] isEqualToString:@""]) {
                self.nameLabel.text = @"未设置";
            }else{
                self.nameLabel.text = [self.modeDic objectForKey:@"appName"];
            }
            
        }
    }else{
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
            
            CGFloat  width = 45,height = 27;
            
            [self.switchButton removeFromSuperview];
            self.switchButton = [[UIButton alloc]init];
            self.switchButton.selected = [self.modeDic[@"isOpen"] boolValue];
            if (!self.switchButton.selected) {
                cell.textLabel.text = @"关 店 中";
                cell.textLabel.textColor = [UIColor colorWithHexString:@"#ED284B"];
            }
            [self.switchButton setImage:[UIImage imageNamed:@"switch_on"] forState:UIControlStateSelected];
            [self.switchButton setImage:[UIImage imageNamed:@"switch_off"] forState:UIControlStateNormal];
            [cell.contentView addSubview:self.switchButton];
            
            [self.switchButton addTarget:self action:@selector(switchButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.switchButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(-kLeftMar);
                make.size.mas_equalTo(CGSizeMake(width, height));
                make.centerY.mas_equalTo(0);
            }];
            
            
        }
    }
    
    return cell;
}

-(void)switchButtonClickAction:(UIButton *)sender{

    if (!self.isSwitching) {
        
        self.isSwitching = YES;
        
        NSString * key = [NSString stringWithFormat:@"%@_firstShopClose",[LoginUser shareLoginUser].shopId];
        
        NSString * firstShopClose = [[NSUserDefaults standardUserDefaults]valueForKey:key];
        
        if (!firstShopClose || ![firstShopClose isEqualToString:@"1"]) {
            FC_CloseShopNoteView * view = [[FC_CloseShopNoteView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
            [view show];
        }

        [self setShopSwitchStateRequestWithSender:sender];
    }
    
}

-(void)setShopSwitchStateRequestWithSender:(UIButton *)sender{
    
    NSDictionary * ShopSetStateDic = @{
                                       @"shopId":[LoginUser shareLoginUser].shopId,
                                       @"type":@"open"
                                       };
    @WeakObj(self)
    [PPNetworkHelper POST:SetPush
               parameters:ShopSetStateDic
                  success:^(id responseObject) {
                      @StrongObj(self)
                      self.isSwitching = NO;
                      if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                          
                           self.switchButton.selected = !self.switchButton.selected;
                          
                          UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.titles.count - 1]];
                          cell.textLabel.text = sender.selected?@"开 店 中":@"关 店 中";
                          NSString * colorStr = sender.selected?@"#333333":@"#ED284B";
                          cell.textLabel.textColor = [UIColor colorWithHexString:colorStr];
                         
                      }else{
                          [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                      }
                  }failure:^(NSError *error) {
                      self.isSwitching = NO;
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 15)];
    view.backgroundColor = [UIColor clearColor];
    return view;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != self.titles.count - 1) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中颜色消失
    }
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相机拍摄",@"从相册中选择", nil];
            actionSheet.delegate = self;
            [actionSheet showInView:self.view];
        }
        if (indexPath.row == 1) {
            self.hidesBottomBarWhenPushed=YES;
            WechatViewController *weVC =   [[WechatViewController alloc] init];
            weVC.NextViewControllerBlock = ^(NSString *tfText) {
                self.wechatLabel.text = tfText;
            };
            weVC.textStr = self.wechatLabel.text;
//            DressViewController *stVC = [[DressViewController alloc]init];
//            stVC.NextViewControllerBlock = ^(NSString *tfText) {
//                NSLog(@"address =  %@",tfText);
//            };
            [self.navigationController pushViewController:weVC animated:YES];
            
        }
        
        if (indexPath.row == 2) {
            [self pushStoreCertificationVc];
        }
        
        if (indexPath.row == 3) {
            self.hidesBottomBarWhenPushed=YES;
            StoreAnnouncementVC *stVC = [[StoreAnnouncementVC alloc]init];
            stVC.NextViewControllerBlock = ^(NSString *tfText) {
                self.announcementLabel.text = tfText;
            };
            stVC.textStr = self.announcementLabel.text;
            [self.navigationController pushViewController:stVC animated:YES];
        }
        
        if (indexPath.row == 4) {
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:[[SetPassWordViewController alloc]init] animated:YES];
            
        }
        if (indexPath.row == 5) {
            self.hidesBottomBarWhenPushed=YES;
            NSString *str =[self.modeDic objectForKey:@"isPay"];
            if ([str isEqualToString:@"1"]) {
                ChangeWithdrawalVC *chVC = [[ChangeWithdrawalVC alloc]init];
                chVC.phoneNumber = [self.modeDic valueForKey:@"mobile"];
                [self.navigationController pushViewController:chVC animated:YES];
            }else
            {
                WithdrawalViewController * setWithdrawPasswordVc = [[WithdrawalViewController alloc]initType:0 controller:self safeInfoDic:self.safeInfoDic];
                setWithdrawPasswordVc.hidesBottomBarWhenPushed = YES;
                @WeakObj(self)
                setWithdrawPasswordVc.setPasswordSuccessedBlock = ^{
                    @StrongObj(self)
                    [self userInfoRequest];
                };
                [self.navigationController pushViewController:setWithdrawPasswordVc animated:YES];
            }
        }

    }else if(indexPath.section == 1){
        [self pushToAppName];
    }
    
}

-(void)pushStoreCertificationVc{
    
    self.hidesBottomBarWhenPushed=YES;
    AF_StoreCertificationVC *storeCertVc = [[AF_StoreCertificationVC alloc]init];
    //    storeCertVc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:storeCertVc animated:YES];
    self.hidesBottomBarWhenPushed=NO;
    
}

// 设置app名称
-(void)pushToAppName{
    self.hidesBottomBarWhenPushed=YES;
    AppNameViewController * nameVc = [[AppNameViewController alloc]init];
    nameVc.shopName = self.modeDic[@"appName"];
    @WeakObj(self)
    nameVc.successedBlock = ^{
        @StrongObj(self)
        [STTextHudTool showSuccessText:@"店铺名称设置成功"];
        [self userInfoRequest];
    };
    [self.navigationController pushViewController:nameVc animated:YES];
}

-(void)userInfoRequest
{
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId]};
    
    [PPNetworkHelper POST:PostMeDetail
               parameters:dic
                  success:^(id responseObject) {
                      
                      if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                          
                          self.modeDic = responseObject[@"data"];
                          
                          [self.tableView reloadData];
                      }
                  } failure:^(NSError *error) {
                      
                  }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    TalkLog(@"buttonIndex = [%ld]",(long)buttonIndex);
    switch (buttonIndex) {
        case 0://照相机
        {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
            imagePicker.delegate = self;
            imagePicker.allowsEditing = YES;
            imagePicker.navigationBar.translucent= NO;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            
            break;
        case 1://本地相册
        {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
            imagePicker.delegate = self;
            imagePicker.allowsEditing = YES;
               imagePicker.navigationBar.translucent= NO;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:YES completion:nil];
            
        }
            break;
        default:
            break;
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    if ([[info objectForKey:UIImagePickerControllerMediaType]isEqualToString:(__bridge NSString *)kUTTypeImage]) {
        UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
        [self performSelector:@selector(saveImage:) withObject:img afterDelay:1];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)saveImage:(UIImage *)image
{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"selfPhoto.jpg"];
    success  = [fileManager fileExistsAtPath:imageFilePath];
    if (success) {
        success = [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    UIImage *smallImage = [self thumbnailWithImageWithoutScale:image size:CGSizeMake(kWidth,kWidth )];
    [UIImageJPEGRepresentation(smallImage, 0.5f) writeToFile:imageFilePath atomically:YES];
    UIImage *selfPhoto = [UIImage imageWithContentsOfFile:imageFilePath];
    
    [OSSImageUploader syncUploadImage:selfPhoto folder:kTempFolder accessKey:self.accessKey secretKey:self.accessKeySecret token:self.tokenStr complete:^(NSString  *names, UploadImageState state) {
         NSLog(@"names---%@", names);
        
        NSString *strH = [NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",names];
        NSDictionary *dic = @{@"img":strH,@"shopId":[[LoginUser shareLoginUser] shopId]};
        [PPNetworkHelper POST:PostChangeStore parameters:dic success:^(id responseObject) {
            NSLog(@"上传头像-%@",responseObject);
            self.imageView.image = selfPhoto;
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }];
//    [OSSImageUploader asyncUploadImages:@[selfPhoto] complete:^(NSArray<NSString *> *names, UploadImageState state) {
//        NSLog(@"names---%@", names);
//    }];
//    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
//
//
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    formatter.dateFormat = @"yyyyMMddHHmmss";
//    NSString *str = [formatter stringFromDate:[NSDate date]];
//    NSString *imageFileName = [NSString stringWithFormat:@"%@.jpg",str];
//
//    NSData *imageData = UIImageJPEGRepresentation(selfPhoto, 1);
//    put.bucketName = @"afcplay-app";
//    put.objectKey = imageFileName;
//    put.uploadingData = imageData; // 直接上传NSData
//    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
//        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
//    };
//    OSSTask * putTask = [_client putObject:put];
//    [putTask continueWithBlock:^id(OSSTask *task) {
//        if (!task.error) {
//            NSLog(@"upload object success! %@",task);
//              self.imageView.image = selfPhoto;
//        } else {
//            NSLog(@"upload object failed, error: %@" , task.error);
//        }
//        return nil;
//    }];
//    [self shangchuan];
}
//改变图像的尺寸，方便上传服务器
-(UIImage *)scaleFromImage:(UIImage *)image toSize:(CGSize)size
{
    UIImage *newImage = nil;
    if (image) {
        UIGraphicsBeginImageContext(size);
        [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newImage;
}
- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width);
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height);
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}
-(void)shangchuan
{
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
   
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *imageFileName = [NSString stringWithFormat:@"%@.jpg",str];
    UIImage *image = self.imageView.image;
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    put.bucketName = @"afcplay-app";
    put.objectKey = imageFileName;
    put.uploadingData = imageData; // 直接上传NSData
    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
    };
    OSSTask * putTask = [_client putObject:put];
    [putTask continueWithBlock:^id(OSSTask *task) {
        if (!task.error) {
            NSLog(@"upload object success! %@",task);
        } else {
            NSLog(@"upload object failed, error: %@" , task.error);
        }
        return nil;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
