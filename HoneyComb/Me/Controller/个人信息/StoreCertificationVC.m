//
//  StoreCertificationVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "StoreCertificationVC.h"
#import "StoreCertificationCell.h"
#import "TZImagePickerController.h"
#import "SaleCollectionViewCell.h"
#import "IdenterCollectionViewCell.h"
#import "OSSImageUploader.h"
#import "CertificationView.h"
@interface StoreCertificationVC ()<TZImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong) UICollectionView *saleCollectionView;
@property (nonatomic,strong) UICollectionView *identerCollectionView;
@property (nonatomic,strong) UICollectionView *storeCollectionView;
@property (nonatomic,strong) UIImageView *salesView;
@property (nonatomic,strong) UIImageView *identerView;
@property (nonatomic,strong) UIImageView *storeView;
@property (nonatomic,strong) UIButton *caseBtn;
@property (nonatomic,strong) NSMutableArray *saleArr;
@property (nonatomic,strong) NSMutableArray *identerArr;
@property (nonatomic,strong) NSMutableArray *storeArr;
@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;
@property (nonatomic,strong) NSMutableArray *caseArr;
@property (nonatomic,strong) CertificationView *cerView;
@property (nonatomic,strong)   UIWindow *keyWin;
@end

@implementation StoreCertificationVC
static NSString *kTempFolder = @"shop/verify";
-(NSMutableArray *)caseArr
{
    if (!_caseArr) {
        _caseArr = [NSMutableArray array];
    }
    return _caseArr;
}
-(NSMutableArray *)saleArr
{
    if (!_saleArr) {
        _saleArr = [NSMutableArray array];
    }
    return _saleArr;
}
-(NSMutableArray *)identerArr
{
    if (!_identerArr) {
        _identerArr = [NSMutableArray array];
    }
    return _identerArr;
}
-(NSMutableArray *)storeArr
{
    if (!_storeArr) {
        _storeArr = [NSMutableArray array];
    }
    return _storeArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"店铺验证";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
      [self creatData];
    [self layoutView];
    [self.left addTarget:self action:@selector(leftAction) forControlEvents:(UIControlEventTouchUpInside)];
}
-(void)creatData
{
    
    [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {
        
        NSLog(@"--%@",responseObject);
        self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
        self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
        self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
        NSLog(@"%@",self.tokenStr);
    } failure:^(NSError *error) {
        NSLog(@"--%@",error);
    }];
    
}
-(void)layoutView
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(kHeight/4.0424);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    [layout setScrollDirection:(UICollectionViewScrollDirectionVertical)];
    self.saleCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(kWidth/9.375, kHeight/44.4666, kWidth/4.2613, kHeight/5.9)collectionViewLayout:layout];
    self.saleCollectionView.scrollEnabled = NO;
    [self.saleCollectionView registerClass:[SaleCollectionViewCell class] forCellWithReuseIdentifier:@"saleCell"];
    self.saleCollectionView.delegate = self;
    self.saleCollectionView.dataSource = self;
    self.saleCollectionView.scrollsToTop = NO;
    self.saleCollectionView.showsVerticalScrollIndicator = YES;
    self.saleCollectionView.showsHorizontalScrollIndicator = NO;
    self.saleCollectionView.backgroundColor = [UIColor whiteColor];
    [view addSubview:self.saleCollectionView];
//    self.salesView = [[UIImageView alloc]init];
//    self.salesView.image = [UIImage imageNamed:@"照片框"];
//    self.salesView.userInteractionEnabled = YES;
//    [view addSubview:self.salesView];
//    UITapGestureRecognizer *salesTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(salesAction)];
//    [ self.salesView addGestureRecognizer:salesTap];
//    [self.salesView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/4.2613);
//        make.height.mas_equalTo(kHeight/5.9);
//        make.left.mas_equalTo(kWidth/9.375);
//        make.top.mas_equalTo(kHeight/44.4666);
//    }];
    UIView *v1 = [[UIView alloc]init];
    v1.backgroundColor = RGB(0, 0, 0, 0.15);
    [view addSubview:v1];
    [v1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(kHeight/5.9);
        make.left.mas_equalTo(kWidth/2-1);
        make.top.mas_equalTo(kHeight/44.4666);
    }];
    UIImageView *imageView1 = [[UIImageView alloc]init];
    imageView1.image = [UIImage imageNamed:@"照片框"];
    [view addSubview:imageView1];
    [imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/4.2613);
        make.height.mas_equalTo(kHeight/5.9);
        make.right.mas_equalTo(-kWidth/9.375);
        make.top.mas_equalTo(kHeight/44.4666);
    }];
    
    UIView *view2 = [[UIView alloc]init];
    view2.backgroundColor = [UIColor whiteColor];
    view2.userInteractionEnabled = YES;
    [self.view addSubview:view2];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(kHeight/4.0424);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(view.mas_bottom).offset(kHeight/44.4666);
    }];
    
    self.identerCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(kWidth/9.375, kHeight/44.4666, kWidth/4.2613, kHeight/5.9)collectionViewLayout:layout];
    self.identerCollectionView.scrollEnabled = NO;
    [self.identerCollectionView registerClass:[IdenterCollectionViewCell class] forCellWithReuseIdentifier:@"idenCell"];
    self.identerCollectionView.delegate = self;
    self.identerCollectionView.dataSource = self;
    self.identerCollectionView.scrollsToTop = NO;
    self.identerCollectionView.showsVerticalScrollIndicator = YES;
    self.identerCollectionView.showsHorizontalScrollIndicator = NO;
    self.identerCollectionView.backgroundColor = [UIColor whiteColor];
    [view2 addSubview:self.identerCollectionView];
//    self.identerView = [[UIImageView alloc]init];
//    self.identerView.image = [UIImage imageNamed:@"照片框"];
//      self.identerView.userInteractionEnabled = YES;
//    [view2 addSubview:self.identerView];
//    UITapGestureRecognizer *identerTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(iderterAction)];
//    [ self.identerView addGestureRecognizer:identerTap];
//    [self.identerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/4.2613);
//        make.height.mas_equalTo(kHeight/5.9);
//        make.left.mas_equalTo(kWidth/9.375);
//        make.top.mas_equalTo(kHeight/44.4666);
//    }];
    UIView *v2 = [[UIView alloc]init];
    v2.backgroundColor = RGB(0, 0, 0, 0.15);
    [view2 addSubview:v2];
    [v2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(kHeight/5.9);
        make.left.mas_equalTo(kWidth/2-1);
        make.top.mas_equalTo(kHeight/44.4666);
    }];
    UIImageView *imageView2 = [[UIImageView alloc]init];
    imageView2.image = [UIImage imageNamed:@"照片框"];
    [view2 addSubview:imageView2];
    [imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/4.2613);
        make.height.mas_equalTo(kHeight/5.9);
        make.right.mas_equalTo(-kWidth/9.375);
        make.top.mas_equalTo(kHeight/44.4666);
    }];
    
    UIView *view3 = [[UIView alloc]init];
    view3.backgroundColor = [UIColor whiteColor];
    view3.userInteractionEnabled = YES;
    [self.view addSubview:view3];
    [view3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(kHeight/4.0424);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(view2.mas_bottom).offset(kHeight/44.4666);
    }];
    self.storeCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(kWidth/9.375, kHeight/44.4666, kWidth/4.2613, kHeight/5.9)collectionViewLayout:layout];
    self.storeCollectionView.scrollEnabled = NO;
    [self.storeCollectionView registerClass:[SaleCollectionViewCell class] forCellWithReuseIdentifier:@"storeCell"];
    self.storeCollectionView.delegate = self;
    self.storeCollectionView.dataSource = self;
    self.storeCollectionView.scrollsToTop = NO;
    self.storeCollectionView.showsVerticalScrollIndicator = YES;
    self.storeCollectionView.showsHorizontalScrollIndicator = NO;
    self.storeCollectionView.backgroundColor = [UIColor whiteColor];
    [view3 addSubview:self.storeCollectionView];
//    self.storeView = [[UIImageView alloc]init];
//    self.storeView.image = [UIImage imageNamed:@"照片框"];
//      self.storeView.userInteractionEnabled = YES;
//    [view3 addSubview:self.storeView];
//    UITapGestureRecognizer *storeTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(storeAction)];
//    [ self.storeView addGestureRecognizer:storeTap];
//    [self.storeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/4.2613);
//        make.height.mas_equalTo(kHeight/5.9);
//        make.left.mas_equalTo(kWidth/9.375);
//        make.top.mas_equalTo(kHeight/44.4666);
//    }];
    UIView *v3 = [[UIView alloc]init];
    v3.backgroundColor = RGB(0, 0, 0, 0.15);
    [view3 addSubview:v3];
    [v3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(kHeight/5.9);
        make.left.mas_equalTo(kWidth/2-1);
        make.top.mas_equalTo(kHeight/44.4666);
    }];
    UIImageView *imageView3 = [[UIImageView alloc]init];
    imageView3.image = [UIImage imageNamed:@"照片框"];
    [view3 addSubview:imageView3];
    [imageView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/4.2613);
        make.height.mas_equalTo(kHeight/5.9);
        make.right.mas_equalTo(-kWidth/9.375);
        make.top.mas_equalTo(kHeight/44.4666);
    }];
    
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"确认" forState:(UIControlStateNormal)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(kHeight/15.5116);
        make.top.mas_equalTo(view3.mas_bottom).offset(15);
        make.left.mas_equalTo(12);
    }];
   
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.saleCollectionView) {
        SaleCollectionViewCell *cell = (SaleCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"saleCell" forIndexPath:indexPath];
        NSLog(@"%ld=%@",(long)indexPath.row,self.saleArr);
        if (self.saleArr.count>0) {
            cell.imageV.image = [self.saleArr lastObject];
        }
        return cell;
    }else if (collectionView == self.identerCollectionView){
        IdenterCollectionViewCell *cell = (IdenterCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"idenCell" forIndexPath:indexPath];

        if (self.identerArr.count>0) {
            cell.imageV.image = [self.identerArr lastObject];
        }
        return cell;
    }else
    {
        SaleCollectionViewCell *cell = (SaleCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"storeCell" forIndexPath:indexPath];
        if (self.storeArr.count>0) {
            cell.imageV.image = [self.storeArr lastObject];
        }
        return cell;
    }
    
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kWidth/4.2613, kHeight/5.9);
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0001;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return kWidth/28.84;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.saleCollectionView) {
        NSLog(@"1111");
        
     
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            NSLog(@"照片%@",photos);
            self.saleArr = [NSMutableArray arrayWithArray:photos];
            
            
            [self.saleCollectionView reloadData];
//            NSData *imageData = UIImageJPEGRepresentation(_saleArr[indexPath.row] , 0.1);
//            cell.imageV.image = [UIImage imageWithData:imageData];
        }];
        
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }else if (collectionView == self.identerCollectionView){

        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            NSLog(@"照片%@",photos);
            [self.identerArr addObjectsFromArray:photos];
            
            [self.identerCollectionView reloadData];
        }];
        
        [self presentViewController:imagePickerVc animated:YES completion:nil];
        NSLog(@"2222");
    }else
    {
        NSLog(@"3333");
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            NSLog(@"照片%@",photos);
            [self.storeArr addObjectsFromArray:photos];
            
            [self.storeCollectionView reloadData];
        }];
        
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
}
#pragma mark 确认
-(void)caseAction{
    if (self.saleArr.count == 0) {
        [STTextHudTool showText:@"请上传全部认证图片"];
        return;
    }
    if (self.identerArr.count == 0) {
        [STTextHudTool showText:@"请上传全部认证图片"];
        return;
    }
    if (self.storeArr.count == 0) {
        [STTextHudTool showText:@"请上传全部认证图片"];
        return;
    }
     [STTextHudTool loadingWithTitle:@"上传认证中..."];
//    NSLog(@"%@--%@--%@",self.saleArr,self.identerArr,self.storeArr);
//    NSArray *arrAll = @[[self.saleArr lastObject],[self.identerArr lastObject],[self.storeArr lastObject]];
    NSArray *arrAll = [NSArray arrayWithObjects:[self.saleArr lastObject],[self.identerArr lastObject],[self.storeArr lastObject], nil];
    NSLog(@"-%@",arrAll);
    [OSSImageUploader asyncUploadImages:arrAll folder:kTempFolder accessKey:self.accessKey secretKey:self.accessKeySecret token:self.tokenStr complete:^(NSArray<NSString *> *names, UploadImageState state) {
        
        NSString *shopSale = [NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",names[0]];
          NSString *shopIdCard = [NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",names[1]];
          NSString *shopPhoto = [NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",names[2]];
        NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"shopSalesLicense":shopSale,@"shopIdcard":shopIdCard,@"shopPhoto":shopPhoto};
        [PPNetworkHelper POST:PostShopInfo parameters:dic success:^(id responseObject) {
            NSLog(@"%@",responseObject);
             [STTextHudTool hideSTHud];
            if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                _cerView = [[CertificationView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
                [_cerView.recentlyBtn addTarget:self action:@selector(recentAction) forControlEvents:(UIControlEventTouchUpInside)];
                _keyWin = [[UIApplication sharedApplication]keyWindow];
                [_keyWin addSubview:_cerView];
                
            }else
            {
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
        } failure:^(NSError *error) {
            [STTextHudTool showErrorText:@"网络较差"];
        }];
        
    }];
   
}
#pragma mark 去我的店铺
-(void)recentAction{
//    [_cerView removeAllSubviews];
//    [_keyWin removeAllSubviews];
    _cerView.hidden = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(void)leftAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
