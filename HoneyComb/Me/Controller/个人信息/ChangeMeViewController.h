//
//  ChangeMeViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeMeViewController : AllViewController
@property (nonatomic,strong) NSDictionary *modeDic;

@property (nonatomic,strong) NSDictionary *safeInfoDic;
@end
