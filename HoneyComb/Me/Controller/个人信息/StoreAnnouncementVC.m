//
//  StoreAnnouncementVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "StoreAnnouncementVC.h"
#import "XMTextView.h"
#import "UITextView+XMExtension.h"
#define WEAKSELF typeof(self) __weak weakSelf = self;
@interface StoreAnnouncementVC ()
{
    NSString *contentStr;
}
@property (nonatomic,strong) UIButton *caseBtn;
@end

@implementation StoreAnnouncementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"店铺公告";
   
    [self layoutView];
    self.view.backgroundColor = RGB(249, 249, 249, 1);
}
-(void)layoutView
{
    XMTextView *tv = [[XMTextView alloc] initWithFrame:CGRectMake(0, 0, kWidth, 200)];
    tv.tvFont = [UIFont systemFontOfSize:14];
    tv.textMaxNum = 60;
    if ([self.textStr isEqualToString:@"未填写"]) {
        tv.placeholder = @"请输入内容";
    }else
    {
        tv.contentStr = self.textStr;
        tv.placeholder = @"";
    }
    [self.view addSubview:tv];
    
   
    tv.textViewListening = ^(NSString *textViewStr) {
        NSLog(@"tv监听输入的内容：%@",textViewStr);
        //        weakSelf.
        contentStr=textViewStr;
    };

    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"保存" forState:(UIControlStateNormal)];
       [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(tv.mas_bottom).offset(20);
        make.left.mas_equalTo(12);
    }];
}
-(void)caseAction
{
    if (contentStr.length == 0) {
            [STTextHudTool showText:@"内容不能为空"];
        return;
    }
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"notice":contentStr};
    [PPNetworkHelper POST:PostChangeStore parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject );
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
          
            if (self.NextViewControllerBlock) {
                self.NextViewControllerBlock(contentStr);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
            [STTextHudTool showText:[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"msg"]]];
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
