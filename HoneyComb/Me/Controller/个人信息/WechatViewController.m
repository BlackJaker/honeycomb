//
//  WechatViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "WechatViewController.h"

@interface WechatViewController ()
@property (nonatomic,strong) UITextField *wechatTf;
@property (nonatomic,strong) UIButton *caseBtn;
@end

@implementation WechatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"微信号";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
}
-(void)layoutView
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    view.userInteractionEnabled = YES;
    UILabel *label = [[UILabel alloc]init];
    label.text = @"微 信 号 :";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = kBlackColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.wechatTf = [[UITextField alloc]init];
//    self.wechatTf.placeholder = @"请输入您的微信号";
    self.wechatTf.textColor = kBlackColor;
    self.wechatTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    if ([self.textStr isEqualToString:@"方便彩民去联系您,请填写"]) {
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请输入您的微信号" attributes:
                                          @{NSForegroundColorAttributeName:kGrayColor,
                                            NSFontAttributeName:self.wechatTf.font
                                            }];
        self.wechatTf.attributedPlaceholder = attrString;
    }else
    {
        self.wechatTf.text = self.textStr;
    }
   
    [view addSubview:self.wechatTf];
    [self.wechatTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(0);
    }];
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"确认" forState:(UIControlStateNormal)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(self.wechatTf.mas_bottom).offset(10);
        make.left.mas_equalTo(12);
    }];
}
-(void)caseAction
{
    if (self.wechatTf.text.length == 0) {
        [STTextHudTool showText:@"微信账号不能为空"];
        return;
    }
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"wechat":self.wechatTf.text};
    [PPNetworkHelper POST:PostChangeStore parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject );
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            [STTextHudTool showText:@"微信账号设置成功"];
            if (self.NextViewControllerBlock) {
                self.NextViewControllerBlock(self.wechatTf.text);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
             [STTextHudTool showText:[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"msg"]]];
        }
    } failure:^(NSError *error) {
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
