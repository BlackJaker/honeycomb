//
//  IdentityViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "IdentityViewController.h"

@interface IdentityViewController ()
@property (nonatomic,strong) UITextField *nameTf;
@property (nonatomic,strong) UITextField *numTf;
@property (nonatomic,strong) UIButton *caseBtn;
@end

@implementation IdentityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"身份验证";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self creatData];
    [self layoutView];
    
    
}
-(void)creatData
{
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId]};
    [PPNetworkHelper POST:PostGetShopIDCard parameters:dic success:^(id responseObject) {
        
        NSLog(@"%@",[responseObject objectForKey:@"msg"]);
        
        if ([[responseObject objectForKey:@"msg"] isEqualToString:@"success"]) {

            self.nameTf.text = responseObject[@"data"][@"name"];
            self.numTf.text = responseObject[@"data"][@"idcard"];
        }
    } failure:^(NSError *error) {
         NSLog(@"%@",error);
    }];
}
-(void)layoutView
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
     view.layer.shadowColor = RGB(235, 235, 235, 1).CGColor;
    view.layer.shadowOpacity = 1;

    view.layer.shadowOffset = CGSizeZero;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(91);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    view.userInteractionEnabled = YES;
    UILabel *label = [[UILabel alloc]init];
    label.text = @"真实姓名:";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = kBlackColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.nameTf = [[UITextField alloc]init];
//    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.nameTf.textColor = kBlackColor;
    self.nameTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请输入您的真实姓名" attributes:
                                      @{NSForegroundColorAttributeName:kGrayColor,
                                        NSFontAttributeName:self.nameTf.font
                                        }];
      self.nameTf.attributedPlaceholder = attrString;
    [view addSubview:self.nameTf];
    [self.nameTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(0);
    }];
    UIView *backView= [[UIView alloc]init];
    backView.backgroundColor =RGB(0, 0, 0, 0.15);
    [view addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(label.mas_bottom).offset(0);
    }];
    
    UILabel *numLabel = [[UILabel alloc]init];
    numLabel.text = @"身份证号:";
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.textColor = kBlackColor;
    numLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:numLabel];
    [numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(backView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
    }];
    self.numTf = [[UITextField alloc]init];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.numTf.textColor = kBlackColor;
    self.numTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *numString = [[NSAttributedString alloc] initWithString:@"请输入您的身份证号码" attributes:
                                      @{NSForegroundColorAttributeName:kGrayColor,
                                        NSFontAttributeName:self.numTf.font
                                        }];
      self.numTf.attributedPlaceholder = numString;
    [view addSubview:self.numTf];
    [self.numTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(backView.mas_bottom);
    }];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"注意"];
    [self.view addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo (10);
        make.left.mas_equalTo(56);
        make.top.mas_equalTo(view.mas_bottom).offset(15);
    }];
    
    UILabel *alertLabel = [[UILabel alloc]init];
    alertLabel.text = @"身份验证是提现的重要凭证,绑定后不可更改";
    alertLabel.textAlignment = NSTextAlignmentLeft;
    alertLabel.textColor = kBlackColor;
    alertLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    [self.view addSubview:alertLabel];
    [alertLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(12);
        make.top.mas_equalTo(view.mas_bottom).offset(14);
        make.left.mas_equalTo(image.mas_right).offset(11);
    }];
    
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"确认" forState:(UIControlStateNormal)];
      [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    if ([self.textStr isEqualToString:@"已认证"]) {
         self.caseBtn.hidden = YES;
        self.nameTf.userInteractionEnabled = NO;
        self.numTf.userInteractionEnabled = NO;
    }else{
        self.caseBtn.hidden = NO;
    }
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"verit"];
    
    if ([str isEqualToString:@"1"]) {
        self.caseBtn.hidden = YES;
    }
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(alertLabel.mas_bottom).offset(15);
        make.left.mas_equalTo(12);
    }];
}
-(void)caseAction{
    if (self.nameTf.text.length == 0) {
        [STTextHudTool showText:@"姓名不能为空"];
        return;
    }
    if (self.numTf.text.length ==0) {
        [STTextHudTool showText:@"身份证号码不能为空"];
        return;
    }
    if (self.numTf.text.length <18 ||self.numTf.text.length >18) {
        [STTextHudTool showText:@"身份证号码长度不正确"];
        return;
    }
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"card":self.numTf.text,@"name":self.nameTf.text};
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:PostShopIDCard parameters:dic success:^(id responseObject) {
        
        @StrongObj(self)
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"verit"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if (self.identitySuccessedBlock) {
                self.identitySuccessedBlock();
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
