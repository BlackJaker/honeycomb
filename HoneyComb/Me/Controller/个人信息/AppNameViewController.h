//
//  AppNameViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AppNameChangeSuccessedBlock)(void);

@interface AppNameViewController : AllViewController

@property (nonatomic,copy) NSString * shopName;

@property (nonatomic,copy) AppNameChangeSuccessedBlock successedBlock;

@end
