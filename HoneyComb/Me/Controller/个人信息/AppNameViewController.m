//
//  AppNameViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AppNameViewController.h"

#import "CertificationView.h"

#import "AF_StoreCertificationVC.h"

@interface AppNameViewController ()

@property (nonatomic,strong) CertificationView *cerView;
@property (nonatomic,strong) UIButton *validationBtn;
@property (nonatomic,strong) UITextField *nameTf;
@property (nonatomic,strong) UIButton *caseBtn;

@end

@implementation AppNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"APP名称";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutVIew];
}
-(void)layoutVIew
{
    UIView *v = [[UIView alloc]init];
    v.backgroundColor = [UIColor whiteColor];
    v.userInteractionEnabled = YES;
    v.layer.shadowColor = RGB(235, 235, 235, 1).CGColor;
    v.layer.borderColor = v.layer.shadowColor; // 边框颜色建议和阴影颜色一致
    v.layer.borderWidth = 0.000001; // 只要不为0就行
    v.layer.cornerRadius = 10;
    v.layer.shadowOpacity = 1;
    v.layer.shadowRadius = 3;
    v.layer.shadowOffset = CGSizeZero;
    [self.view addSubview:v];
    [v mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(127);
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(12);
    }];
    
    UILabel *titleL = [[UILabel alloc]init];
    [v addSubview:titleL];
    titleL.text = @"请设置您的彩店名称";
    titleL.textColor = kBlackColor;
    titleL.textAlignment = NSTextAlignmentCenter;
    titleL.font = [UIFont fontWithName:kPingFangRegular size:15];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(40);
    }];
    
    UIView *blackView = [[UIView alloc]init];
    blackView.backgroundColor = [UIColor whiteColor];
    blackView.userInteractionEnabled = YES;
    blackView.layer.borderColor = RGB(154, 154, 154, 1).CGColor; // 边框颜色建议和阴影颜色一致
    blackView.layer.borderWidth = 0.5; // 只要不为0就行
    blackView.layer.cornerRadius = 1;
    blackView.layer.masksToBounds = YES;
    [v addSubview:blackView];
    [blackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24-51);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(titleL.mas_bottom);
        make.left.mas_equalTo(25);
    }];
    
    self.validationBtn = [[UIButton alloc]init];
    [self.validationBtn setTitle:@"验证" forState:(UIControlStateNormal)];
    [self.validationBtn setTitleColor:kRedColor forState:(UIControlStateNormal)];
    self.validationBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [self.validationBtn addTarget:self action:@selector(validationAction) forControlEvents:(UIControlEventTouchUpInside)];
    [blackView addSubview:self.validationBtn];
    [self.validationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    UIView *verticalView = [[UIView alloc]init];
    verticalView.backgroundColor =RGB(154, 154, 154, 1);
   
    [blackView addSubview:verticalView];
    [verticalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(0.5);
        make.height.mas_equalTo(35);
        make.top.mas_equalTo(5);
        make.right.mas_equalTo(self.validationBtn.mas_left).offset(0);
    }];
    
    self.nameTf = [[UITextField alloc]init];
    self.nameTf.text = self.shopName;
    self.nameTf.font = [UIFont fontWithName:kPingFangRegular size:15];
    self.nameTf.textAlignment = NSTextAlignmentCenter;
    self.nameTf.textColor = kBlackColor;
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请填写店铺名字" attributes:
                                      @{NSForegroundColorAttributeName:kGrayColor,
                                        NSFontAttributeName:self.nameTf.font
                                        }];
    self.nameTf.attributedPlaceholder = attrString;
    [blackView addSubview:self.nameTf];
    [self.nameTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(196);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(50);
    }];
    
    
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"确定" forState:(UIControlStateNormal)];
    [self.caseBtn addTarget:self action:@selector(nextAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(v.mas_bottom).offset(20);
        make.left.mas_equalTo(12);
    }];
}
-(void)validationAction
{
    
    if (self.nameTf.text.length ==0 ) {
        [STTextHudTool showText:@"App名称不能为空"];
        return;
    }
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"appName":self.nameTf.text};
    [PPNetworkHelper POST:PostVerifyName parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
//        NSDictionary *d = [solveJsonData changeType:[responseObject objectForKey:@"data"] ];
       
        if ([[[responseObject objectForKey:@"data"] objectForKey:@"isExist"] integerValue]==0) {
            [STTextHudTool showText:@"App名称可用"];
            
        }else
        {
                   [STTextHudTool showText:@"App名称不可用"];
        }
    } failure:^(NSError *error) {
        
    }];
//    _cerView = [[CertificationView alloc]initWithFrame:CGRectMake(0, 64, kWidth, kHeight)];
//    UIWindow *keyWin = [[UIApplication sharedApplication]keyWindow];
//    [keyWin addSubview:_cerView];
}
-(void)nextAction
{
    
    if (self.nameTf.text.length ==0 ) {
        [STTextHudTool showText:@"App名称不能为空"];
        return;
    }
    @WeakObj(self)
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"appName":self.nameTf.text};
    [PPNetworkHelper POST:PostChangeStore parameters:dic success:^(id responseObject) {
        
        @StrongObj(self)
        NSLog(@"%@",responseObject);
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            if (self.successedBlock) {
                self.successedBlock();
            }

            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            [STTextHudTool showErrorText:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        
    }];
    
  
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
