//
//  WithdrawalViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SetPasswordSuccessedBlock)(void);

@interface WithdrawalViewController : AllViewController

-(instancetype)initType:(NSInteger)type controller:(AllViewController *)viewController safeInfoDic:(NSDictionary *)safeInfoDic;

@property (nonatomic,copy) SetPasswordSuccessedBlock setPasswordSuccessedBlock;

@end
