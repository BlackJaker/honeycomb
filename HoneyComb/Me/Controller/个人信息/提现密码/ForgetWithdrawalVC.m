//
//  ForgetWithdrawalVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "ForgetWithdrawalVC.h"
#import "UIButton+Timer.h"
#import "ForgetWithdrawalTwoVC.h"

@interface ForgetWithdrawalVC ()<UITextFieldDelegate>

@property (nonatomic,strong) UITextField *phoneTf;
@property (nonatomic,strong) UITextField *numTf;
@property (nonatomic,strong) UIButton *caseBtn;
@property (nonatomic,strong) UIButton *sendBtn;//获取验证码

@end

@implementation ForgetWithdrawalVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.titleLabe.text = @"忘记提现密码";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
}
-(void)layoutView
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
    view.layer.shadowColor = RGB(235, 235, 235, 1).CGColor;
    view.layer.shadowOpacity = 1;
    
    view.layer.shadowOffset = CGSizeZero;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(91);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    view.userInteractionEnabled = YES;
    UILabel *label = [[UILabel alloc]init];
    label.text = @"手 机 号:";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = kBlackColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.phoneTf = [[UITextField alloc]init];
    self.phoneTf.textColor = kBlackColor;
    self.phoneTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    
    if (self.phoneNumber) {
        self.phoneTf.userInteractionEnabled = NO;
        NSString * phoneNum = self.phoneNumber;
        NSString * subString = [phoneNum substringWithRange:NSMakeRange(3, 4)];
        self.phoneTf.text = [phoneNum stringByReplacingOccurrencesOfString:subString withString:@"****"];
    }
    [view addSubview:self.phoneTf];
    [self.phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(0);
    }];
    UIView *backView= [[UIView alloc]init];
    backView.backgroundColor =RGB(0, 0, 0, 0.15);
    [view addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(label.mas_bottom).offset(0);
    }];
    
    UILabel *numLabel = [[UILabel alloc]init];
    numLabel.text = @"验 证 码:";
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.textColor = kBlackColor;
    numLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:numLabel];
    [numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(backView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
    }];
    self.numTf = [[UITextField alloc]init];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.numTf.textColor = kBlackColor;
    self.numTf.delegate = self;
    self.numTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *numString = [[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:
                                     @{NSForegroundColorAttributeName:kGrayColor,
                                       NSFontAttributeName:self.numTf.font
                                       }];
    self.numTf.attributedPlaceholder = numString;
    [view addSubview:self.numTf];
    [self.numTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(backView.mas_bottom);
    }];
    self.sendBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    
    [self.sendBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.sendBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    [self.sendBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.sendBtn setTitleColor:[UIColor redColor] forState:(UIControlStateNormal)];
    [view addSubview:self.sendBtn];
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(backView.mas_bottom);
        make.right.mas_equalTo(-20);
    }];
    
    
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"下一步" forState:(UIControlStateNormal)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(numLabel.mas_bottom).offset(15);
        make.left.mas_equalTo(12);
    }];
}
#pragma mark 下一步
-(void)caseAction
{
    if (self.numTf.text.length == 0) {
        [STTextHudTool showText:@"验证码不能为空"];
        return;
    }
    if (self.phoneTf.text.length == 0) {
        [STTextHudTool showText:@"手机号不能为空"];
        return;
    }
    if (self.numTf.text.length == 0) {
        [STTextHudTool showText:@"验证码不能为空"];
        return;
    }
    if (self.numTf.text.length != 6) {
        [STTextHudTool showText:@"验证码格式不正确"];
        return;
    }
    [STTextHudTool showWaitText:@"验证中..."];
    NSDictionary *dic = @{@"shopId":[LoginUser shareLoginUser].shopId,@"verifyCode":self.numTf.text};
    [PPNetworkHelper POST:PostResetPayVerifyCode parameters:dic success:^(id responseObject) {
        [STTextHudTool hideSTHud];
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            [STTextHudTool showText:@"验证成功"];
             [self.navigationController pushViewController:[[ForgetWithdrawalTwoVC alloc]init] animated:YES];
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSError *error) {
        
    }];
   
}

#pragma mark --倒计时
-(void)buttonAction:(UIButton *)button
{
    if (_phoneTf.text.length == 0) {
        NSLog(@"手机号不能为空");
        [STTextHudTool showText:@"手机号不能为空"];
        return;
    }
    if (_phoneTf.text.length != 11) {
          [STTextHudTool showText:@"手机号长度不正确"];
        return;
    }
    
    NSDictionary *dic = @{@"shopId":[LoginUser shareLoginUser].shopId};
    
    [PPNetworkHelper POST:PostSetPayVerifyCode parameters:dic success:^(id responseObject) {
        NSLog(@"成功-%@",responseObject);
        [STTextHudTool showSuccessText:@"验证码已发送"];
    } failure:^(NSError *error) {
        NSLog(@"失败-%@",error);
        [STTextHudTool showErrorText:@"验证码发送失败"];
    }];
    
    button.time = 60;
    button.format = @"%ld秒后重试";
    [button startTimer];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  --  textfield delegate  --

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
    if (range.length == 1 && string.length == 0) {
        return YES;
    }else if(textField == self.numTf) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBER] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        BOOL isRight = [string isEqualToString:filtered];
        
        if (textField.text.length >= 6) {
            isRight = NO;
        }
        
        return isRight;
    }
    return YES;
    
}

@end
