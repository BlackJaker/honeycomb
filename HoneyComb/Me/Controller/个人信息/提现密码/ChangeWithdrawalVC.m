//
//  ChangeWithdrawalVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "ChangeWithdrawalVC.h"
#import "ForgetWithdrawalVC.h"
@interface ChangeWithdrawalVC ()<UITextFieldDelegate>
@property (nonatomic,strong) UITextField *oldTf;
@property (nonatomic,strong) UITextField *nameTf;
@property (nonatomic,strong) UITextField *numTf;
@property (nonatomic,strong) UIButton *caseBtn;
@end

@implementation ChangeWithdrawalVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.titleLabe.text = @"修改提现密码";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
}
-(void)layoutView
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
    view.layer.shadowColor = RGB(235, 235, 235, 1).CGColor;
    view.layer.shadowOpacity = 1;
    
    view.layer.shadowOffset = CGSizeZero;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(137);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    view.userInteractionEnabled = YES;
    UILabel *label = [[UILabel alloc]init];
    label.text = @"旧 密 码:";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = kBlackColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.oldTf = [[UITextField alloc]init];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.oldTf.textColor = kBlackColor;
    self.oldTf.delegate = self;
    self.oldTf.secureTextEntry = YES;
    self.oldTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请输入旧密码" attributes:
                                      @{NSForegroundColorAttributeName:kGrayColor,
                                        NSFontAttributeName:self.oldTf.font
                                        }];
    self.oldTf.attributedPlaceholder = attrString;
    [view addSubview:self.oldTf];
    [self.oldTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(0);
    }];
    UIView *backView= [[UIView alloc]init];
    backView.backgroundColor =RGB(0, 0, 0, 0.15);
    [view addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(label.mas_bottom).offset(0);
    }];
    
    UILabel *numLabel = [[UILabel alloc]init];
    numLabel.text = @"新 密 码:";
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.textColor = kBlackColor;
    numLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:numLabel];
    [numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(backView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
    }];
    self.nameTf = [[UITextField alloc]init];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.nameTf.delegate = self;
    self.nameTf.secureTextEntry = YES;
    self.nameTf.textColor = kBlackColor;
    self.nameTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *numString = [[NSAttributedString alloc] initWithString:@"请输入6-12位数字/字母或组合" attributes:
                                     @{NSForegroundColorAttributeName:kGrayColor,
                                       NSFontAttributeName:self.nameTf.font
                                       }];
    self.nameTf.attributedPlaceholder = numString;
    [view addSubview:self.nameTf];
    [self.nameTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(backView.mas_bottom);
    }];
    
    UIView *backView2= [[UIView alloc]init];
    backView2.backgroundColor =RGB(0, 0, 0, 0.15);
    [view addSubview:backView2];
    [backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(numLabel.mas_bottom).offset(0);
    }];
    UILabel *numLabel2 = [[UILabel alloc]init];
    numLabel2.text = @"新 密 码:";
    numLabel2.textAlignment = NSTextAlignmentCenter;
    numLabel2.textColor = kBlackColor;
    numLabel2.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:numLabel2];
    [numLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(backView2.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
    }];
    self.numTf = [[UITextField alloc]init];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.numTf.textColor = kBlackColor;
    self.numTf.delegate = self;
    self.numTf.secureTextEntry = YES;
    self.numTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *numString2 = [[NSAttributedString alloc] initWithString:@"请再次输入" attributes:
                                     @{NSForegroundColorAttributeName:kGrayColor,
                                       NSFontAttributeName:self.numTf.font
                                       }];
    self.numTf.attributedPlaceholder = numString2;
    [view addSubview:self.numTf];
    [self.numTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(backView2.mas_bottom);
    }];
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"确认" forState:(UIControlStateNormal)];
    [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(numLabel2.mas_bottom).offset(15);
        make.left.mas_equalTo(12);
    }];
    
    UIButton *forgetBtn = [[UIButton alloc]init];
    [forgetBtn setTitle:@"忘记提现密码?" forState:(UIControlStateNormal)];
    [forgetBtn setTitleColor:kGrayColor forState:(UIControlStateNormal)];
    forgetBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [forgetBtn addTarget:self action:@selector(forgetAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:forgetBtn];
    
    CGFloat  bottom = IPHONE_X?TAB_BAR_SAFE_HEIGHT:25;
    
    [forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(16);
        make.left.mas_equalTo(kWidth/2-50);
        make.bottom.mas_equalTo(-bottom);
    }];
}
-(void)caseAction
{
    if (self.oldTf.text.length == 0) {
        [STTextHudTool showText:@"旧密码不能为空"];
        return;
    }
    if (self.oldTf.text.length < 6 || self.oldTf.text.length > 12) {
        [STTextHudTool showText:@"旧密码格式不正确"];
        return;
    }
    if (self.nameTf.text.length == 0) {
        [STTextHudTool showText:@"新密码不能为空"];
        return;
    }
    if (self.nameTf.text.length < 6 || self.nameTf.text.length > 12) {
        [STTextHudTool showText:@"新密码格式不正确"];
        return;
    }
    if (self.numTf.text.length == 0) {
        [STTextHudTool showText:@"新密码不能为空"];
        return;
    }
    if (self.numTf.text.length < 6 || self.numTf.text.length > 12) {
        [STTextHudTool showText:@"重复密码格式不正确"];
        return;
    }
    if (self.numTf.text != self.nameTf.text) {
        [STTextHudTool showText:@"两次密码不一致"];
        return;
    }
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"oldPayPassword":self.oldTf.text,@"newPayPassword":self.nameTf.text};
    NSLog(@"%@",dic);
    [PPNetworkHelper POST:PostUpdatePassWord parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            [STTextHudTool showText:@"修改成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)forgetAction
{
    ForgetWithdrawalVC * forgetWithdrawVc = [[ForgetWithdrawalVC alloc]init];
    forgetWithdrawVc.phoneNumber = self.phoneNumber;
    [self.navigationController pushViewController:forgetWithdrawVc animated:YES];
}
#pragma mark  --  textfield delegate  --

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
    if (range.length == 1 && string.length == 0) {
        return YES;
    }else if(textField == self.nameTf || textField == self.numTf || textField == self.oldTf) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ALPHANUM] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        BOOL isRight = [string isEqualToString:filtered];
        
        if (textField.text.length >= 12) {
            isRight = NO;
        }
        
        return isRight;
    }
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
