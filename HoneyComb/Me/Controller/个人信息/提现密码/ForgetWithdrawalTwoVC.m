//
//  ForgetWithdrawalTwoVC.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "ForgetWithdrawalTwoVC.h"

@interface ForgetWithdrawalTwoVC ()<UITextFieldDelegate>
@property (nonatomic,strong) UITextField *nameTf;
@property (nonatomic,strong) UITextField *numTf;
@property (nonatomic,strong) UIButton *caseBtn;
@end

@implementation ForgetWithdrawalTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabe.text = @"忘记提现密码";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    [self layoutView];
}
-(void)layoutView
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    view.userInteractionEnabled = YES;
    view.layer.shadowColor = RGB(235, 235, 235, 1).CGColor;
    view.layer.shadowOpacity = 1;
    
    view.layer.shadowOffset = CGSizeZero;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(91);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    view.userInteractionEnabled = YES;
    UILabel *label = [[UILabel alloc]init];
    label.text = @"新 密 码:";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = kBlackColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.nameTf = [[UITextField alloc]init];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.nameTf.textColor = kBlackColor;
    self.nameTf.delegate = self;
    self.nameTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请输入6-12位数字/字母或组合" attributes:
                                      @{NSForegroundColorAttributeName:kGrayColor,
                                        NSFontAttributeName:self.nameTf.font
                                        }];
    self.nameTf.attributedPlaceholder = attrString;
    [view addSubview:self.nameTf];
    [self.nameTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(0);
    }];
    UIView *backView= [[UIView alloc]init];
    backView.backgroundColor =RGB(0, 0, 0, 0.15);
    [view addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(label.mas_bottom).offset(0);
    }];
    
    UILabel *numLabel = [[UILabel alloc]init];
    numLabel.text = @"确认密码:";
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.textColor = kBlackColor;
    numLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:numLabel];
    [numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(backView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
    }];
    self.numTf = [[UITextField alloc]init];
    //    self.nameTf.placeholder = @"请输入您的真实姓名";
    self.numTf.textColor = kBlackColor;
    self.numTf.delegate = self;
    self.numTf.font = [UIFont fontWithName:kPingFangRegular size:13];
    NSAttributedString *numString = [[NSAttributedString alloc] initWithString:@"请再次输入" attributes:
                                     @{NSForegroundColorAttributeName:kGrayColor,
                                       NSFontAttributeName:self.numTf.font
                                       }];
    self.numTf.attributedPlaceholder = numString;
    [view addSubview:self.numTf];
    [self.numTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-100);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(90);
        make.top.mas_equalTo(backView.mas_bottom);
    }];
    
    
    self.caseBtn = [[UIButton alloc]init];
    [self.caseBtn setTitle:@"确认" forState:(UIControlStateNormal)];
    [self.caseBtn addTarget:self action:@selector(caseAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.caseBtn setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.caseBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:19];
    self.caseBtn.layer.cornerRadius = 3.0f;
    self.caseBtn.layer.masksToBounds = YES;
    self.caseBtn.backgroundColor = kRedColor;
    [self.view addSubview:self.caseBtn];
    [self.caseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-24);
        make.height.mas_equalTo(43);
        make.top.mas_equalTo(numLabel.mas_bottom).offset(15);
        make.left.mas_equalTo(12);
    }];
}
-(void)caseAction
{
    if (self.nameTf.text.length == 0 || self.numTf.text.length == 0) {
        [STTextHudTool showText:@"密码不能为空"];
        return;
    }
    if (self.nameTf.text.length <6 || self.nameTf.text.length>12) {
        [STTextHudTool showText:@"密码长度不正确"];
        return;
    }
    if (self.numTf.text != self.nameTf.text) {
        [STTextHudTool showText:@"两次密码不一致"];
        return;
    }
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser]shopId ],@"payPassword":self.numTf.text};
    [PPNetworkHelper POST:PostSerPayPassWord parameters:dic success:^(id responseObject) {
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            [STTextHudTool showText:@"提现密码重置成功"];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark  --  textfield delegate  --

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //这里的if时候为了获取删除操作,如果没有次if会造成当达到字数限制后删除键也不能使用的后果.
    if (range.length == 1 && string.length == 0) {
        return YES;
    }else if(textField == self.nameTf || textField == self.numTf) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ALPHANUM] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        BOOL isRight = [string isEqualToString:filtered];
        
        if (textField.text.length >= 12) {
            isRight = NO;
        }
        
        return isRight;
    }
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
