//
//  IdentityViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/25.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^IdentitySuccessedBlock)(void);

@interface IdentityViewController : AllViewController

@property (nonatomic,copy) NSString *textStr;

@property (nonatomic,copy) IdentitySuccessedBlock  identitySuccessedBlock;

@end
