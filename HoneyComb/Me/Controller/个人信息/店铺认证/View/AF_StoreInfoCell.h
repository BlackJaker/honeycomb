//
//  AF_StoreInfoCell.h
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AF_StoreInfoCell : UITableViewCell

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *line;
@property (nonatomic,strong) UITextField *contentTextField;

@property (nonatomic,strong) UITextField *contentButton;

-(void)reloadCell;

@end

NS_ASSUME_NONNULL_END
