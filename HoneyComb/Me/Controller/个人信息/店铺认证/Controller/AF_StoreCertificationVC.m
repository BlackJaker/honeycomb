//
//  AF_StoreCertificationVC.m
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AF_StoreCertificationVC.h"

#import "AF_StoreInfoCell.h"

#import "YGDatePickerView.h"

#import "OSSImageUploader.h"

#import "TZImagePickerController.h"

#import "CertificationView.h"

static NSString *kTempFolder = @"shop/verify";

static NSString * kOSSImageUrl = @"http://aliimg.afcplay.com/";

@interface AF_StoreCertificationVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,TZImagePickerControllerDelegate>
{
    CGFloat  noteLabelHeight;
    
    CGFloat  rechangedViewHeight;
    
    CGFloat  originY;
    CGFloat  leftMar;
    CGFloat  rightMar;
    CGFloat  headHeight;
    CGFloat  cellHeight;
    CGFloat  confrimHeight;
    CGFloat  photoBackHeight;
    
    UITextField * currentTextField;
}
@property (nonatomic,strong) YYLabel * noteLabel;

@property (nonatomic,strong) UIScrollView * contentScrollView;

@property (nonatomic,strong) UITableView * tableView;

@property (nonatomic,copy) NSArray * names;

@property (nonatomic,copy) NSArray * placeholdes;

@property (nonatomic,copy) YGDatePickerView * datePicker;

@property (nonatomic,strong) UIView * saleCardView;

@property (nonatomic,strong) UIImageView * saleCardImgView;

@property (nonatomic,strong) UIButton * saleCardButton;

@property (nonatomic,strong) UIView * photoView;

@property (nonatomic,strong) UIImageView * photoImgView;

@property (nonatomic,strong) UIButton * photoButton;

@property (nonatomic,strong) UIView * idCardView;

@property (nonatomic,strong) UIImageView * idCardImgView;

@property (nonatomic,strong) UIButton * idCardButton;

@property (nonatomic,strong) NSMutableArray *saleArr;
@property (nonatomic,strong) NSMutableArray *photoArr;
@property (nonatomic,strong) NSMutableArray *idCardArr;
@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;

@property (nonatomic,strong) NSMutableArray *texts;  //

@property (nonatomic,strong) CertificationView *cerView;
@property (nonatomic,strong)   UIWindow *keyWin;

@property (nonatomic,strong) UIButton * confirmButton;

@property (nonatomic,strong) UITextField * currentTextField;

@property (nonatomic,strong) NSDictionary * certificationInfo;

@end

@implementation AF_StoreCertificationVC

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.shopStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"shopStatus"];
    
    if ([self.shopStatus isEqualToString:@"NORMAL"]) {
        [self certificationInfoRequest];
    }
    
    [self propertySetup];
    
    [self contentSetup];
    
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    self.titleLabe.text = @"店铺认证";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f7f7f7"];
    
    originY = 10;
    leftMar = 16;
    rightMar = 20;
    headHeight = 39;
    cellHeight = 52;
    
    noteLabelHeight = 62;
    
    rechangedViewHeight = 405;
    
    photoBackHeight = 185;
    
    confrimHeight = TAB_BAR_ITEM_HEIGHT;
    
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self.contentScrollView addSubview:self.noteLabel];
    
    [self.tableView reloadData];
    
    [self saleCardAndPhotoViewSetup];
    
    [_contentScrollView setContentSize:CGSizeMake(kWidth, noteLabelHeight + self.tableView.height + (originY + photoBackHeight) * 3 + originY)];

}

-(void)saleCardAndPhotoViewSetup{
    
    [self.saleCardButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
    [self.photoButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
    [self.idCardButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
    self.saleCardImgView.backgroundColor = [UIColor clearColor];
    self.photoImgView.backgroundColor = [UIColor clearColor];
    self.idCardImgView.backgroundColor = [UIColor clearColor];
    
    if (![self.shopStatus isEqualToString:@"NORMAL"]) {
        [self.confirmButton setTitle:@"确认" forState:(UIControlStateNormal)];
    }
}

#pragma mark  --  lazy  --

-(UIScrollView *)contentScrollView{
    
    CGFloat  bottomMar = IPHONE_X?TabBarHeight:TabBarHeight + originY * 2 + 10;
    
    if (!_contentScrollView) {
        
        _contentScrollView = [[UIScrollView alloc]init];
        _contentScrollView.backgroundColor = [UIColor clearColor];
        
        [self.view addSubview:_contentScrollView];
        
        [_contentScrollView makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(0);
            make.right.equalTo(0);
            make.top.equalTo(0);
            make.bottom.equalTo(-bottomMar);
        }];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignResponderTap)];
        [_contentScrollView addGestureRecognizer:tap];
    }
    return _contentScrollView;
}

-(void)resignResponderTap{
    for (int i = 0; i < 4; i ++) {
        UITextField * tf = [self.view viewWithTag:1000 + i];
        [tf resignFirstResponder];
    }
}

-(YYLabel *)noteLabel{
    
    NSMutableAttributedString *text  = [[NSMutableAttributedString alloc] initWithString:@"为了给您和彩民持续提供安全便捷的服务,请您认真上传认证资料,我们会在24小时内电话通知您审核结果。"];
    text.lineSpacing = 10;
    text.font = [UIFont systemFontOfSize:13];
    text.color = [UIColor colorWithHexString:@"#666666"];
    
    if (!_noteLabel) {
        _noteLabel = [[YYLabel alloc]initWithFrame:CGRectMake(leftMar, 0,kWidth - (leftMar + rightMar) , noteLabelHeight)];
        _noteLabel.numberOfLines = 0;
        _noteLabel.attributedText = text;
    }
    return _noteLabel;
}

-(UITableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, noteLabelHeight, kWidth, headHeight + self.names.count * cellHeight)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.scrollEnabled = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.contentScrollView addSubview:_tableView];
    }
    return _tableView;
}

-(NSArray *)names{
    
    if (!_names) {
        _names = @[@"店铺名称:",@"真实姓名:",@"身份证号:",@"店铺地址:"];
        
    }
    return _names;
}
-(NSArray *)placeholdes{
    
    if (!_placeholdes) {
        _placeholdes = @[@"请输入店铺名称",@"请输入真实姓名",@"请输入身份证号",@"请输入店铺地址"];
        
    }
    return _placeholdes;
}

// 代销证

-(UIView *)saleCardView{
    
    if (!_saleCardView) {
        _saleCardView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame) + originY, kWidth, photoBackHeight)];
        _saleCardView.backgroundColor = [UIColor whiteColor];
        [self.contentScrollView addSubview:_saleCardView];
    }
    return _saleCardView;
}

-(UIButton *)saleCardButton{
    
    CGFloat  top = 26,width = 88,height = 113,left = 50,yOrigin = 11,nHeight = 13;
    
    if (!_saleCardButton) {
        _saleCardButton = [[UIButton alloc]initWithFrame:CGRectMake(left, top, width, height)];
        [_saleCardButton addTarget:self action:@selector(addSaleCardAction) forControlEvents:UIControlEventTouchUpInside];
        [self.saleCardView addSubview:_saleCardButton];
        
        UILabel * noteLabel = [[UILabel alloc]initWithFrame:CGRectMake(left, top + height + yOrigin, width, nHeight)];
        noteLabel.text = @"上传代销证";
        noteLabel.textColor = [UIColor colorWithHexString:@"#5c6272"];
        noteLabel.font = [UIFont fontWithName:kMedium size:13];
        noteLabel.textAlignment = NSTextAlignmentCenter;
        [self.saleCardView addSubview:noteLabel];
        
        UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, height)];
        line.center = CGPointMake(self.saleCardView.centerx, top + height/2);
        line.backgroundColor = [UIColor colorWithHexString:@"#e0e0e0"];
        [self.saleCardView addSubview:line];
    }
    return _saleCardButton;
}

-(void)addSaleCardAction{
    
    [self choosedImgWithType:0];
    
}

-(UIImageView *)saleCardImgView{
    
    CGFloat  top = 26,width = 88,height = 113,right = 50;
    
    if (!_saleCardImgView) {
        _saleCardImgView = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth - (width + right), top, width, height)];
        _saleCardImgView.image = [UIImage imageNamed:@"shili1"];
        [self.saleCardView addSubview:_saleCardImgView];
    }
    return _saleCardImgView;
}

// 门头照
-(UIView *)photoView{
    
    if (!_photoView) {
        _photoView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.saleCardView.frame) + originY, kWidth, photoBackHeight)];
        _photoView.backgroundColor = [UIColor whiteColor];
        [self.contentScrollView addSubview:_photoView];
        
    }
    return _photoView;
}

-(UIButton *)photoButton{
    
    CGFloat  top = 26,width = 156,height = 113,left = 16,yOrigin = 11,nHeight = 13;
    
    if (!_photoButton) {
        _photoButton = [[UIButton alloc]initWithFrame:CGRectMake(left, top, width, height)];
        [_photoButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
        [_photoButton addTarget:self action:@selector(addPhotoButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.photoView addSubview:_photoButton];
        
        UILabel * noteLabel = [[UILabel alloc]initWithFrame:CGRectMake(left, top + height + yOrigin, width, nHeight)];
        noteLabel.text = @"上传门头照";
        noteLabel.textColor = [UIColor colorWithHexString:@"#5c6272"];
        noteLabel.font = [UIFont fontWithName:kMedium size:13];
        noteLabel.textAlignment = NSTextAlignmentCenter;
        [self.photoView addSubview:noteLabel];
        
        UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, height)];
        line.center = CGPointMake(self.photoView.centerx, top + height/2);
        line.backgroundColor = [UIColor colorWithHexString:@"#e0e0e0"];
        [self.photoView addSubview:line];
    }
    return _photoButton;
}

-(void)addPhotoButtonAction{
    [self choosedImgWithType:1];
}

-(UIImageView *)photoImgView{
    
    CGFloat  top = 26,width = 156,height = 113,right = 16;
    
    if (!_photoImgView) {
        _photoImgView = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth - (width + right), top, width, height)];
        _photoImgView.image = [UIImage imageNamed:@"shili2"];
        [self.photoView addSubview:_photoImgView];
    }
    return _photoImgView;
}

// 身份证
-(UIView *)idCardView{
    
    if (!_idCardView) {
        _idCardView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.photoView.frame) + originY, kWidth, photoBackHeight)];
        _idCardView.backgroundColor = [UIColor whiteColor];
        [self.contentScrollView addSubview:_idCardView];
        
    }
    return _idCardView;
}

-(UIButton *)idCardButton{
    
    CGFloat  top = 26,width = 88,height = 113,left = 50,yOrigin = 11,nHeight = 13;
    
    if (!_idCardButton) {
        _idCardButton = [[UIButton alloc]initWithFrame:CGRectMake(left, top, width, height)];
        [_idCardButton setImage:[UIImage imageNamed:@"pic_default"] forState:UIControlStateNormal];
        [_idCardButton addTarget:self action:@selector(addIdCardButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.idCardView addSubview:_idCardButton];
        
        UILabel * noteLabel = [[UILabel alloc]initWithFrame:CGRectMake(left - 20, top + height + yOrigin, width + 40, nHeight)];
        noteLabel.text = @"上传手持身份证";
        noteLabel.textColor = [UIColor colorWithHexString:@"#5c6272"];
        noteLabel.font = [UIFont fontWithName:kMedium size:13];
        noteLabel.textAlignment = NSTextAlignmentCenter;
        [self.idCardView addSubview:noteLabel];
        
        UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, height)];
        line.center = CGPointMake(self.idCardView.centerx, top + height/2);
        line.backgroundColor = [UIColor colorWithHexString:@"#e0e0e0"];
        [self.idCardView addSubview:line];
    }
    return _idCardButton;
}

-(void)addIdCardButtonAction{
    [self choosedImgWithType:2];
}

-(UIImageView *)idCardImgView{
    
    CGFloat  top = 26,width = 88,height = 113,right = 50;
    
    if (!_idCardImgView) {
        _idCardImgView = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth - (width + right), top, width, height)];
        _idCardImgView.image = [UIImage imageNamed:@"shenfen"];
        [self.idCardView addSubview:_idCardImgView];
    }
    return _idCardImgView;
}

-(void)choosedImgWithType:(NSInteger)type{
    
    NSArray * buttons = @[self.saleCardButton,self.photoButton,self.idCardButton];
    
    @WeakObj(self)
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {

        @StrongObj(self)
        
        switch (type) {
            case 0:
                self.saleArr = [NSMutableArray arrayWithArray:photos];
                break;
            case 1:
                self.photoArr = [NSMutableArray arrayWithArray:photos];
                break;
            case 2:
                self.idCardArr = [NSMutableArray arrayWithArray:photos];
                break;
            default:
                break;
        }
        
        UIButton * button = [buttons objectAtIndex:type];
        [button setImage:[photos firstObject] forState:UIControlStateNormal];

    }];
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

-(UIButton *)confirmButton{
    
    CGFloat leftMar = 12,bottomMargin = IPHONE_X?TAB_BAR_SAFE_HEIGHT:originY;
    
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc]init];
        [_confirmButton setTitleColor:RGB(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _confirmButton.titleLabel.font = [UIFont fontWithName:kBold size:19];
        _confirmButton.layer.cornerRadius = 5.0f;
        _confirmButton.layer.masksToBounds = YES;
        [_confirmButton addTarget:self action:@selector(storeCeritificationAction) forControlEvents:(UIControlEventTouchUpInside)];
        _confirmButton.backgroundColor = kRedColor;
        [self.view addSubview:_confirmButton];
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kWidth - leftMar * 2);
            make.height.mas_equalTo(confrimHeight);
            make.bottom.mas_equalTo(-bottomMargin);
            make.left.mas_equalTo(leftMar);
        }];
    }
    return _confirmButton;
    
}

-(void)storeCeritificationAction{
    
    NSString * errorMessage = [self getErrorMessage];
    
    if (errorMessage) {
        [FC_Manager showToastWithText:errorMessage];
        return;
    }
    
    [self postTokenUpdateRequest];      // 获取token
}

-(NSString *)getErrorMessage{
    
    NSString * errorMessage = nil;
    
    NSString * message = @"请确保所有信息已经填写完整";
    
    [self.texts removeAllObjects];
    for (int i = 0; i < 4; i ++) {
        UITextField * textField = (UITextField *)[self.view viewWithTag:1000 + i];
        if (textField.text.length == 0) {
            errorMessage = message;
            break;
        }else{
            [self.texts addObject:textField.text];
        }
    }
    if (!errorMessage) {
        if (self.saleArr.count == 0 || self.photoArr.count == 0 || self.idCardArr.count == 0) {
            errorMessage = message;
        }
    }
    
    return errorMessage;
}

#pragma mark  --  table view datasource  --

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.names.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString * alipayCell = @"alipayCell";
    
    AF_StoreInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:alipayCell];
    
    if (cell == nil) {
        cell = [[AF_StoreInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:alipayCell];
    }
    cell.nameLabel.text = [self.names objectAtIndex:indexPath.row];
    if (![self.shopStatus isEqualToString:@"NORMAL"]) {
        cell.contentTextField.enabled = YES;
        cell.contentTextField.placeholder = [self.placeholdes objectAtIndex:indexPath.row];
        cell.contentTextField.delegate = self;
        cell.contentTextField.tag = 1000 + indexPath.row;

        
    }else{
        cell.contentTextField.enabled = NO;

        if (self.certificationInfo) {
            NSArray * info = @[self.certificationInfo[@"appName"],self.certificationInfo[@"name"],self.certificationInfo[@"idCard"],self.certificationInfo[@"address"]];
            cell.contentTextField.text = info[indexPath.row];
        }
        
    }

    cell.indexPath = indexPath;
    
    [cell reloadCell];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark  --  table view delegate  --

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, headHeight)];
    
    CGFloat  top = 21;
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(leftMar, top, kWidth - (leftMar + rightMar), headHeight - top)];
    label.attributedText = [self attributeSubString:@"(此页全为必填项目,提交后不可修改)"];
    [view addSubview:label];
    
    return view;
    
}

-(NSAttributedString *)attributeSubString:(NSString *)subString{
    
    NSString * prizeStr = [NSString stringWithFormat:@"添加信息 %@",subString];
    
    NSDictionary * attributes = @{
                                  NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#333333"],
                                  NSFontAttributeName:[UIFont boldSystemFontOfSize:18]};
    
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc]initWithString:prizeStr attributes:attributes];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#F02E2E"] range:[prizeStr rangeOfString:subString]];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kMedium size:14] range:[prizeStr rangeOfString:subString]];
    return attributeString;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return cellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return  headHeight;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    for (int i = 0; i < 4; i ++) {
        UITextField * tf = [self.view viewWithTag:1000 + i];
        [tf resignFirstResponder];
    }
    
}

#pragma mark  --  textfield  delegate  --

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 1002) {
        textField.keyboardType = UIKeyboardTypePhonePad;
    }else if(textField.tag == 1003){
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    }else{
        textField.keyboardType = UIKeyboardTypeDefault;
    }

    currentTextField = textField;
    
    return  YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (range.length == 1 && string.length == 0) {
        return YES;
    }else if(textField.tag == 1000 || textField.tag == 1001) {
        return textField.text.length < 16;
    }else if(textField.tag == 1002){
        
        BOOL isIdCardNumber =  [self validateNumber:string];
        
        if (isIdCardNumber) {
            if (textField.text.length < 20) {
                isIdCardNumber = YES;
            }else{
                isIdCardNumber = NO;
            }
        }
        
        return isIdCardNumber;
    }else if(textField.tag == 1003){
        return textField.text.length < 30;
    }
    return YES;
    
}

-(BOOL)validateNumber:(NSString *)string{
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBER] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    BOOL isRight = [string isEqualToString:filtered];
    
    return isRight;
}

#pragma mark  -- date picker setup  --

-(YGDatePickerView *)datePicker{
    
    UITextField * textField = [self.view viewWithTag:1006];
    
    if (!_datePicker) {
        
        @WeakObj(self)
        
        _datePicker = [YGDatePickerView showWithStartDate:nil endDate:nil titleString:@"" datePickerMode:UIDatePickerModeDate handler:^(NSDate *selectedDate) {
            
            @StrongObj(self)
            
            textField.text = [self dateStringWithDate:selectedDate type:0];
            
            
        }];
        
    }
    
    return _datePicker;
}

-(NSString *)dateStringWithDate:(NSDate *)date type:(NSInteger)type{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (type == 0) {
       [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    }else{
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    
    return [dateFormatter stringFromDate:date];
    
}

-(NSString *)dateStringWithText:(NSString *)text{
    
    text = [text stringByReplacingOccurrencesOfString:@"年" withString:@"-"];
    text = [text stringByReplacingOccurrencesOfString:@"月" withString:@"-"];
    text = [text stringByReplacingOccurrencesOfString:@"日" withString:@""];

    return  text;
    
}

#pragma mark  --  array lazy   --

-(NSMutableArray *)texts
{
    if (!_texts) {
        _texts = [NSMutableArray array];
    }
    return _texts;
}
-(NSMutableArray *)saleArr
{
    if (!_saleArr) {
        _saleArr = [NSMutableArray array];
    }
    return _saleArr;
}
-(NSMutableArray *)photoArr
{
    if (!_photoArr) {
        _photoArr = [NSMutableArray array];
    }
    return _photoArr;
}
-(NSMutableArray *)idCardArr
{
    if (!_idCardArr) {
        _idCardArr = [NSMutableArray array];
    }
    return _idCardArr;
}

#pragma mark  --  request  --

-(void)postTokenUpdateRequest
{
    
    @WeakObj(self)
    
    [STTextHudTool loadingWithTitle:@"认证中..."];
    
    [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {
        
        @StrongObj(self)
        
        if ([responseObject[@"state"] isEqualToString:@"success"]) {
            
            self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
            self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
            self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
            
            [self  OSSImageUploadRequest];
            
        }else{
            [STTextHudTool showErrorText:responseObject[@"msg"]];
        }
        

    } failure:^(NSError *error) {
          [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
    }];
    
}

-(void)OSSImageUploadRequest{
    
    NSArray * allImages = [NSArray arrayWithObjects:[self.saleArr lastObject],[self.photoArr lastObject],[self.idCardArr lastObject], nil];
    
    @WeakObj(self)
    
    [OSSImageUploader asyncUploadImages:allImages
                                 folder:kTempFolder
                              accessKey:self.accessKey
                              secretKey:self.accessKeySecret
                                  token:self.tokenStr
                               complete:^(NSArray<NSString *> *names, UploadImageState state) {
                                   
                                   @StrongObj(self)
                                   
                                   if (state == UploadImageSuccess) {
                                       [self storeCeritificationRequestWithNames:names];
                                   }
                                   
                               }];
}


// 店铺认证
-(void)storeCeritificationRequestWithNames:(NSArray *)names{
    
    NSDictionary * shopInfo = [self postShopInfoWithNames:names];
    
    @WeakObj(self)
    [PPNetworkHelper POST:PostShopInfo
               parameters:shopInfo
                  success:^(id responseObject) {
                      
                      NSLog(@"%@",responseObject);
                      
                      @StrongObj(self)
                      
                      [STTextHudTool hideSTHud];
                      if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                          
                          [[NSUserDefaults standardUserDefaults] setValue:@"AUDIT" forKey:@"shopStatus"];
                          
                          [self showCerView];
                          
                      }else{
                          [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                      }
                  }failure:^(NSError *error) {
                      [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
                  }];
}

// 获取图片
-(NSDictionary *)postShopInfoWithNames:(NSArray *)names{
    
    NSString *shopSale = [NSString stringWithFormat:@"%@%@",kOSSImageUrl,names[0]];
    NSString *shopIdCard = [NSString stringWithFormat:@"%@%@",kOSSImageUrl,names[1]];
    NSString *shopPhoto = [NSString stringWithFormat:@"%@%@",kOSSImageUrl,names[2]];

    NSDictionary *shopInfo = @{
                               @"shopId":[[LoginUser shareLoginUser] shopId],
                               @"shopSalesLicense":shopSale,
                               @"shopIdcard":shopIdCard,
                               @"shopPhoto":shopPhoto,
                               @"appName":[self.texts firstObject],
                               @"name":[self.texts objectAtIndex:1],
                               @"idCard":[self.texts objectAtIndex:2],
                               @"address":[self.texts objectAtIndex:3]
                               };
    
    return shopInfo;
    
}

-(void)showCerView{
    
    self.cerView = [[CertificationView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    [self.cerView.recentlyBtn addTarget:self action:@selector(recentAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.keyWin = [[UIApplication sharedApplication]keyWindow];
    [self.keyWin addSubview:self.cerView];
}

#pragma mark 去我的店铺
-(void)recentAction{
    
    _cerView.hidden = YES;
    if (self.storeCertificaSuccessedBlock) {
        self.storeCertificaSuccessedBlock();
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark  --  info request  --

-(void)certificationInfoRequest{
    
    @WeakObj(self)
    
    [STTextHudTool loadingWithTitle:@"加载中..."];
    
    [PPNetworkHelper POST:GetShopInfo parameters:@{@"shopId":[[LoginUser shareLoginUser] shopId]} success:^(id responseObject) {
        
        [STTextHudTool hideSTHud];
        
        @StrongObj(self)
        
        if ([responseObject[@"state"] isEqualToString:@"success"]) {
            
            [self upDateCertificationInfo:responseObject];
            
        }else{
            [STTextHudTool showErrorText:responseObject[@"msg"]];
        }
        
        
    } failure:^(NSError *error) {
        [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
    }];
    
}

-(void)upDateCertificationInfo:(NSDictionary *)responseObject{
    
    self.certificationInfo = responseObject[@"data"];
    
    [self.tableView reloadData];
    
    self.saleCardButton.enabled = self.photoButton.enabled = self.idCardButton.enabled = NO;
    
    [self.saleCardButton sd_setImageWithURL:[NSURL URLWithString:self.certificationInfo[@"shopSalesLicense"]] forState:UIControlStateNormal];
    [self.photoButton sd_setImageWithURL:[NSURL URLWithString:self.certificationInfo[@"shopPhoto"]] forState:UIControlStateNormal];
    [self.idCardButton sd_setImageWithURL:[NSURL URLWithString:self.certificationInfo[@"shopIdcard"]] forState:UIControlStateNormal];
    
    CGFloat bottomMar = IPHONE_X?TAB_BAR_SAFE_HEIGHT:0;
    [_contentScrollView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(-bottomMar);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
