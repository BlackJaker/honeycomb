//
//  AF_StoreCertificationVC.h
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

typedef void(^StoreCertificaSuccessedBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface AF_StoreCertificationVC : AllViewController

@property (nonatomic,copy) StoreCertificaSuccessedBlock storeCertificaSuccessedBlock;

@property (nonatomic,copy) NSString * shopStatus;

@end

NS_ASSUME_NONNULL_END
