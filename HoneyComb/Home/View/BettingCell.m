//
//  BettingCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "BettingCell.h"

@implementation BettingCell
-(void)setModel:(AllModel *)model
{
    _model = model;
    self.nameLabel.text = model.playName;
    NSString *typeStr = model.type;
    if ([typeStr isEqualToString:@"1"]) {
        self.headerImage.image = [UIImage imageNamed:@"football"];
    }else if ([typeStr isEqualToString:@"2"]){
         self.headerImage.image = [UIImage imageNamed:@"basketball"];
    }else if ([typeStr isEqualToString:@"3"]){
         self.headerImage.image = [UIImage imageNamed:@"daletou"];
    }else if ([typeStr isEqualToString:@"4"]){
         self.headerImage.image = [UIImage imageNamed:@"pailiesan"];
    }else if ([typeStr isEqualToString:@"5"]){
         self.headerImage.image = [UIImage imageNamed:@"pailiewu"];
    }else if([typeStr isEqualToString:@"6"]){
         self.headerImage.image = [UIImage imageNamed:@"qixingcai"];
    }else if([typeStr isEqualToString:@"7"]){
        self.headerImage.image = [UIImage imageNamed:@"win_lose"];
    }else if([typeStr isEqualToString:@"8"]){
        self.headerImage.image = [UIImage imageNamed:@"any_nine"];
    }

    if ([model.type isEqualToString:@"1"] ||[model.type isEqualToString:@"2"] ) {
        
        NSString * bunchStr = model.bunch;
        if ([bunchStr isEqualToString:@"1"]) {
            bunchStr = @"单关";
        }else{
            bunchStr = [NSString stringWithFormat:@"%@串1",model.bunch];
        }
        
        NSString *bunch = [NSString stringWithFormat:@"%@ %@ 注 ",bunchStr,model.account];
        NSString *times = [NSString stringWithFormat:@"%@ 倍 ",model.times];
        NSString *price = [NSString stringWithFormat:@"%@元",model.price];
        
        NSString *str = [NSString stringWithFormat:@"%@%@%@",bunch,times,price];
        //   根据字符串长度计算label宽度
       
       
        NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:str];
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(0, bunch.length-2)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(0, bunch.length-2)];
        
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 153, 153, 1)} range:NSMakeRange(bunch.length-2, 1)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunch.length-2, 1)];
        
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(bunch.length, times.length-3)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunch.length, times.length-3)];
//
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(bunch.length+times.length-2, 1)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunch.length+times.length-2, 1)];
        
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(bunch.length+times.length, price.length-1)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunch.length+times.length, price.length-1)];

        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(str.length-1,1 )];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(str.length-1, 1)];
        self.priceLabel.attributedText = rushAttributed;
    }else
    {
        self.numberLabel.text = [NSString stringWithFormat:@"第%@期",model.issue];
        NSString *str = [NSString stringWithFormat:@"%@ 注 %@ 倍 %@元",model.account,model.times,model.price];
        NSString *account = [NSString stringWithFormat:@"%@",model.account];
        NSString *times =[NSString stringWithFormat:@"%@",model.times];
        NSString *price =[NSString stringWithFormat:@"%@",model.price];
        
        NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:str];
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(0, account.length)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(0, account.length)];
        
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 153, 153, 1)} range:NSMakeRange(account.length+1, 1)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+1, 1)];
        
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(account.length+3, times.length)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+3, times.length)];
        //
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(account.length+3+times.length+1, 1)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+3+times.length+1, 1)];
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(account.length+2+1+times.length+3, price.length)];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+2+1+times.length+3, price.length)];
        
        [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(account.length+3+times.length+3+price.length,1 )];
        [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+3+times.length+3+price.length, 1)];
         self.priceLabel.attributedText = rushAttributed;
    }
//     self.priceLabel.text = @"2串1注1倍 2.00元";
  
     self.dateLabel.text = model.createDate;
    
    
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}
-(void)layoutView
{
    self.headerImage = [[UIImageView alloc]init];
//    self.headerImage.image = [UIImage imageNamed:@"图层2"];
    [self.contentView addSubview:self.headerImage];
    [self.headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(kWidth/11.3636);
        make.left.mas_equalTo(kWidth/31.25);
        make.top.mas_equalTo(kWidth/25);
        
    }];
    
    self.nameLabel = [[UILabel alloc]init];
//    self.nameLabel.text = @"超级大乐透";
      self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.textColor = kBlackColor;
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-30);
        make.height.mas_equalTo(kWidth/23.4375);
        make.top.mas_equalTo(kWidth/15.625);
        make.left.mas_equalTo(self.headerImage.mas_right).offset(10);
    }];
    
    self.numberLabel = [[UILabel alloc]init];
//    self.numberLabel.text = @"第2018083期";
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    self.numberLabel.textColor = kGrayColor;
    self.numberLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
    [self.contentView addSubview:self.numberLabel];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-55);
        make.height.mas_equalTo(kWidth/25);
        make.top.mas_equalTo(kWidth/15);
        make.right.mas_equalTo(-12);
    }];
    
    self.priceLabel = [[YYLabel alloc]init];
//    self.priceLabel.text = @"2串1注1倍 2.00元";
//    self.priceLabel.textColor = kGrayColor;
     self.priceLabel.textAlignment = NSTextAlignmentLeft;
//    self.priceLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [self.contentView addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-12);
        make.height.mas_equalTo(kWidth/25);
        make.top.mas_equalTo(self.headerImage.mas_bottom).offset(10);
        make.left.mas_equalTo(12);
    }];
    
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.text = @"2018-07-30 15:51:35";
    self.dateLabel.textColor = kGrayColor;
    self.dateLabel.textAlignment = NSTextAlignmentRight;
    self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [self.contentView addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-12);
        make.height.mas_equalTo(kWidth/25);
        make.top.mas_equalTo(self.headerImage.mas_bottom).offset(10);
        make.right.mas_equalTo(-12);
    }];
    
    self.cancleBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.cancleBtn];
    [self.cancleBtn setTitleColor:kGrayColor forState:(UIControlStateNormal)];
    [self.cancleBtn setTitle:@"撤单" forState:(UIControlStateNormal)];
    self.cancleBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/28.8461];
    self.cancleBtn.layer.cornerRadius =4;
    self.cancleBtn.layer.masksToBounds = YES;
    [self.cancleBtn.layer setBorderWidth:1.0];
    self.cancleBtn.layer.borderColor = kGrayColor.CGColor;
//    self.cancleBtn.backgroundColor = kRedColor;
    [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/8.928);
        make.height.mas_equalTo(kWidth/12.096);
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).offset(kWidth/25);
    }];
    
    self.lookBtn = [[UIButton alloc]init];
    [self.contentView addSubview:self.lookBtn];
    [self.lookBtn setTitleColor:kRedColor forState:(UIControlStateNormal)];
    [self.lookBtn setTitle:@"查看详情" forState:(UIControlStateNormal)];
    self.lookBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/28.8461];
    self.lookBtn.layer.cornerRadius =4;
    self.lookBtn.layer.masksToBounds = YES;
    [self.lookBtn.layer setBorderWidth:1.0];
    self.lookBtn.layer.borderColor = kRedColor.CGColor;
    //    self.cancleBtn.backgroundColor = kRedColor;
    [self.lookBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/4.2613);
        make.height.mas_equalTo(kWidth/12.096);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).offset(kWidth/25);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
