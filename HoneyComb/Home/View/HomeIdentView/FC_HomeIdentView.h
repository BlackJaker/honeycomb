//
//  FC_HomeIdentView.h
//  HoneyComb
//
//  Created by afc on 2018/11/24.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^IdentificationActionBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface FC_HomeIdentView : UIView

@property (nonatomic,strong) UILabel * noteLabel;
@property (nonatomic,strong) UIButton * identButton;
@property (nonatomic,strong) UIButton * removeButton;

@property (nonatomic,copy) IdentificationActionBlock identBlock;

-(void)reload;

@end

NS_ASSUME_NONNULL_END
