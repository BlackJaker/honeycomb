//
//  FC_HomeIdentView.m
//  HoneyComb
//
//  Created by afc on 2018/11/24.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_HomeIdentView.h"

@interface FC_HomeIdentView ()
{
    CGFloat  buttonHeight;
    CGFloat  buttonWidth;
}
@end

@implementation FC_HomeIdentView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self contentSetup];
    }
    return self;
}

#pragma mark  --  contentSetup  --

-(void)contentSetup{
    
    buttonWidth = 73;
    buttonHeight = 38;
    
}


#pragma mark  --  lazy  --

-(UILabel *)noteLabel{
    
    if (!_noteLabel) {
        _noteLabel= [[UILabel alloc]init];
        _noteLabel.userInteractionEnabled = YES;
        NSString * string = @"为了更方便您的彩民投注,建议您认证个人信息,定制您的彩民版应用~";
        _noteLabel.text = string;
        _noteLabel.font = [UIFont fontWithName:kMedium size:13];
        _noteLabel.textColor = [UIColor colorWithHexString:@"#fb5b54"];
        [self addSubview:_noteLabel];
        
        [_noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(- (2 * kLeftMar + buttonWidth));
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
    }
    return _noteLabel;
}

-(UIButton *)identButton{
    
    if (!_identButton) {
        _identButton= [[UIButton alloc]init];
        _identButton.layer.cornerRadius = 5;
        _identButton.clipsToBounds = YES;
        
        CAGradientLayer *gradientLayer =  [CAGradientLayer layer];
        gradientLayer.frame = CGRectMake(0, 0, buttonWidth, buttonHeight);
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(1, 0);
        gradientLayer.locations = @[@(0.5),@(1.0)];//渐变点
        [gradientLayer setColors:@[(id)[[UIColor colorWithHexString:@"#ec7379"] CGColor],(id)[[UIColor colorWithHexString:@"#eb4162"] CGColor]]];//渐变数组
        [_identButton.layer addSublayer:gradientLayer];
        
        _identButton.titleLabel.font = [UIFont fontWithName:kMedium size:14];
        [_identButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [_identButton addTarget:self action:@selector(identAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_identButton];
        
        [_identButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(buttonWidth, buttonHeight));
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(-kLeftMar);
        }];
    }
    return _identButton;
    
}

-(void)identAction{
    
    if (self.identBlock) {
        self.identBlock();
    }
    
}

#pragma mark  --  reload  --

-(void)reload{
    
    self.noteLabel.backgroundColor = [UIColor clearColor];
    [UILabel changeLineSpaceForLabel:self.noteLabel WithSpace:5];
    [self.identButton setTitle:@"立即认证" forState:UIControlStateNormal];
}

@end
