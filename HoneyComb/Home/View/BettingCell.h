//
//  BettingCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BettingCell : UITableViewCell
@property (nonatomic,strong) AllModel *model;
@property (nonatomic,strong) UIImageView *headerImage;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *numberLabel;
@property (nonatomic,strong) YYLabel *priceLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UIButton *cancleBtn;
@property (nonatomic,strong) UIButton *lookBtn;
@end
