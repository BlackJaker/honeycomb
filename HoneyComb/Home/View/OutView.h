//
//  OutView.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OutView : UIView
-(void)showView;
@property (nonatomic,strong)UIButton *recentlyBtn;
@property (nonatomic,strong) UILabel *weekLabel;//星期
@property (nonatomic,strong) UILabel *dateLabel;//时间
@property (nonatomic,strong) UILabel *leftCity;
@property (nonatomic,strong) UILabel *rightCity;
@property (nonatomic,strong) UILabel *vsLabel;
@property (nonatomic,strong) UILabel *meLabel;
@property (nonatomic,strong) UILabel *resultsLabel;//结果
@property (nonatomic,strong) UITextField *resultTf;
@end
