//
//  FC_CurrentBuyListHeadView.m
//  HoneyComb
//
//  Created by afc on 2018/12/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_CurrentBuyListHeadView.h"

@interface FC_CurrentBuyListHeadView ()

@property (nonatomic,copy) NSArray * titles;

@end

@implementation FC_CurrentBuyListHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self gradientLayerSetup];
        [self titleSetup];
    }
    return self;
}

-(void)gradientLayerSetup{
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, self.width, self.height);
    gl.startPoint = CGPointMake(0, 0);
    gl.endPoint = CGPointMake(1, 1);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:241/255.0 green:34/255.0 blue:127/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:237/255.0 green:49/255.0 blue:82/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0.0),@(1.0f)];
    
    [self.layer addSublayer:gl];
    
}

-(void)titleSetup{
    
    CGFloat  width = (kWidth - 30)/self.titles.count;
    
    for (int i = 0; i < self.titles.count; i ++) {
        UILabel * label = [UILabel lableWithText:[self.titles objectAtIndex:i]
                                            font:[UIFont boldSystemFontOfSize:14]
                                       textColor:COLOR_HEX(0xffffff, 1)];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(width * i);
            make.width.equalTo(width);
            make.top.equalTo(0);
            make.bottom.equalTo(0);
        }];
    }
    
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"用户名",@"购买份数",@"认购时间",@"预计奖金"];
    }
    return _titles;
}

@end
