//
//  FC_NumberLottoryDetailPhotoView.m
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_NumberLottoryDetailPhotoView.h"

@implementation FC_NumberLottoryDetailPhotoView

-(instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self contentSetup];
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup
{
    
    CGFloat  leftMar = 12,height = 40;
    
    UILabel *conLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/31.25, kWidth/25, kWidth/2, kWidth/22.058)];
    conLabel.textAlignment = NSTextAlignmentLeft;
    conLabel.text = @"彩票照片";
    conLabel.textColor = kBlackColor;
    conLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
    [self addSubview:conLabel];
    
    [conLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
        make.left.mas_equalTo(leftMar);
        make.right.mas_equalTo(-leftMar);
        make.top.mas_equalTo(0);
    }];

}

#pragma mark  --  lazy  --

-(UIButton *)doneButton{
    
    CGFloat rightMar = 12,width = 40,height = 40;
    
    if (!_doneButton) {
        _doneButton = [[UIButton alloc]init];
        [_doneButton setTitle:@"完成" forState:(UIControlStateNormal)];
        _doneButton.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
        _doneButton.hidden = YES;
        [_doneButton setTitleColor:[UIColor blueColor] forState:(UIControlStateNormal)];
        [_doneButton addTarget:self action:@selector(photoViewDoneAction) forControlEvents:(UIControlEventTouchUpInside)];
        [self addSubview:_doneButton];
        
        [_doneButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
            make.right.mas_equalTo(-rightMar);
            make.width.mas_equalTo(width);
            make.top.mas_equalTo(0);
        }];
    }
    return _doneButton;
}

-(void)photoViewDoneAction{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(done)]) {
        [self.delegate done];
    }
    
}

-(UIButton *)editButton{
    
    CGFloat rightMar = 12,width = 40,height = 40;
    
    if (!_editButton) {
        _editButton = [[UIButton alloc]init];
        [_editButton setTitle:@"编辑" forState:(UIControlStateNormal)];
        _editButton.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
        _editButton.hidden = YES;
        [_editButton setTitleColor:[UIColor blueColor] forState:(UIControlStateNormal)];
        [_editButton addTarget:self action:@selector(photoViewEditAction) forControlEvents:(UIControlEventTouchUpInside)];
        [self addSubview:_editButton];
        
        [_editButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
            make.right.mas_equalTo(-rightMar);
            make.width.mas_equalTo(width);
            make.top.mas_equalTo(0);
        }];
    }
    return _editButton;
}

-(void)photoViewEditAction{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(edit)]) {
        [self.delegate edit];
    }
    
}

-(PYPhotosView *)photoView{
    
    CGFloat leftMar = 12,topMar = 40;
    
    if (!_photoView) {
        // 1. 常见一个发布图片时的photosView
        _photoView = [PYPhotosView photosView];
        
        _photoView.py_x = leftMar;
        _photoView.py_y = topMar;
        // 2.1 设置本地图片
        _photoView.images = nil;
        // 3. 设置代理
        _photoView.photosMaxCol = 3;//每行显示最大图片个数
        _photoView.imagesMaxCountWhenWillCompose = 3;//最多选择图片的个数
        
        // 4. 添加photosView
        [self addSubview:_photoView];
    }
    return _photoView;
}

@end
