//
//  FC_NumberLottoryDetailPhotoView.h
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol FC_DetailPhotoViewDelegate <NSObject>

-(void)done;

-(void)edit;

@end

NS_ASSUME_NONNULL_BEGIN

@interface FC_NumberLottoryDetailPhotoView : UIView

@property (nonatomic,strong) PYPhotosView * photoView;

@property (nonatomic,strong) UIButton* doneButton;

@property (nonatomic,strong) UIButton* editButton;

@property (nonatomic,assign) id<FC_DetailPhotoViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
