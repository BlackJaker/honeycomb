//
//  FC_NumberLottoryBettingView.m
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_NumberLottoryBettingView.h"

#import "FC_NumberLottoryDetailModel.h"

#import "FC_NumberLottoryDetailItemModel.h"

@interface FC_NumberLottoryBettingView ()
{
    CGFloat  headHeight;
}

@property (nonatomic,copy) NSMutableArray * betItems;

@property (nonatomic,assign) NSInteger currentType;

@end

@implementation FC_NumberLottoryBettingView

-(instancetype)initWithFrame:(CGRect)frame type:(NSInteger)type{
    
    if (self = [super initWithFrame:frame]) {
        
        self.currentType = type;
        
        [self contentSetup];
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    headHeight = 40;
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self noteLabelSetup];
    
}

-(void)noteLabelSetup{
    
    UIView * view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    [self addSubview:view];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(headHeight);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    CGFloat  leftMar = 12,width = 100;
    UILabel * noteLabel = [[UILabel alloc]init];
    noteLabel.textColor = RGB(51, 51, 51, 1.0f);
    noteLabel.text = @"投注号码";
    noteLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [view addSubview:noteLabel];
    
    [noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(leftMar);
        make.width.mas_equalTo(width);
        make.top.mas_equalTo(0);
    }];
    
}

#pragma mark  --  lazy  --

-(UILabel *)betInfoLabel{
    
    CGFloat  rightMar = 12,width = 280;
    
    if (!_betInfoLabel) {
        _betInfoLabel = [[UILabel alloc]init];
        _betInfoLabel.textAlignment = NSTextAlignmentRight;
        _betInfoLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
        [self addSubview:_betInfoLabel];
        
        [_betInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(headHeight);
            make.right.mas_equalTo(-rightMar);
            make.width.mas_equalTo(width);
            make.top.mas_equalTo(0);
        }];
    }
    return _betInfoLabel;
}

-(NSMutableArray *)betItems{
    
    if (!_betItems) {
        _betItems = [NSMutableArray new];
    }
    return _betItems;
}

-(void)reloadBettingWithModel:(FC_NumberLottoryDetailModel *)model{
    
    [self updateBetInfoModel:model];
    
    CGFloat  height = headHeight,leftMar = 12;

    for (int i = 0 ; i < model.detailList.count; i ++) {
        FC_NumberLottoryDetailItemModel * itemModel = [model.detailList objectAtIndex:i];
        [self.betItems addObject:itemModel];
        
        UILabel * label = [[UILabel alloc]init];
        label.font = [UIFont fontWithName:kPingFangRegular size:13];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByCharWrapping;
        [self addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
            make.left.mas_equalTo(leftMar);
            make.right.mas_equalTo(-leftMar);
            make.top.mas_equalTo(headHeight + height * i);
        }];
        
        label.attributedText = [self getAttributeTextWithModel:itemModel];
    }
    
}

-(void)updateBetInfoModel:(FC_NumberLottoryDetailModel *)model{
    
    NSString * infoString ;
    
    if (!model.bunch || [model.bunch isEqualToString:@""] || [model.bunch isEqualToString:@"-"]) {
        infoString = model.playName;
    }else{
        infoString = [model.bunch isEqualToString:@"1"]?@"单式":@"复式";
    }

    NSString *account = [NSString stringWithFormat:@"%@ %@ 注 ",infoString,model.account];
    NSString *times = [NSString stringWithFormat:@"%@ 倍 ",model.times];
    NSString *price = [NSString stringWithFormat:@"%@元",model.price];
    NSString *str = [NSString stringWithFormat:@"%@%@%@",account,times,price];
    
    NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:str];
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(0, account.length-2)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.785] range:NSMakeRange(0, account.length-2)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 153, 153, 1)} range:NSMakeRange(account.length-2, 1)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.785] range:NSMakeRange(account.length-2, 1)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(account.length, times.length-3)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.785] range:NSMakeRange(account.length, times.length-3)];
    //
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(account.length+times.length-2, 1)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.785] range:NSMakeRange(account.length+times.length-2, 1)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(account.length+times.length, price.length-1)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.785] range:NSMakeRange(account.length+times.length, price.length-1)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(str.length-1,1 )];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.785] range:NSMakeRange(str.length-1, 1)];
    
    self.betInfoLabel.attributedText = rushAttributed;
    
}

-(NSMutableAttributedString *)getAttributeTextWithModel:(FC_NumberLottoryDetailItemModel *)itemModel{
    
    NSString * text = nil;
    
    switch (self.currentType) {
        case 3:
            if ([itemModel.type isEqualToString:@"0"]) {
                text = @"标准";
            }
            if ([itemModel.type isEqualToString:@"1"]) {
                text = @"胆拖";
            }
            break;
        case 4:
            if ([itemModel.type isEqualToString:@"1"]) {
                text = @"直选";
            }
            if ([itemModel.type isEqualToString:@"2"]) {
                text = @"组三单式";
            }
            if ([itemModel.type isEqualToString:@"3"]) {
                text = @"组三复式";
            }
            if ([itemModel.type isEqualToString:@"4"]) {
                text = @"组六";
            }
            break;
        default:
            break;
    }
    
    NSString * redStr = nil;
    NSString * blueStr = nil;
    NSString * numberString = nil;
    
    if (text) {
        
        if (self.currentType == 3) {
            
            if ([itemModel.type isEqualToString:@"0"]) {
                redStr = itemModel.red;
                blueStr = itemModel.blue;
            }else{
                redStr = [NSString stringWithFormat:@"%@ # %@",itemModel.redBile,itemModel.red];
                blueStr = [NSString stringWithFormat:@"%@ # %@",itemModel.blueBile,itemModel.blue];
            }
            numberString = [NSString stringWithFormat:@"%@  %@ | %@",text,redStr,blueStr];
        }else{
            redStr = itemModel.content;
            numberString = [NSString stringWithFormat:@"%@  %@",text,redStr];
        }
        
    }else{
        if (self.currentType == 3) {
            
            if ([itemModel.type isEqualToString:@"0"]) {
                redStr = itemModel.red;
                blueStr = itemModel.blue;
            }else{
                redStr = [NSString stringWithFormat:@"%@ # %@",itemModel.redBile,itemModel.red];
                blueStr = [NSString stringWithFormat:@"%@ # %@",itemModel.blueBile,itemModel.blue];
            }
            numberString = [NSString stringWithFormat:@" %@ | %@",redStr,blueStr];
        }else{
            redStr = itemModel.content;
            numberString = [NSString stringWithFormat:@" %@",redStr];
        }
    }
    
    NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:numberString];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(253, 37, 37, 1.0f)} range:[numberString rangeOfString:redStr]];
    
    if (blueStr) {
         [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(17, 58, 235, 1.0f)} range:NSMakeRange(numberString.length - blueStr.length, blueStr.length)];
    }
    
    
    return rushAttributed;
}


@end
