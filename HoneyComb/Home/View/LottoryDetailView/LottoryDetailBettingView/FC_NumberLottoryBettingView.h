//
//  FC_NumberLottoryBettingView.h
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_NumberLottoryDetailModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_NumberLottoryBettingView : UIView

-(instancetype)initWithFrame:(CGRect)frame type:(NSInteger)type;

@property (nonatomic,strong) UILabel * betInfoLabel;

-(void)reloadBettingWithModel:(FC_NumberLottoryDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
