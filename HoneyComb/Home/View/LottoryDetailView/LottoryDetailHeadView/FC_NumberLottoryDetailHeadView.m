//
//  FC_NumberLottoryDetailHeadView.m
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_NumberLottoryDetailHeadView.h"

#import "FC_NumberLottoryDetailModel.h"

#define kDarkColor RGB(51, 51, 51, 1)
#define kTintColor RGB(153, 153, 153, 1)
#define kRedNormalColor RGB(253, 37, 37, 1)

@interface FC_NumberLottoryDetailHeadView ()

@property (nonatomic,copy) NSString * currentType;

@end

@implementation FC_NumberLottoryDetailHeadView

-(instancetype)initWithFrame:(CGRect)frame type:(NSString *)type{
    
    if (self = [super initWithFrame:frame]) {
        
        self.currentType = type;
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self imageViewSetup];
    
}

-(void)imageViewSetup{
    
    NSInteger  currentType = [self.currentType integerValue];
    
    NSString * imgName = @"football";
    
    switch (currentType) {
        case 1:
            imgName = @"football";
            break;
        case 2:
            imgName = @"basketball";
            break;
        case 3:
            imgName = @"daletou";
            break;
        case 4:
            imgName = @"pailiesan";
            break;
        case 5:
            imgName = @"pailiewu";
            break;
        case 6:
            imgName = @"qixingcai";
            break;
        default:
            break;
    }
    
    self.imgView.image = [UIImage imageNamed:imgName];
}

#pragma mark  --  lazy  --

-(UIImageView *)imgView{
    
    CGFloat  topMar = 23,leftMar = 12,width = 40,height = 40;
    
    if (!_imgView) {
        
        _imgView = [[UIImageView alloc]init];
        
        [self addSubview:_imgView];
        
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(leftMar);
            make.top.mas_equalTo(topMar);
        }];
        
    }
    return _imgView;
}

-(UILabel *)nameLabel{
    
    CGFloat  topMar = 23,leftMar = 15,width = 80,height = 15;
    
    if (!_nameLabel) {
        
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = kDarkColor;
        _nameLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
        [self addSubview:_nameLabel];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(self.imgView.mas_right).offset(leftMar);
            make.top.mas_equalTo(topMar);
        }];
        
    }
    return _nameLabel;
}

-(UILabel *)isImageLabel{
    
    CGFloat  topMar = 23,leftMar = 10,width = 100,height = 15;
    
    if (!_isImageLabel) {
        
        _isImageLabel = [[UILabel alloc]init];
        _isImageLabel.textColor = kRedColor;
        _isImageLabel.text = @"|  提供照片";
        _isImageLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
        [self addSubview:_isImageLabel];
        
        [_isImageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(self.nameLabel.mas_right).offset(leftMar);
            make.top.mas_equalTo(topMar);
        }];
        
    }
    return _isImageLabel;
}

-(UILabel *)issueLabel{
    
    CGFloat  topMar = 10,leftMar = 15,width = 180,height = 15;
    
    if (!_issueLabel) {
        
        _issueLabel = [[UILabel alloc]init];
        _issueLabel.textColor = kTintColor;
        _issueLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
        [self addSubview:_issueLabel];
        
        [_issueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(width, height));
            make.left.mas_equalTo(self.imgView.mas_right).offset(leftMar);
            make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(topMar);
        }];
        
    }
    return _issueLabel;
}

#pragma mark  --  reload  --

-(void)reloadWithModel:(FC_NumberLottoryDetailModel *)model{
    
    self.nameLabel.text = model.playName;
    
    NSDictionary *attributes = @{NSFontAttributeName:self.nameLabel.font};
    CGSize textSize = [self.nameLabel.text boundingRectWithSize:CGSizeMake(kWidth, 15) options:NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    
    [self.nameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(textSize.width);
    }];
    
    if ([model.isImage isEqualToString:@"1"]) {
        self.isImageLabel.hidden = NO;
    }else{
        self.isImageLabel.hidden = YES;
    }
    
    self.issueLabel.text = [NSString stringWithFormat:@"第%@期",model.issue];
}

@end
