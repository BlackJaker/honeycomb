//
//  FC_NumberLottoryDetailHeadView.h
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_NumberLottoryDetailModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_NumberLottoryDetailHeadView : UIView

-(instancetype)initWithFrame:(CGRect)frame type:(NSString *)type;

@property (nonatomic,strong) UIImageView * imgView;
@property (nonatomic,strong) UILabel * nameLabel;
@property (nonatomic,strong) UILabel * isImageLabel;
@property (nonatomic,strong) UILabel * issueLabel;

-(void)reloadWithModel:(FC_NumberLottoryDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
