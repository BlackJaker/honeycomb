//
//  FC_NumberLottoryDetailOrderView.h
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_NumberLottoryDetailModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_NumberLottoryDetailOrderView : UIView

@property (nonatomic,strong) UIView * backView;

-(void)reloadNumberOrderDataWithModel:(FC_NumberLottoryDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
