//
//  FC_NumberLottoryDetailOrderView.m
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_NumberLottoryDetailOrderView.h"

#import "FC_NumberLottoryDetailModel.h"

@interface FC_NumberLottoryDetailOrderView ()
{
    CGFloat  titleLabelHeight;
    
    NSString * phoneNumber;
}

@property (nonatomic,copy) NSArray * titles;

@end

@implementation FC_NumberLottoryDetailOrderView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self == [super initWithFrame:frame]) {
        
        titleLabelHeight = 40;
        
        [self contentSetup];
        
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    [self noteLabelSetup];
    
    phoneNumber = nil;
}

#pragma mark  --  lazy  --

-(UIView *)backView{
    
    if (!_backView) {
        
        _backView = [[UIView alloc]initWithFrame:self.bounds];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 8.0f;
        [self addSubview:_backView];
        
    }
    return _backView;
}

-(void)reloadLabel:(UILabel *)label withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    
    label.textColor = textColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:fontSize];
    label.text = @"-";
    label.textAlignment = NSTextAlignmentCenter;
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"用 户 名 : ",@"订单编号 : ",@"购买时间 : "];
    }
    return _titles;
}

-(void)noteLabelSetup{
    
    CGFloat  labelHeight = 45,leftMar = 12;
    
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(leftMar,0, DEFAULT_WIDTH, labelHeight)];
    [self reloadLabel:label withFontSize:17 textColor:RGB(51, 51, 51, 1.0f)];
    label.text = @"订单信息";
    label.textAlignment = NSTextAlignmentLeft;
    [self.backView addSubview:label];
    
}

#pragma mark  --  reload info  --

-(void)reloadNumberOrderDataWithModel:(FC_NumberLottoryDetailModel *)model{
    
    UIView * view = [self viewWithTag:-1234];
    [view removeFromSuperview];
    
    NSMutableDictionary * items = [NSMutableDictionary new];
    
    if ([self isItemWith:model.nickName]) {
        [items setValue:model.nickName forKey:@"用 户 名 : "];
    }
    
    if ([self isItemWith:model.mobile]) {
        [items setValue:model.mobile forKey:@"手 机 号 : "];
    }
    
    if ([self isItemWith:model.orderNumber]) {
        [items setValue:model.orderNumber forKey:@"订单编号 : "];
    }

    if ([self isItemWith:model.createDate]) {
        [items setValue:model.createDate forKey:@"购买时间 : "];
    }
    
    CGFloat  itemHeight = 30,leftMar = 12;
    
    for (int i = 0; i < items.allKeys.count; i ++) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(leftMar, titleLabelHeight + itemHeight * i, DEFAULT_WIDTH, itemHeight)];
        [self reloadLabel:label withFontSize:14 textColor:RGB(153, 153, 153, 1.0f)];
        label.tag = -1234;
        
        NSString * key = [items.allKeys objectAtIndex:i];
        NSString * value = [items valueForKey:key];
        
        if ([key isEqualToString:@"手 机 号 : "]) {
            
            label.text = [NSString stringWithFormat:@"%@ %@(长按拨打)",key,value];
            label.userInteractionEnabled = YES;
            phoneNumber = value;
            UILongPressGestureRecognizer * longGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(callAction:)];
            [label addGestureRecognizer:longGesture];
            
        }else{
            label.text = [NSString stringWithFormat:@"%@ %@",key,value];
        }
        label.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:label];
    }
    
}

-(void)callAction:(UILongPressGestureRecognizer *)longGesture{

    if (longGesture.state == UIGestureRecognizerStateBegan) {
        
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",phoneNumber];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }
   
    
}

-(BOOL)isItemWith:(NSString *)item{
    
    if ([item isEqualToString:@""] || !item) {
        return NO;
    }
    if ([item isEqualToString:@"-"]) {
        return  NO;
    }
    
    return  YES;
}


@end
