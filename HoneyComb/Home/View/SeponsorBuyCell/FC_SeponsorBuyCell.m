//
//  FC_SeponsorBuyCell.m
//  HoneyComb
//
//  Created by afc on 2018/12/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_SeponsorBuyCell.h"

@implementation FC_SeponsorBuyCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        UIView *backView = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
        backView.cornerRadius = 5;
        [self.contentView addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(kDEVICE_SCALE_FACTOR(12)));
            make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-12)));
            make.height.equalTo(@(kDEVICE_SCALE_FACTOR(150)));
            make.top.equalTo(@(kDEVICE_SCALE_FACTOR(10)));
        }];
        
        self.icon = ({
            UIImageView *image = [[UIImageView alloc] init];
            [backView addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(16)));
                make.top.equalTo(@(kDEVICE_SCALE_FACTOR(18)));
                make.width.height.equalTo(@(kDEVICE_SCALE_FACTOR(25)));
            }];
            image.cornerRadius = kDEVICE_SCALE_FACTOR(25)/2.;
            image;
        });
        
        self.typenamelab = ({
            UILabel *label = [UILabel lableWithText:@""
                                               font:[UIFont boldSystemFontOfSize:14]
                                          textColor:COLOR_HEX(0x333333, 1)];
            [backView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.icon.mas_right).offset(kDEVICE_SCALE_FACTOR(15));
                make.centerY.equalTo(self.icon);
            }];
            label;
        });
        
//        self.timelab = ({
//            UILabel *label = [UILabel lableWithText:@""
//                                               font:[UIFont systemFontOfSize:15]
//                                          textColor:COLOR_HEX(0x7F8491, 1)];
//            [backView addSubview:label];
//            [label mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.centerY.equalTo(self.typenamelab);
//                make.left.equalTo(self.typenamelab.mas_right).offset(kDEVICE_SCALE_FACTOR(15));
//            }];
//            label;
//        });
        
//        self.aborttimelab = ({
//            UILabel *label = [UILabel lableWithText:@""
//                                               font:[UIFont systemFontOfSize:13]
//                                          textColor:COLOR_HEX(0xE73A59, 1)];
//            [backView addSubview:label];
//            [label mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.centerY.equalTo(self.typenamelab);
//                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
//            }];
//            label;
//        });
        
//        UIView *line = [UIView viewWithBackgroundColor:[UIColor groupTableViewBackgroundColor]];
//        [backView addSubview:line];
//        [line mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
//            make.height.equalTo(@(1));
//            make.top.equalTo(@(kDEVICE_SCALE_FACTOR(49)));
//        }];
        
        self.namelab = ({
            UILabel *lab = [UILabel lableWithText:@""
                                             font:[UIFont systemFontOfSize:14]
                                        textColor:COLOR_HEX(0x464E5F, 1)];
            [backView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(18)));
                make.top.equalTo(self.icon.mas_bottom).offset(kDEVICE_SCALE_FACTOR(15));
            }];
            lab;
        });
        
        self.biolab = ({
            UILabel *lab = [UILabel lableWithText:@""
                                             font:[UIFont systemFontOfSize:12]
                                        textColor:COLOR_HEX(0x7F8491, 1)];
            [backView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.top.equalTo(self.namelab.mas_bottom).offset(kDEVICE_SCALE_FACTOR(10));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-90)));
            }];
            lab;
        });
        
        self.percentView = ({
            SponsorBuyListPercentView *view = [[SponsorBuyListPercentView alloc] init];
            [backView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-10)));
                make.width.height.equalTo(@(kDEVICE_SCALE_FACTOR(60)));
                make.top.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
            }];
            view;
        });
        
        UIView *infoBackView = [UIView viewWithBackgroundColor:COLOR_HEX(0xF0EFF4, 1)];
        infoBackView.cornerRadius = 5;
        [backView addSubview:infoBackView];
        [infoBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(kDEVICE_SCALE_FACTOR(12)));
            make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-12)));
            make.height.equalTo(@(kDEVICE_SCALE_FACTOR(51)));
            make.bottom.equalTo(@(kDEVICE_SCALE_FACTOR(-14)));
        }];
        
        self.numberlabs = [NSMutableArray array];
        self.deslabs = [NSMutableArray array];
        for (int i = 0; i < 4; i++) {
            UILabel *numlab = [UILabel lableWithText:@""
                                                font:[UIFont boldSystemFontOfSize:12]
                                           textColor:COLOR_HEX(0x464E5F, 1)];
            numlab.textAlignment = NSTextAlignmentCenter;
            [infoBackView addSubview:numlab];
            [numlab mas_makeConstraints:^(MASConstraintMaker *make) {
                if (i == 0) {
                    make.left.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
                }else{
                    make.left.equalTo(self.numberlabs.lastObject.mas_right);
                }
                make.top.equalTo(@(kDEVICE_SCALE_FACTOR(10)));
                make.width.equalTo(infoBackView.mas_width).multipliedBy(0.25);
            }];
            
            UILabel *deslab = [UILabel lableWithText:@""
                                                font:[UIFont systemFontOfSize:11]
                                           textColor:COLOR_HEX(0x7F8491, 1)];
            deslab.textAlignment = NSTextAlignmentCenter;
            [infoBackView addSubview:deslab];
            [deslab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.width.equalTo(numlab);
                make.bottom.equalTo(@(kDEVICE_SCALE_FACTOR(-8)));
            }];
            
            [self.numberlabs addObject:numlab];
            [self.deslabs addObject:deslab];
            
        }
        
//        self.joinBuy = ({
//            UIButton *button = [UIButton buttonWithTitle:@"合买"
//                                                    font:[UIFont boldSystemFontOfSize:14]
//                                              titleColor:[UIColor whiteColor]
//                                            controlState:UIControlStateNormal];
//            button.backgroundColor = COLOR_HEX(0xE73A59, 1);
//            [infoBackView addSubview:button];
//            [button mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
//                make.centerY.equalTo(@0);
//                make.width.equalTo(self.numberlabs.lastObject);
//                make.height.equalTo(infoBackView);
//            }];
//            button;
//        });
        
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
