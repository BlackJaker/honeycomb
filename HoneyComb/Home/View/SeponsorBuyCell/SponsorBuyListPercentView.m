//
//  SponsorBuyListPercentView.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/6.
//  Copyright © 2018 op150. All rights reserved.
//

#import "SponsorBuyListPercentView.h"

#define SelfFrame CGRectMake(0,0,kDEVICE_SCALE_FACTOR(60),kDEVICE_SCALE_FACTOR(60))

@interface SponsorBuyListPercentView()
@property (nonatomic, strong) UILabel *havelab;
@property (nonatomic, strong) UILabel *baodilab;

@end

@implementation SponsorBuyListPercentView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        UILabel *havelab = [UILabel lableWithText:@"50%"
                                             font:[UIFont boldSystemFontOfSize:15]
                                        textColor:COLOR_HEX(0xD02332, 1)];
        havelab.textAlignment = NSTextAlignmentCenter;
        self.havelab = havelab;
        [self addSubview:havelab];
        [havelab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(@(kDEVICE_SCALE_FACTOR(16)));
        }];
        
        UILabel *baodilab = [UILabel lableWithText:@"+保30%"
                                             font:[UIFont boldSystemFontOfSize:10]
                                        textColor:COLOR_HEX(0x999999, 1)];
        self.baodilab = baodilab;
        baodilab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:baodilab];
        [baodilab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(@(kDEVICE_SCALE_FACTOR(33)));
        }];
        
    }
    return self;
}

- (void)setBaodiPercent:(CGFloat)baodiPercent{
    _baodiPercent = baodiPercent;
    self.baodilab.text = [NSString stringWithFormat:@"+保%.0f%%",roundf(baodiPercent * 100)];
    [self setNeedsDisplay];
}

- (void)setHavePercent:(CGFloat)havePercent{
    _havePercent = havePercent;
    self.havelab.text = [NSString stringWithFormat:@"%.0f%%",roundf(havePercent * 100)];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect{
    
    CGPoint center = CGPointMake(SelfFrame.origin.x + SelfFrame.size.width/2., SelfFrame.origin.y+SelfFrame.size.height/2.);
    CGFloat radius = SelfFrame.size.width/2.;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineWidth = 5;
    [path addArcWithCenter:center
                    radius:radius-5
                startAngle:0
                  endAngle:2*M_PI
                 clockwise:YES];
    [COLOR_HEX(0xF0EFF4, 1) setStroke];
    [path stroke];
    
    CGFloat path2StarPoint = -M_PI_2;
    CGFloat path2EndPoint = 2*M_PI * self.havePercent + path2StarPoint;
    UIBezierPath *path2 = [UIBezierPath bezierPath];
    path2.lineWidth = 5;
    [path2 addArcWithCenter:center
                    radius:radius-5
                startAngle:path2StarPoint
                  endAngle:path2EndPoint
                 clockwise:YES];
    [COLOR_HEX(0xE73A59, 1) setStroke];
    [path2 stroke];
    
    CGFloat  baodiPercent = self.baodiPercent;
    if (self.baodiPercent + self.havePercent > 1) {
        baodiPercent = 1 - self.havePercent;
    }
    
    CGFloat path3StartPoint = path2EndPoint;
    CGFloat path3EndPoint = 2*M_PI * baodiPercent + path3StartPoint;
    UIBezierPath *path3 = [UIBezierPath bezierPath];
    path3.lineWidth = 5;
    [path3 addArcWithCenter:center
                     radius:radius-5
                 startAngle:path3StartPoint
                   endAngle:path3EndPoint
                  clockwise:YES];
    [COLOR_HEX(0x43b3fd, 1) setStroke];
    [path3 stroke];
    
    
}


@end
