//
//  SponsorBuyListPercentView.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/6.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SponsorBuyListPercentView : UIView
@property (nonatomic, assign) CGFloat havePercent;      //0~1
@property (nonatomic, assign) CGFloat baodiPercent; //0~1
@end

NS_ASSUME_NONNULL_END
