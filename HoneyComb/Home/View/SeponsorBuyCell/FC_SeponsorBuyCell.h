//
//  FC_SeponsorBuyCell.h
//  HoneyComb
//
//  Created by afc on 2018/12/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SponsorBuyListPercentView.h"

NS_ASSUME_NONNULL_BEGIN

@interface FC_SeponsorBuyCell : UITableViewCell

@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *typenamelab;
@property (nonatomic, strong) UILabel *timelab;
@property (nonatomic, strong) UILabel *aborttimelab;

@property (nonatomic, strong) UILabel *namelab;
@property (nonatomic, strong) UILabel *biolab;
@property (nonatomic, strong) SponsorBuyListPercentView *percentView;


@property (nonatomic, strong) NSMutableArray <UILabel *> *numberlabs;
@property (nonatomic, strong) NSMutableArray <UILabel *> *deslabs;
@property (nonatomic, strong) UIButton *joinBuy;

@end

NS_ASSUME_NONNULL_END
