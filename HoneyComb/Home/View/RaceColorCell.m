//
//  RaceColorCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "RaceColorCell.h"

@implementation RaceColorCell

-(void)setModel:(AllModel *)model
{
    
    _model = model;
    self.weekLabel.text = model.number;
    self.leftCity.text = model.hostTeam;
    self.rightCity.text = model.visitingTeam;
    self.leagueLabel.text = model.league;
    
    if (![model.content isEqualToString:@"-"]) {
        NSString * resultStr = @"";
        NSArray * array = [model.content componentsSeparatedByString:@","];
        for (int i = 0; i < array.count; i ++) {
            NSString * string = [array objectAtIndex:i];
            if (i == 0) {
                resultStr = string;
            }else{
                NSString * str = i%2 == 0?@"\n":@" , ";
                resultStr = [resultStr stringByAppendingString:[NSString stringWithFormat:@"%@%@",str,string]];
            }
        }
        
        CGFloat  labelHeight = [self labelHeightWithString:resultStr];
        CGFloat  contentHeight = kWidth/9.375;
        if (labelHeight > contentHeight) {
            contentHeight = labelHeight;
            [self.resultsLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(contentHeight);
            }];
        }
        
        self.resultsLabel.text = resultStr;
    }
    
    self.damImage.hidden = ![model.dan isEqualToString:@"1"]?YES:NO;
    
    if (model.startTime && ![model.startTime isEqualToString:@"-"]) {
        
        NSArray * dateStrings = [model.startTime componentsSeparatedByString:@" "];
        NSArray * dateArr = [[dateStrings firstObject] componentsSeparatedByString:@"-"];
        
        NSInteger  dateNum = dateArr.count > 2?1:0;
        NSString * dateStr = @"";
        for (int i = 0; i < dateArr.count; i ++) {
            if (i >= dateNum) {
                NSString * string = i == dateNum?@"月":@"日";
                dateStr = [dateStr stringByAppendingString:[NSString stringWithFormat:@"%@%@",[dateArr objectAtIndex:i],string]];
            }
        }
        
        NSString * timeStr = [[dateStrings lastObject] substringToIndex:5];
        
        self.dateLabel.text = [NSString stringWithFormat:@"%@\n%@截止",dateStr,timeStr];

    }
}

- (CGFloat)labelHeightWithString:(NSString *)resultString{

    CGSize maxSize = CGSizeMake(kWidth/1.720, MAXFLOAT);
    
    // 计算内容label的高度
    CGFloat textH = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont fontWithName:kPingFangRegular size:13]} context:nil].size.height;
    return textH + 10;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}
-(void)layoutView
{

    self.weekLabel = [[UILabel alloc]init];
    self.weekLabel.textAlignment = NSTextAlignmentLeft;
    self.weekLabel.textColor = kGrayColor;
    self.weekLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    [self.contentView addSubview:self.weekLabel];

    [self.weekLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/5);
        make.height.mas_equalTo(kWidth/28.8461);
        make.top.mas_equalTo(kWidth/46.875);
        make.left.mas_equalTo(kWidth/31.25);
    }];
    
    self.leagueLabel = [[UILabel alloc]init];
    self.leagueLabel.textAlignment = NSTextAlignmentLeft;
    self.leagueLabel.textColor = kGrayColor;
    self.leagueLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    [self.contentView addSubview:self.leagueLabel];
    [self.leagueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/5);
        make.height.mas_equalTo(kWidth/28.8461);
        make.top.mas_equalTo(self.weekLabel.mas_bottom).offset(kWidth/62.5);
        make.left.mas_equalTo(kWidth/31.25);
    }];
    
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.numberOfLines = 0;
    self.dateLabel.textAlignment = NSTextAlignmentLeft;
    self.dateLabel.textColor = kGrayColor;
    self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    [self.contentView addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/5);
        make.height.mas_equalTo(kWidth/10.7142);
        make.top.mas_equalTo(self.leagueLabel.mas_bottom).offset(kWidth/62.5);
        make.left.mas_equalTo(kWidth/31.25);
    }];
    self.leftCity = [[UILabel alloc]init];
    self.leftCity.textAlignment = NSTextAlignmentLeft;
    self.leftCity.textColor = kBlackColor;
    self.leftCity.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [self.contentView addSubview:self.leftCity];
    [self.leftCity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/3);
        make.height.mas_equalTo(kWidth/23.4375);
        make.top.mas_equalTo(kWidth/25);
        make.left.mas_equalTo(self.dateLabel.mas_right).offset(kWidth/75);
    }];
    self.vsLabel = [[UILabel alloc]init];
    self.vsLabel.text = @"VS";
    self.vsLabel.textAlignment = NSTextAlignmentCenter;
    self.vsLabel.textColor = kBlackColor;
    self.vsLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [self.contentView addSubview:self.vsLabel];
    [self.vsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/20.8333);
        make.height.mas_equalTo(23.4275);
        make.top.mas_equalTo(kWidth/25);
        make.left.mas_equalTo(self.leftCity.mas_right).offset(0);
    }];
    self.rightCity = [[UILabel alloc]init];
    self.rightCity.textAlignment = NSTextAlignmentRight;
    self.rightCity.textColor = kBlackColor;
    self.rightCity.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [self.contentView addSubview:self.rightCity];
    [self.rightCity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/3);
        make.height.mas_equalTo(kWidth/23.4375);
        make.top.mas_equalTo(kWidth/25);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-kWidth/26.7857);
    }];
    
    self.imageV = [[UIImageView alloc]init];
    self.imageV.image = [UIImage imageNamed:@"组3"];
    [self.contentView addSubview:self.imageV];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/15.625);
        make.height.mas_equalTo(kWidth/9.375);
         make.left.mas_equalTo(self.dateLabel.mas_right).offset(5);
         make.top.mas_equalTo(kWidth/9.375);
    }];
    
    self.damImage = [[UIImageView alloc]init];
    self.damImage.image = [UIImage imageNamed:@"dan"];
    self.damImage.hidden = YES;
    [self.contentView addSubview:self.damImage];
    [self.damImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/20.8333);
        make.height.mas_equalTo(kWidth/9.375);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(kWidth/9.375);
    }];
    self.resultsLabel = [[UILabel alloc]init];
    self.resultsLabel.textAlignment = NSTextAlignmentCenter;
    self.resultsLabel.textColor = kRedColor;
    self.resultsLabel.numberOfLines = 0;
    self.resultsLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    self.resultsLabel.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.resultsLabel];
    [self.resultsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/1.720);
        make.height.mas_equalTo(kWidth/9.375);
        make.top.mas_equalTo(kWidth/9.375);
        make.left.mas_equalTo(self.imageV.mas_right).offset(5);
    }];
    self.speView = [[UIView alloc]init];
    self.speView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.speView];
    [self.speView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(-1);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
