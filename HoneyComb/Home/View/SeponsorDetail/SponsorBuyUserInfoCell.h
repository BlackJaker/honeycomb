//
//  SponsorBuyUserInfoCell.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/3.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 详情用户信息cell
 */
@interface SponsorBuyUserInfoCell : UITableViewCell
@property (nonatomic, strong) UIImageView *userIcon;
@property (nonatomic, strong) UILabel *usernamelab;
@property (nonatomic, strong) UILabel *biolab;
@end

NS_ASSUME_NONNULL_END
