//
//  SponsorBuyFriendCell.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/5.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 合买彩友 cell高度固定 50
 */
@interface SponsorBuyFriendCell : UITableViewCell
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *namelab;
@property (nonatomic, strong) UILabel *numlab;
@property (nonatomic, strong) UILabel *percentlab;
@end
//
//@interface SponsorBuyFriendHeaderView : UIView
//
//@end

NS_ASSUME_NONNULL_END
