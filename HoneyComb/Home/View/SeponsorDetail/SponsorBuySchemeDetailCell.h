//
//  SponsorBuySchemeDetailCell.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/3.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SponsorBuyProgressBarView.h"
NS_ASSUME_NONNULL_BEGIN

/**
 详情方案详情Cell
 */
@interface SponsorBuySchemeDetailCell : UITableViewCell

//infoView
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *namelab;
@property (nonatomic, strong) UILabel *timelab;
@property (nonatomic, strong) UILabel *aborttimelab;
@property (nonatomic, strong) UILabel *issueLabel;
@property (nonatomic, strong) SponsorBuyProgressBarView *scheduleView;
@property (nonatomic, strong) NSMutableArray <UILabel *>*numberlabs;
@property (nonatomic, strong) NSMutableArray <UILabel *>*deslabs;
//detailView
@property (nonatomic, strong) UILabel *ordernumlab;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *userNumLabel;
@property (nonatomic, strong) UIButton *userNumButton;
@property (nonatomic, strong) UILabel *baodijinelab;
@property (nonatomic, strong) UILabel *zhongjiangyongjinlab;
@property (nonatomic, strong) UILabel *fanganzhuangtailab;
@end

NS_ASSUME_NONNULL_END
