//
//  SponsorBuyProgressBarView.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/3.
//  Copyright © 2018 op150. All rights reserved.
//

#import "SponsorBuyProgressBarView.h"

@interface PercentLabel : UIView
@property (nonatomic, strong) UILabel *label;
@end

@implementation PercentLabel

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.label = [UILabel lableWithText:@"35%"
                                       font:[UIFont systemFontOfSize:11]
                                  textColor:[UIColor whiteColor]];
        self.label.frame = CGRectMake(0, 0, frame.size.width, kDEVICE_SCALE_FACTOR(18));
        self.label.backgroundColor = COLOR_HEX(0xE73A59, 1);
        self.label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.label];
    }
    return self;
}

- (void)drawRect:(CGRect)rect{
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(kDEVICE_SCALE_FACTOR(21-4), kDEVICE_SCALE_FACTOR(18))];
    [path addLineToPoint:CGPointMake(kDEVICE_SCALE_FACTOR(21), kDEVICE_SCALE_FACTOR(20)+kDEVICE_SCALE_FACTOR(7))];
    [path addLineToPoint:CGPointMake(kDEVICE_SCALE_FACTOR(21+4), kDEVICE_SCALE_FACTOR(18))];
    [COLOR_HEX(0xE73A59, 1) setFill];
    [path fill];
}

@end

@interface SponsorBuyProgressBarView()
{
//    UIView *_progressView;
    UILabel *_percentagelab;
    PercentLabel *_baodiPercentagelab;
}
@end

@implementation SponsorBuyProgressBarView

#define CGSelfProgressViewRect CGRectMake(0, 0, kWidth - kDEVICE_SCALE_FACTOR(15)*2, kDEVICE_SCALE_FACTOR(30))

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self initView];
    }
    return self;
}

- (void)initView{
    UILabel *label = [UILabel lableWithText:@"35%"
                                       font:[UIFont systemFontOfSize:11]
                                  textColor:[UIColor whiteColor]];
    _percentagelab = label;
    [self addSubview:label];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = COLOR_HEX(0xED1C24, 1);
    label.frame = CGRectMake(0,kDEVICE_SCALE_FACTOR(30 - 20)/2, kDEVICE_SCALE_FACTOR(45), kDEVICE_SCALE_FACTOR(20));
    label.cornerRadius = kDEVICE_SCALE_FACTOR(20)/2.;
    
    PercentLabel *label2 = [[PercentLabel alloc] initWithFrame:CGRectMake(0, kDEVICE_SCALE_FACTOR(30/2) - kDEVICE_SCALE_FACTOR(25), kDEVICE_SCALE_FACTOR(41), kDEVICE_SCALE_FACTOR(25))];
    _baodiPercentagelab = label2;
    [self addSubview:label2];
}

- (void)setBaodiPercent:(CGFloat)baodiPercent{
    _baodiPercent = baodiPercent;
    _baodiPercentagelab.label.text = [NSString stringWithFormat:@"%.0f%%",roundf(baodiPercent*100)];
    _baodiPercentagelab.center = CGPointMake(CGSelfProgressViewRect.size.width*(baodiPercent+self.havePercent), _baodiPercentagelab.center.y);
    
    _baodiPercentagelab.hidden = !baodiPercent;
    
    [self setNeedsDisplay];
}

- (void)setHavePercent:(CGFloat)havePercent{
    _havePercent = havePercent;
    _percentagelab.text = [NSString stringWithFormat:@"%.0f%%",roundf(havePercent*100)];
    _percentagelab.center = CGPointMake(CGSelfProgressViewRect.size.width*havePercent, _percentagelab.center.y);
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect{
    
    CGFloat centerY = CGSelfProgressViewRect.size.height/2.;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineWidth = kDEVICE_SCALE_FACTOR(3);
    [path moveToPoint:CGPointMake(0, rect.size.height/2.)];
    [path addLineToPoint:CGPointMake(rect.size.width, rect.size.height/2.)];
    [COLOR_HEX(0xB6B6B6, 1) setStroke];
    [path stroke];
    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    path1.lineWidth = kDEVICE_SCALE_FACTOR(3);
    [path1 moveToPoint:CGPointMake(0, centerY)];
    [path1 addLineToPoint:CGPointMake(CGSelfProgressViewRect.size.width*self.havePercent, centerY)];
    [COLOR_HEX(0xED1C24, 1) setStroke];
    [path1 stroke];
    
    UIBezierPath *path2 = [UIBezierPath bezierPath];
    path2.lineWidth = kDEVICE_SCALE_FACTOR(3);
    [path2 moveToPoint:CGPointMake(CGSelfProgressViewRect.size.width*self.havePercent, centerY)];
    [path2 addLineToPoint:CGPointMake(CGSelfProgressViewRect.size.width*(self.havePercent+self.baodiPercent), centerY)];
    [COLOR_HEX(0xFDC689, 1) setStroke];
    [path2 stroke];
}



@end
