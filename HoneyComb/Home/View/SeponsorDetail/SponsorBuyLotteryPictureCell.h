//
//  SponsorBuyLotteryPictureCell.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/5.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN



/**
 彩票照片  cell高度固定210
 */
@interface SponsorBuyLotteryPictureCell : UITableViewCell
@property (nonatomic, strong) UIImageView *imag1;
@property (nonatomic, strong) UIImageView *imag2;
@property (nonatomic, strong) UIImageView *imag3;
@end

NS_ASSUME_NONNULL_END
