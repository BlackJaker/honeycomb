//
//  SponsorBuySchemeDetailCell.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/3.
//  Copyright © 2018 op150. All rights reserved.
//

#import "SponsorBuySchemeDetailCell.h"

@implementation SponsorBuySchemeDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat  timeHeight = 32;
        
        self.timelab = ({
            UILabel *label = [UILabel lableWithText:@"截止时间 : "
                                               font:[UIFont systemFontOfSize:13]
                                          textColor:RGB(102, 102, 102, 1.0f)];
            label.textAlignment = NSTextAlignmentCenter;
            label.backgroundColor = RGB(247, 247, 247, 1.0f);
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(0);
                make.right.mas_equalTo(0);
                make.left.mas_equalTo(0);
                make.height.mas_equalTo(timeHeight);
            }];
            label;
        });
        
        self.icon = ({
            UIImageView *image = [[UIImageView alloc] init];
            [self.contentView addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(@(kDEVICE_SCALE_FACTOR(16)));
                make.top.mas_equalTo(self.timelab.mas_bottom).offset(23);
                make.width.height.mas_equalTo(@(kDEVICE_SCALE_FACTOR(45)));
            }];
            image.cornerRadius = kDEVICE_SCALE_FACTOR(45)/2.;
            image;
        });
        
        CGFloat  width = 200;
        
        self.namelab = ({
            UILabel *label = [UILabel lableWithText:@""
                                               font:[UIFont boldSystemFontOfSize:16]
                                          textColor:COLOR_HEX(0x333333, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.icon.mas_right).offset(kDEVICE_SCALE_FACTOR(15));
                make.width.equalTo(width);
                make.centerY.equalTo(self.icon);
            }];
            label;
        });
        
       
        
//        self.aborttimelab = ({
//            UILabel *label = [UILabel lableWithText:@""
//                                               font:[UIFont systemFontOfSize:13]
//                                          textColor:COLOR_HEX(0xE73A59, 1)];
//            [self.contentView addSubview:label];
//            [label mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.centerY.equalTo(self.namelab);
//                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
//            }];
//            label;
//        });
        
        self.issueLabel = ({
            UILabel *label = [UILabel lableWithText:@""
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x666666, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.namelab.mas_right).offset(12);
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-12)));
                make.centerY.equalTo(self.icon);
            }];
            label;
        });
        
        self.scheduleView = ({
            SponsorBuyProgressBarView *view = [[SponsorBuyProgressBarView alloc] init];
            [self.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
                make.top.equalTo(self.icon.mas_bottom).offset(16);
                make.height.equalTo(@(kDEVICE_SCALE_FACTOR(30)));
            }];
            view;
        });
        
        UIView *shadowView = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
        shadowView.layer.shadowColor = COLOR_HEX(0x000000,1).CGColor;
        shadowView.layer.shadowRadius = 7;
        shadowView.layer.shadowOpacity = 0.08;
        shadowView.layer.shadowOffset = CGSizeMake(0, 2);
        [self.contentView addSubview:shadowView];
        [shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
            make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
            make.top.equalTo(self.scheduleView.mas_bottom).offset(2);
            make.height.equalTo(@(kDEVICE_SCALE_FACTOR(86)));
        }];
        
        UIView *backView = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
        backView.cornerRadius = 7;
        [shadowView addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(@0);
        }];
        
        self.numberlabs = [NSMutableArray array];
        self.deslabs = [NSMutableArray array];
        for (int i = 0; i < 4; i++) {
            UILabel *numlab = [UILabel lableWithText:@""
                                                font:[UIFont boldSystemFontOfSize:19]
                                           textColor:COLOR_HEX(0xD02332, 1)];
            numlab.textAlignment = NSTextAlignmentCenter;
            [backView addSubview:numlab];
            [numlab mas_makeConstraints:^(MASConstraintMaker *make) {
                if (i == 0) {
                    make.left.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
                }else{
                    make.left.equalTo(self.numberlabs.lastObject.mas_right);
                }
                make.top.equalTo(@(kDEVICE_SCALE_FACTOR(25)));
                make.width.equalTo(backView.mas_width).multipliedBy(0.25);
            }];
            
            UILabel *deslab = [UILabel lableWithText:@""
                                                font:[UIFont systemFontOfSize:12]
                                           textColor:COLOR_HEX(0x999999, 1)];
            deslab.textAlignment = NSTextAlignmentCenter;
            [backView addSubview:deslab];
            [deslab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.width.equalTo(numlab);
                make.top.equalTo(numlab.mas_bottom).offset(kDEVICE_SCALE_FACTOR(10));
            }];
            
            [self.numberlabs addObject:numlab];
            [self.deslabs addObject:deslab];
            
        }
        
        UIView *redView = [UIView viewWithBackgroundColor:COLOR_HEX(0xE73A59, 1)];
        [self.contentView addSubview:redView];
        [redView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
            make.top.equalTo(shadowView.mas_bottom).offset(kDEVICE_SCALE_FACTOR(20));
            make.width.equalTo(@(kDEVICE_SCALE_FACTOR(5)));
            make.height.equalTo(@(kDEVICE_SCALE_FACTOR(16)));
        }];
        
        UILabel *infolab = [UILabel lableWithText:@"方案详情"
                                             font:[UIFont boldSystemFontOfSize:18]
                                        textColor:COLOR_HEX(0x464E5F, 1)];
        [self.contentView addSubview:infolab];
        [infolab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(redView.mas_right).offset(kDEVICE_SCALE_FACTOR(10));
            make.centerY.equalTo(redView);
        }];
        
        self.ordernumlab = ({
            UILabel *label = [UILabel lableWithText:@"订单编号："
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x666666, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
                make.top.equalTo(infolab.mas_bottom).offset(kDEVICE_SCALE_FACTOR(15));
            }];
            label;
        });
        
        self.userNameLabel = ({
            UILabel *label = [UILabel lableWithText:@"发 起 人："
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x666666, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
                make.top.equalTo(self.ordernumlab.mas_bottom).offset(kDEVICE_SCALE_FACTOR(15));
            }];
            label;
        });
        
        self.userNumButton = ({
            UIButton *button = [UIButton buttonWithTitle:@"认购列表"
                                                    font:[UIFont boldSystemFontOfSize:14]
                                              titleColor:[UIColor redColor]
                                            controlState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"withdraw_next_step"] forState:UIControlStateNormal];
            [self.contentView addSubview:button];

            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(-15);
                make.height.equalTo(45);
                make.width.equalTo(120);
                make.top.equalTo(self.userNameLabel.mas_bottom);
            }];
            
            CGFloat margin = 2.5;
            CGFloat imageWidth = button.imageView.bounds.size.width;
            CGFloat labelWidth = button.titleLabel.bounds.size.width;
            button.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth-margin, 0, imageWidth+margin);
            button.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth+margin, 0, -labelWidth-margin);
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            button;
        });
        
        self.userNumLabel = ({
            UILabel *label = [UILabel lableWithText:@"参与人数："
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x666666, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.right.equalTo(self.userNumButton.mas_left).offset(15);
                make.top.equalTo(self.userNameLabel.mas_bottom).offset(kDEVICE_SCALE_FACTOR(15));
            }];
            label;
        });
        
        self.baodijinelab = ({
            UILabel *label = [UILabel lableWithText:@"保底金额："
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x666666, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
                make.top.equalTo(self.userNumLabel.mas_bottom).offset(kDEVICE_SCALE_FACTOR(14));
            }];
            label;
        });
        
        self.zhongjiangyongjinlab = ({
            UILabel *label = [UILabel lableWithText:@"奖励奖金："
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x666666, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
                make.top.equalTo(self.baodijinelab.mas_bottom).offset(kDEVICE_SCALE_FACTOR(14));
            }];
            label;
        });
        
        self.fanganzhuangtailab = ({
            UILabel *label = [UILabel lableWithText:@"方案状态："
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x666666, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
                make.top.equalTo(self.zhongjiangyongjinlab.mas_bottom).offset(kDEVICE_SCALE_FACTOR(14));
            }];
            label;
        });
        
    }
    return self;
}

@end
