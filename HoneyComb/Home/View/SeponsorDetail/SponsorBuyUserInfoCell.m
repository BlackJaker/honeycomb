//
//  SponsorBuyUserInfoCell.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/3.
//  Copyright © 2018 op150. All rights reserved.
//

#import "SponsorBuyUserInfoCell.h"

@implementation SponsorBuyUserInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.userIcon = ({
            UIImageView *image = [[UIImageView alloc] init];
            [self.contentView addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.width.height.equalTo(@(kDEVICE_SCALE_FACTOR(50)));
            }];
            image.cornerRadius = kDEVICE_SCALE_FACTOR(50)/2.;
            image;
        });

        
        self.usernamelab = ({
            UILabel *label = [UILabel lableWithText:@""
                                               font:[UIFont boldSystemFontOfSize:24]
                                          textColor:COLOR_HEX(0x464E5F, 1)];
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.userIcon.mas_right).offset(kDEVICE_SCALE_FACTOR(15));
                make.centerY.equalTo(self.userIcon);
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
            }];
            label;
        });
        
        self.biolab = ({
            UILabel *label = [UILabel lableWithText:@""
                                               font:[UIFont systemFontOfSize:14]
                                          textColor:COLOR_HEX(0x7F8491, 1)];
            label.numberOfLines = 0;
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.userIcon.mas_bottom).offset(kDEVICE_SCALE_FACTOR(15));
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(20)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-20)));
                make.bottom.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
            }];
            label;
        });
        
    }
    return self;
}

@end
