//
//  SponsorBuyLotteryPictureCell.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/5.
//  Copyright © 2018 op150. All rights reserved.
//

#import "SponsorBuyLotteryPictureCell.h"

@implementation SponsorBuyLotteryPictureCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.imag1 = ({
            UIImageView *img = [[UIImageView alloc] init];
            [self.contentView addSubview:img];
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(kDEVICE_SCALE_FACTOR(110)));
                make.height.equalTo(@(kDEVICE_SCALE_FACTOR(185)));
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.top.equalTo(@(kDEVICE_SCALE_FACTOR(20)));
            }];
            img;
        });
        
        self.imag2 = ({
            UIImageView *img = [[UIImageView alloc] init];
            [self.contentView addSubview:img];
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(kDEVICE_SCALE_FACTOR(110)));
                make.height.equalTo(@(kDEVICE_SCALE_FACTOR(185)));
                make.centerX.equalTo(@0);
                make.top.equalTo(@(kDEVICE_SCALE_FACTOR(20)));
            }];
            img;
        });
        
        self.imag3 = ({
            UIImageView *img = [[UIImageView alloc] init];
            [self.contentView addSubview:img];
            [img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(kDEVICE_SCALE_FACTOR(110)));
                make.height.equalTo(@(kDEVICE_SCALE_FACTOR(185)));
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
                make.top.equalTo(@(kDEVICE_SCALE_FACTOR(20)));
            }];
            img;
        });
    }
    return self;
}

@end
