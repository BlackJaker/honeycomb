//
//  SponsorSchemeCell.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/3.
//  Copyright © 2018 op150. All rights reserved.
//

#import "SponsorSchemeCell.h"

@implementation SponsorSchemeHeaderView
- (instancetype)initWithType:(SponsorSchemeHeaderViewType)type{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        UIView *redView = [UIView viewWithBackgroundColor:COLOR_HEX(0xE73A59, 1)];
        [self addSubview:redView];
        [redView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(15);
            make.top.equalTo(20);
            make.width.equalTo(5);
            make.height.equalTo(16);
        }];
        
        UILabel *infolab = [UILabel lableWithText:@"投注信息"
                                             font:[UIFont boldSystemFontOfSize:18]
                                        textColor:COLOR_HEX(0x464E5F, 1)];
        self.titlelab = infolab;
        [self addSubview:infolab];
        [infolab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(redView.mas_right).offset(10);
            make.centerY.equalTo(redView);
        }];
        
        self.typelab = ({
            UILabel *typelab = [UILabel lableWithText:@""
                                                 font:[UIFont boldSystemFontOfSize:18]
                                            textColor:COLOR_HEX(0x464E5F, 1)];
            [self addSubview:typelab];
            [typelab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(infolab.mas_right).offset(40);
                make.centerY.equalTo(infolab);
            }];
            typelab;
        });
        
        if (type == DefaultSponsorSchemeType) {
            self.frame = CGRectMake(0, 0, kWidth, 40);
        }else if (type == DaLeTouSponsorSchemeType){
            self.frame = CGRectMake(0, 0, kWidth, 40);
        }else if (type == OtherSponsorSchemeType){
            UIView *redBackView = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
            redBackView.frame = CGRectMake(15, 62, kWidth - 15*2, 38);
            CAGradientLayer *gl = [CAGradientLayer layer];
            gl.frame = redBackView.bounds;
            gl.startPoint = CGPointMake(0, 0);
            gl.endPoint = CGPointMake(1, 1);
            gl.colors = @[(__bridge id)[UIColor colorWithRed:241/255.0 green:34/255.0 blue:127/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:237/255.0 green:49/255.0 blue:82/255.0 alpha:1.0].CGColor];
            gl.locations = @[@(0.0),@(1.0f)];
            [redBackView.layer addSublayer:gl];
            [self addSubview:redBackView];
            
            NSArray *titles = @[@"场次",@"主队VS客队",@"投注内容",@"赛果"];
            NSMutableArray <UILabel *>*titlelabs = [NSMutableArray array];
            for (int i = 0; i < titles.count; i++) {
                UILabel *lab = [UILabel lableWithText:titles[i]
                                                 font:[UIFont boldSystemFontOfSize:14]
                                            textColor:[UIColor whiteColor]];
                lab.textAlignment = NSTextAlignmentCenter;
                [redBackView addSubview:lab];
                [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(0);
                    make.height.equalTo(redBackView);
                    make.width.equalTo(redBackView).multipliedBy(1./titles.count);
                    if (i == 0) {
                        make.left.equalTo(0);
                    }else{
                        make.left.equalTo(titlelabs.lastObject.mas_right);
                    }
                }];
                [titlelabs addObject:lab];
            }
            
            self.frame = CGRectMake(0, 0, kWidth, 100);
        }else if (type == OtherSponsorSchemeNoResultType){
            UIView *redBackView = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
            redBackView.frame = CGRectMake(15, 62, kWidth - 15*2, 38);
            CAGradientLayer *gl = [CAGradientLayer layer];
            gl.frame = redBackView.bounds;
            gl.startPoint = CGPointMake(0, 0);
            gl.endPoint = CGPointMake(1, 1);
            gl.colors = @[(__bridge id)[UIColor colorWithRed:241/255.0 green:34/255.0 blue:127/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:237/255.0 green:49/255.0 blue:82/255.0 alpha:1.0].CGColor];
            gl.locations = @[@(0.0),@(1.0f)];
            [redBackView.layer addSublayer:gl];
            [self addSubview:redBackView];
            
            NSArray *titles = @[@"场次",@"主队VS客队",@"投注内容"];
            NSMutableArray <UILabel *>*titlelabs = [NSMutableArray array];
            for (int i = 0; i < titles.count; i++) {
                UILabel *lab = [UILabel lableWithText:titles[i]
                                                 font:[UIFont boldSystemFontOfSize:14]
                                            textColor:[UIColor whiteColor]];
                lab.textAlignment = NSTextAlignmentCenter;
                [redBackView addSubview:lab];
                [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(0);
                    make.height.equalTo(redBackView);
                    make.width.equalTo(redBackView).multipliedBy(1./titles.count);
                    if (i == 0) {
                        make.left.equalTo(0);
                    }else{
                        make.left.equalTo(titlelabs.lastObject.mas_right);
                    }
                }];
                [titlelabs addObject:lab];
            }
            
            self.frame = CGRectMake(0, 0, kWidth, 100);

        }
    }
    return self;
}

-(UILabel *)followNumLabel{
    
    CGFloat  width = 80,height = 50,right = 16;
    
    if (!_followNumLabel) {
       _followNumLabel = [UILabel lableWithText:@"0人跟单"
                                         font:[UIFont boldSystemFontOfSize:14]
                                    textColor:RGB(231, 58, 89, 1.0f)];
        _followNumLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_followNumLabel];
        [_followNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(height);
            make.width.equalTo(width);
            make.right.equalTo(-right);
            make.top.equalTo(0);
        }];
    }
    return _followNumLabel;
}

@end

@implementation DefaultSponsorSchemeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UILabel *lab = [UILabel lableWithText:@"投注后可见"
                                         font:[UIFont systemFontOfSize:13]
                                    textColor:COLOR_HEX(0x999999, 1)];
        [self.contentView addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(16);
            make.right.equalTo(-16);
            make.top.equalTo(14);
            make.bottom.equalTo(self.contentView.mas_bottom).offset(-16);
        }];
    }
    return self;
}

@end

@implementation DaleTouSponsorSchemeCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.numlab = ({
            UILabel *lab = [UILabel lableWithText:@""
                                             font:[UIFont systemFontOfSize:12]
                                        textColor:COLOR_HEX(0xffffff, 1)];
            lab.backgroundColor = COLOR_HEX(0xFD2525, 1);
            lab.textAlignment = NSTextAlignmentCenter;
            lab.cornerRadius = 25/2.;
            [self.contentView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(@0);
            }];
            lab;
        });
    }
    return self;
}

@end

@implementation DaLeTouSponsorSchemeCell
static NSString *cellID = @"cellID";
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _titleLabel = [UILabel lableWithText:@"普通投注"
                                           font:[UIFont systemFontOfSize:13]
                                      textColor:COLOR_HEX(0x333333, 1)];
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(13);
            make.top.equalTo(15);
        }];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(25, 25);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(10, 0, 10, 0);
        
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        collectionView.backgroundColor = [UIColor whiteColor];
        collectionView.scrollEnabled = NO;
        [collectionView registerClass:[DaleTouSponsorSchemeCollectionViewCell class] forCellWithReuseIdentifier:cellID];
        collectionView.delegate = self;
        collectionView.dataSource = self;
        [self.contentView addSubview:collectionView];
        [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleLabel.mas_right).offset(20);
            make.right.equalTo(-20);
            make.top.bottom.equalTo(0);
        }];
    }
    return self;
}

#pragma mark UICollectionViewDelegate UICollectionViewDateSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DaleTouSponsorSchemeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    NSArray * colors = @[[UIColor colorWithRed:254/255.0 green:83/255.0 blue:131/255.0 alpha:1.0],
                         [UIColor colorWithRed:253/255.0 green:37/255.0 blue:37/255.0 alpha:1.0],
                         [UIColor colorWithRed:0/255.0 green:0/255.0 blue:255/255.0 alpha:1.0],
                         [UIColor colorWithRed:37/255.0 green:164/255.0 blue:253/255.0 alpha:1.0]
                         ];
    
    NSMutableDictionary * dic = self.listArr[indexPath.row];
    cell.numlab.backgroundColor = [colors objectAtIndex:[dic[@"type"] integerValue]];
    cell.numlab.text = dic[@"ball"];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.listArr count];
}

//设置点击高亮和非高亮效果！
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

@end

@implementation OtherSponsorSchemeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backView = ({
            UIView *view = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
            [self.contentView addSubview:view];

            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(15);
                make.right.equalTo(-15);
                make.bottom.equalTo(0);
                make.top.equalTo(0);
            }];
            
            view;
        });
        
        self.titlelabs = [NSMutableArray array];
        for (int i = 0; i < 4; i++) {
            UILabel *lab = [UILabel lableWithText:@""
                                             font:[UIFont systemFontOfSize:12]
                                        textColor:COLOR_HEX(0x565D6E, 1)];
            lab.textAlignment = NSTextAlignmentCenter;
            lab.numberOfLines = 0;
            [self.backView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(self.backView);
                make.width.equalTo(self.backView).multipliedBy(.25);
                if (i == 0) {
                    make.left.equalTo(0);
                }else{
                    make.left.equalTo(self.titlelabs.lastObject.mas_right);
                }
            }];
            [self.titlelabs addObject:lab];
        }
    }
    return self;
}

@end

@implementation OtherSponsorSchemeNoResultCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backView = ({
            UIView *view = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
            [self.contentView addSubview:view];
            
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(15);
                make.right.equalTo(-15);
                make.bottom.equalTo(0);
                make.top.equalTo(0);
            }];
            
            view;
        });
        
        self.titlelabs = [NSMutableArray array];
        for (int i = 0; i < 3; i++) {
            UILabel *lab = [UILabel lableWithText:@""
                                             font:[UIFont systemFontOfSize:11]
                                        textColor:COLOR_HEX(0x565D6E, 1)];
            lab.textAlignment = NSTextAlignmentCenter;
            lab.numberOfLines = 0;
            [self.backView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(self.backView);
                make.width.equalTo(self.backView).multipliedBy(1/3.);
                if (i == 0) {
                    make.left.equalTo(0);
                }else{
                    make.left.equalTo(self.titlelabs.lastObject.mas_right);
                }
            }];
            [self.titlelabs addObject:lab];
        }
    }
    return self;
}

@end
