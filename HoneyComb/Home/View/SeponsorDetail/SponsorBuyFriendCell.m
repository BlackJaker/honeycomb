//
//  SponsorBuyFriendCell.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/5.
//  Copyright © 2018 op150. All rights reserved.
//

#import "SponsorBuyFriendCell.h"


//@implementation SponsorBuyFriendHeaderView
//- (instancetype)initWithFrame:(CGRect)frame{
//    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor whiteColor];
//        UIView *redView = [UIView viewWithBackgroundColor:COLOR_HEX(0xE73A59, 1)];
//        [self addSubview:redView];
//        [redView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
//            make.top.equalTo(@(kDEVICE_SCALE_FACTOR(20)));
//            make.width.equalTo(@(kDEVICE_SCALE_FACTOR(5)));
//            make.height.equalTo(@(kDEVICE_SCALE_FACTOR(16)));
//        }];
//        
//        UILabel *infolab = [UILabel lableWithText:@"合买彩友"
//                                             font:[UIFont boldSystemFontOfSize:18]
//                                        textColor:COLOR_HEX(0x464E5F, 1)];
//        [self addSubview:infolab];
//        [infolab mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(redView.mas_right).offset(kDEVICE_SCALE_FACTOR(10));
//            make.centerY.equalTo(redView);
//        }];
//        
//    }
//    return self;
//}
//@end

@implementation SponsorBuyFriendCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.icon = ({
            UIImageView *image = [[UIImageView alloc] init];
            [self.contentView addSubview:image];
            [image mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
                make.centerY.equalTo(@0);
                make.width.height.equalTo(@(kDEVICE_SCALE_FACTOR(30)));
            }];
            image.cornerRadius = kDEVICE_SCALE_FACTOR(30)/2;
            image;
        });
        
        self.namelab = ({
            UILabel *lab = [UILabel lableWithText:@""
                                             font:[UIFont systemFontOfSize:13]
                                        textColor:COLOR_HEX(0x565D6E, 1)];
            [self.contentView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.icon.mas_right).offset(kDEVICE_SCALE_FACTOR(15));
                make.centerY.equalTo(@0);
            }];
            lab;
        });
        
        self.numlab = ({
            UILabel *lab = [UILabel lableWithText:@"1份"
                                             font:[UIFont systemFontOfSize:13]
                                        textColor:COLOR_HEX(0x565D6E, 1)];
            [self.contentView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-170)));
                make.centerY.equalTo(@0);
            }];
            lab;
        });
        
        self.percentlab = ({
            UILabel *lab = [UILabel lableWithText:@"25%"
                                             font:[UIFont systemFontOfSize:13]
                                        textColor:COLOR_HEX(0x565D6E, 1)];
            [self.contentView addSubview:lab];
            [lab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-45)));
                make.centerY.equalTo(@0);
            }];
            lab;
        });
        
    }
    return self;
}

@end
