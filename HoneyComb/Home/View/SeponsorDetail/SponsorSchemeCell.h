//
//  SponsorSchemeCell.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/3.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SponsorSchemeHeaderViewType) {
    DefaultSponsorSchemeType,
    DaLeTouSponsorSchemeType,
    OtherSponsorSchemeType,
    OtherSponsorSchemeNoResultType,
};
@interface SponsorSchemeHeaderView : UIView
@property (nonatomic, strong) UILabel *titlelab;
@property (nonatomic, strong) UILabel *typelab;
@property (nonatomic, strong) UILabel *followNumLabel;
- (instancetype)initWithType:(SponsorSchemeHeaderViewType)type;
@end

/**
 方案内容
 */
@interface DefaultSponsorSchemeCell : UITableViewCell

@end

@interface DaleTouSponsorSchemeCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *numlab;
@end

//  cell的高度计算
//  NSInteger numberOfRow = arr.count/8 + (arr.count % 8 != 0);
//  kDEVICE_SCALE_FACTOR(25)*numberOfRow+kDEVICE_SCALE_FACTOR(10)*(numberOfRow+1);

/*
     NSArray *arr;
     if (indexPath.row == 0) {
         arr = @[@"00",@"01",@"02",@"03",@"04"];
     }else{
         arr = @[@"00",@"01",@"02",@"03",@"04",
                 @"05",@"06",@"07",@"08",@"09",
                 @"10",@"11",@"12",@"13",@"14",
                 @"10",@"11",@"12",@"13",@"14"];
     }

     NSInteger numberOfRow = arr.count/8 + (arr.count % 8 != 0);

     return kDEVICE_SCALE_FACTOR(25)*numberOfRow+kDEVICE_SCALE_FACTOR(10)*(numberOfRow+1);
 */


@interface DaLeTouSponsorSchemeCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *listArr;
@property (nonatomic, strong) UILabel *titleLabel;
@end

//  cell的高度 kDEVICE_SCALE_FACTOR(65)
@interface OtherSponsorSchemeCell : UITableViewCell
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) NSMutableArray <UILabel *>*titlelabs;
@end

//  cell的高度 kDEVICE_SCALE_FACTOR(65)
@interface OtherSponsorSchemeNoResultCell : UITableViewCell
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) NSMutableArray <UILabel *>*titlelabs;
@end


NS_ASSUME_NONNULL_END
