//
//  RaceColorCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceColorCell : UITableViewCell

@property (nonatomic,strong) AllModel *model;
@property (nonatomic,copy) NSString * typeStr;
@property (nonatomic,strong) UILabel *weekLabel;//星期
@property (nonatomic,strong) UILabel *leagueLabel;//联赛
@property (nonatomic,strong) UILabel *dateLabel;//时间
@property (nonatomic,strong) UILabel *leftCity;
@property (nonatomic,strong) UILabel *rightCity;
@property (nonatomic,strong) UILabel *vsLabel;
@property (nonatomic,strong) UIImageView *imageV;
@property (nonatomic,strong) UILabel *resultsLabel;//结果
@property (nonatomic,strong) UIView *speView;
@property (nonatomic,strong) UIImageView *damImage;

@property (nonatomic,strong) UIView * leftNumBackView;

@end
