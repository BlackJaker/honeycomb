//
//  OutView.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OutView.h"
@interface OutView()

@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIWindow *window;

@end
@implementation OutView
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self layoutView];
    }
    return self;
    
}
-(void)layoutView
{
    _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    _bgView.alpha = 0.4;
    _bgView.backgroundColor = [UIColor blackColor];
    [self addSubview:_bgView];
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(12,kHeight/2-212/2, kWidth-24, 212)];
    v.userInteractionEnabled = YES;
    v.backgroundColor = [UIColor whiteColor];
    [self addSubview:v];
//    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissContactView:)];
//    [_bgView addGestureRecognizer:tapGes];
    UILabel *recenLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, kWidth-24-90, 17)];
    recenLabel.text =@"确认出票";
    recenLabel.font = [UIFont fontWithName:kPingFangRegular size:16];
    recenLabel.textColor = RGB(51, 51, 51, 1);
    recenLabel.textAlignment = NSTextAlignmentCenter;
    [v addSubview:recenLabel];
    
    
    UIButton *cancleBtn = [[UIButton alloc]init];
//    [cancleBtn setImage:@"" forState:(UIControlStateNormal)];
    cancleBtn.backgroundColor = kRedColor;
    [cancleBtn addTarget:self action:@selector(cancleAction) forControlEvents:(UIControlEventTouchUpInside)];
    [v addSubview:cancleBtn];
    [cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(45);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    self.weekLabel = [[UILabel alloc]init];
    self.weekLabel.text = @"周三 051";
    self.weekLabel.textAlignment = NSTextAlignmentCenter;
    self.weekLabel.textColor = kBlackColor;
    self.weekLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [v addSubview:self.weekLabel];
    [self.weekLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(103);
        make.height.mas_equalTo(15);
        make.top.mas_equalTo(69);
        make.left.mas_equalTo(0);
    }];
    
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.text = @"07月19日\n23:  55";
    self.dateLabel.numberOfLines = 0;
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    self.dateLabel.textColor = kGrayColor;
    self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    [v addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(103);
        make.height.mas_equalTo(35);
        make.top.mas_equalTo(self.weekLabel.mas_bottom).offset(8);
        make.left.mas_equalTo(0);
    }];
    
    UIView *backView = [[UIView alloc]init];
    backView.backgroundColor = kGrayColor;
    [v addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(46);
        make.top.mas_equalTo(69);
        make.left.mas_equalTo(self.weekLabel.mas_right).offset(0);
    }];
    
    self.leftCity = [[UILabel alloc]init];
    self.leftCity.text = @"鸟栖沙岩";
    self.leftCity.textAlignment = NSTextAlignmentLeft;
    self.leftCity.textColor = kBlackColor;
    self.leftCity.font = [UIFont fontWithName:kPingFangRegular size:15];
    [v addSubview:self.leftCity];
    [self.leftCity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(16);
        make.top.mas_equalTo(69);
        make.left.mas_equalTo(backView.mas_right).offset(22);
    }];
    self.vsLabel = [[UILabel alloc]init];
    self.vsLabel.text = @"VS";
    self.vsLabel.textAlignment = NSTextAlignmentCenter;
    self.vsLabel.textColor = kBlackColor;
    self.vsLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
    [v addSubview:self.vsLabel];
    [self.vsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(18);
        make.height.mas_equalTo(16);
        make.top.mas_equalTo(69);
        make.left.mas_equalTo(self.leftCity.mas_right).offset(0);
    }];
    self.rightCity = [[UILabel alloc]init];
    self.rightCity.text = @"湘南海洋";
    self.rightCity.textAlignment = NSTextAlignmentRight;
    self.rightCity.textColor = kBlackColor;
    self.rightCity.font = [UIFont fontWithName:kPingFangRegular size:15];
    [v addSubview:self.rightCity];
    [self.rightCity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(90);
        make.height.mas_equalTo(16);
        make.top.mas_equalTo(69);
        make.right.mas_equalTo(v.mas_right).offset(-28);
    }];
    self.meLabel = [[UILabel alloc]init];
    self.meLabel.text = @"我的投注:  主胜";
    self.meLabel.textAlignment = NSTextAlignmentLeft;
    self.meLabel.textColor = kBlackColor;
    self.meLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    [v addSubview:self.meLabel];
    [self.meLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(107);
        make.height.mas_equalTo(16);
        make.top.mas_equalTo(self.leftCity.mas_bottom).offset(13);
        make.left.mas_equalTo(backView.mas_right).offset(22);
    }];
    
    self.resultTf = [[UITextField alloc]init];
    self.resultTf.textAlignment = NSTextAlignmentCenter;
    self.resultTf.layer.borderColor =RGB(0, 0, 0, 0.5).CGColor;
    [self.resultTf.layer setBorderWidth:1.0];
    self.resultTf.text = @"3.33";
    self.resultTf.textColor = kBlackColor;
    [v addSubview:self.resultTf];
    [self.resultTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(51);
        make.height.mas_equalTo(24);
        make.left.mas_equalTo(self.meLabel.mas_right).offset(5);
        make.top.mas_equalTo(93);
    }];
    
    self.recentlyBtn = [[UIButton alloc]init];
    [self.recentlyBtn setTitle:@"确认" forState:(UIControlStateNormal)];
    [self.recentlyBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.recentlyBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:15];
    self.recentlyBtn.layer.cornerRadius =4;
    self.recentlyBtn.layer.masksToBounds = YES;
    self.recentlyBtn.backgroundColor = kRedColor;
    [v addSubview:self.recentlyBtn];
    [self.recentlyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(308);
        make.height.mas_equalTo(44);
        make.left.mas_equalTo(43);
        make.bottom.mas_equalTo(-24);
    }];
    
}
-(void)cancleAction
{
       [self dismissView];
}

-(void)dismissContactView:(UITapGestureRecognizer *)tagGes
{
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"moreTongzhi" object:nil userInfo:nil];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    [self dismissView];
}
-(void)showView
{
    _window = [[UIApplication sharedApplication].windows lastObject];
    [_window addSubview:self];
}
-(void)dismissView
{
    __weak typeof(self)weakSelf =self;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.alpha = 0;
    } completion:^(BOOL finished) {
        [weakSelf removeFromSuperview];
        [self removeFromSuperview];
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
