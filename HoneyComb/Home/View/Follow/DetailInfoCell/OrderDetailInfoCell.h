//
//  OrderDetailInfoCell.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/11.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderDetailInfoCell : UITableViewCell
@property (nonatomic, strong) UILabel *lab;
@end

NS_ASSUME_NONNULL_END
