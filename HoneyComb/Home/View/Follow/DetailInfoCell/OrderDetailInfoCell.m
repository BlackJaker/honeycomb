//
//  OrderDetailInfoCell.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/11.
//  Copyright © 2018 op150. All rights reserved.
//

#import "OrderDetailInfoCell.h"

@implementation OrderDetailInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.lab = [UILabel lableWithText:@""
                                     font:[UIFont systemFontOfSize:14]
                                textColor:COLOR_HEX(0x565D6E, 1)];
        self.lab.numberOfLines = 0;
        [self.contentView addSubview:self.lab];
        [self.lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(kDEVICE_SCALE_FACTOR(15)));
            make.right.equalTo(@(kDEVICE_SCALE_FACTOR(-15)));
            make.top.equalTo(@(kDEVICE_SCALE_FACTOR(10)));
        }];
        
        
    }
    return self;
}

@end
