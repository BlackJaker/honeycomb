//
//  YBButton.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/9.
//  Copyright © 2018 op150. All rights reserved.
//

#import "YBButton.h"

@implementation YBButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.normalBackgroundColor = COLOR_HEX(0xFB3024, 1);
        self.selectedBackgroundColor = COLOR_HEX(0xF0EFF4, 1);
        
    }
    return self;
}

- (void)setNormalBackgroundColor:(UIColor *)normalBackgroundColor{
    _normalBackgroundColor = normalBackgroundColor;
    self.backgroundColor = normalBackgroundColor;
}

- (void)setSelected:(BOOL)selected{
    if (selected) {
        self.backgroundColor = self.selectedBackgroundColor;
    }else{
        self.backgroundColor = self.normalBackgroundColor;
    }
    [super setSelected:selected];
}

@end


@implementation GendandatingSegmentButton

- (void)setSelected:(BOOL)selected{
    if (selected) {
        self.titleLabel.font = [UIFont boldSystemFontOfSize:19];
    }else{
        self.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    }
    [super setSelected:selected];
}


@end
