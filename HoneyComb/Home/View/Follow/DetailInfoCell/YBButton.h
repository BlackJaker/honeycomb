//
//  YBButton.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/9.
//  Copyright © 2018 op150. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YBButton : UIButton
@property (nonatomic, strong) UIColor *selectedBackgroundColor;
@property (nonatomic, strong) UIColor *normalBackgroundColor;
@end


@interface GendandatingSegmentButton : UIButton
@property (nonatomic, strong) UIColor *selectedBackgroundColor;
@property (nonatomic, strong) UIColor *normalBackgroundColor;
@end

NS_ASSUME_NONNULL_END
