//
//  FC_DocumentaryCell.m
//  HoneyComb
//
//  Created by afc on 2018/12/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_DocumentaryCell.h"

#import "FC_FollowListModel.h"

@implementation FC_DocumentaryCell

- (void)awakeFromNib {
    
    [super awakeFromNib];

    self.backView.layer.cornerRadius = 5.0f;
    self.backView.clipsToBounds = YES;
    
}

-(void)reloadDocumentaryCellWithModel:(FC_FollowItemModel *)model{
    
    NSString * imgName = [model.type integerValue] == 1?@"football":@"basketball";
    self.playTypeImg.image = [UIImage imageNamed:imgName];
    
    self.playNameLabel.text = model.play_name;
    [self.userButton setTitle:model.nick_name forState:UIControlStateNormal];
    self.followNumLabel.text = model.follow_count;
    self.yongJinPercentLabel.text = [NSString stringWithFormat:@"%@%%",model.commission];
    self.singleMoney.text = model.money;
    self.totalMoneyLabel.text = model.all_money;
    
    [self.userButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.userButton.imageView.frame.size.width, 0, self.userButton.imageView.frame.size.width + 4)];
    [self.userButton setImageEdgeInsets:UIEdgeInsetsMake(0, self.userButton.titleLabel.frame.size.width + 4, 0,-self.userButton.titleLabel.frame.size.width)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)userButtonAction:(id)sender {
}
@end
