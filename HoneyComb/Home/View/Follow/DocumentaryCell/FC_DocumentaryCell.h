//
//  FC_DocumentaryCell.h
//  HoneyComb
//
//  Created by afc on 2018/12/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FC_FollowItemModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_DocumentaryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *followNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *yongJinPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *singleMoney;
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *userButton;
- (IBAction)userButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *playNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *playTypeImg;

-(void)reloadDocumentaryCellWithModel:(FC_FollowItemModel *)model;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end

NS_ASSUME_NONNULL_END
