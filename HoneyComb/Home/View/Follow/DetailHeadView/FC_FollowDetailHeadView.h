//
//  FC_FollowDetailHeadView.h
//  HoneyComb
//
//  Created by afc on 2018/12/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_FollowDetailHeadView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *playNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *yongJinLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalYongJinLabel;
@property (weak, nonatomic) IBOutlet UILabel *caiMinYongJinLabel;
@property (weak, nonatomic) IBOutlet UILabel *dianZhuYongJinLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalOrderMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *ziGouLabel;
@property (weak, nonatomic) IBOutlet UILabel *danBeiLabel;
@property (weak, nonatomic) IBOutlet UILabel *followNumLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelWidthConstrains;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playNameLabelWidthConstrains;

@end

NS_ASSUME_NONNULL_END
