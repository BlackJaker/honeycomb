//
//  RaceColorViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "RaceColorViewController.h"
#import "RaceColorCell.h"
#import "OutView.h"
#import "OSSImageUploader.h"

#import "FC_PartBillViewController.h"

#import "FC_NumberLottoryDetailPhotoView.h"

#import "AF_OddsNoteView.h"

///弱引用/强引用
#define CCWeakSelf __weak typeof(self) weakSelf = self;
@interface RaceColorViewController ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,UINavigationControllerDelegate,PYPhotosViewDelegate,FC_DetailPhotoViewDelegate>
{
    NSDictionary *mDic;
    CGFloat   detailPhotoHeight;
    CGFloat  noteHeight;
}
@property (nonatomic, weak) PYPhotosView *publishPhotosView;//属性 保存选择的图片
@property(nonatomic,strong)NSMutableArray *photos;
@property (nonatomic,strong) OutView *outView;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIImageView *headerImage;
@property (nonatomic,strong) UILabel *nameLabel;//彩种名称
@property (nonatomic,strong) UILabel *provideLabel;//提供照片
@property (nonatomic,strong) UILabel *issueLabel;//彩种名称
@property (nonatomic,strong) UILabel *priceLabel;//价格
@property (nonatomic,strong) UILabel *payNumber;//胆拖
@property (nonatomic,strong) UILabel *nickNameLabel;//用户名
@property (nonatomic,strong) UILabel *phoneLabel;//手机号码
@property (nonatomic,strong) UILabel *serialNumber;//订单编号
@property (nonatomic,strong) UILabel *dateLabel;//购买时间
@property (nonatomic,strong) UIImageView *photoImage;
@property (nonatomic,strong) UIButton *cancleBtn;
@property (nonatomic,strong) UIButton *outBtn;
@property (nonatomic,strong) NSMutableArray *listArr;
@property (nonatomic,strong) UIButton *doneBtn;//完成
@property (nonatomic,strong) UIButton *editBtn;//编辑
@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;

@property (nonatomic,strong) UIButton * partBillButton;   // 拆单

@property (nonatomic,strong) UIView * tableHeadView;   // head view

@property (nonatomic,strong) FC_NumberLottoryDetailPhotoView * detailPhotoView;   // 底部视图

@property (nonatomic,assign) BOOL isClicking;

@property (nonatomic,strong) AF_OddsNoteView * noteView;

@end

@implementation RaceColorViewController

static NSString *kTempFolder = @"shop/order";

-(NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
    
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    self.titleLabe.text = @"订单详情";
    detailPhotoHeight = 130;
    noteHeight = 40;
    self.isClicking = NO;
    
    [self layoutView];
    [self creatData];
    [self creatToken];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(deleteAction:) name:@"deleteTongzhi" object:nil];
}
-(void)deleteAction:(NSNotification *)dic
{
    self.doneBtn.hidden = YES;
}
-(void)creatToken
{
    
    [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {
//        NSLog(@"--%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
            self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
            self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
        }
        
    } failure:^(NSError *error) {
//        NSLog(@"--%@",error);
    }];
    
    
}
-(void)creatData
{
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId],@"type":self.strType,@"orderTotalId":self.orderId};
    
    NSString * postUrlStr = [self.strType integerValue] < 3?PostAthleticeOrder:[self.strType integerValue] == 7?PostSuccessOrder:PostNineOrder;
    
    [PPNetworkHelper POST:postUrlStr parameters:dic success:^(id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            NSDictionary *listDic = [NSDictionary dictionary];
            mDic = [NSDictionary dictionary];
            listDic = [solveJsonData changeType:[[responseObject objectForKey:@"data"]objectForKey:@"detailList"] ];
            
            self.matchList = responseObject[@"data"][@"detailList"];
            
            mDic = [solveJsonData changeType:[responseObject objectForKey:@"data"]];
            self.listArr = [AllModel mj_objectArrayWithKeyValuesArray:listDic];
            
            [self updateHead];
            
            [self.tableView reloadData];
            
            
        }else
        {
            
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)updateHead{
    
    self.nickNameLabel.text = [NSString stringWithFormat:@"用 户 名 : %@",[mDic valueForKey:@"nickName"]];
    self.phoneLabel.text = [NSString stringWithFormat:@"手 机 号 : %@(长按拨打)",[mDic objectForKey:@"mobile"]];
    if (mDic[@"orderNumber"]) {
        self.serialNumber.text = [NSString stringWithFormat:@"订单编号: %@",[mDic objectForKey:@"orderNumber"]];
    }
    
    self.dateLabel.text = [NSString stringWithFormat:@"购买时间: %@",[mDic objectForKey:@"createDate"]];
    
    if (self.photos.count > 0) {
        self.detailPhotoView.photoView.images = self.photos;
    }else{
        if ([[mDic objectForKey:@"image"] isEqualToString:@""]) {
            self.doneBtn.hidden = YES;
        }else{
            self.editBtn.hidden = YES;
            self.doneBtn.hidden = YES;
            NSString * imageStr = [mDic objectForKey:@"image"];
            if ([imageStr hasSuffix:@","]) {
                imageStr = [imageStr substringToIndex:imageStr.length - 1];
            }
            NSArray *arr = [imageStr componentsSeparatedByString:@","];
            NSMutableArray *arrAy = [NSMutableArray arrayWithArray:arr];
            self.detailPhotoView.photoView.originalUrls = arrAy;    //  加载网络图片
            
        }
    }
}

-(void)layoutView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight-TabBarHeight-NavgationBarHeight) style:(UITableViewStyleGrouped)];
    self.tableView.dataSource =self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = RGB(249, 249, 249, 1);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[RaceColorCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
    
    CGFloat  buttonWidth = [self.strType integerValue] < 3?kWidth/3:kWidth/2;
    
    self.cancleBtn = [[UIButton alloc]init];
    [self.view addSubview:self.cancleBtn];
    [self.cancleBtn setTitleColor:kGrayColor forState:(UIControlStateNormal)];
    [self.cancleBtn setTitle:@"撤单" forState:(UIControlStateNormal)];
    [self.cancleBtn addTarget:self action:@selector(cancleAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.cancleBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    self.cancleBtn.backgroundColor = [UIColor whiteColor];
    [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(buttonWidth);
        make.height.mas_equalTo(kWidth/7.653);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(kHeight-NavgationBarHeight-TabBarHeight);
    }];
    
    self.outBtn = [[UIButton alloc]init];
    [self.view addSubview:self.outBtn];
    [self.outBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.outBtn setTitle:@"出票" forState:(UIControlStateNormal)];
    self.outBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [self.outBtn addTarget:self action:@selector(outAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.outBtn.backgroundColor = kRedColor;
    [self.outBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(buttonWidth);
        make.height.mas_equalTo(kWidth/7.653);
        make.left.mas_equalTo(self.partBillButton.mas_right);
        make.top.mas_equalTo(kHeight-NavgationBarHeight-TabBarHeight);
    }];
}

-(UIButton *)partBillButton{
    
    if (!_partBillButton) {
        
        _partBillButton = [[UIButton alloc]init];
        [self.view addSubview:_partBillButton];
        
        [_partBillButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [_partBillButton setTitle:@"拆单" forState:(UIControlStateNormal)];
        _partBillButton.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        [_partBillButton addTarget:self action:@selector(partBillAction) forControlEvents:(UIControlEventTouchUpInside)];
        [_partBillButton setBackGroundColor:[UIColor colorWithHexString:@"#999999"] controlState:UIControlStateNormal];
        
        CGFloat  partBillButtonWidth = [self.strType integerValue] < 3?kWidth/3:0;
        
        [_partBillButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(partBillButtonWidth);
            make.height.mas_equalTo(kWidth/7.653);
            make.left.mas_equalTo(self.cancleBtn.mas_right);
            make.top.mas_equalTo(kHeight-NavgationBarHeight-TabBarHeight);
        }];
        
    }
    return _partBillButton;
}

-(void)partBillAction{
    
    FC_PartBillViewController * partBillVc = [[FC_PartBillViewController alloc]initWithOrderTotalId:self.orderId];
    [self.navigationController pushViewController:partBillVc animated:YES];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CGFloat viewHeight = kWidth/2.7173 + noteHeight;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, viewHeight)];
    view.backgroundColor = [UIColor whiteColor];
    
    _noteView = [[AF_OddsNoteView alloc]init];
    _noteView.backgroundColor = [UIColor colorWithHexString:@"#FFEEEC"];
    [view addSubview:_noteView];
    
    [_noteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(noteHeight);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        
    }];
    
    self.headerImage = [[UIImageView alloc]initWithFrame:CGRectMake(kWidth/31.25, kWidth/16.3 + noteHeight, kWidth/9.375, kWidth/9.375)];
    if ([self.strType isEqualToString:@"1"]) {
        self.headerImage.image = [UIImage imageNamed:@"football"];
    }else if ([self.strType isEqualToString:@"2"]){
        self.headerImage.image = [UIImage imageNamed:@"basketball"];
    }else if ([self.strType isEqualToString:@"3"]){
        self.headerImage.image = [UIImage imageNamed:@"daletou"];
    }else if ([self.strType isEqualToString:@"4"]){
        self.headerImage.image = [UIImage imageNamed:@"pailiesan"];
    }else if ([self.strType isEqualToString:@"5"]){
        self.headerImage.image = [UIImage imageNamed:@"pailiewu"];
    }else if ([self.strType isEqualToString:@"6"]){
        self.headerImage.image = [UIImage imageNamed:@"qixingcai"];
    }else if ([self.strType isEqualToString:@"7"]){
        self.headerImage.image = [UIImage imageNamed:@"win_lose"];
    }else{
        self.headerImage.image = [UIImage imageNamed:@"any_nine"];
    }
    [view addSubview:self.headerImage];
    
    NSInteger  currentType = [self.strType integerValue];
    CGFloat  height = 30,top = currentType >= 3?20 + noteHeight:(viewHeight - height - noteHeight)/4 + noteHeight;
    
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.textAlignment = NSTextAlignmentLeft;
    self.nameLabel.textColor = kBlackColor;
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
    [view addSubview:self.nameLabel];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.left.mas_equalTo(CGRectGetMaxX(self.headerImage.frame)+kWidth/25);
        make.size.mas_equalTo(CGSizeMake(kWidth/3.75, height));
        
    }];
    
    self.provideLabel = [[UILabel alloc]init];
    self.provideLabel.text = @"|   提供照片";
    self.provideLabel.textAlignment = NSTextAlignmentLeft;
    self.provideLabel.textColor = kRedColor;
    self.provideLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
    [view addSubview:self.provideLabel];
    
    [self.provideLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.left.mas_equalTo(self.nameLabel.mas_right).offset(kWidth/37.5);
        make.size.mas_equalTo(CGSizeMake(kWidth/4.4117, height));
        
    }];
    
    CGFloat  issueWidth = 200;
    
    self.issueLabel = [[UILabel alloc]init];
    self.issueLabel.textAlignment = NSTextAlignmentLeft;
    self.issueLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    self.issueLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    [view addSubview:self.issueLabel];
    
    [self.issueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom);
        make.left.mas_equalTo(CGRectGetMaxX(self.headerImage.frame)+kWidth/25);
        make.size.mas_equalTo(CGSizeMake(issueWidth, height));
        
    }];
    
    UIView *grayView = [[UIView alloc]initWithFrame:CGRectMake(0, kWidth/4.518 + noteHeight, kWidth, kWidth/37.5)];
    grayView.backgroundColor = RGB(249, 249, 249, 1);
    [view addSubview:grayView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/31.25, CGRectGetMaxY(grayView.frame), 80, kWidth/8.3333)];
    label.text = @"投注号码";
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = kBlackColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.785];
    [view addSubview:label];
    
    self.priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(kWidth/31.25 + 80, CGRectGetMaxY(grayView.frame), kWidth-kWidth/31.25 * 2 - 80, kWidth/8.3333)];
    self.priceLabel.textColor = kGrayColor;
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    self.priceLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
    [view addSubview:self.priceLabel];
    
    if (mDic) {
        self.nameLabel.text = [mDic objectForKey:@"playName"];
        if (self.nameLabel.text.length > 0) {
            CGFloat  width = [self labelWidthWithString:self.nameLabel.text font:[UIFont fontWithName:kPingFangRegular size:kWidth/23.4375] height:height];
            [self.nameLabel updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(width);
            }];
        }
        if (mDic && mDic[@"issue"] && ![mDic[@"issue"] isEqualToString:@"-"] && [mDic[@"issue"] length] > 0) {
            self.issueLabel.text = [NSString stringWithFormat:@"第%@期",mDic[@"issue"]];
        }
        if ([[mDic objectForKey:@"isImage"] isEqualToString:@"1"]) {
            self.provideLabel.hidden = NO;
        }else{
            self.provideLabel.hidden = YES;
        }
        
        NSString *bunthStr = @"" ;
        if ([[mDic objectForKey:@"bunch"] isEqualToString:@"1"]) {
            
            if ([self.strType integerValue] < 3) {
                bunthStr = @"单关 ";
            }
            NSString *account = [NSString stringWithFormat:@"%@%@ 注 ",bunthStr,[mDic objectForKey:@"account"]];
            NSString *times = [NSString stringWithFormat:@"%@ 倍 ",[mDic objectForKey:@"times"]];
            NSString *price = [NSString stringWithFormat:@"%@元",[mDic objectForKey:@"price"]];
            NSString *str = [NSString stringWithFormat:@"%@%@%@",account,times,price];
            
            NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:str];
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(0, account.length)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(0, account.length)];
            
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 153, 153, 1)} range:NSMakeRange(account.length-2, 1)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length-2, 1)];
            
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(account.length, times.length-2)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length, times.length-2)];
            //
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(account.length+3+times.length+1, 1)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+3+times.length+1, 1)];
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(account.length+times.length, price.length)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+times.length, price.length)];
            
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(account.length+times.length+price.length-1,1 )];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(account.length+times.length+price.length-1, 1)];
            self.priceLabel.attributedText = rushAttributed;
            
        }else
        {
            if ([self.strType integerValue] < 3) {
                bunthStr = [NSString stringWithFormat:@"%@串1 ",[mDic objectForKey:@"bunch"]];
            }
            bunthStr = [NSString stringWithFormat:@"%@%@ 注 ",bunthStr,[mDic objectForKey:@"account"]];
            
            NSString *times = [NSString stringWithFormat:@"%@ 倍 ",[mDic objectForKey:@"times"]];
            NSString *price = [NSString stringWithFormat:@"%@元",[mDic objectForKey:@"price"]];
            NSString *str = [NSString stringWithFormat:@"%@%@%@",bunthStr,times,price];
            
            NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:str];
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(0, bunthStr.length-3)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(0, bunthStr.length-3)];
            
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 153, 153, 1)} range:NSMakeRange(bunthStr.length-2, 1)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunthStr.length-2, 1)];
            
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kBlackColor} range:NSMakeRange(bunthStr.length, times.length-3)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunthStr.length, times.length-3)];
            //
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(bunthStr.length+times.length-2, 1)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunthStr.length+times.length-2, 1)];
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kRedColor} range:NSMakeRange(bunthStr.length+times.length, price.length-1)];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(bunthStr.length+times.length, price.length-1)];
            
            [rushAttributed setAttributes:@{NSForegroundColorAttributeName:kGrayColor} range:NSMakeRange(str.length-1,1 )];
            [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kWidth/26.7857] range:NSMakeRange(str.length-1, 1)];
            self.priceLabel.attributedText = rushAttributed;
            
        }
    }

    return view;
}

- (CGFloat)labelWidthWithString:(NSString *)resultString font:(UIFont *)font height:(CGFloat)height{
    
    CGSize maxSize = CGSizeMake(MAXFLOAT, height);
    
    // 计算内容label的高度
    
    CGFloat width = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :font} context:nil].size.width;
    return width + 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * view = self.tableHeadView;

    return view;
}


-(UIView *)tableHeadView{
    
    if (!_tableHeadView) {
        
        CGFloat  orderLabelHeight = 40,itemHeight = 25;
        
        _tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 280)];
        _tableHeadView.backgroundColor = [UIColor whiteColor];
        
        UILabel *orderLabel = [[UILabel alloc]init];
        orderLabel.textAlignment = NSTextAlignmentLeft;
        orderLabel.text = @"订单信息";
        orderLabel.textColor = kBlackColor;
        orderLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/23.4375];
        [_tableHeadView addSubview:orderLabel];
        
        [orderLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.top.mas_equalTo(0);
            make.height.mas_equalTo(orderLabelHeight);
        }];

        
        self.nickNameLabel = [[UILabel alloc]init];
        self.nickNameLabel.textColor = kGrayColor;
        self.nickNameLabel.text = @"-";
        self.nickNameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        self.nickNameLabel.userInteractionEnabled = YES;
        [_tableHeadView addSubview:self.nickNameLabel];
        
        [self.nickNameLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.top.mas_equalTo(orderLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        self.phoneLabel = [[UILabel alloc]init];
        self.phoneLabel.textColor = kGrayColor;
        self.phoneLabel.text = @"-";
        self.phoneLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        self.phoneLabel.userInteractionEnabled = YES;
        [_tableHeadView addSubview:self.phoneLabel];
        
        [self.phoneLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.top.mas_equalTo(self.nickNameLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        // 手势拨打电话
        UILongPressGestureRecognizer * longGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(raceColorVCCallAction:)];
        [self.phoneLabel addGestureRecognizer:longGesture];
        
        self.serialNumber = [[UILabel alloc]init];
        self.serialNumber.textColor = kGrayColor;
        self.serialNumber.text = @"-";
        self.serialNumber.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        [_tableHeadView addSubview:self.serialNumber];
        
        [self.serialNumber makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.top.mas_equalTo(self.phoneLabel.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];

        
        self.dateLabel = [[UILabel alloc]init];
        self.dateLabel.textColor = kGrayColor;
        self.dateLabel.text = @"-";
        self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.7857];
        [_tableHeadView addSubview:self.dateLabel];
        
        [self.dateLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLeftMar);
            make.right.mas_equalTo(-kLeftMar);
            make.top.mas_equalTo(self.serialNumber.mas_bottom);
            make.height.mas_equalTo(itemHeight);
        }];
        
        CGFloat  grayViewHeight = 10;
        
        UIView *grayView = [[UIView alloc]init];
        grayView.backgroundColor = RGB(249, 249, 249, 1);
        [_tableHeadView addSubview:grayView];
        
        [grayView makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(self.dateLabel.mas_bottom);
            make.height.mas_equalTo(grayViewHeight);
        }];
        
        [_tableHeadView addSubview:self.detailPhotoView];
        
    }
    return _tableHeadView;
}

-(FC_NumberLottoryDetailPhotoView *)detailPhotoView{
    
    if (!_detailPhotoView) {
        _detailPhotoView = [[FC_NumberLottoryDetailPhotoView alloc]init];
        _detailPhotoView.delegate = self;
        _detailPhotoView.photoView.delegate = self;
        [self.tableHeadView addSubview:_detailPhotoView];

        [_detailPhotoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kWidth, detailPhotoHeight));
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
    }
    return _detailPhotoView;
}

// 拨打电话
-(void)raceColorVCCallAction:(UILongPressGestureRecognizer *)longGesture{
    
    // 长按手势state began时响应一次其余都是state end
    if (longGesture.state == UIGestureRecognizerStateBegan) {
        NSLog(@"long pressTap state :begin");
        
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",[mDic objectForKey:@"mobile"]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }else {
        NSLog(@"long pressTap state :end");
    } 
}

#pragma mark  -- detail photo view delegate  --

-(void)done{
    
    CGFloat  leftMar = 12,topMar = 40;
    
    self.detailPhotoView.photoView.py_x = leftMar;
    self.detailPhotoView.photoView.py_y = topMar;
    
    NSArray *arrAll = self.photos;
    NSLog(@"-%@",arrAll);
    
    [OSSImageUploader asyncUploadImages:arrAll folder:kTempFolder accessKey:self.accessKey secretKey:self.accessKeySecret token:self.tokenStr complete:^(NSArray<NSString *> *names, UploadImageState state) {
        NSLog(@"彩票照片-%@",names);
        
        NSMutableArray *arraY = [NSMutableArray array];
        for (NSString *str in names) {
            [arraY addObject:[NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",str]];
        }
        NSString *string =[arraY componentsJoinedByString:@","];
        NSLog(@"转换-%@",string);
        NSDictionary *dic = @{@"img":string,@"orderTotalId":self.orderId};
        [PPNetworkHelper POST:PostSetOrderImage parameters:dic success:^(id responseObject) {
            NSLog(@"%@",responseObject);
        } failure:^(NSError *error) {
            
        }];
        
    }];
    self.detailPhotoView.photoView.imagesMaxCountWhenWillCompose = self.photos.count;
    NSDictionary *dic = @{@"hiden":@"1"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.detailPhotoView.doneButton.hidden = YES;
    self.detailPhotoView.editButton.hidden =NO;
    
}

-(void)edit{
    
    CGFloat  leftMar = 12,topMar = 40;
    
    self.detailPhotoView.photoView.py_x = leftMar;
    self.detailPhotoView.photoView.py_y = topMar;
    self.detailPhotoView.photoView.imagesMaxCountWhenWillCompose = 3;
    
    NSDictionary *dic = @{@"hiden":@"2"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.detailPhotoView.doneButton.hidden = NO;
    self.detailPhotoView.editButton.hidden = YES;
    
}

#pragma mark --- PYPhotosViewDelegate  ---

- (void)photosView:(PYPhotosView *)photosView didAddImageClickedWithImages:(NSMutableArray *)images{
    // 在这里做当点击添加图片按钮时，你想做的事。
    
    [self getPhotos];
}
// 进入预览图片时调用, 可以在此获得预览控制器，实现对导航栏的自定义
- (void)photosView:(PYPhotosView *)photosView didPreviewImagesWithPreviewControlelr:(PYPhotosPreviewController *)previewControlelr{
    NSLog(@"进入预览图片");
}
//进入相册的方法:
-(void)getPhotos{
    CCWeakSelf;
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3-weakSelf.photos.count delegate:weakSelf];
    imagePickerVc.maxImagesCount = 3;//最大照片张数,默认是0
    imagePickerVc.sortAscendingByModificationDate = NO;// 对照片排序，按修改时间升序，默认是YES。如果设置为NO,最新的照片会显示在最前面，内部的拍照按钮会排在第一个
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets,BOOL isSelectOriginalPhoto){
        NSLog(@"选中图片photos === %@",photos);
        //        for (UIImage *image in photos) {
        //            [weakSelf requestData:image];//requestData:图片上传方法 在这里就不贴出来了
        //        }
        [weakSelf.photos addObjectsFromArray:photos];
        if (weakSelf.photos.count == 0) {
            self.detailPhotoView.doneButton.hidden = YES;
        }else
        {
            self.detailPhotoView.doneButton.hidden = NO;
        }
        NSLog(@"%ld",weakSelf.photos.count);
        [self.detailPhotoView.photoView reloadDataWithImages:weakSelf.photos];
    }];
    [weakSelf presentViewController:imagePickerVc animated:YES completion:nil];
}

-(NSMutableArray *)photos{
    if (_photos == nil) {
        _photos = [[NSMutableArray alloc]init];
    }
    return _photos;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RaceColorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[RaceColorCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    cell.model = self.listArr[indexPath.row];
    cell.backgroundColor = RGB(249, 249, 249, 1);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllModel * model = [self.listArr objectAtIndex:indexPath.row];
    
    if (![model.content isEqualToString:@"-"]) {
        NSString * resultStr = @"";
        NSArray * array = [model.content componentsSeparatedByString:@","];
        for (int i = 0; i < array.count; i ++) {
            NSString * string = [array objectAtIndex:i];
            if (i == 0) {
                resultStr = string;
            }else{
                NSString * str = i%2 == 0?@"\n":@" , ";
                resultStr = [resultStr stringByAppendingString:[NSString stringWithFormat:@"%@%@",str,string]];
            }
        }
        
        CGFloat  labelHeight = [self labelHeightWithString:resultStr];

        CGFloat  contentHeight = kWidth/9.375;

        if (labelHeight > contentHeight) {
            return kWidth/3.9 + labelHeight - contentHeight;
        }
        
        return kWidth/3.9;
    }
    
    return kWidth/3.9;
}

- (CGFloat)labelHeightWithString:(NSString *)resultString{
    
    CGSize maxSize = CGSizeMake(kWidth/1.720, MAXFLOAT);

    // 计算内容label的高度
    CGFloat textH = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont fontWithName:kPingFangRegular size:13]} context:nil].size.height;
    return textH + 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kWidth/2.7173 + noteHeight;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 280;
}
#pragma mark 出票
-(void)outAction
{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        NSMutableDictionary * dic = [NSMutableDictionary new];
        [dic setValue:self.strType forKey:@"type"];
        [dic setValue:self.orderId forKey:@"orderTotalId"];
        if ([self.strType integerValue] < 3) {
            [dic setValue:self.matchList forKey:@"matchList"];
        }
        
        @WeakObj(self)
        [STTextHudTool loadingWithTitle:@"出票中..."];
        [PPNetworkHelper POST:PostPrintOrder parameters:dic success:^(id responseObject) {
            
            NSLog(@"%@",responseObject);
            @StrongObj(self)
            [STTextHudTool hideSTHud];
            
            if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                if (self.NextViewControllerBlock) {
                    self.NextViewControllerBlock(1);
                }
                [STTextHudTool showText:@"出票成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
            self.isClicking = NO;
        } failure:^(NSError *error) {
            @StrongObj(self)
            [STTextHudTool hideSTHud];
            [STTextHudTool showText:@"出票失败"];
            self.isClicking = NO;
        }];
        
    }

}
#pragma mark 撤单
-(void)cancleAction
{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        NSDictionary *dic = @{
                              @"orderTotalId":self.orderId
                              };
        @WeakObj(self)
        
        [PPNetworkHelper POST:PostCancleOrder
                   parameters:dic
                      success:^(id responseObject) {
                          
                          @StrongObj(self)
                          
                          if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                              if (self.NextViewControllerBlock) {
                                  self.NextViewControllerBlock(1);
                              }
                              [STTextHudTool showText:@"订单撤销成功"];
                              [self.navigationController popViewControllerAnimated:YES];
                          }else
                          {
                              [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
                          }
                          self.isClicking = NO;
                      } failure:^(NSError *error) {
                          @StrongObj(self)
                          [STTextHudTool showText:@"订单撤销失败"];
                          self.isClicking = NO;
                      }];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
