//
//  SponsorBuyDetailViewController.h
//  HoneyComb
//
//  Created by afc on 2018/12/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

typedef void(^OutTicketSuccessBlock)(void);

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SponsorBuyDetailControllerType) {
    SponsorBuyDetailControllerTypeJoin = 1000,
    SponsorBuyDetailControllerTypeAll,
};

@interface SponsorBuyDetailViewController : AllViewController

@property (nonatomic, strong) UIImage * playImg;
@property (nonatomic, copy) NSString *playType;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) SponsorBuyDetailControllerType type;

@property (nonatomic, copy) OutTicketSuccessBlock outTicketSuccessBlock;

@end

NS_ASSUME_NONNULL_END
