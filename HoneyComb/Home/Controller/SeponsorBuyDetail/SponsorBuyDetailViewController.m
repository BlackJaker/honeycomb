//
//  SponsorBuyDetailViewController.m
//  HoneyComb
//
//  Created by afc on 2018/12/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "SponsorBuyDetailViewController.h"

#import "SponsorBuyUserInfoCell.h"
#import "SponsorBuySchemeDetailCell.h"
#import "SponsorSchemeCell.h"
#import "SponsorBuyFriendCell.h"
#import "SponsorBuyLotteryPictureCell.h"
#import "SponsorBuyDetailModel.h"
#import "UWHttpTool.h"
#import "MinePlanModel.h"

#import "NSAttributedString+TRCreat.h"

#import "FC_PartBillViewController.h"

#import "FC_CurrentBuyListViewController.h"

#import "FC_NumberLottoryDetailPhotoView.h"

#import "OSSImageUploader.h"

#import "CountDown.h"

@interface SponsorBuyDetailViewController ()<UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,FC_DetailPhotoViewDelegate,PYPhotosViewDelegate,TZImagePickerControllerDelegate>
{
    CGFloat  footerHeight;
    
    //截止时间
    NSString *_endTime;
    NSString *_startTime;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UITextField *fenshu;
@property (nonatomic, strong) UILabel *shengyv;
@property (nonatomic, strong) UILabel *yirengou;
@property (nonatomic, strong) NSMutableArray <UIButton *> *changeNumerBtns;
@property (nonatomic, strong) SponsorBuyDetailModel *detailModel;
@property (nonatomic, strong) NSArray *detailList;

@property (nonatomic, strong) FC_NumberLottoryDetailPhotoView * detailPhotoView;

@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;

@property(nonatomic,strong)NSMutableArray *photos;
@property(nonatomic,strong)NSArray *matchList;

@property(nonatomic,copy)NSString * times;

@property(nonatomic,strong)CountDown *countDown;

@property(nonatomic,strong)NSDictionary *dict;

@property(nonatomic,assign)BOOL  isClicking;

@end

static NSString *SponsorBuyUserInfoCellID = @"SponsorBuyUserInfoCellID";
static NSString *SponsorBuySchemeDetailCellID = @"SponsorBuySchemeDetailCellID";

static NSString *DaLeTouSponsorSchemeCellID = @"DaLeTouSponsorSchemeCellID";
static NSString *OtherSponsorSchemeCellID = @"OtherSponsorSchemeCellID";
static NSString *SponsorBuyFriendCellID = @"SponsorBuyFriendCellID";

static NSString *SponsorBuyLotteryPictureCellID = @"SponsorBuyLotteryPictureCellID";

static NSString *kTempFolder = @"shop/order";

@implementation SponsorBuyDetailViewController

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    footerHeight = IPHONE_X?TAB_BAR_HEIGHT:60;
    
    self.times = nil;
    _startTime = @"";
    _endTime = @"";
    
    self.isClicking = NO;
    
    [self buildNav];
    [self buildUI];
    
    [self getDetailAllInfo];

}

- (void)buildNav{
    
    self.titleLabe.text = @"方案详情";
    
    UIButton * rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10,15, 16)];
//    [rightBtn setImage:[UIImage imageNamed:@"分类图标"] forState:(UIControlStateNormal)];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(tongzhiBetting:) name:@"bettingTongzhi" object:nil];
    
    [rightBtn addTarget:self action:@selector(rightAction) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *le = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = le;
}

-(void)tongzhiBetting:(NSNotification *)noti{
    
    
}

-(void)rightAction{
    
    
    
}

- (void)buildUI{
    
    self.tableView = ({
        
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.tableFooterView = [UIView new];
        
        [tableView registerClass:[SponsorBuySchemeDetailCell class]
          forCellReuseIdentifier:SponsorBuySchemeDetailCellID];
        [tableView registerClass:[DaLeTouSponsorSchemeCell class]
          forCellReuseIdentifier:DaLeTouSponsorSchemeCellID];
        [tableView registerClass:[OtherSponsorSchemeCell class]
          forCellReuseIdentifier:OtherSponsorSchemeCellID];
        [tableView registerClass:[SponsorBuyLotteryPictureCell class]
          forCellReuseIdentifier:SponsorBuyLotteryPictureCellID];
        [self.view addSubview:tableView];
        tableView;
    });

        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@(0));
            make.left.right.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
            make.bottom.equalTo(self.view.mas_bottom).offset(kDEVICE_SCALE_FACTOR(-footerHeight));
        }];
        
        [self buildFooter];
}

- (void)buildFooter{
    
    UIView *footer = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:footer];
    
    [footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
        make.right.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
        make.height.equalTo(@(kDEVICE_SCALE_FACTOR(footerHeight)));
        make.bottom.equalTo(@(0));
    }];

    NSArray * titles = [self.playType integerValue] < 3 ?@[@"拆单",@"出票"]:@[@"出票"];
    CGFloat  mar = 14,centerMar = [self.playType integerValue] < 3?12:0,width = (kWidth - mar * 2 - centerMar)/titles.count,height = 40,top = 10;

    NSArray * colors = @[@[(__bridge id)[UIColor colorWithRed:237/255.0 green:171/255.0 blue:64/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:238/255.0 green:191/255.0 blue:4/255.0 alpha:1.0].CGColor],
                         @[(__bridge id)[UIColor colorWithRed:243/255.0 green:56/255.0 blue:31/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:251/255.0 green:48/255.0 blue:36/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:234/255.0 green:29/255.0 blue:73/255.0 alpha:1.0].CGColor]];
    
    for (int i = 0; i < titles.count; i ++) {
        UIButton * button =  [[UIButton alloc]init];
        button.tag = 1234 + i;
        [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
        NSArray * colorArr = [self.playType integerValue]<3?[colors objectAtIndex:i]:[colors lastObject];
        [self updateButtonWithColors:colorArr button:button frame:CGRectMake(0, 0, width, height)];
        [footer addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(mar + (width + centerMar)*i);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(top);
        }];
    }

}

-(void)updateButtonWithColors:(NSArray *)colors button:(UIButton *)button frame:(CGRect)frame{
    
    button.layer.cornerRadius = 5;
    button.layer.masksToBounds = YES;
    
    CAGradientLayer *gradientLayer =  [CAGradientLayer layer];
    gradientLayer.frame = frame;
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1, 1);
    if (colors.count == 2) {
        gradientLayer.locations = @[@(0.0),@(1.0)];//渐变点
    }else{
        gradientLayer.locations = @[@(0.0),@(0.3),@(1.0)];//渐变点
    }
    
    [gradientLayer setColors:colors];//渐变数组
    [button.layer addSublayer:gradientLayer];
    
    button.titleLabel.font = [UIFont fontWithName:kBold size:17];
    [button setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    
    [button addTarget:self action:@selector(bottomButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)bottomButtonAction:(UIButton *)button{
    
    if ([self.playType integerValue] < 3) {
        if (button.tag == 1234) {
            if (self.detailModel.order_total_id) {
                FC_PartBillViewController * partBillVc = [[FC_PartBillViewController alloc]initWithOrderTotalId:self.detailModel.order_total_id];
                [self.navigationController pushViewController:partBillVc animated:YES];
            }
            
        }else{
            [self outTicketAction];
        }
    }else{
        [self outTicketAction];
    }
}
#pragma mark UITableViewDelegate,UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        SponsorBuySchemeDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:SponsorBuySchemeDetailCellID];
        cell.icon.image = self.playImg;
        CGFloat  playNameWidth = [self labelWidthWithString:self.detailModel.play_name font:[UIFont boldSystemFontOfSize:16] height:cell.namelab.height];
        [cell.namelab updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(playNameWidth);
        }];
        cell.namelab.text = self.detailModel.play_name;
        cell.issueLabel.text = @"";
        if ([self.playType integerValue] > 2) {
            cell.issueLabel.text = [NSString stringWithFormat:@"期号: %@",self.detailModel.issue];
        }
        
        _endTime = [self.countDown activeCountDownAction:_startTime surplusTime:^(NSString *surplusTime) {
            cell.timelab.text = [NSString stringWithFormat:@"截止时间 : %@",surplusTime];
        }];
        
        cell.aborttimelab.text = @"到期";
        
        cell.scheduleView.havePercent = [self.detailModel.yigou floatValue];
        cell.scheduleView.baodiPercent = [self.detailModel.bao floatValue];
        [cell.userNumButton addTarget:self action:@selector(showMoreUserAction) forControlEvents:UIControlEventTouchUpInside];
        
        NSArray *numbers = [NSArray arrayWithObjects:self.detailModel.price,self.detailModel.unitPrice,self.detailModel.people,self.detailModel.shengyu, nil];
        
        NSArray *deses = @[@"方案总额",@"单份金额",@"参与人数",@"剩余份数"];
        for (int i = 0; i < numbers.count; i++) {
            cell.numberlabs[i].text = numbers[i];
            cell.deslabs[i].text = deses[i];
        }
        cell.ordernumlab.text = [NSString stringWithFormat:@"订单编号：%@",self.detailModel.numberid];
        cell.userNameLabel.text = [NSString stringWithFormat:@"发  起  人：%@",self.detailModel.nick_name];
        cell.userNumLabel.text = [NSString stringWithFormat:@"参与人数：%@人",self.detailModel.people];
        cell.baodijinelab.text = [NSString stringWithFormat:@"保底金额：%@元",self.detailModel.guard_price];
        cell.zhongjiangyongjinlab.text = [NSString stringWithFormat:@"奖金比例：%@%%",@([self.detailModel.commission floatValue]*100)];
        cell.fanganzhuangtailab.text = [NSString stringWithFormat:@"方案状态：%@",self.detailModel.statusName];
        
        return cell;
    }else if (indexPath.section == 1){
        
        if ([self.playType integerValue] < 3 || [self.playType integerValue] > 6) {
            MinePlanDetailListModel *model = self.detailList[indexPath.row];
            OtherSponsorSchemeCell *cell = [tableView dequeueReusableCellWithIdentifier:OtherSponsorSchemeCellID];
            cell.backView.backgroundColor = indexPath.row%2 == 0 ? COLOR_HEX(0xF0EFF4, 1):COLOR_HEX(0xffffff, 1);
            cell.backView.layer.borderColor = COLOR_HEX(0xF0EFF4, 1).CGColor;
            cell.backView.layer.borderWidth = 1;
           
            NSArray *arr = @[[self attributeWithString:[NSString stringWithFormat:@"%@\n%@\n%@截止",model.number,model.league,model.startTime]],
                             [NSAttributedString attributedStringWithAttributedString:[self attributeWithString:[NSString stringWithFormat:@"%@\nvs\n%@",model.hostTeam,model.visitingTeam]] hightlightString:@"vs" highlightColor:COLOR_HEX(0xE73A59, 1)],
                             [self attributeWithString:model.content.length == 0 ? @"":[model.content stringByReplacingOccurrencesOfString:@"," withString:@"\n"]],
                             [self attributeWithString:model.result]];
            
            for (UILabel *lab in cell.titlelabs) {
                NSInteger index = [cell.titlelabs indexOfObject:lab];
                lab.attributedText = arr[index];
                lab.textAlignment = NSTextAlignmentCenter;
            }
            
            return cell;
        }

        DaLeTouSponsorSchemeCell *cell = [tableView dequeueReusableCellWithIdentifier:DaLeTouSponsorSchemeCellID];
        
        cell.listArr = [[self getListArray] objectAtIndex:indexPath.row];
        
        MineDaLeTouPlanDetailListModel * model = [self.detailList objectAtIndex:indexPath.row];
        
        if (model.redBile.length > 0 || model.blueBile.length > 0) {
            cell.titleLabel.text = @"胆拖投注";
        }else{
            cell.titleLabel.text = @"普通投注";
        }

        return cell;
        
    }else if (indexPath.section ==2){
        SponsorBuyLotteryPictureCell * cell = [tableView dequeueReusableCellWithIdentifier:SponsorBuyLotteryPictureCellID];
        cell.imag1.backgroundColor = [UIColor groupTableViewBackgroundColor];
        cell.imag2.backgroundColor = [UIColor groupTableViewBackgroundColor];
        cell.imag3.backgroundColor = [UIColor groupTableViewBackgroundColor];
        return cell;
    }
    SponsorBuyUserInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:SponsorBuyUserInfoCellID];
    return cell;
}

- (CGFloat)labelWidthWithString:(NSString *)resultString font:(UIFont *)font height:(CGFloat)height{
    
    CGSize maxSize = CGSizeMake(MAXFLOAT, height);
    
    // 计算内容label的高度
    
    CGFloat width = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :font} context:nil].size.width;
    return width + 5;
}

-(NSMutableArray *)getListArray{
    
    NSMutableArray * list = [NSMutableArray new];
    
    for (MineDaLeTouPlanDetailListModel * model in self.detailList) {
        
         NSMutableArray * array = [NSMutableArray new];
        
        if (model.redBile.length > 0) {
            
            NSString * redBile = [self checkString:model.redBile];
            
            NSArray * redBileArr = [redBile componentsSeparatedByString:@","];
           
            for (NSString * ball in redBileArr) {
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                [dic setValue:@(0) forKey:@"type"];
                [dic setValue:ball forKey:@"ball"];
                [array addObject:dic];
            }
        }
        if (model.red.length > 0) {
            
            NSString * red = [self checkString:model.red];
            
            NSArray * redArr = [red componentsSeparatedByString:@","];
            for (NSString * ball in redArr) {
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                [dic setValue:@(1) forKey:@"type"];
                [dic setValue:ball forKey:@"ball"];
                [array addObject:dic];
            }
        }
        if (model.blueBile.length > 0) {
            
            NSString * blueBile = [self checkString:model.blueBile];
            
            NSArray * blueBileArr = [blueBile componentsSeparatedByString:@","];
            
            for (NSString * ball in blueBileArr) {
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                [dic setValue:@(2) forKey:@"type"];
                [dic setValue:ball forKey:@"ball"];
                [array addObject:dic];
            }
        }
        if (model.blue.length > 0) {
            
            NSString * blue = [self checkString:model.blue];
            
            NSArray * blueArr = [blue componentsSeparatedByString:@","];

            for (NSString * ball in blueArr) {
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                [dic setValue:@(3) forKey:@"type"];
                [dic setValue:ball forKey:@"ball"];
                [array addObject:dic];
            }
        }
        [list addObject:array];
    }
    return list;
}

-(NSString *)checkString:(NSString *)string{
    
    if ([string hasPrefix:@","]) {
        
        string = [string substringFromIndex:1];
        
    }
    
    //字符串末尾有某字符串；
    
    if ([string hasSuffix:@","]) {
        
        string = [string substringToIndex:string.length - 1];
        
    }
    return string;
    
}

// 更多参与人数
-(void)showMoreUserAction{
    self.hidesBottomBarWhenPushed = YES;
    FC_CurrentBuyListViewController * listVc = [[FC_CurrentBuyListViewController alloc]init];
    listVc.detailModel = self.detailModel;
    [self.navigationController pushViewController:listVc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

- (NSMutableAttributedString *)attributeWithString:(NSString *)str{
    if (!str) {
        return nil;
    }
    NSMutableAttributedString *astr = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 5;
    style.alignment = NSTextAlignmentCenter;
    [astr addAttributes:@{NSParagraphStyleAttributeName:style} range:NSMakeRange(0, str.length)];
    return astr;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {   //方案详情
        return kDEVICE_SCALE_FACTOR(480);
    }else if (indexPath.section == 1){  //方案内容
        
        if ([self.playType integerValue] < 3 || [self.playType integerValue] > 6) {
            MinePlanDetailListModel * model = [self.detailList objectAtIndex:indexPath.row];
            return [self getCurrentContentHeightWithItemModel:model];
        }
        NSMutableArray * array = [[self getListArray] objectAtIndex:indexPath.section - 1];
        
        NSInteger  lineCount = 7,itemHeight = 25,centerMar = 10;
        
        NSInteger num = array.count%lineCount!=0?(array.count/lineCount + 1):array.count/lineCount;
        
        CGFloat  height = num * (itemHeight + centerMar);
        
        return height;
        
    }else if (indexPath.section == 2){  //彩票照片
        return kDEVICE_SCALE_FACTOR(0);
    }
    return UITableViewAutomaticDimension;
}

-(CGFloat)getCurrentContentHeightWithItemModel:(MinePlanDetailListModel *)model{
    
    CGFloat  normalHeight = 65,itemHeight = 18;

    NSArray * contentArr = [model.content componentsSeparatedByString:@","];

    if (contentArr.count > 3) {
        normalHeight += (contentArr.count - 3) * itemHeight;
    }

    return normalHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return self.detailList.count;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (section == 1) {
        UIView *view = [UIView viewWithBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        view.frame = CGRectMake(0, 0, kWidth, kDEVICE_SCALE_FACTOR(40)+10);
        UILabel *lab = [UILabel lableWithText:@"赔率以实际出票为准"
                                         font:[UIFont systemFontOfSize:14]
                                    textColor:COLOR_HEX(0x7F8491, 1)];
        lab.backgroundColor = [UIColor whiteColor];
        lab.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
            make.height.equalTo(@(kDEVICE_SCALE_FACTOR(40)));
        }];
        return view;
    }
    if (section == 2) {
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 130)];
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:self.detailPhotoView];
        
        [_detailPhotoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kWidth, 130));
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
        return  view;
    }
    
    return [UIView viewWithBackgroundColor:[UIColor groupTableViewBackgroundColor]];
}

-(FC_NumberLottoryDetailPhotoView *)detailPhotoView{
    
    if (!_detailPhotoView) {
        _detailPhotoView = [[FC_NumberLottoryDetailPhotoView alloc]init];
        _detailPhotoView.delegate = self;
        _detailPhotoView.photoView.delegate = self;
        
    }
    return _detailPhotoView;
}

#pragma mark  -- detail photo view delegate  --

-(void)done{
    
    [self creatToken];

}

-(void)creatToken
{
    [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {
        //        NSLog(@"--%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
            self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
            self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
            
            CGFloat  leftMar = 12,topMar = 40;
            
            self.detailPhotoView.photoView.py_x = leftMar;
            self.detailPhotoView.photoView.py_y = topMar;
            
            NSArray *arrAll = self.photos;
            NSLog(@"-%@",arrAll);
            
            [OSSImageUploader asyncUploadImages:arrAll folder:kTempFolder accessKey:self.accessKey secretKey:self.accessKeySecret token:self.tokenStr complete:^(NSArray<NSString *> *names, UploadImageState state) {
                NSLog(@"彩票照片-%@",names);
                
                NSMutableArray *arraY = [NSMutableArray array];
                for (NSString *str in names) {
                    [arraY addObject:[NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",str]];
                }
                NSString *string =[arraY componentsJoinedByString:@","];
                NSLog(@"转换-%@",string);
                NSDictionary *dic = @{@"img":string,@"orderTotalId":self.detailModel.order_total_id};
                [PPNetworkHelper POST:PostSetOrderImage parameters:dic success:^(id responseObject) {
                    NSLog(@"%@",responseObject);
                } failure:^(NSError *error) {
                    
                }];
                
            }];
            self.detailPhotoView.photoView.imagesMaxCountWhenWillCompose = self.photos.count;
            NSDictionary *dic = @{@"hiden":@"1"};
            //创建通知
            NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
            //通过通知中心发送通知
            [[NSNotificationCenter defaultCenter] postNotification:notification];
            self.detailPhotoView.doneButton.hidden = YES;
            self.detailPhotoView.editButton.hidden =NO;
            
        }
        
    } failure:^(NSError *error) {
        //        NSLog(@"--%@",error);
    }];
    
    
}

-(void)edit{
    
    CGFloat  leftMar = 12,topMar = 40;
    
    self.detailPhotoView.photoView.py_x = leftMar;
    self.detailPhotoView.photoView.py_y = topMar;
    self.detailPhotoView.photoView.imagesMaxCountWhenWillCompose = 3;
    
    NSDictionary *dic = @{@"hiden":@"2"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.detailPhotoView.doneButton.hidden = NO;
    self.detailPhotoView.editButton.hidden = YES;
    
}

#pragma mark --- PYPhotosViewDelegate  ---

- (void)photosView:(PYPhotosView *)photosView didAddImageClickedWithImages:(NSMutableArray *)images{
    // 在这里做当点击添加图片按钮时，你想做的事。
    
    [self getPhotos];
}
// 进入预览图片时调用, 可以在此获得预览控制器，实现对导航栏的自定义
- (void)photosView:(PYPhotosView *)photosView didPreviewImagesWithPreviewControlelr:(PYPhotosPreviewController *)previewControlelr{
    NSLog(@"进入预览图片");
}
//进入相册的方法:
-(void)getPhotos{
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3-self.photos.count delegate:self];
    imagePickerVc.maxImagesCount = 3;//最大照片张数,默认是0
    imagePickerVc.sortAscendingByModificationDate = NO;// 对照片排序，按修改时间升序，默认是YES。如果设置为NO,最新的照片会显示在最前面，内部的拍照按钮会排在第一个
    // 你可以通过block或者代理，来得到用户选择的照片.
    
    @WeakObj(self);
    
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets,BOOL isSelectOriginalPhoto){
        NSLog(@"选中图片photos === %@",photos);
        @StrongObj(self)
        [self.photos addObjectsFromArray:photos];
        if (self.photos.count == 0) {
            self.detailPhotoView.doneButton.hidden = YES;
        }else
        {
            self.detailPhotoView.doneButton.hidden = NO;
        }
        NSLog(@"%ld",self.photos.count);
        [self.detailPhotoView.photoView reloadDataWithImages:self.photos];
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

-(NSMutableArray *)photos{
    if (_photos == nil) {
        _photos = [[NSMutableArray alloc]init];
        
    }
    return _photos;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return 40+10;
    }
    if (section == 2) {
        return 130;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        
        SponsorSchemeHeaderViewType type = [self.playType integerValue] < 3 || [self.playType integerValue] > 6?OtherSponsorSchemeType:DaLeTouSponsorSchemeType;
        
        SponsorSchemeHeaderView *view = [[SponsorSchemeHeaderView alloc] initWithType:type];
        if (self.dict) {
            NSString * bunch = self.dict[@"bunch"];
            if (bunch && bunch.length > 0) {
                if ([bunch hasSuffix:@","]) {
                    bunch = [bunch substringToIndex:bunch.length - 1];
                }
            }
           
            NSString * betStr = [self.playType integerValue] < 3?[NSString stringWithFormat:@"%@串1 %@注 %@倍",bunch,self.dict[@"account"],self.times]:[NSString stringWithFormat:@"%@注 %@倍",self.dict[@"account"],self.times];
           view.typelab.text = betStr;
        }
        
        return view;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return [self.playType integerValue] < 3 || [self.playType integerValue] > 6?100:40;
    }
    return 0;
}

#pragma mark HTTP

- (void)getDetailAllInfo{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"1.0",@"apiversion",
                            @"apisafecode",@"safecode",
                            [LoginUser shareLoginUser].shopId,@"userId",
                            self.ID,@"cooperationId", nil];
    
    [PPNetworkHelper formPOST:GetBuyOrderDetail params:params responseCache:^(id responseCache) {
        
    } success:^(id responseObject) {
        
        if ([responseObject[@"code"] integerValue] == 200) {
            
            self.detailModel = [SponsorBuyDetailModel mj_objectWithKeyValues:responseObject[@"res"][@"data"]];
            
            [self reloadButtonTitle];
            
            [self updatePhotoView];
            
            _startTime = [NSString stringWithFormat:@"%@",self.detailModel.stopTime];
            
            [self getList];
            [self.tableView reloadData];
        }else{
            [FC_Manager showToastWithText:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        [FC_Manager showToastWithText:@"网络连接错误,请稍后重试"];
    }];
    
}

-(CountDown *)countDown{
    
    if (!_countDown) {
        _countDown = [[CountDown alloc]init];
    }
    return _countDown;
}


-(void)reloadButtonTitle{
    
    NSInteger  stauts = [self.detailModel.status integerValue];
    
    if ([self.playType integerValue] < 3) {
        UIButton * button = [self.view viewWithTag:1235];
        if (stauts == 0) {
            [button setTitle:@"提前出票" forState:UIControlStateNormal];
        }
    }else{
        UIButton * button = [self.view viewWithTag:1234];
        if (stauts == 0) {
            [button setTitle:@"提前出票" forState:UIControlStateNormal];
        }
    }
    
}

-(void)updatePhotoView{
    
    if (self.photos.count > 0) {
        self.detailPhotoView.photoView.images = self.photos;
    }else{
        if (self.detailModel.image.length == 0) {
            self.detailPhotoView.doneButton.hidden = YES;
        }else{
            self.detailPhotoView.editButton.hidden = YES;
            self.detailPhotoView.doneButton.hidden = YES;
            NSString * imageStr = self.detailModel.image;
            if ([imageStr hasSuffix:@","]) {
                imageStr = [imageStr substringToIndex:imageStr.length - 1];
            }
            NSArray *arr = [imageStr componentsSeparatedByString:@","];
            self.photos = [NSMutableArray arrayWithArray:arr];
            NSMutableArray *arrAy = [NSMutableArray arrayWithArray:arr];
            self.detailPhotoView.photoView.originalUrls = arrAy;    //  加载网络图片
            
        }
    }
    
}

- (void)getList{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:self.detailModel.order_total_id,@"orderTotalId",self.playType,@"type", nil];
    
    NSString * urlStr = [self.playType integerValue] < 3?PostAthleticeOrder:PostNumberOrder;
    if ([self.playType integerValue] == 7) {
        urlStr = PostSuccessOrder;
    }
    if ([self.playType integerValue] == 8) {
        urlStr = PostNineOrder;
    }
    
    [UWHttpTool postWithURL:urlStr
                     params:params
                    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        
                        self.dict = responseObject[@"data"];
                        
                        self.times = responseObject[@"data"][@"times"];
                        
                        self.matchList = responseObject[@"data"][@"detailList"];
                        if ([self.playType integerValue] < 3 || [self.playType integerValue] > 6) {
                            self.detailList = [MinePlanDetailListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"detailList"]];
                        }else{
                            self.detailList = [MineDaLeTouPlanDetailListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"detailList"]];
                        }
                        
                        [self.tableView reloadData];
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSString * _Nonnull errorMsg) {
                        
                    }];
}

#pragma mark  -------   出票   ---------
-(void)outTicketAction
{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        if (self.photos.count == 0) {
            [FC_Manager showToastWithText:@"请先添加彩票照片"];
            self.isClicking = NO;
            return;
        }
        
        if ([self.detailModel.status isEqualToString:@"0"]) {
            [self showAlertViewController];
        }else{
            [self outTicketRequest];
        }
    }
    
    
}

-(void)showAlertViewController{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"出票提醒" message:@"若提前出票,彩民可继续认购。认购金额(含保底)将在方案截止后同一结算。不足部分需您线下垫付" preferredStyle:UIAlertControllerStyleAlert];
    @WeakObj(self)
    UIAlertAction *savePhotoAction = [UIAlertAction actionWithTitle:@"确认出票" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @StrongObj(self)
        [self outTicketRequest];
        //        [self beforeOutTicketRequest];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancelAction];
    [alertController addAction:savePhotoAction];
    
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    
}
//// 提前出票
//-(void)beforeOutTicketRequest{
//
//    @WeakObj(self)
//    [STTextHudTool loadingWithTitle:@"出票中..."];
//
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:self.detailModel.order_total_id,@"orderId",nil];
//
//    [UWHttpTool formDataRequestWithURL:CooperationTickert params:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//
//        @StrongObj(self)
//        [STTextHudTool hideSTHud];
//
//        if ([responseObject[@"code"] integerValue] == 200) {
//            if (self.outTicketSuccessBlock) {
//                self.outTicketSuccessBlock();
//            }
//            [STTextHudTool showText:@"出票成功"];
//            [self.navigationController popViewControllerAnimated:YES];
//        }else{
//            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
//        }
//
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSString * _Nonnull errorMsg) {
//        [STTextHudTool hideSTHud];
//        [STTextHudTool showText:@"出票失败"];
//    }];
//
//}
// 出票
-(void)outTicketRequest{
    
    @WeakObj(self)
    [STTextHudTool loadingWithTitle:@"出票中..."];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.playType forKey:@"type"];
    [dic setValue:self.detailModel.order_total_id forKey:@"orderTotalId"];
    if ([self.playType integerValue] < 3) {
        [dic setValue:self.matchList forKey:@"matchList"];
    }

    if ([self.detailModel.status isEqualToString:@"0"]) {  // 提前出票
        [dic setValue:@"3" forKey:@"classes"];
    }

    [PPNetworkHelper POST:PostPrintOrder parameters:dic success:^(id responseObject) {
        
        NSLog(@"%@",responseObject);
        @StrongObj(self)
        [STTextHudTool hideSTHud];
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            if (self.outTicketSuccessBlock) {
                self.outTicketSuccessBlock();
            }
            [STTextHudTool showText:@"出票成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
        self.isClicking = NO;
    } failure:^(NSError *error) {
        @StrongObj(self)
        [STTextHudTool hideSTHud];
        [STTextHudTool showText:@"出票失败"];
        self.isClicking = NO;
    }];
    
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:self.detailModel.order_total_id,@"orderId",nil];
//
//    [UWHttpTool formDataRequestWithURL:CooperationTickert params:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//
//        @StrongObj(self)
//        [STTextHudTool hideSTHud];
//
//        if ([responseObject[@"code"] integerValue] == 200) {
//            if (self.outTicketSuccessBlock) {
//                self.outTicketSuccessBlock();
//            }
//            [STTextHudTool showText:@"出票成功"];
//            [self.navigationController popViewControllerAnimated:YES];
//        }else
//        {
//            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
//        }
//
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSString * _Nonnull errorMsg) {
//        [STTextHudTool hideSTHud];
//        [STTextHudTool showText:@"出票失败"];
//    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = YES;
    
}

@end
