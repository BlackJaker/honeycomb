//
//  FC_CurrentBuyListViewController.h
//  HoneyComb
//
//  Created by afc on 2018/12/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

@class SponsorBuyDetailModel;

NS_ASSUME_NONNULL_BEGIN

@interface FC_CurrentBuyListViewController : AllViewController

@property (nonatomic, strong) SponsorBuyDetailModel *detailModel;

@end

NS_ASSUME_NONNULL_END
