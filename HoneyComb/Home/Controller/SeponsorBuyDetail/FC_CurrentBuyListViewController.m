//
//  FC_CurrentBuyListViewController.m
//  HoneyComb
//
//  Created by afc on 2018/12/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_CurrentBuyListViewController.h"

#import "FC_CurrentBuyListHeadView.h"

#import "SponsorSchemeCell.h"

#import "SponsorBuyDetailModel.h"

static NSString *OtherSponsorSchemeCellID = @"OtherSponsorSchemeCellID";

@interface FC_CurrentBuyListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView * currentListTableView;

@property (nonatomic,copy) NSMutableArray * listData;

@property (nonatomic,copy) FC_CurrentBuyListHeadView * headView;

@end

@implementation FC_CurrentBuyListViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    self.titleLabe.text = @"认购列表";
    
    [self.currentListTableView reloadData];
}

#pragma mark  --  table view datasource  --

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.detailModel.caiyou.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OtherSponsorSchemeCell *cell = [tableView dequeueReusableCellWithIdentifier:OtherSponsorSchemeCellID];
    
    cell.backView.backgroundColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    
    SponsorCaiYouInfo * info = [self.detailModel.caiyou objectAtIndex:indexPath.row];
    
    NSString * time = @"-";
    if (info.time) {
        time = [info.time substringFromIndex:5];
    }
    
    NSArray *arr = @[info.nick_name,
                     info.subscription,
                     time,
                     info.yuji];
    
    for (UILabel *lab in cell.titlelabs) {
        NSInteger index = [cell.titlelabs indexOfObject:lab];
        lab.text = arr[index];
    }
    return cell;
 
}

#pragma mark  --  table view delegate  --

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 40)];
    [view addSubview:self.headView];
    
    return view;
}

-(FC_CurrentBuyListHeadView *)headView{
    
    if (!_headView) {
        _headView = [[FC_CurrentBuyListHeadView alloc]initWithFrame:CGRectMake(15, 0, kWidth - 30, 40)];
    }
    return _headView;
}

#pragma mark  --  lazy  --

-(UITableView *)currentListTableView{
    
    if (!_currentListTableView) {
        _currentListTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _currentListTableView.delegate = self;
        _currentListTableView.dataSource = self;
        _currentListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _currentListTableView.backgroundColor = [UIColor clearColor];
    
        [_currentListTableView registerClass:[OtherSponsorSchemeCell class]
          forCellReuseIdentifier:OtherSponsorSchemeCellID];
        
        [self.view addSubview:_currentListTableView];
        
        [_currentListTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@(kTopMar));
            make.left.right.equalTo(0);
            make.right.equalTo(0);
            make.bottom.equalTo(-TAB_BAR_SAFE_HEIGHT);
        }];
    }
    return _currentListTableView;
}


@end
