//
//  HomeViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "HomeViewController.h"
#import "SegmentViewController.h"
#import "BettingViewController.h"
#import "ChippedViewController.h"
#import "DocumentaryViewController.h"
#import "BettingView.h"

#import "AF_StatementView.h"

static CGFloat const ButtonHeight = 40;
@interface HomeViewController ()
{
     BOOL isUser;
}
@property (nonatomic,strong) NSMutableArray *arrCount;
@property (nonatomic,strong) UIButton *rightBtn;
@property (nonatomic,strong) BettingView *betView;
@property (nonatomic,strong)UIWindow *window;
@end

@implementation HomeViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self checkStatement];

    self.left.hidden = YES;
    isUser = YES;
    self.titleLabe.text = @"订单管理";
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    _rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10,15, 16)];
    [_rightBtn setImage:[UIImage imageNamed:@"分类图标"] forState:(UIControlStateNormal)];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(tongzhiBetting:) name:@"bettingTongzhi" object:nil];
    
    [_rightBtn addTarget:self action:@selector(rightAction) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *le = [[UIBarButtonItem alloc]initWithCustomView:_rightBtn];
    self.navigationItem.rightBarButtonItem = le;
    SegmentViewController *vc = [[SegmentViewController alloc]init];
   
    NSArray *titleArray = @[@"订单 (0)",@"合买 (0)",@"跟单 (0)"];
    vc.titleArray = titleArray;
    NSMutableArray *controlArray = [[NSMutableArray alloc]init];
    
    BettingViewController *vc1 = [[BettingViewController alloc]init];
    
    vc1.reloadTotalCountBlock = ^(NSInteger totalCount) {
        [vc reloadTotalCountWithIndex:0 count:totalCount];
    };
    
    [controlArray addObject:vc1];
    
    ChippedViewController *vc2 = [[ChippedViewController alloc]init];
    vc2.reloadTotalCountBlock = ^(NSInteger totalCount) {
        [vc reloadTotalCountWithIndex:1 count:totalCount];
    };
    [controlArray addObject:vc2];
    
    DocumentaryViewController *vc3 = [[DocumentaryViewController alloc]init];
    vc3.reloadTotalCountBlock = ^(NSInteger totalCount) {
        [vc reloadTotalCountWithIndex:2 count:totalCount];
    };
    [controlArray addObject:vc3];
    vc.source = @"home";
    vc.titleSelectedColor = [UIColor redColor];
    vc.subViewControllers = controlArray;
    vc.buttonWidth = self.view.frame.size.width/3;
    vc.buttonHeight = ButtonHeight;
    vc.bottomCount = titleArray.count;
    self.vc = vc;
    [vc initSegment];
    [vc addParentController:self];
    

}
-(void)tongzhiBetting:(NSNotification *)dic
{
     isUser = YES;
}
-(void)rightAction
{
    if (isUser) {
        _betView = [[BettingView alloc]initWithFrame:CGRectMake(0, NavgationBarHeight, kWidth, kHeight)];
        UIWindow *keyWin = [[UIApplication sharedApplication]keyWindow];
        [keyWin addSubview:_betView];
        isUser = NO;
    }else{
        _betView.hidden = YES;
        isUser = YES;
    }
}

#pragma mark  --  statement  --

-(void)checkStatement{
    
    NSString * statement = [[NSUserDefaults standardUserDefaults] valueForKey:[LoginUser shareLoginUser].shopId];
    
    if (!statement || ![statement isEqualToString:@"1"]) {
        AF_StatementView *statementView = [[AF_StatementView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
        [statementView show];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
