//
//  GendanDetailViewController.m
//  彩票
//
//  Created by 陈亚勃 on 2018/12/11.
//  Copyright © 2018 op150. All rights reserved.
//

#import "GendanDetailViewController.h"
#import "SponsorSchemeCell.h"
#import "OrderDetailInfoCell.h"
#import "YBButton.h"
#import "UWHttpTool.h"
#import "FollowViewListModel.h"

#import "NSAttributedString+TRCreat.h"

#import "FC_FollowDetailHeadView.h"


@interface GendanDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
//header

@property (nonatomic, strong) FC_FollowDetailHeadView *headView;

@property (nonatomic, strong) UIImageView *userIcon;
@property (nonatomic, strong) UILabel *usernamelab;

@property (nonatomic, strong) NSMutableArray <UILabel *> *percentlabs;

@property (nonatomic, strong) FollowViewDetailModel *detailModel;

@end

static NSString *OtherSponsorSchemeCellID = @"OtherSponsorSchemeCellID";
static NSString *OtherSponsorSchemeNoResultCellID = @"OtherSponsorSchemeNoResultCellID";
static NSString *OrderDetailInfoCellID = @"OrderDetailInfoCellID";

@implementation GendanDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self buildNav];
    [self buildUI];
    
    [self getDetail];
}

- (void)buildNav{
    self.titleLabe.text = @"方案详情";
}

- (void)buildUI{
    
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        tableView.delegate = self;
        tableView.dataSource = self;
        
        [tableView registerClass:[OtherSponsorSchemeCell class] forCellReuseIdentifier:OtherSponsorSchemeCellID];
        [tableView registerClass:[OtherSponsorSchemeNoResultCell class] forCellReuseIdentifier:OtherSponsorSchemeNoResultCellID];
        [tableView registerClass:[OrderDetailInfoCell class] forCellReuseIdentifier:OrderDetailInfoCellID];
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [self.view addSubview:tableView];
        tableView;
    });
    
    self.tableView.tableHeaderView = self.headView;

    CGFloat  bottomMar = IPHONE_X?TAB_BAR_SAFE_HEIGHT:0;
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(0));
        make.left.right.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
        make.bottom.equalTo(-bottomMar);
    }];
    

}

-(FC_FollowDetailHeadView *)headView{
    
    if (!_headView) {
        
        UINib *nib = [UINib nibWithNibName:@"FC_FollowDetailHeadView" bundle:nil];
        _headView = [[nib instantiateWithOwner:nil options:nil] firstObject];
    }
    return _headView;
}

#pragma mark UITableViewDelegate,UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        GendanDetailListInfoModel *model = self.detailModel.games_info.detailList[indexPath.row];
        OtherSponsorSchemeNoResultCell *cell = [tableView dequeueReusableCellWithIdentifier:OtherSponsorSchemeNoResultCellID forIndexPath:indexPath];
        
        cell.backView.backgroundColor = indexPath.row%2 == 0 ? COLOR_HEX(0xF0EFF4, 1):COLOR_HEX(0xffffff, 1);

        cell.backView.layer.borderColor = COLOR_HEX(0xF0EFF4, 1).CGColor;
        cell.backView.layer.borderWidth = 1;
        
        NSArray *arr = @[[self attributeWithString:[NSString stringWithFormat:@"%@\n%@\n%@截止",model.number,model.league,model.startTime]],
                         [NSAttributedString attributedStringWithAttributedString:[self attributeWithString:[NSString stringWithFormat:@"%@\nvs\n%@",model.hostTeam,model.visitingTeam]] hightlightString:@"vs" highlightColor:COLOR_HEX(0xE73A59, 1)],
                         [self attributeWithString:model.content.length == 0 ? @"":[model.content stringByReplacingOccurrencesOfString:@"," withString:@"\n"]]];
        
        for (UILabel *lab in cell.titlelabs) {
            NSInteger index = [cell.titlelabs indexOfObject:lab];
            lab.attributedText = arr[index];
        }

        return cell;
        
    }else{
        
        OrderDetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:OrderDetailInfoCellID];
        NSString *str = [NSString stringWithFormat:@"发 起 人 ：%@\n"
                         "提交时间：%@\n"
                         "投注份数：%@\n"
                         "金       额：%@\n"
                         "订单状态：%@\n"
                         "奖励奖金：%@\n",
                         self.detailModel.nick_name,
                         self.detailModel.create_date,
                         [NSString stringWithFormat:@"%@",self.detailModel.follow_count],
                         [NSString stringWithFormat:@"%@元",self.detailModel.total_money],
                         self.detailModel.order_statusStr,
                         self.detailModel.result];
        
        NSMutableAttributedString *astr = [[NSMutableAttributedString alloc] initWithString:str];
        NSMutableParagraphStyle *paragraphstyle = [[NSMutableParagraphStyle alloc] init];
        paragraphstyle.lineSpacing = kDEVICE_SCALE_FACTOR(15);
        [astr setAttributes:@{NSParagraphStyleAttributeName:paragraphstyle} range:NSMakeRange(0, str.length)];
        cell.lab.attributedText = astr;
        
        return cell;
    }
}


- (NSMutableAttributedString *)attributeWithString:(NSString *)str{
    if (!str) {
        return nil;
    }
    NSMutableAttributedString *astr = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 5;
    style.alignment = NSTextAlignmentCenter;
    [astr addAttributes:@{NSParagraphStyleAttributeName:style} range:NSMakeRange(0, str.length)];
    return astr;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        GendanDetailListInfoModel * model = [self.detailModel.games_info.detailList objectAtIndex:indexPath.row];
        return [self getCurrentContentHeightWithItemModel:model];
    }else{
        return kDEVICE_SCALE_FACTOR(230);
    }
}

-(CGFloat)getCurrentContentHeightWithItemModel:(GendanDetailListInfoModel *)model{
    
    CGFloat  normalHeight = 65,itemHeight = 18;
    
    NSArray * contentArr = [model.content componentsSeparatedByString:@","];
    
    if (contentArr.count > 3) {
        normalHeight += (contentArr.count - 3) * itemHeight;
    }
    
    return normalHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return self.detailModel.games_info.detailList.count;
    }else{
        return 1;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (section == 0) {
        UIView *view = [UIView viewWithBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        view.frame = CGRectMake(0, 0, kWidth, kDEVICE_SCALE_FACTOR(40)+10);
        UILabel *lab = [UILabel lableWithText:@"赔率以实际出票为准"
                                         font:[UIFont systemFontOfSize:14]
                                    textColor:COLOR_HEX(0x7F8491, 1)];
        lab.backgroundColor = [UIColor whiteColor];
        lab.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
            make.height.equalTo(@(kDEVICE_SCALE_FACTOR(40)));
        }];
        return view;
    }else{
        return nil;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return kDEVICE_SCALE_FACTOR(40)+10;
    }else{
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        SponsorSchemeHeaderView *view = [[SponsorSchemeHeaderView alloc] initWithType:OtherSponsorSchemeNoResultType];
        view.titlelab.text = @"跟单内容";
        if (self.detailModel.games_info.detailList.count > 0) {
            
            view.typelab.text = [NSString stringWithFormat:@"%@串1  %@注  %@倍",[self checkString:self.detailModel.games_info.bunch],self.detailModel.games_info.account,self.detailModel.games_info.times];
            view.typelab.font = [UIFont fontWithName:kMedium size:16];
            view.typelab.textColor = [UIColor colorWithHexString:@"#464E5F"];
        }
        return view;
    }else{
        SponsorSchemeHeaderView *view = [[SponsorSchemeHeaderView alloc] initWithType:DefaultSponsorSchemeType];
        view.titlelab.text = @"订单详情";
        if (self.detailModel) {
            view.followNumLabel.text = [NSString stringWithFormat:@"%@人跟单",self.detailModel.follow_count];
        }
        
        return view;

    }
    return nil;
}

-(NSString *)checkString:(NSString *)string{
    
    if ([string hasPrefix:@","]) {
        
        string = [string substringFromIndex:1];
        
    }
    
    //字符串末尾有某字符串；
    
    if ([string hasSuffix:@","]) {
        
        string = [string substringToIndex:string.length - 1];
        
    }
    return string;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 100;
    }else if(section == 1){
        return 50;
    }
    return 0;
}

- (void)getDetail{
    [UWHttpTool getWithURL:GenDanDetailUrl
                    params:@{@"shop_id":[LoginUser shareLoginUser].shopId,@"plan_id":self.plan_id}
                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                       
                       self.detailModel = [FollowViewDetailModel mj_objectWithKeyValues:responseObject[@"data"]];
                       
                       [self reloadView];
                       
                   } failure:^(NSURLSessionDataTask * _Nullable task, NSString * _Nonnull errorMsg) {
                       
                   }];
}

- (void)reloadView{

    [self reloadHeadView];
    
    [self.tableView reloadData];
}

-(void)reloadHeadView{
    
    [self.headView.headImgView sd_setImageWithURL:[NSURL URLWithString:self.detailModel.img] placeholderImage:[UIImage imageNamed:@"默认头像"]];

    CGFloat  normalWidth = (kWidth - 114)/2;
    CGFloat  nameWidth = [self labelWidthWithString:self.detailModel.nick_name font:[UIFont boldSystemFontOfSize:22] height:30];
    CGFloat  playNameWidth = [self labelWidthWithString:self.detailModel.play_name font:[UIFont systemFontOfSize:12] height:self.headView.playNameLabel.height];
    
    if (nameWidth > normalWidth) {
        nameWidth = normalWidth;
    }
    if (playNameWidth > normalWidth) {
        playNameWidth = normalWidth;
    }
    self.headView.nameLabelWidthConstrains.constant = nameWidth;

    self.headView.userNameLabel.text = self.detailModel.nick_name;
    
    self.headView.playNameLabelWidthConstrains.constant = playNameWidth;

    self.headView.playNameLabel.text = self.detailModel.play_name;
    
    if (self.playType == 2) {
        [self.headView.playNameLabel setBackgroundColor:[UIColor colorWithHexString:@"#0D6EFF"]];
    }
    
    self.headView.yongJinLabel.text = [NSString stringWithFormat:@"%@%%",self.detailModel.commission];
    self.headView.totalYongJinLabel.text = self.detailModel.total_commission;
    self.headView.caiMinYongJinLabel.text = self.detailModel.total_plan_commission;
    self.headView.dianZhuYongJinLabel.text = self.detailModel.shop_commission;
    self.headView.totalOrderMoneyLabel.text = self.detailModel.total_money;
    self.headView.ziGouLabel.text = self.detailModel.pay_money;
    self.headView.danBeiLabel.text = self.detailModel.money;
    self.headView.followNumLabel.text = self.detailModel.follow_count;
    
}

- (CGFloat)labelWidthWithString:(NSString *)resultString font:(UIFont *)font height:(CGFloat)height{
    
    CGSize maxSize = CGSizeMake(MAXFLOAT, height);
    
    // 计算内容label的高度
    
    CGFloat width = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :font} context:nil].size.width;
    return width + 5;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = YES;
    
}

@end
