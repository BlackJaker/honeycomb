//
//  GendanDetailViewController.h
//  彩票
//
//  Created by 陈亚勃 on 2018/12/11.
//  Copyright © 2018 op150. All rights reserved.
//

#import "AllViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GendanDetailViewController : AllViewController

@property (nonatomic, assign) NSInteger playType;
@property (nonatomic, copy) NSString *plan_id;

@end

NS_ASSUME_NONNULL_END
