//
//  DocumentaryViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ReloadCurrentTotalCountBlock)(NSInteger totalCount);

@interface DocumentaryViewController : UIViewController

@property (nonatomic,copy) ReloadCurrentTotalCountBlock  reloadTotalCountBlock;

@end
