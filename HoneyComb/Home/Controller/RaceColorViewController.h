//
//  RaceColorViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaceColorViewController : AllViewController
@property (nonatomic,copy) NSString *strType;
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSArray *matchList;
@property (nonatomic,copy) void (^NextViewControllerBlock)(NSInteger tfText);
@end
