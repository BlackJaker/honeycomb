//
//  HomeViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SegmentViewController;

@interface HomeViewController : AllViewController

@property (nonatomic,strong) SegmentViewController *vc;

@end
