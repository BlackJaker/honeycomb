//
//  ChippedViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "ChippedViewController.h"

#import "SponsorBuyLobbyListModel.h"

#import "FC_SeponsorBuyCell.h"

#import "SponsorBuyDetailViewController.h"

@interface ChippedViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
{
    NSInteger type;
    NSInteger page;
    NSInteger totalNum; //是否是最后一页
    
    UIButton *pulldownButton;
    UIView *navPullDownView;
    UIView *blackBackView;
    NSMutableArray <UIButton *>*filtrateButtons;
    
    BOOL  isRefresh;
    
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray <SponsorBuyLobbyListModel *>*listArr;

@end

static NSString *SponsorBuyListViewCellID = @"SponsorBuyListViewCellID";

@implementation ChippedViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    isRefresh = YES;
    
    self.view.backgroundColor = RGB(249, 249, 249, 1);

    [self initSubviews];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reload)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)initSubviews{
    
    self.tableView = ({
        
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.emptyDataSetSource = self;
        tableView.emptyDataSetDelegate = self;
        tableView.tableFooterView = [UIView new];
        tableView.backgroundColor = [UIColor clearColor];
        
        
        [tableView registerClass:[FC_SeponsorBuyCell class] forCellReuseIdentifier:SponsorBuyListViewCellID];
        [self.view addSubview:tableView];
        tableView;
    });
    
    [self.tableView makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
//    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(@(0));
//        make.left.right.equalTo(@(kDEVICE_SCALE_FACTOR(0)));
//        if (@available(iOS 11.0, *)) {
//            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(kDEVICE_SCALE_FACTOR(0));
//        } else {
//            make.bottom.equalTo(self.view.mas_bottom).offset(kDEVICE_SCALE_FACTOR(0));
//        }
//    }];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self->page = 1;
        [self getList];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (self.listArr.count >= self->totalNum) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            self->page++;
            [self getList];
        }
    }];
    
}

#pragma mark UITableViewDelegate,UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SponsorBuyLobbyListModel *model = self.listArr[indexPath.row];
    FC_SeponsorBuyCell *cell = [tableView dequeueReusableCellWithIdentifier:SponsorBuyListViewCellID];
    
    NSString * typeStr = model.type;
    if ([typeStr isEqualToString:@"1"]) {
        cell.icon.image = [UIImage imageNamed:@"football"];
    }else if ([typeStr isEqualToString:@"2"]){
        cell.icon.image = [UIImage imageNamed:@"basketball"];
    }else if ([typeStr isEqualToString:@"3"]){
        cell.icon.image = [UIImage imageNamed:@"daletou"];
    }else if ([typeStr isEqualToString:@"4"]){
        cell.icon.image = [UIImage imageNamed:@"pailiesan"];
    }else if ([typeStr isEqualToString:@"5"]){
        cell.icon.image = [UIImage imageNamed:@"pailiewu"];
    }else if ([typeStr isEqualToString:@"6"]){
        cell.icon.image = [UIImage imageNamed:@"qixingcai"];
    }else if ([typeStr isEqualToString:@"7"]){
        cell.icon.image = [UIImage imageNamed:@"win_lose"];
    }else{
        cell.icon.image = [UIImage imageNamed:@"any_nine"];
    }
    cell.namelab.text = [NSString stringWithFormat:@"发起人 : %@",model.nick_name];
//    if ([model.type isEqualToString:@"1"]) {
//        cell.typenamelab.text = @"足球";
//        cell.icon.image = [UIImage imageNamed:@"ic_football"];
//    }else if ([model.type isEqualToString:@"2"]){
//        cell.typenamelab.text = @"篮球";
//        cell.icon.image = [UIImage imageNamed:@"ic_basketball"];
//    }
    cell.typenamelab.text = model.play_name;
    
//    cell.timelab.text = @"";
//    cell.aborttimelab.text = @"到期 2天07:03:32";
//    cell.namelab.text = model.nick_name;
//    cell.biolab.text = model.describe;
    
    NSArray *numbers = @[model.price,model.unitPrice,model.people,model.shengyu];
    NSArray *deses = @[@"方案总额",@"单份金额",@"参与人数",@"剩余份数"];
    for (int i = 0; i < numbers.count; i++) {
        cell.numberlabs[i].text = numbers[i];
        cell.deslabs[i].text = deses[i];
    }
    
    cell.percentView.havePercent = [model.yigou floatValue];
    cell.percentView.baodiPercent = [model.bao floatValue];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kDEVICE_SCALE_FACTOR(160);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.listArr.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    isRefresh = NO;
    
    FC_SeponsorBuyCell * cell = [tableView cellForRowAtIndexPath:indexPath];

    self.hidesBottomBarWhenPushed = YES;
    SponsorBuyLobbyListModel *model = self.listArr[indexPath.row];
    SponsorBuyDetailViewController *vc = [[SponsorBuyDetailViewController alloc] init];
    vc.type = SponsorBuyDetailControllerTypeAll;
    vc.playType = model.type;
    vc.playImg = cell.icon.image;
    vc.ID = model.ID;
    
    @WeakObj(self)
    vc.outTicketSuccessBlock = ^{
        @StrongObj(self)
        page = 1;
        [self getList];
    };
    
    [self.navigationController pushViewController:vc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

- (void)getList{
    NSDictionary *params = @{@"apiversion":@"1.0",
                             @"safecode":@"apisafecode",
                             @"shopId":[LoginUser shareLoginUser].shopId,
                             @"type":@(type),
                             @"page":@(page),
                             @"pagenum":@"10",
                             @"role":@(1)
                             };
    [PPNetworkHelper formPOST:GetBuyOrderList params:params responseCache:^(id responseCache) {
        
    } success:^(id responseObject) {
        
        if ([responseObject[@"code"] integerValue] == 200) {
            
            if (page == 1) {
                [self.listArr removeAllObjects];
            }
            
            totalNum = [responseObject[@"res"][@"total"] integerValue];
            if (self.tableView.mj_footer.isRefreshing) {
                [self.listArr addObjectsFromArray:[SponsorBuyLobbyListModel mj_objectArrayWithKeyValuesArray:responseObject[@"res"][@"list"]]];
            }else{
                self.listArr = [SponsorBuyLobbyListModel mj_objectArrayWithKeyValuesArray:responseObject[@"res"][@"list"]];
            }
            [self.tableView reloadData];
            [self endRefresh];
            
            if (self.reloadTotalCountBlock) {
                self.reloadTotalCountBlock(totalNum);
            }
            
        }else{
            [FC_Manager showToastWithText:responseObject[@"msg"]];
        }
       
        
    } failure:^(NSError *error) {
        [FC_Manager showToastWithText:@"网络连接错误,请稍后重试"];
        [self endRefresh];
    }];
}

- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}
#pragma mark DZ
-(CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 25;
}
-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"meiyouOrder"];
}
-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    return YES;
}
-(BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateFrameWithMargin:(CGFloat)margin{
    
    CGRect frame = self.tableView.frame;
    frame.size.height = kHeight-NavgationBarHeight-40-TabBarHeight - margin;
    self.tableView.frame = frame;
    
}
#pragma mark  --  view will appear  --

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (isRefresh) {
        page = 1;
        [self getList];
    }else{
        isRefresh = YES;
    }
 
}

-(void)reload{
    
    if (isRefresh) {
        page = 1;
        [self getList];
    }
    
}

@end
