//
//  BigLotteryViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "BigLotteryViewController.h"

#import "OSSImageUploader.h"

#import "FC_NumberLottoryDetailModel.h"

#import "FC_NumberLottoryDetailHeadView.h"

#import "FC_NumberLottoryBettingView.h"

#import "FC_NumberLottoryDetailOrderView.h"

#import "FC_NumberLottoryDetailPhotoView.h"

#import "AF_OddsNoteView.h"

///弱引用/强引用
#define CCWeakSelf __weak typeof(self) weakSelf = self;

@interface BigLotteryViewController ()<UITableViewDelegate,UITableViewDataSource,TZImagePickerControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,UINavigationControllerDelegate,PYPhotosViewDelegate,FC_DetailPhotoViewDelegate>
{
    BOOL isEdit;
    
    CGFloat  noteHeight;
    CGFloat  headHeight;
    CGFloat  betInfoHeight;
    CGFloat  orderHeight;
    CGFloat  detailPhotoViewHeight;
}

@property(nonatomic,strong)NSMutableArray *photos;
@property(nonatomic,assign)int repeatClickInt;
@property (nonatomic, strong) UIImagePickerController *imagePickerVc;

@property (nonatomic,strong) UIButton *cancleBtn;
@property (nonatomic,strong) UIButton *outBtn;
@property (nonatomic,copy) NSString *tokenStr;
@property (nonatomic,copy) NSString *accessKey;
@property (nonatomic,copy) NSString *accessKeySecret;

@property (nonatomic,strong) UIScrollView * contentScrollView;

@property (nonatomic,strong) FC_NumberLottoryDetailModel * detailModel;

@property (nonatomic,strong) FC_NumberLottoryDetailHeadView * headView;

@property (nonatomic,strong) FC_NumberLottoryBettingView * bettingInfoView;

@property (nonatomic,strong) FC_NumberLottoryDetailOrderView * detailOrderView;

@property (nonatomic,strong) FC_NumberLottoryDetailPhotoView * detailPhotoView;

@property (nonatomic,strong) AF_OddsNoteView * noteView;

@property (nonatomic,assign) BOOL isClicking;

@end

@implementation BigLotteryViewController

static NSString *kTempFolder = @"shop/order";

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
  
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    noteHeight = 40;
    headHeight = 83;
    betInfoHeight = 40;
    orderHeight = 45;
    detailPhotoViewHeight = 130;
    
    isEdit = YES;
    self.titleLabe.text = @"订单详情";
    self.isClicking = NO;
    
    [self layoutView];
    
    [self numberLottoryDetailInfoRequest];
    [self postTokenRequest];
    //获取通知中心
    NSNotificationCenter * centerMore =[NSNotificationCenter defaultCenter];
    [centerMore addObserver:self selector:@selector(deleteAction:) name:@"deleteTongzhi" object:nil];
}
-(void)deleteAction:(NSNotification *)dic
{
    self.detailPhotoView.doneButton.hidden = YES;
}

#pragma mark  --  lazy  --

-(UIScrollView *)contentScrollView{
    
    if (!_contentScrollView) {
        _contentScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight - (NAVIGATION_HEIGHT + TabBarHeight))];
        [self.view addSubview:_contentScrollView];
        
        self.noteView.backgroundColor = [UIColor colorWithHexString:@"#FFEEEC"];
    }
    return _contentScrollView;
}

-(AF_OddsNoteView *)noteView{
    
    if (!_noteView) {
        _noteView = [[AF_OddsNoteView alloc]init];
        [self.view addSubview:_noteView];
        
        [_noteView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(noteHeight);
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(0);
            
        }];
        
    }
    return _noteView;
}

-(FC_NumberLottoryDetailHeadView *)headView{
    
    CGRect frame = CGRectMake(0, noteHeight, kWidth, headHeight);
    
    if (!_headView) {
        _headView = [[FC_NumberLottoryDetailHeadView alloc]initWithFrame:frame
                                                                    type:self.strType];
        [self.contentScrollView addSubview:_headView];
    }
    return _headView;
}

-(FC_NumberLottoryBettingView *)bettingInfoView{
    
    CGRect frame = CGRectMake(0,noteHeight + headHeight + kTopMar, kWidth, betInfoHeight);
    
    if (!_bettingInfoView) {
        _bettingInfoView = [[FC_NumberLottoryBettingView alloc]initWithFrame:frame
                                                                    type:[self.strType integerValue]];
        [self.contentScrollView addSubview:_bettingInfoView];
    }
    return _bettingInfoView;
}

-(FC_NumberLottoryDetailOrderView *)detailOrderView{
    
    CGRect frame = CGRectMake(0, noteHeight + headHeight + kTopMar + betInfoHeight, kWidth, orderHeight);
    
    if (!_detailOrderView) {
        _detailOrderView = [[FC_NumberLottoryDetailOrderView alloc]initWithFrame:frame];
        [self.contentScrollView addSubview:_detailOrderView];
    }
    return _detailOrderView;
}

-(FC_NumberLottoryDetailPhotoView *)detailPhotoView{
    
    CGRect frame = CGRectMake(0, noteHeight +headHeight + kTopMar * 2 + betInfoHeight + orderHeight, kWidth, detailPhotoViewHeight);
    
    if (!_detailPhotoView) {
        _detailPhotoView = [[FC_NumberLottoryDetailPhotoView alloc]initWithFrame:frame];
        _detailPhotoView.delegate = self;
        _detailPhotoView.photoView.delegate = self;
        [self.contentScrollView addSubview:_detailPhotoView];
    }
    return _detailPhotoView;
}

#pragma mark  -- detail photo view delegate  --

-(void)done{
    
    CGFloat  leftMar = 12,topMar = 40;
    
    self.detailPhotoView.photoView.py_x = leftMar;
    self.detailPhotoView.photoView.py_y = topMar;

    NSArray *arrAll = self.photos;
    NSLog(@"-%@",arrAll);
    
    [OSSImageUploader asyncUploadImages:arrAll folder:kTempFolder accessKey:self.accessKey secretKey:self.accessKeySecret token:self.tokenStr complete:^(NSArray<NSString *> *names, UploadImageState state) {
        NSLog(@"彩票照片-%@",names);
        
        NSMutableArray *arraY = [NSMutableArray array];
        for (NSString *str in names) {
            [arraY addObject:[NSString stringWithFormat:@"http://aliimg.afcplay.com/%@",str]];
        }
        NSString *string =[arraY componentsJoinedByString:@","];
        NSLog(@"转换-%@",string);
        NSDictionary *dic = @{@"img":string,@"orderTotalId":self.orderId};
        [PPNetworkHelper POST:PostSetOrderImage parameters:dic success:^(id responseObject) {
            NSLog(@"%@",responseObject);
        } failure:^(NSError *error) {
            
        }];
        
    }];
    self.detailPhotoView.photoView.imagesMaxCountWhenWillCompose = self.photos.count;
    NSDictionary *dic = @{@"hiden":@"1"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.detailPhotoView.doneButton.hidden = YES;
    self.detailPhotoView.editButton.hidden =NO;
    
}

-(void)edit{
    
    CGFloat  leftMar = 12,topMar = 40;
    
    self.detailPhotoView.photoView.py_x = leftMar;
    self.detailPhotoView.photoView.py_y = topMar;
    self.detailPhotoView.photoView.imagesMaxCountWhenWillCompose = 3;
    
    NSDictionary *dic = @{@"hiden":@"2"};
    //创建通知
    NSNotification *notification =[NSNotification notificationWithName:@"doneTongzhi" object:nil userInfo:dic];
    //通过通知中心发送通知
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    self.detailPhotoView.doneButton.hidden = NO;
    self.detailPhotoView.editButton.hidden = YES;
    
}

#pragma mark --- PYPhotosViewDelegate  ---

- (void)photosView:(PYPhotosView *)photosView didAddImageClickedWithImages:(NSMutableArray *)images{
    // 在这里做当点击添加图片按钮时，你想做的事。
    
    [self getPhotos];
}
// 进入预览图片时调用, 可以在此获得预览控制器，实现对导航栏的自定义
- (void)photosView:(PYPhotosView *)photosView didPreviewImagesWithPreviewControlelr:(PYPhotosPreviewController *)previewControlelr{
    NSLog(@"进入预览图片");
}
//进入相册的方法:
-(void)getPhotos{
    CCWeakSelf;
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:3-weakSelf.photos.count delegate:weakSelf];
    imagePickerVc.maxImagesCount = 3;//最大照片张数,默认是0
    imagePickerVc.sortAscendingByModificationDate = NO;// 对照片排序，按修改时间升序，默认是YES。如果设置为NO,最新的照片会显示在最前面，内部的拍照按钮会排在第一个
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets,BOOL isSelectOriginalPhoto){
        NSLog(@"选中图片photos === %@",photos);
        //        for (UIImage *image in photos) {
        //            [weakSelf requestData:image];//requestData:图片上传方法 在这里就不贴出来了
        //        }
        [weakSelf.photos addObjectsFromArray:photos];
        if (weakSelf.photos.count == 0) {
            self.detailPhotoView.doneButton.hidden = YES;
        }else
        {
            self.detailPhotoView.doneButton.hidden = NO;
        }
        NSLog(@"%ld",weakSelf.photos.count);
        [self.detailPhotoView.photoView reloadDataWithImages:weakSelf.photos];
    }];
    [weakSelf presentViewController:imagePickerVc animated:YES completion:nil];
}

-(NSMutableArray *)photos{
    if (_photos == nil) {
        _photos = [[NSMutableArray alloc]init];
        
    }
    return _photos;
}

#pragma mark  --  request  --

-(void)numberLottoryDetailInfoRequest
{
    NSDictionary *dic = @{
                          @"shopId":[[LoginUser shareLoginUser] shopId],
                          @"type":self.strType,
                          @"orderTotalId":self.orderId
                          };
    [STTextHudTool showWaitText:@"加载中..."];
    @WeakObj(self)
    [PPNetworkHelper POST:PostNumberOrder parameters:dic success:^(id responseObject) {

        @StrongObj(self)
        NSLog(@"数字彩%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            [STTextHudTool hideSTHud];
            
            self.detailModel = [FC_NumberLottoryDetailModel mj_objectWithKeyValues:responseObject[@"data"]];
            
            // head
            [self.headView reloadWithModel:self.detailModel];
            
            // bet
            betInfoHeight = (self.detailModel.detailList.count + 1) * 40;
            self.bettingInfoView.frame = CGRectMake(0, CGRectGetMaxY(self.headView.frame) + kTopMar, kWidth, betInfoHeight);
            [self.bettingInfoView reloadBettingWithModel:self.detailModel];
            
            // order
            [self reloadOrderView];
            
            // photo view

            if ([self.detailModel.image isEqualToString:@""] || [self.detailModel.image isEqualToString:@"-"]) {
                self.detailPhotoView.doneButton.hidden = YES;
            }else{
                self.detailPhotoView.editButton.hidden = YES;
                self.detailPhotoView.doneButton.hidden = YES;
                NSString * imageStr = self.detailModel.image;
                if ([imageStr hasSuffix:@","]) {
                    imageStr = [imageStr substringToIndex:imageStr.length - 1];
                }
                NSArray *arr = [imageStr componentsSeparatedByString:@","];
                self.photos = [NSMutableArray arrayWithArray:arr];
                self.detailPhotoView.photoView.originalUrls = self.photos;    //  加载网络图片
            }
            
            CGFloat  contentHeight = kHeight - (NAVIGATION_HEIGHT + TabBarHeight);
            if (contentHeight < noteHeight + headHeight + betInfoHeight + orderHeight + detailPhotoViewHeight + kTopMar * 3) {
                contentHeight = noteHeight + headHeight + betInfoHeight + orderHeight + detailPhotoViewHeight + kTopMar * 3;
            }
            [self.contentScrollView setContentSize:CGSizeMake(kWidth, contentHeight)];

        }else{
            [STTextHudTool showErrorText:responseObject[@"msg"] withSecond:1];
        }
    } failure:^(NSError *error) {
        [STTextHudTool showErrorText:@"网络连接失败,请稍后重试" withSecond:1];
    }];
    
}

-(void)reloadOrderView{
    
    NSMutableDictionary * items = [NSMutableDictionary new];
    
    if ([self isItemWith:self.detailModel.nickName]) {
        [items setValue:self.detailModel.nickName forKey:@"用 户 名 : "];
    }
    
    if ([self isItemWith:self.detailModel.mobile]) {
        [items setValue:self.detailModel.mobile forKey:@"手 机 号 : "];
    }
    
    if ([self isItemWith:self.detailModel.orderNumber]) {
        [items setValue:self.detailModel.orderNumber forKey:@"订单编号 : "];
    }
    
    if ([self isItemWith:self.detailModel.createDate]) {
        [items setValue:self.detailModel.createDate forKey:@"购买时间 : "];
    }
    
    orderHeight = 45 + items.count * 30;
    self.detailOrderView.frame = CGRectMake(0, noteHeight + headHeight + betInfoHeight + kTopMar, kWidth, orderHeight);
    [self.detailOrderView reloadNumberOrderDataWithModel:self.detailModel];
    
}

-(BOOL)isItemWith:(NSString *)item{
    
    if ([item isEqualToString:@""] || !item) {
        return NO;
    }
    if ([item isEqualToString:@"-"]) {
        return  NO;
    }
    
    return  YES;
}

-(void)postTokenRequest
{
   
        [PPNetworkHelper POST:PostToken parameters:nil success:^(id responseObject) {
            NSLog(@"--%@",responseObject);
            if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                self.tokenStr =    [[responseObject objectForKey:@"data"] objectForKey:@"SecurityToken"];
                self.accessKey =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeyId"];
                self.accessKeySecret =    [[responseObject objectForKey:@"data"] objectForKey:@"AccessKeySecret"];
            }
          
        } failure:^(NSError *error) {
            NSLog(@"--%@",error);
        }];
        
  
}
-(void)layoutView
{
    
    self.cancleBtn = [[UIButton alloc]init];
    [self.view addSubview:self.cancleBtn];
    [self.cancleBtn setTitleColor:kGrayColor forState:(UIControlStateNormal)];
    [self.cancleBtn setTitle:@"撤单" forState:(UIControlStateNormal)];
     [self.cancleBtn addTarget:self action:@selector(cancleAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.cancleBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.785];
    self.cancleBtn.backgroundColor = [UIColor whiteColor];
    [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2);
        make.height.mas_equalTo(kWidth/7.653);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(kHeight-NavgationBarHeight-TabBarHeight);
    }];
    
    self.outBtn = [[UIButton alloc]init];
    [self.view addSubview:self.outBtn];
    [self.outBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.outBtn setTitle:@"出票" forState:(UIControlStateNormal)];
    self.outBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/26.785];
    [self.outBtn addTarget:self action:@selector(outAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.outBtn.backgroundColor = kRedColor;
    [self.outBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2);
        make.height.mas_equalTo(kWidth/7.653);
        make.left.mas_equalTo(kWidth/2);
        make.top.mas_equalTo(kHeight-NavgationBarHeight-TabBarHeight);
    }];
}

#pragma mark --------- 撤单 -----------

-(void)cancleAction
{
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        @WeakObj(self)
        
        NSDictionary *dic = @{
                              @"orderTotalId":self.orderId
                              };
        [PPNetworkHelper POST:PostCancleOrder parameters:dic success:^(id responseObject) {
            
            @StrongObj(self)
            
            if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                if (self.NextViewControllerBlock) {
                    self.NextViewControllerBlock(1);
                }
                [STTextHudTool showText:@"订单撤销成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
            self.isClicking = NO;
            
        } failure:^(NSError *error) {
            self.isClicking = NO;
            [STTextHudTool showText:@"订单撤销失败"];
        }];
        
    }
}

#pragma mark ---  出票  ---
-(void)outAction
{
    
    if (!self.isClicking) {
        
        self.isClicking = YES;
        
        if ([self.detailModel.isImage isEqualToString:@"1"] && self.photos.count == 0) {
            [FC_Manager showToastWithText:@"请提供彩票照片"];
            self.isClicking = NO;
            return;
        }
        
        NSDictionary *dic = @{
                              @"type":self.strType,
                              @"orderTotalId":self.orderId
                              };
        @WeakObj(self)
        
        [PPNetworkHelper POST:PostPrintOrder parameters:dic success:^(id responseObject) {
            
            @StrongObj(self)
            
            if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                if (self.NextViewControllerBlock) {
                    self.NextViewControllerBlock(1);
                }
                [STTextHudTool showText:@"出票成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else
            {
                [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            }
            
            self.isClicking = NO;
            
        } failure:^(NSError *error) {
            
            @StrongObj(self)
            self.isClicking = NO;
            [STTextHudTool showText:@"出票失败"];
            
        }];
        
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
