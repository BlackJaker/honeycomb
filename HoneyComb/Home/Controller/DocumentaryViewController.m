//
//  DocumentaryViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "DocumentaryViewController.h"

#import "FC_DocumentaryCell.h"

#import "UWHttpTool.h"

#import "FC_FollowListModel.h"

#import "GendanDetailViewController.h"

@interface DocumentaryViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
{
    CGFloat  defaultHeadHeight;
}
@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,assign) NSInteger pageNum;

@property (nonatomic,assign) NSInteger pageCount;

@property (nonatomic,assign) NSInteger type;

@property (nonatomic,assign) NSInteger totalNum;

@property (nonatomic,strong) FC_FollowListModel *followListModel;

@property (nonatomic,copy) NSMutableArray * followListData;

@end

@implementation DocumentaryViewController

static NSString *documentaryCellID = @"documentaryCell";

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self propertySetup];
    
    [self layoutView];
    
    [self shopFollowListRequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reload)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

#pragma mark  --  property Setup  --

-(void)propertySetup{
    
    defaultHeadHeight = 45;
    self.pageNum = 1;
    self.pageCount = 10;
    self.type = 0;
    
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    
}

-(void)layoutView
{
    self.tableView = [[UITableView alloc]init];
    self.tableView.dataSource =self;
    self.tableView.delegate = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    [self.tableView makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    @WeakObj(self)
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @StrongObj(self)
        [ self.tableView.mj_footer resetNoMoreData];
        self.pageNum = 1;
        [self shopFollowListRequest];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (self.followListData.count >= self.totalNum) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            self.pageNum ++;
            [self shopFollowListRequest];
        }
    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.followListData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FC_DocumentaryCell *cell = [tableView dequeueReusableCellWithIdentifier:documentaryCellID];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"FC_DocumentaryCell" owner:nil options:nil] firstObject];
    }
    
    FC_FollowItemModel * model = [self.followListData objectAtIndex:indexPath.row];
    
    [cell reloadDocumentaryCellWithModel:model];

    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 115;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FC_FollowItemModel * model = [self.followListData objectAtIndex:indexPath.row];
    
    self.hidesBottomBarWhenPushed = YES;
    GendanDetailViewController *vc = [[GendanDetailViewController alloc] init];
    vc.plan_id = model.plan_id;
    vc.playType = [model.type integerValue];
    [self.navigationController pushViewController:vc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

#pragma mark DZ
-(CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 25;
}
-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"meiyouOrder"];
}
-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    return YES;
}
-(BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateFrameWithMargin:(CGFloat)margin{
    
    CGRect frame = self.tableView.frame;
    frame.size.height = kHeight-NavgationBarHeight-40-TabBarHeight - margin;
    self.tableView.frame = frame;
    
}

#pragma mark  --  request  --

-(void)shopFollowListRequest{
    
    NSDictionary *params = @{@"shop_id":[LoginUser shareLoginUser].shopId,
                             @"page":@(self.pageNum),
                             @"pageCount":@(self.pageCount),
                             @"type":@(self.type)
                             };
    @WeakObj(self)
    [UWHttpTool getWithURL:ShopFollowList
                    params:params
                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                       
                       @StrongObj(self)
                       if ([responseObject[@"status"] integerValue] == 0) {
                           
                           if (self.pageNum == 1) {
                               [self.followListData removeAllObjects];
                               [self.tableView.mj_header endRefreshing];
                           }else{
                               [self.tableView.mj_footer endRefreshing];
                           }
                           
                           self.followListModel = [FC_FollowListModel mj_objectWithKeyValues:responseObject[@"data"]];
                           
                           for (FC_FollowItemModel * model in self.followListModel.list) {
                               [self.followListData addObject:model];
                           }
                           
                           self.totalNum = [self.followListModel.total integerValue];
                           
                           [self.tableView reloadData];
                           
                       }else{
                           [FC_Manager showToastWithText:responseObject[@"msg"]];
                       }
                       
                       if (self.reloadTotalCountBlock) {
                           self.reloadTotalCountBlock(self.totalNum);
                       }
                       
                   } failure:^(NSURLSessionDataTask * _Nullable task, NSString * _Nonnull errorMsg) {
                       [FC_Manager showToastWithText:@"网络链接失败,请稍后重试"];
                       [self.tableView.mj_header endRefreshing];
                       [self.tableView.mj_footer endRefreshing];
                   }];
    
}

-(NSMutableArray *)followListData{
    
    if (!_followListData) {
        _followListData = [NSMutableArray new];
    }
    return _followListData;
}

-(void)reload{

    self.pageNum = 1;
    [self shopFollowListRequest];
    
}

@end
