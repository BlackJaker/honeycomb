//
//  BettingViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ReloadCurrentTotalCountBlock)(NSInteger totalCount);

@interface BettingViewController : AllViewController

@property (nonatomic,copy) NSString *titleStr;

@property (nonatomic,copy) ReloadCurrentTotalCountBlock  reloadTotalCountBlock;

-(void)reloadData;
@end
