//
//  FC_PartBillViewController.h
//  HoneyComb
//
//  Created by afc on 2018/12/15.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FC_PartBillViewController : AllViewController

-(instancetype)initWithOrderTotalId:(NSString *)orderTotalId;

@end

NS_ASSUME_NONNULL_END
