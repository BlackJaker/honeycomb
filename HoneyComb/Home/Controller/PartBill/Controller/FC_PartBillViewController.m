//
//  FC_PartBillViewController.m
//  HoneyComb
//
//  Created by afc on 2018/12/15.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_PartBillViewController.h"

#import "FC_PartBillTableHeadView.h"

#import "FC_PartBillCell.h"

@interface FC_PartBillViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    CGFloat  headViewHeight;
    CGFloat  rowHeight;
}
@property (nonatomic,strong) FC_PartBillTableHeadView * headView;

@property (nonatomic,strong) UITableView * partBillTabelView;

@property (nonatomic,copy) NSMutableArray * partBillDatas;

@property (nonatomic,copy) NSString * orderTotalId;

@end

@implementation FC_PartBillViewController

-(instancetype)initWithOrderTotalId:(NSString *)orderTotalId{
    
    if (self = [super init]) {
        self.orderTotalId = orderTotalId;
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self propertySetup];
    
    [self partBillInfoRequest];
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    self.titleLabe.text = @"拆单列表";
    
    headViewHeight = 125;
    rowHeight = 44;
    
}

#pragma mark  --  table view datasource  --

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.partBillDatas.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * partBillCell = @"partBillCell";
    
    FC_PartBillCell * cell =[tableView dequeueReusableCellWithIdentifier:partBillCell];
    
    if (cell == nil) {
        cell = [[FC_PartBillCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:partBillCell];
    }
    
    cell.indexPath = indexPath;
    cell.bottomLine.hidden = indexPath.row == self.partBillDatas.count - 1?YES:NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary * dic = [self.partBillDatas objectAtIndex:indexPath.row];
    
    [cell reloadPartBillCellWithDictionary:dic];
    
    return cell;
}

#pragma mark  --  table delegate  --

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat  height = rowHeight - 1;
    
    NSDictionary * dictionary = [self.partBillDatas objectAtIndex:indexPath.row];
    
    NSArray * matchs = dictionary[@"match"];
    
    NSString * content = @"";
    
    for (int i = 0; i < [matchs count]; i ++) {
        
        NSDictionary * dic = [matchs objectAtIndex:i];
        
        if (i == 0) {
            content = [NSString stringWithFormat:@"%@ | %@",dic[@"match"],dic[@"content"]];
        }else{
            content = [content stringByAppendingString:[NSString stringWithFormat:@"\n%@ | %@",dic[@"match"],dic[@"content"]]];
        }
    }
    
    CGFloat  labelHeight = [self labelHeightWithString:content];
    
    if (labelHeight > height) {
        return labelHeight;
    }
    
    return  height;
}

- (CGFloat)labelHeightWithString:(NSString *)resultString{
    
    CGFloat  width = kWidth - 45 * 3 - 72 - 4;
    
    CGSize maxSize = CGSizeMake(width, MAXFLOAT);
    
    // 计算内容label的高度
    CGFloat textH = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont fontWithName:kPingFangRegular size:13]} context:nil].size.height;
    return textH + 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return headViewHeight;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return self.headView;
    
}

#pragma mark  --  lazy  --

-(NSMutableArray *)partBillDatas{
    
    if (!_partBillDatas) {
        _partBillDatas = [NSMutableArray new];
    }
    return _partBillDatas;
}

-(UITableView *)partBillTabelView{
    
    if (!_partBillTabelView) {
        
        _partBillTabelView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _partBillTabelView.delegate = self;
        _partBillTabelView.dataSource = self;
        _partBillTabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _partBillTabelView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_partBillTabelView];
        
        [_partBillTabelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(- TAB_BAR_SAFE_HEIGHT);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(0);
        }];
        
    }
    return _partBillTabelView;
}

-(FC_PartBillTableHeadView *)headView{
    
    if (!_headView) {
        
        UINib *nib = [UINib nibWithNibName:@"FC_PartBillTableHeadView" bundle:nil];
        _headView = [[nib instantiateWithOwner:nil options:nil] firstObject];
    }
    return _headView;
}

#pragma mark  --  request  --

-(void)partBillInfoRequest{
    
    [STTextHudTool loadingWithTitle:@"加载中..."];
    
    NSDictionary *dic = @{
                          @"orderTotalId":self.orderTotalId
                          };
    @WeakObj(self)
    [PPNetworkHelper POST:PostSplitOrder parameters:dic success:^(id responseObject) {
        
        @StrongObj(self)
        
        [STTextHudTool hideSTHud];
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            NSLog(@"%@",responseObject);

            [self reloadWithData:responseObject[@"data"]];
            
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
        }
    } failure:^(NSError *error) {
        [STTextHudTool hideSTHud];
        [STTextHudTool showText:@"获取拆单详情失败"];
    }];
    
}

-(void)reloadWithData:(NSDictionary *)data{
    
    // head view
    NSString * string = data[@"mobile"];
    if (string.length > 0 && ![string isEqualToString:@"-"]) {
        NSString *subString = [string substringWithRange:NSMakeRange(3, 4)];
        string = [string stringByReplacingOccurrencesOfString:subString withString:@"****"];
    }
    self.headView.userNameLabel.text = string;
    
    NSString * betInfoString = [NSString stringWithFormat:@"%@ 注 %@ 倍 %@ 元",data[@"account"],data[@"times"],data[@"price"]];
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc]initWithString:betInfoString attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#999999"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    
    NSRange betRange = NSMakeRange(0, [data[@"account"] length]);
    NSRange timeRange = NSMakeRange([data[@"account"] length] + 3, [data[@"times"] length]);
    NSRange priceRange = NSMakeRange(betInfoString.length - [data[@"price"] length] - 2, [data[@"price"] length]);
    
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#333333"] range:betRange];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#333333"] range:timeRange];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#fd2525"] range:priceRange];
    
    self.headView.betInfoLabel.attributedText = attributedString;
    
    //
    
    [self.partBillDatas removeAllObjects];
    
    for (NSDictionary * dic in data[@"group"]) {
        
        [self.partBillDatas addObject:dic];
        
    }
    
    [self.partBillTabelView reloadData];
    
}

@end
