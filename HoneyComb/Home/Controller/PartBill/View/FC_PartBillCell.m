//
//  FC_PartBillCell.m
//  HoneyComb
//
//  Created by afc on 2018/12/17.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_PartBillCell.h"

#define kNumWidth  45
#define kBonusWidth 72

@implementation FC_PartBillCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self contentSetup];
    }
    return self;
}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    UIView * view = [self viewWithTag:1];
    [view removeFromSuperview];
    
    for (int i = 0; i < 4; i ++) {
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = RGB(235, 235, 235, 1.0f);
        line.tag = 1;
        [self addSubview:line];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i < 2) {
                make.left.mas_equalTo(kNumWidth * (i + 1) + i);
            }else{
                if (i == 3) {
                    make.right.mas_equalTo(-(kNumWidth + kBonusWidth + 1));
                }else{
                    make.right.mas_equalTo(- kBonusWidth);
                }
            }
            make.width.mas_equalTo(1);
            make.bottom.mas_equalTo(1);
            
            make.top.mas_equalTo(0);
        }];
    }
    
    self.bottomLine.hidden = NO;
    
}

#pragma mark  --  lazy  --

-(UILabel *)numLabel{
    
    if (!_numLabel) {
        
        _numLabel = [[UILabel alloc]init];
        [self updateLabel:_numLabel];
        [self addSubview:_numLabel];
        
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kNumWidth);
            make.bottom.mas_equalTo(1);
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(0);
        }];
        
    }
    return _numLabel;
}

-(void)updateLabel:(UILabel *)label{
    label.font = [UIFont fontWithName:kPingFangRegular size:13];
    label.textColor = [UIColor colorWithHexString:@"#333333"];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.text = @"-";
}

-(UILabel *)bonusLabel{
    
    if (!_bonusLabel) {
        
        _bonusLabel = [[UILabel alloc]init];
        [self updateLabel:_bonusLabel];
        [self addSubview:_bonusLabel];
        
        [_bonusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kBonusWidth);
            make.bottom.mas_equalTo(1);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(0);
        }];
        
    }
    return _bonusLabel;
}

-(UILabel *)bunchLabel{
    
    if (!_bunchLabel) {
        
        _bunchLabel = [[UILabel alloc]init];
        [self updateLabel:_bunchLabel];
        [self addSubview:_bunchLabel];
        
        [_bunchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kNumWidth);
            make.bottom.mas_equalTo(1);
            make.left.mas_equalTo(self.numLabel.mas_right);
            make.top.mas_equalTo(0);
        }];
        
    }
    return _bunchLabel;
}

-(UILabel *)timesLabel{
    
    if (!_timesLabel) {
        
        _timesLabel = [[UILabel alloc]init];
        [self updateLabel:_timesLabel];
        [self addSubview:_timesLabel];
        
        [_timesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(kNumWidth);
            make.bottom.mas_equalTo(1);
            make.right.mas_equalTo(self.bonusLabel.mas_left);
            make.top.mas_equalTo(0);
        }];
        
    }
    return _timesLabel;
}

-(UILabel *)groupLabel{
    
    if (!_groupLabel) {
        
        _groupLabel = [[UILabel alloc]init];
        [self updateLabel:_groupLabel];
        [self addSubview:_groupLabel];
        
        [_groupLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.bunchLabel.mas_right);
            make.bottom.mas_equalTo(1);
            make.right.mas_equalTo(self.timesLabel.mas_left);
            make.top.mas_equalTo(0);
        }];
        
    }
    return _groupLabel;
}

-(UILabel *)bottomLine{
    
    if (!_bottomLine) {
        
        _bottomLine = [[UILabel alloc]init];
        _bottomLine.backgroundColor = RGB(235, 235, 235, 1.0f);
        [self addSubview:_bottomLine];
        
        [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(1);
        }];
        
    }
    return _bottomLine;
}

-(void)reloadPartBillCellWithDictionary:(NSDictionary *)dictionary{
    
    self.numLabel.text = [NSString stringWithFormat:@"%ld",self.indexPath.row + 1];
    
    NSString * bunchStr = dictionary[@"bunch"];
    if ([bunchStr isEqualToString:@"1"]) {
        bunchStr = @"单关";
    }else{
        bunchStr = [NSString stringWithFormat:@"%@串1",bunchStr];
    }
    self.bunchLabel.text = [NSString stringWithFormat:@"%@",bunchStr];
    self.timesLabel.text = dictionary[@"times"];
    
    NSArray * matchs = dictionary[@"match"];
    
    NSString * content = @"";
    
    for (int i = 0; i < [matchs count]; i ++) {
        
        NSDictionary * dic = [matchs objectAtIndex:i];
        
        if (i == 0) {
            content = [NSString stringWithFormat:@"%@ | %@",dic[@"match"],dic[@"content"]];
        }else{
            content = [content stringByAppendingString:[NSString stringWithFormat:@"\n%@ | %@",dic[@"match"],dic[@"content"]]];
        }
    }
    
    self.groupLabel.text = content;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
