//
//  FC_PartBillCell.h
//  HoneyComb
//
//  Created by afc on 2018/12/17.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_PartBillCell : UITableViewCell

@property (nonatomic,strong) NSIndexPath * indexPath;

@property (nonatomic,strong) UILabel * numLabel;
@property (nonatomic,strong) UILabel * bunchLabel;
@property (nonatomic,strong) UILabel * groupLabel;
@property (nonatomic,strong) UILabel * timesLabel;
@property (nonatomic,strong) UILabel * bonusLabel;
@property (nonatomic,strong) UILabel * bottomLine;

-(void)reloadPartBillCellWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
