//
//  FC_SortView.m
//  HoneyComb
//
//  Created by afc on 2019/1/21.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import "FC_SortView.h"

@interface FC_SortView ()
{
    CGFloat backWidth;
    CGFloat labelWidth;
}
@end

@implementation FC_SortView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self propertySetup];
        
    }
    return self;
}

#pragma mark  --  property setup  --

-(void)propertySetup{
    
    backWidth = 125;
    labelWidth = kWidth - backWidth - kLeftMar;
    self.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.sortItemButton];
    [self.label setBackgroundColor:[UIColor clearColor]];
}

#pragma mark  --  lazy  --

-(UIButton *)sortItemButton{
    
    if (!_sortItemButton) {
        _sortItemButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, backWidth, self.height)];
        _sortItemButton.selected = NO;
        [_sortItemButton setTitle:@"当前-期" forState:UIControlStateNormal];
        _sortItemButton.titleLabel.font = [UIFont fontWithName:kMedium size:12];
        [_sortItemButton setTitleColor:[UIColor colorWithHexString:@"#7F8491"] forState:UIControlStateNormal];
        [_sortItemButton setImage:[UIImage imageNamed:@"ic_xiangxia"] forState:UIControlStateNormal];
        [_sortItemButton setImage:[UIImage imageNamed:@"ic_xiangshang"] forState:UIControlStateSelected];
        
        [self reloadTitleButtonEdgeInsets];
        [_sortItemButton addTarget:self action:@selector(issueChooseAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _sortItemButton;
    
}

-(void)reloadTitleButtonEdgeInsets{
    
    [_sortItemButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -_sortItemButton.imageView.frame.size.width - 2, 0, _sortItemButton.imageView.frame.size.width + 2)];
    [_sortItemButton setImageEdgeInsets:UIEdgeInsetsMake(0, _sortItemButton.titleLabel.frame.size.width + 2, 0,-_sortItemButton.titleLabel.frame.size.width - 2)];
    
}

-(void)issueChooseAction:(UIButton *)sender{
    
    sender.selected = !sender.selected;
    
    if (self.choosedBlock) {
        self.choosedBlock(sender.selected);
    }
}

-(UILabel *)label{
    
    if (!_label) {
        
        _label = [[UILabel alloc]init];
        _label.textAlignment = NSTextAlignmentRight;
        _label.text = @"";
        _label.font = [UIFont fontWithName:kPingFangRegular size:13];
        _label.textColor = [UIColor colorWithHexString:@"#7F8491"];
        [self addSubview:_label];
        
        [_label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.right.mas_equalTo(-kLeftMar);
            make.width.mas_equalTo(labelWidth);
            make.bottom.mas_equalTo(0);
        }];
        
    }
    return _label;
}

#pragma mark  --  reload  --

-(void)reloadWithInfo:(NSDictionary *)info{
    [self.sortItemButton setTitle:info[@"issue"] forState:UIControlStateNormal];
    self.label.text = [NSString stringWithFormat:@"%@截止",info[@"time"]];
}


@end
