//
//  FC_SortView.h
//  HoneyComb
//
//  Created by afc on 2019/1/21.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^DismissBlock)(void);

typedef void(^ChoosedBlock)(NSInteger isSelected);

@interface FC_SortView : UIView

@property (nonatomic,copy) ChoosedBlock choosedBlock;

@property (nonatomic,copy) DismissBlock dismissBlock;

@property (nonatomic,strong) UIButton * sortItemButton;

@property (nonatomic,strong) UILabel * label;

-(void)reloadWithInfo:(NSDictionary *)info;

-(void)reloadTitleButtonEdgeInsets;

@end

NS_ASSUME_NONNULL_END
