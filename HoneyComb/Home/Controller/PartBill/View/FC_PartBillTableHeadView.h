//
//  FC_PartBillTableHeadView.h
//  HoneyComb
//
//  Created by afc on 2018/12/15.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_PartBillTableHeadView : UIView

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *betInfoLabel;

@end

NS_ASSUME_NONNULL_END
