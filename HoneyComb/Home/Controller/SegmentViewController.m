//
//  SegmentViewController.m
//  CLSegment
//
//  Created by CL on 2018/4/9.
//  Copyright © 2018年 cl. All rights reserved.
//

#import "SegmentViewController.h"

#import "FC_HomeIdentView.h"

#import "AppNameViewController.h"

#import "AF_StoreCertificationVC.h"

#import <objc/objc.h>

#define HEADBTN_TAG                 10000
#define Default_BottomLineColor     themeColor
#define Default_BottomLineHeight    2
#define Default_ButtonHeight        45
#define Default_BottomCount         3

#define Default_HeadViewBackgroundColor  [UIColor whiteColor]
#define Default_FontSize            16

///获取屏幕宽高
#define HK_Screen_Width         [[UIScreen mainScreen] bounds].size.width
#define HK_Screen_Height        [[UIScreen mainScreen] bounds].size.height



@interface SegmentViewController ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView  *headerView;
@property (nonatomic, strong) UIScrollView  *mainScrollView;
@property (nonatomic, strong) UIView        *lineView;
@property (nonatomic, assign) NSInteger     selectIndex;
@property (nonatomic, strong) FC_HomeIdentView        *homeIdentView;

@property (nonatomic, copy) NSString * shopStatus;
@property (nonatomic, copy) NSString * appName;

@end

@implementation SegmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    
    self.appName = @"";
    
}

- (void)initSegment
{
    [self addButtonInScrollHeader:_titleArray];
    [self addContentViewScrollView:_subViewControllers];
}

/*!
 *  @brief  根据传入的title数组新建button显示在顶部scrollView上
 *
 *  @param titleArray  title数组
 */
- (void)addButtonInScrollHeader:(NSArray *)titleArray
{
    
    self.headerView.backgroundColor = self.headViewBackgroundColor;
    
    for (NSInteger index = 0; index < titleArray.count; index++) {
        UIButton *segmentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        segmentBtn.frame = CGRectMake(self.buttonWidth * index, 0, self.buttonWidth, self.buttonHeight);
        [segmentBtn setTitle:titleArray[index] forState:UIControlStateNormal];
        segmentBtn.titleLabel.font = [UIFont systemFontOfSize:self.fontSize];
        segmentBtn.tag = index + HEADBTN_TAG;
        [segmentBtn setTitleColor:self.titleColor forState:UIControlStateNormal];
        [segmentBtn setTitleColor:self.titleSelectedColor forState:UIControlStateSelected];
        [segmentBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.headerView addSubview:segmentBtn];
        if (index == 0) {
            segmentBtn.selected = YES;
            segmentBtn.titleLabel.font = [UIFont boldSystemFontOfSize:20];
            self.selectIndex = segmentBtn.tag;
        }
    }
    
    NSInteger lineWidth = 30;
    NSInteger margin=(self.buttonWidth-lineWidth)/2;
   
    _lineView = [[UIView alloc] initWithFrame:CGRectMake(margin, self.buttonHeight - self.bottomLineHeight, lineWidth, self.bottomLineHeight)];
    _lineView.backgroundColor = self.bottomLineColor;
    [self.headerView addSubview:_lineView];
}

-(FC_HomeIdentView *)homeIdentView{
    
    if (!_homeIdentView) {
        _homeIdentView = [[FC_HomeIdentView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0)];
        _homeIdentView.backgroundColor = [UIColor colorWithHexString:@"#ffeeec"];
        [self.view addSubview:_homeIdentView];
        
        @WeakObj(self)
        _homeIdentView.identBlock = ^{
            @StrongObj(self)
            [self goToIdentification];
        };
    }
    return _homeIdentView;
}

-(void)goToIdentification{
    
        AF_StoreCertificationVC *storeCertVc = [[AF_StoreCertificationVC alloc]init];
        storeCertVc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:storeCertVc animated:YES];
    
}

/*!
 *  @brief  根据传入的viewController数组，将viewController的view添加到显示内容的scrollView
 *
 *  @param subViewControllers  viewController数组
 */
- (void)addContentViewScrollView:(NSArray *)subViewControllers
{
    
    NSInteger  bottomHeight = [self.source isEqualToString:@"home"]?TAB_BAR_HEIGHT:TAB_BAR_SAFE_HEIGHT;
    
    _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.buttonHeight, HK_Screen_Width, HK_Screen_Height - NAVIGATION_HEIGHT - bottomHeight - self.buttonHeight)];
    [_mainScrollView setScrollEnabled:YES];
    [_mainScrollView setPagingEnabled:YES];
    _mainScrollView.backgroundColor = RGB(249, 249, 249, 1);
    [_mainScrollView setShowsVerticalScrollIndicator:NO];
    [_mainScrollView setShowsHorizontalScrollIndicator:NO];
    _mainScrollView.bounces = NO;
    _mainScrollView.delegate = self;
    
    [self.view addSubview:_mainScrollView];
    
    _mainScrollView.contentSize = CGSizeMake(HK_Screen_Width * self.subViewControllers.count, HK_Screen_Height - NAVIGATION_HEIGHT - bottomHeight - self.buttonHeight);

    
    [subViewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        
        UIViewController *viewController = (UIViewController *)_subViewControllers[idx];
        viewController.view.frame = CGRectMake(idx * HK_Screen_Width, 0, HK_Screen_Width, _mainScrollView.frame.size.height);
        
        [_mainScrollView addSubview:viewController.view];
        [self addChildViewController:viewController];
    }];
}

- (void)addParentController:(UIViewController *)viewController
{
    if ([viewController respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        viewController.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [viewController addChildViewController:self];
    [viewController.view addSubview:self.view];
}

#pragma mark 点击按钮方法

#pragma mark 点击按钮方法

- (void)btnClick:(UIButton *)button
{
    [self uploadCurrentTag:button.tag];
}

-(void)uploadCurrentTag:(NSInteger)tag{
    
    [_mainScrollView scrollRectToVisible:CGRectMake((tag - HEADBTN_TAG) *HK_Screen_Width, 0, HK_Screen_Width, _mainScrollView.frame.size.height) animated:YES];
    [self didSelectSegmentIndex:tag];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hiddenAction" object:nil];
    
}

-(void)reloadTotalCountWithIndex:(NSInteger)index count:(NSInteger)count{
    
    UIButton * button = [self.view viewWithTag:index + 10000];
    NSArray * array = [button.titleLabel.text componentsSeparatedByString:@" "];
    [button setTitle:[NSString stringWithFormat:@"%@ (%ld)",[array firstObject],count] forState:UIControlStateNormal];

}

/*!
 *  @brief  设置顶部选中button下方线条位置
 *
 *  @param index 第几个
 */
- (void)didSelectSegmentIndex:(NSInteger)index
{
    UIButton *btn = (UIButton *)[self.view viewWithTag:self.selectIndex];
    btn.selected = NO;
    btn.titleLabel.font = [UIFont fontWithName:kMedium size:15];
    self.selectIndex = index;
    UIButton *currentSelectBtn = (UIButton *)[self.view viewWithTag:index];
    currentSelectBtn.selected = YES;
    currentSelectBtn.titleLabel.font = [UIFont boldSystemFontOfSize:20];
    CGRect rect = self.lineView.frame;
    
    NSInteger lineWidth=52;
    NSInteger margin=(self.buttonWidth-lineWidth)/2;
    
    rect.origin.x = (index - HEADBTN_TAG) * (HK_Screen_Width/_bottomCount)+margin;
    [UIView animateWithDuration:0.3 animations:^{
        self.lineView.frame = rect;
    }];
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hiddenAction" object:nil];
    
    if (scrollView.contentOffset.x<=960) {
        if (scrollView == _mainScrollView) {
            float xx = scrollView.contentOffset.x * (_buttonWidth / HK_Screen_Width) - _buttonWidth;
            [_headerView scrollRectToVisible:CGRectMake(xx, 0, HK_Screen_Width, _headerView.frame.size.height) animated:YES];
            NSInteger currentIndex = scrollView.contentOffset.x / HK_Screen_Width;
            [self didSelectSegmentIndex:currentIndex + HEADBTN_TAG];
        }
    }else{
        [scrollView setScrollEnabled:NO];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
{
    float xx = scrollView.contentOffset.x * (_buttonWidth / HK_Screen_Width) - _buttonWidth;
    [_headerView scrollRectToVisible:CGRectMake(xx, 0, HK_Screen_Width, _headerView.frame.size.height) animated:YES];
    
    
}

#pragma mark - setter/getter
- (UIScrollView *)headerView
{
    if (_headerView == nil) {
        _headerView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kWidth, self.buttonHeight)];
        [_headerView setShowsVerticalScrollIndicator:NO];
        [_headerView setShowsHorizontalScrollIndicator:NO];
        _headerView.bounces = NO;
        
        [self.view addSubview:self.headerView];
        
        if (_segmentHeaderType == 0) {
            self.headerView.contentSize = CGSizeMake(self.buttonWidth * _titleArray.count, self.buttonHeight);
        }else{
            self.headerView.contentSize = CGSizeMake(HK_Screen_Width, self.buttonHeight);
        }
    }
    return _headerView;
}

- (UIColor *)headViewBackgroundColor
{
    if (_headViewBackgroundColor == nil) {
        _headViewBackgroundColor = Default_HeadViewBackgroundColor;
    }
    return _headViewBackgroundColor;
}

- (UIColor *)titleColor
{
    if (_titleColor == nil) {
        _titleColor = kGrayColor;
    }
    return _titleColor;
}

- (UIColor *)titleSelectedColor
{
    if (_titleSelectedColor == nil) {
        _titleSelectedColor = kGrayColor;
    }
    return _titleSelectedColor;
}

- (CGFloat)fontSize
{
    if (_fontSize == 0) {
        _fontSize = Default_FontSize;
    }
    return _fontSize;
}

- (CGFloat)buttonWidth
{
    if (_buttonWidth == 0) {
        _buttonWidth = HK_Screen_Width / 3;
    }
    return _buttonWidth;
}

- (CGFloat)buttonHeight
{
    if (_buttonHeight == 0) {
        _buttonHeight = Default_ButtonHeight;
    }
    return _buttonHeight;
}

- (CGFloat)bottomLineHeight
{
    if (_bottomLineHeight == 0) {
        _bottomLineHeight = Default_BottomLineHeight;
    }
    return _bottomLineHeight;
}

- (CGFloat)bottomCount
{
    if (_bottomCount == 0) {
        _bottomCount = Default_BottomCount;
    }
    return _bottomCount;
}

- (UIColor *)bottomLineColor
{
    if (_bottomLineColor == nil) {
        _bottomLineColor = kRedColor;
    }
    return _bottomLineColor;
}

#pragma mark  --  view will applear  --

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if ([self.source isEqualToString:@"home"]) {
        self.shopStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"shopStatus"];
        
//        if (!self.shopStatus || [self.shopStatus isEqualToString:@""] || [self.shopStatus isEqualToString:@"UNVERIFIED"] || [self.shopStatus isEqualToString:@"REJECT"]) {
        [self homeGetIndexRequest];   // 获取首页认证信息
//        }
    }
}

#pragma mark  --  request  --

-(void)homeGetIndexRequest{
    
    NSDictionary * getIndexDic = @{
                                   @"shopId":[LoginUser shareLoginUser].shopId
                                   };
    @WeakObj(self)
    [PPNetworkHelper POST:GetIndex
               parameters:getIndexDic
                  success:^(id responseObject) {
                      
                      @StrongObj(self)
                      
                      if ([responseObject[@"state"] isEqualToString:@"success"]) {
                          self.shopStatus = responseObject[@"data"][@"shopStatus"];
                          self.appName = responseObject[@"data"][@"appName"];
                          [[NSUserDefaults standardUserDefaults] setValue:self.shopStatus forKey:@"shopStatus"];
                          [self  reload];
                      }
 
                  } failure:^(NSError *error) {
                      
                  }];
    
}

-(void)reload{
    
    CGFloat  height = ([self.shopStatus isEqualToString:@"UNVERIFIED"] || [self.shopStatus isEqualToString:@"REJECT"])?70:0;
    
    self.headerView.frame = CGRectMake(0, 0, kWidth, self.buttonHeight);
    self.homeIdentView.frame = CGRectMake(0, self.buttonHeight, kWidth, height);
    
    NSInteger  bottomHeight = [self.source isEqualToString:@"home"]?TAB_BAR_HEIGHT:TAB_BAR_SAFE_HEIGHT;
    
    _mainScrollView.frame = CGRectMake(0, self.buttonHeight + height, HK_Screen_Width, HK_Screen_Height - NAVIGATION_HEIGHT - bottomHeight - self.buttonHeight - height);
     _mainScrollView.contentSize = CGSizeMake(HK_Screen_Width * self.subViewControllers.count, HK_Screen_Height - NAVIGATION_HEIGHT - bottomHeight - self.buttonHeight - height);
    
    if (height == 70) {
        self.homeIdentView.hidden = NO;
        [self.homeIdentView reload];
    }else{
        self.homeIdentView.hidden = YES;
    }
    
    for (AllViewController * controller in self.subViewControllers) {
        
        [controller updateFrameWithMargin:height];
    }
    
}

@end
