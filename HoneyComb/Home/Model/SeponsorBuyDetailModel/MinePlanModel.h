//
//  MinePlanModel.h
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/17.
//  Copyright © 2018 afc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MinePlanModel : NSObject
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *all_money;
@property (nonatomic, copy) NSString *commission;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *create_date;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, copy) NSString *order_status;
@property (nonatomic, copy) NSString *order_total_id;
@property (nonatomic, copy) NSString *play_name;
@property (nonatomic, copy) NSString *result;
@property (nonatomic, copy) NSString *secret;
@property (nonatomic, copy) NSString *shop_id;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *status;   //1可购买，2截止，3完成
@property (nonatomic, copy) NSString *statusStr;
@property (nonatomic, copy) NSString *shop_date;
@property (nonatomic, copy) NSString *total_plan_commission;
@property (nonatomic, copy) NSString *total_platform_commission;
@property (nonatomic, copy) NSString *total_shop_commission;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *user_id;
@end


@class MinePlanDetailPalyInfo;
@class MinePlanDetailGameInfo;
@interface MinePlanDetailModel : NSObject
@property (nonatomic, copy) NSString *award;
@property (nonatomic, copy) NSString *commission;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *create_date;
@property (nonatomic, copy) NSString *follow_count;
@property (nonatomic, strong)MinePlanDetailGameInfo *games_info;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, copy) NSString *order_status;
@property (nonatomic, copy) NSString *order_statusStr;
@property (nonatomic, copy) NSString *pay_money;
@property (nonatomic, copy) NSString *plan_commission;
@property (nonatomic, copy) NSString *plan_id;
@property (nonatomic, copy) NSString *plan_user;
@property (nonatomic, strong) MinePlanDetailPalyInfo *play_info;
@property (nonatomic, copy) NSString *type;
@end

@interface MinePlanDetailPalyInfo : NSObject
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *bunch;
@property (nonatomic, copy) NSString *times;
@end

@class MinePlanDetailListModel;
@class MineDaLeTouPlanDetailListModel;
@interface MinePlanDetailGameInfo : NSObject
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *award;
@property (nonatomic, copy) NSString *bunch;
@property (nonatomic, copy) NSString *cancelDate;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, strong) NSArray <MinePlanDetailListModel *>*detailList;
@property (nonatomic, strong) NSArray <MineDaLeTouPlanDetailListModel *>*daLeTouDetailList;
@property (nonatomic, strong) NSURL *image;
@property (nonatomic, copy) NSString *isImage;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *orderNumber;
@property (nonatomic, copy) NSString *playName;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *times;
@end

@interface MinePlanDetailListModel : NSObject
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *dan;
@property (nonatomic, copy) NSString *hostTeam;
@property (nonatomic, copy) NSString *league;
@property (nonatomic, copy) NSString *letCount;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *orderDetailId;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *visitingTeam;
@property (nonatomic, copy) NSString *result;

@end

@interface MineDaLeTouPlanDetailListModel : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *orderTotalId;
@property (nonatomic, copy) NSString *redBile;
@property (nonatomic, copy) NSString *blue;
@property (nonatomic, copy) NSString *red;
@property (nonatomic, copy) NSString *blueBile;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *bunch;


@end

NS_ASSUME_NONNULL_END
