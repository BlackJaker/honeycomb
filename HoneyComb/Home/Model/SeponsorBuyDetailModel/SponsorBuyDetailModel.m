//
//  SponsorBuyDetailModel.m
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/16.
//  Copyright © 2018 afc. All rights reserved.
//

#import "SponsorBuyDetailModel.h"

@implementation SponsorCaiYouInfo

@end

@implementation SponsorBuyDetailModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"caiyou":[SponsorCaiYouInfo class]
             };
}
@end
