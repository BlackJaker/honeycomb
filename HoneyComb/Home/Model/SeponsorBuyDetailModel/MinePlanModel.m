//
//  MinePlanModel.m
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/17.
//  Copyright © 2018 afc. All rights reserved.
//

#import "MinePlanModel.h"

@implementation MinePlanModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}

- (NSString *)statusStr{
    return [NSString stateStringWithType:[_status integerValue]];
}

@end

@implementation MinePlanDetailModel
- (NSString *)order_statusStr{
    return [NSString stateStringWithType:[_order_status integerValue]];
}
@end

@implementation MinePlanDetailPalyInfo

@end

@implementation MinePlanDetailGameInfo
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"detailList":[MinePlanDetailListModel class],
             @"daLeTouDetailList":[MineDaLeTouPlanDetailListModel class]
             };
}
@end

@implementation MinePlanDetailListModel

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key{
    
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property{

    if (!oldValue) {// 以字符串类型为例
        return  @"-";
    }
    
    return oldValue;
}

@end

@implementation MineDaLeTouPlanDetailListModel

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key{
    
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property{
    
    if (!oldValue) {// 以字符串类型为例
        return  @"-";
    }
    
    return oldValue;
}

@end
