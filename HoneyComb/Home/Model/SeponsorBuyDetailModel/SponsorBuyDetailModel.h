//
//  SponsorBuyDetailModel.h
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/16.
//  Copyright © 2018 afc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SponsorCaiYouInfo :  NSObject
@property (nonatomic, copy) NSString *baifenbi;
@property (nonatomic, strong) NSURL *img;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *subscription;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *yuji;
@end

@interface SponsorBuyDetailModel : NSObject
@property (nonatomic, copy) NSString *award;
@property (nonatomic, copy) NSString *bao;
@property (nonatomic, copy) NSString *baomi;
@property (nonatomic, copy) NSString *commission;
@property (nonatomic, strong) NSArray <SponsorCaiYouInfo *>*caiyou;
@property (nonatomic, copy) NSString *danfen;
@property (nonatomic, copy) NSString *describe;
@property (nonatomic, copy) NSString *guard;
@property (nonatomic, copy) NSString *guard_price;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, strong) NSURL *img;
@property (nonatomic, copy) NSString *issue;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *numberid;
@property (nonatomic, copy) NSString *order_total_id;
@property (nonatomic, copy) NSString *people;
@property (nonatomic, copy) NSString *play_name;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *secrecy;
@property (nonatomic, copy) NSString *shengyu;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *statusName;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *unitPrice;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *yigou;
@property (nonatomic, copy) NSString *yigoufenshu;
@property (nonatomic, copy) NSString *stopTime;
@end

NS_ASSUME_NONNULL_END
