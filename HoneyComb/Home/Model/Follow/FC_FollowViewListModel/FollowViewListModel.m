//
//  FollowViewListModel.m
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/14.
//  Copyright © 2018 afc. All rights reserved.
//

#import "FollowViewListModel.h"

@implementation FollowViewListModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}

- (NSString *)statusStr{

    return [NSString stateStringWithType:[_status integerValue]];
}

@end

@implementation FollowViewDetailModel
- (NSString *)order_statusStr{

    return [NSString stateStringWithType:[_order_status integerValue]];
}

@end

@implementation FollowViewDetailPlayInfoModel


@end
