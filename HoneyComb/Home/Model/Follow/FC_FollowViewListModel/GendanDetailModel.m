//
//  GendanDetailModel.m
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/12.
//  Copyright © 2018 afc. All rights reserved.
//

#import "GendanDetailModel.h"

@implementation GendanDetailModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"detailList":[GendanDetailListInfoModel class]
             };
}
@end

@implementation GendanDetailListInfoModel

@end
