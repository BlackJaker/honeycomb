//
//  FollowViewListModel.h
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/14.
//  Copyright © 2018 afc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GendanDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FollowViewListModel : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *shop_id;
@property (nonatomic, copy) NSString *play_name;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *status;//订单状态（0未付款，1已付款，2已出票，3已中奖，4未中奖,5已派奖,6已撤单,7已取票）
@property (nonatomic, copy) NSString *statusStr;//解析后的状态
@property (nonatomic, copy) NSString *award;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *is_image;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *issue;
@property (nonatomic, copy) NSString *create_date;
@property (nonatomic, copy) NSString *print_date;
@property (nonatomic, copy) NSString *finish_date;
@property (nonatomic, copy) NSString *cancel_date;
@property (nonatomic, copy) NSString *classes;
@property (nonatomic, copy) NSString *plan_order_id;
@property (nonatomic, copy) NSString *del_flag;
@end

@class FollowViewDetailPlayInfoModel;

@interface FollowViewDetailModel : NSObject

@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *commission;
@property (nonatomic, strong) GendanDetailModel *games_info;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *create_date;
@property (nonatomic, copy) NSString *follow_count;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, strong) FollowViewDetailPlayInfoModel *play_info;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *order_status;
@property (nonatomic, copy) NSString *order_statusStr;
@property (nonatomic, copy) NSString *pay_money;
@property (nonatomic, copy) NSString *play_name;
@property (nonatomic, copy) NSString *result;
@property (nonatomic, copy) NSString *shop_commission;
@property (nonatomic, copy) NSString *shop_follow_count;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *statusStr;
@property (nonatomic, copy) NSString *total_commission;
@property (nonatomic, copy) NSString *total_money;
@property (nonatomic, copy) NSString *total_plan_commission;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *user_id;

@end

@interface FollowViewDetailPlayInfoModel : NSObject
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *bunch;
@property (nonatomic, copy) NSString *times;
@end

NS_ASSUME_NONNULL_END
