//
//  GendanDetailModel.h
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/12.
//  Copyright © 2018 afc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class GendanDetailListInfoModel;
@interface GendanDetailModel : NSObject

@property (nonatomic, copy) NSString *account;  //注数
@property (nonatomic, copy) NSString *award;    //奖金
@property (nonatomic, copy) NSString *bunch;    //串
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, strong) NSArray <GendanDetailListInfoModel *>*detailList;
@property (nonatomic, strong) NSURL *image;
@property (nonatomic, copy) NSString *isImage;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *orderNumber;
@property (nonatomic, copy) NSString *playName; //类型
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *times;    //倍数
@end

@interface GendanDetailListInfoModel : NSObject
@property (nonatomic, copy) NSString *content;  //押注内容
@property (nonatomic, copy) NSString *dan;
@property (nonatomic, copy) NSString *hostTeam; //主队
@property (nonatomic, copy) NSString *league;   //联盟名称
@property (nonatomic, copy) NSString *letCount; //是否胆场
@property (nonatomic, copy) NSString *number;   //赛事编号
@property (nonatomic, copy) NSString *orderDetailId;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *visitingTeam; //客队
@end

NS_ASSUME_NONNULL_END
