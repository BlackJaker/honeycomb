//
//  FC_FollowListModel.m
//  HoneyComb
//
//  Created by afc on 2018/12/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_FollowListModel.h"


@implementation FC_FollowItemModel

@end

@implementation FC_FollowListModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"list":[FC_FollowItemModel class]
            };
}

@end



