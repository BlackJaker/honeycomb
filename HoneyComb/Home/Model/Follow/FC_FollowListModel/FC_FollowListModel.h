//
//  FC_FollowListModel.h
//  HoneyComb
//
//  Created by afc on 2018/12/20.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class FC_FollowItemModel;

@interface FC_FollowListModel : NSObject

@property (nonatomic, copy) NSString *hasmore;
@property (nonatomic, strong) NSArray <FC_FollowItemModel *>*list;
@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *page;

@end

@interface FC_FollowItemModel : NSObject

@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *play_name;
@property (nonatomic, copy) NSString *all_money;
@property (nonatomic, copy) NSString *follow_count;
@property (nonatomic, copy) NSString *commission;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *plan_id;
@property (nonatomic, copy) NSString *money;

@end


NS_ASSUME_NONNULL_END
