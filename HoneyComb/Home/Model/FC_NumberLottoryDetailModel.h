//
//  FC_NumberLottoryDetailModel.h
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_NumberLottoryDetailModel : NSObject

@property (nonatomic, copy) NSString * state;
@property (nonatomic, copy) NSString * issue;
@property (nonatomic, copy) NSString * award;
@property (nonatomic, copy) NSString * mobile;
@property (nonatomic, copy) NSString * isImage;
@property (nonatomic, copy) NSString * createDate;
@property (nonatomic, copy) NSString * price;
@property (nonatomic, copy) NSString * type;
@property (nonatomic, copy) NSString * image;
@property (nonatomic, copy) NSString * bunch;
@property (nonatomic, copy) NSString * times;
@property (nonatomic, copy) NSString * orderNumber;
@property (nonatomic, copy) NSString * playName;
@property (nonatomic, copy) NSString * account;
@property (nonatomic, strong) NSArray * detailList;
@property (nonatomic, copy) NSString * printDate;
@property (nonatomic, copy) NSString * result;
@property (nonatomic, copy) NSString * stopTime;
@property (nonatomic, copy) NSString * nickName;

@end

NS_ASSUME_NONNULL_END
