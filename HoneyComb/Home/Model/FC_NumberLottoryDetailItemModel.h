//
//  FC_NumberLottoryDetailItemModel.h
//  HoneyComb
//
//  Created by afc on 2018/11/28.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_NumberLottoryDetailItemModel : NSObject

@property (nonatomic, copy) NSString * ID;
@property (nonatomic, copy) NSString * orderTotalId;
@property (nonatomic, copy) NSString * redBile;
@property (nonatomic, copy) NSString * blueBile;
@property (nonatomic, copy) NSString * type;
@property (nonatomic, copy) NSString * red;
@property (nonatomic, copy) NSString * blue;
@property (nonatomic, copy) NSString * bunch;
@property (nonatomic, copy) NSString * content;

@end

NS_ASSUME_NONNULL_END
