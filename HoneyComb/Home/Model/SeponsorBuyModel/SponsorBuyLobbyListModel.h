//
//  SponsorBuyLobbyListModel.h
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/16.
//  Copyright © 2018 afc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 合买大厅
 */
@interface SponsorBuyLobbyListModel : NSObject
@property (nonatomic, copy) NSString *bao;
@property (nonatomic, copy) NSString *describe;
@property (nonatomic, copy) NSString *guard;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, strong) NSURL *img;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *order_total_id;
@property (nonatomic, copy) NSString *people;
@property (nonatomic, copy) NSString *play_name;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *shengyu;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *unitPrice;
@property (nonatomic, copy) NSString *yigou;
@end

NS_ASSUME_NONNULL_END
