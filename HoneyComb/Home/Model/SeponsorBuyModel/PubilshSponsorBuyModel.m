//
//  PubilshSponsorBuyModel.m
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/15.
//  Copyright © 2018 afc. All rights reserved.
//

#import "PubilshSponsorBuyModel.h"

@implementation PubilshSponsorBuyModel

@end


@implementation MineSponsorBuyListModel

- (NSString *)stateStr{
    
        
        NSString * stateStr = @"";
        
        switch ([_state integerValue]) {
            case 0:
                stateStr = @"未付款";
                break;
            case 1:
                stateStr = @"待出票";
                break;
            case 2:
                stateStr = @"待开奖";
                break;
            case 3:
                stateStr = @"已中奖";
                break;
            case 4:
                stateStr = @"未中奖";
                break;
            case 5:
                stateStr = @"已派奖";
                break;
            case 6:
                stateStr = @"已撤单";
                break;
            case 7:
                stateStr = @"已取票";
                break;
            default:
                break;
        }
        

    return stateStr;

}

@end
