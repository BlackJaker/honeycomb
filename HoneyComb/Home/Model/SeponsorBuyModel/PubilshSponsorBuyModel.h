//
//  PubilshSponsorBuyModel.h
//  LotteryApp
//
//  Created by 陈亚勃 on 2018/12/15.
//  Copyright © 2018 afc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PubilshSponsorBuyModel : NSObject
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *number;   //份数
@property (nonatomic, copy) NSString *commission;   //佣金比例
@property (nonatomic, copy) NSString *subscription; //认购份数
@property (nonatomic, copy) NSString *guard;    //保底份数
@property (nonatomic, copy) NSString *secrecy;  //保密类型（1公开 2投注后可见 3截止后可见）
@property (nonatomic, copy) NSString *describe; //方案描述
@end

@interface MineSponsorBuyListModel : NSObject
@property (nonatomic, copy) NSString *award;
@property (nonatomic, copy) NSString *createdate;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *issue;    //期号
@property (nonatomic, copy) NSString *ordertotalid;
@property (nonatomic, copy) NSString *playName;
@property (nonatomic, copy) NSString *play_name;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *stateStr;
@property (nonatomic, copy) NSString *type;
@end

NS_ASSUME_NONNULL_END
