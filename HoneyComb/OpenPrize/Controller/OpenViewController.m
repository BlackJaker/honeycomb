//
//  OpenViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OpenViewController.h"
#import "UserCell.h"
#import "OtherUserViewController.h"
#import "UserClassificationView.h"
@interface OpenViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UIButton *rightBtn;

    NSString *total;
    NSString *typeStr;

    NSString *mobile;
    NSString *nickName;
}
@property (nonatomic,strong) NSMutableArray *numberArr;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *userListArr;
@property (strong, nonatomic) NSMutableArray  *selectIndexs;
@property (nonatomic,strong) UserClassificationView *userView;
@property (nonatomic,strong)UIWindow *window;
@property (nonatomic,strong) UITextField *searchTf;
@property (nonatomic,assign)NSInteger page;

@property (nonatomic,copy)NSString * dateStr;
@property (nonatomic,copy)NSString * orderMoneyStr;
@property (nonatomic,assign)BOOL isDown;

@end

@implementation OpenViewController

static NSString *cellID = @"cell";

-(NSMutableArray *)numberArr
{
    if (!_numberArr) {
        _numberArr = [NSMutableArray array];
    }
    return _numberArr;
}
-(NSMutableArray *)userListArr
{
    if (!_userListArr) {
        _userListArr = [NSMutableArray array];
    }
    return _userListArr;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
    
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = RGB(249, 249, 249, 1);
    self.titleLabe.text = @"用户管理";
    self.left.hidden = YES;
    
    self.isDown = YES;
    self.dateStr = @"";
    self.orderMoneyStr = @"";
    
    [self layoutView];
    [self setupRefresh];
   
}

#pragma mark --创建上拉加载和下拉刷新
- (void)setupRefresh {
    __block OpenViewController *weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf loadUserTopics];
    }];
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadUserNewTopics];
    }];
}
-(void)loadUserTopics
{
    self.page = 1;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] =[[LoginUser shareLoginUser] shopId];
    dic[@"pageNum"] = pageStr;
    
    if (self.dateStr.length > 0) {
        dic[@"day"] = self.dateStr;
    }
    if (self.orderMoneyStr.length > 0) {
        dic[@"orderMoney"] = self.orderMoneyStr;
    }
    
    @WeakObj(self)
    [PPNetworkHelper POST:PostUserList parameters:dic success:^(id responseObject) {
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            
            @StrongObj(self)
            
            [self.userListArr removeAllObjects];
            
            NSArray * arr = [[responseObject objectForKey:@"data"] objectForKey:@"userList"];
            
            [self.tableView.mj_footer resetNoMoreData];
            
            if (arr.count > 0) {
                
                total = [[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"];
                
                if ([total integerValue] == self.page) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
                
                self.userListArr = [AllModel mj_objectArrayWithKeyValuesArray:arr];
                
            }else
            {
                [self.userListArr removeAllObjects];
                self.tableView.mj_footer.hidden = YES;
                [STTextHudTool showText:@"暂无数据!"];
            }
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
        }else
        {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_header endRefreshing];
        }
    } failure:^(NSError *error) {
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)loadUserNewTopics
{
    self.page ++;
    NSString *pageStr = [NSString stringWithFormat:@"%ld",(long)self.page];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] =[[LoginUser shareLoginUser] shopId];
    dic[@"pageNum"] = pageStr;
    
    if (self.dateStr.length > 0) {
        dic[@"day"] = self.dateStr;
    }
    if (self.orderMoneyStr.length > 0) {
        dic[@"orderMoney"] = self.orderMoneyStr;
    }

    [PPNetworkHelper POST:PostUserList parameters:dic success:^(id responseObject) {
        
        NSLog(@"%@",responseObject);
        
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {

            NSArray  *arr =[AllModel mj_objectArrayWithKeyValuesArray:[[responseObject objectForKey:@"data"]objectForKey:@"userList"]];
            
            if (arr.count == 0) {
                [STTextHudTool showText:@"暂无更多数据!"];
                [self.tableView.mj_header endRefreshing];
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.tableView.mj_footer.hidden = YES;
                [self.tableView reloadData];
                //                [self.view showError:@"暂无信息"];
            }else
            {
                [self.userListArr addObjectsFromArray:arr];
                
                [self.tableView reloadData];
                total = [NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"data"]objectForKey:@"totalNumber"] ];
                //                GFLog(@"total=%@",total);
                NSString *str= [NSString stringWithFormat:@"%ld",(long)self.page];
                if ([total isEqualToString:str]) {
                    //                    GFLog(@"2233");
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else
                {
                    [self.tableView.mj_footer endRefreshing];
                }
            }
        }else
        {
            [STTextHudTool showText:[responseObject objectForKey:@"msg"]];
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header endRefreshing];
    }];
}

-(void)layoutView
{
    rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,30, 30)];
    [rightBtn setImage:[UIImage imageNamed:@"分类图标"] forState:(UIControlStateNormal)];
    [rightBtn addTarget:self action:@selector(rightAction) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *le = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = le;
    
    UIView *backView = [[UIView alloc]init];
    [self.view addSubview:backView];
    backView.backgroundColor = [UIColor whiteColor];
    backView.layer.borderColor = kGrayColor.CGColor;
      [backView.layer setBorderWidth:1.0];
    backView.layer.masksToBounds = YES;
    backView.layer.cornerRadius = 3;
    backView.userInteractionEnabled = YES;
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-kWidth/15.625);
        make.height.mas_equalTo(kWidth/10.7142);
        make.top.mas_equalTo(kWidth/25);
        make.left.mas_equalTo(kWidth/31.25);
    }];
    UIButton *searchBtn  =[[UIButton alloc]init];
    [searchBtn setImage:[UIImage imageNamed:@"放大镜"] forState:(UIControlStateNormal)];
    [searchBtn addTarget:self action:@selector(searchAction) forControlEvents:(UIControlEventTouchUpInside)];
//    imageV.image = [UIImage imageNamed:@"放大镜"];
    [backView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(kWidth/31.25);
        make.right.mas_equalTo(-kWidth/31.25);
        make.top.mas_equalTo(kWidth/34.0909);
    }];
    
    self.searchTf = [[UITextField alloc]init];
    [backView addSubview:self.searchTf];
  
    self.searchTf.keyboardType = UIKeyboardTypeDefault;
    self.searchTf.textColor = kBlackColor;
    self.searchTf.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请输入要搜索的用户手机号" attributes:
                                      @{NSForegroundColorAttributeName:kGrayColor,
                                        NSFontAttributeName:self.searchTf.font
                                        }];
    self.searchTf.attributedPlaceholder = attrString;
   
    [self.searchTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth-kWidth/5.8593);
        make.height.mas_equalTo(kWidth/10.7142);
        make.left.mas_equalTo(kWidth/31.25);
        make.top.mas_equalTo(0);
    }];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kWidth/5.7692, kWidth, kHeight-TabBarHeight-NavgationBarHeight-kWidth/5.7692) style:(UITableViewStylePlain)];
    self.tableView.dataSource = self;
    self.tableView.delegate =self;
    self.tableView.backgroundColor =RGB(249, 249, 249, 1);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[UserCell class] forCellReuseIdentifier:cellID];
    [self.view addSubview:self.tableView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.userListArr.count;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0)];
        v.backgroundColor = RGB(249, 249, 249, 1);
        return v;
    }else{
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 0)];
        v.backgroundColor=RGB(249, 249, 249, 1);
        return v;
    }
}
//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//
//        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 5)];
//    v.backgroundColor  =RGB(249, 249, 249, 1);
//        return v;
//
//}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserCell *cell =[tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath ];
    if (cell == nil) {
        cell = [[UserCell alloc]initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:cellID];
    }
    cell.model = self.userListArr[indexPath.row];

    cell.numberLabel.text = [NSString stringWithFormat:@"%02ld",indexPath.row+1];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = RGB(249, 249, 249, 1);
   
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kWidth/4.9342;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 5;
//}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.0001;
}
-(void)rightAction
{
    if (_isDown) {
        
        _isDown = NO;
        _userView = [[UserClassificationView alloc]initWithFrame:CGRectMake(0, NavgationBarHeight, kWidth, kHeight) withDateStr:self.dateStr orderMoneyStr:self.orderMoneyStr];
        [_userView showView];
        
        @WeakObj(self)
        _userView.dismissBlock = ^{
            @StrongObj(self)
            self.isDown = YES;
        };
        
        _userView.choosedBlock = ^(NSDictionary *info) {
            
            @StrongObj(self)
            self.isDown = YES;
            
            self.dateStr = info[@"date"];
            self.orderMoneyStr = info[@"orderMoneyStr"];

            [self loadUserTopics];
            
        };

    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OtherUserViewController *otV = [[OtherUserViewController alloc]init];
    AllModel *model =self.userListArr[indexPath.row];
    otV.userId = model.userId;
    @WeakObj(self)
    otV.remarkBlock = ^{
        @StrongObj(self)
        [self loadUserTopics];
    };
    [self.navigationController pushViewController:otV animated:YES];
}
-(void)searchAction
{
    if (self.searchTf.text.length == 0) {
        [STTextHudTool showText:@"搜索内容不能为空"];
        return;
    }
    if ( [Check isMobileNumber:self.searchTf.text]) {
        NSString *s;
        nickName = s;
        mobile = self.searchTf.text;
    }else
    {
        NSString *s;
        mobile = s;
        nickName = self.searchTf.text;
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = [[LoginUser shareLoginUser] shopId];
    dic[@"nickName"] = nickName;
    dic[@"mobile"] = mobile;
    NSLog(@"参数-%@",dic);
    [PPNetworkHelper POST:PostUesrSearch parameters:dic success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
            NSMutableArray *arr = [solveJsonData changeType:[[responseObject objectForKey:@"data"]objectForKey:@"userList"]];
            if (arr.count == 0) {
                self.userListArr = [AllModel mj_objectArrayWithKeyValuesArray:arr];
                [self.tableView reloadData];
                 self.tableView.mj_footer.hidden = YES;
                [STTextHudTool showText:@"暂无用户"];
            }else
            {
                self.userListArr = [AllModel mj_objectArrayWithKeyValuesArray:arr];
                [self.tableView reloadData];
            }
          
        }else
        {
            
        }
        
    } failure:^(NSError *error) {
          NSLog(@"%@",error);
    }];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.searchTf resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
