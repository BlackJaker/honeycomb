//
//  OtherUserViewController.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/12.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^RemarkSuccesedBlock)(void);

@interface OtherUserViewController : AllViewController

@property (nonatomic,copy) NSString *userId;

@property (nonatomic,copy) RemarkSuccesedBlock remarkBlock;

@end
