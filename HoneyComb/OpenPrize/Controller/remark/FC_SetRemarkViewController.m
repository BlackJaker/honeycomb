//
//  FC_SetRemarkViewController.m
//  HoneyComb
//
//  Created by afc on 2019/1/17.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import "FC_SetRemarkViewController.h"

#import "XMTextView.h"

@interface FC_SetRemarkViewController ()<UITextFieldDelegate>

@property (nonatomic,strong) NSDictionary * userInfo;

@property (nonatomic,assign) BOOL isRemarking;

@property (nonatomic,copy) NSString * descri;

@end

@implementation FC_SetRemarkViewController

-(instancetype)initWithUserInfo:(NSDictionary *)userInfo{
    
    if (self = [super init]) {

        self.userInfo = userInfo;
        
        self.confirmButton.hidden = YES;
        
        _isRemarking = NO;
        
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.titleLabe.text = @"设置备注";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self contentSetup];
    
    if ([self.userInfo[@"remark"] length] > 0) {
        self.remarkTextField.text = self.userInfo[@"remark"];
    }else{
        self.remarkTextField.text = self.userInfo[@"nickName"];
    }
    self.descriTextView.backgroundColor = [UIColor clearColor];

}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    NSArray * titles = @[@"备注名",@"描述"];
    
    CGFloat  centerMar = 110,leftMar = 20,topMar = 45,height = 20;
    
    for (int i = 0; i < titles.count; i ++) {
        
        UILabel * label = [UILabel createLabelWithFrame:CGRectZero
                                                   text:[titles objectAtIndex:i]
                                               fontSize:17
                                              textColor:[UIColor colorWithHexString:@"#333333"]];
        label.textAlignment = NSTextAlignmentLeft;
        label.tag = 1000 + i;
        [self.view addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar);
            make.right.mas_equalTo(-leftMar);
            make.top.mas_equalTo(topMar + centerMar * i);
            make.height.mas_equalTo(height);
        }];
        
    }
  
}

-(UITextField *)remarkTextField{
 
    if (!_remarkTextField) {
        
        CGFloat  leftMar = 20,topMar = 15,height = 45;
        
        UILabel * label = [self.view viewWithTag:1000];
        
        _remarkTextField = [[UITextField alloc]init];
        _remarkTextField.placeholder = @"添加备注名";
        _remarkTextField.textColor = [UIColor colorWithHexString:@"#565D6E"];
        _remarkTextField.font = [UIFont systemFontOfSize:15];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldDidChanged) name:UITextFieldTextDidChangeNotification object:_remarkTextField];
        [self.view addSubview:_remarkTextField];
        
        [_remarkTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar);
            make.right.mas_equalTo(-leftMar);
            make.top.mas_equalTo(label.mas_bottom).offset(topMar);
            make.height.mas_equalTo(height);
        }];
        
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = [UIColor colorWithHexString:@"#F3F2F6"];
        [self.view addSubview:line];
        [line makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(_remarkTextField.mas_bottom);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(1);
            
        }];
        
    }
    return _remarkTextField;
}

-(void)textFieldDidChanged{
    
    if (self.confirmButton.hidden) {
        self.confirmButton.hidden = NO;
    }
    
}

-(UIButton *)confirmButton{
    
    CGFloat leftMar = 15,bottomMargin = 180,height = 52;
    
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc]init];
    
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = CGRectMake(0, 0, kWidth - leftMar * 2, height);
        gl.startPoint = CGPointMake(0, 0);
        gl.endPoint = CGPointMake(1, 1);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:243/255.0 green:56/255.0 blue:31/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:251/255.0 green:48/255.0 blue:36/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:234/255.0 green:29/255.0 blue:73/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0.0),@(0.5),@(1.0)];
        
        [_confirmButton.layer addSublayer:gl];
        
        [_confirmButton addTarget:self action:@selector(setRemarkClick) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:_confirmButton];
        
        [_confirmButton setTitle:@"完成" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _confirmButton.titleLabel.font = [UIFont fontWithName:kBold size:17];
        _confirmButton.layer.cornerRadius = 5.0f;
        _confirmButton.layer.masksToBounds = YES;
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-leftMar);
            make.height.mas_equalTo(height);
            make.bottom.mas_equalTo(-bottomMargin);
            make.left.mas_equalTo(leftMar);
        }];
    }
    return _confirmButton;
    
}

-(void)setRemarkClick{
    
    if (!_isRemarking) {
        _isRemarking = YES;
        [self updateUserInfoRequest];
    }
    
}


-(XMTextView *)descriTextView{
    
    if (!_descriTextView) {
        
        CGFloat  leftMar = 20,height = 75;
        

        _descriTextView = [[XMTextView alloc]initWithFrame:CGRectMake(leftMar, 197, kWidth - leftMar * 2, height)];
        
        __block NSInteger  number = 0;
        
        @WeakObj(self)
        _descriTextView.textViewListening = ^(NSString *textViewStr) {

            @StrongObj(self)
            
            self.descri = textViewStr;
            
            if (self.confirmButton.hidden) {
                self.confirmButton.hidden = NO;
            }
        
            CGFloat  labelHeight = [self labelHeightWithString:textViewStr];
            
            if ((NSInteger)labelHeight/15 != number) {
                
                if (labelHeight > 90) {
                    labelHeight = 90;
                }
                CGRect frame = self.descriTextView.frame;
                frame.size.height = labelHeight + 30;
                self.descriTextView.frame = frame;
                
                [self.descriTextView layoutFrame];
            }

            
            number = (NSInteger)labelHeight/15;
        };
        
        _descriTextView.isSetBorder = NO;
        _descriTextView.placeholderColor = [UIColor colorWithHexString:@"#A9AFBC"];
        _descriTextView.tvColor = [UIColor colorWithHexString:@"#565D6E"];
        _descriTextView.tvFont = [UIFont systemFontOfSize:15];
        _descriTextView.placeholder = @"添加更多备注信息";
        if ([self.userInfo[@"describe"] length] > 0) {
            _descriTextView.contentStr = self.userInfo[@"describe"];
        }
        _descriTextView.textMaxNum = 140;
        _descriTextView.maxNumFont = [UIFont systemFontOfSize:15];
        _descriTextView.maxNumColor = [UIColor colorWithHexString:@"#A9AFBC"];
        _descriTextView.maxNumState = XMMaxNumStateDiminishing;
        [self.view addSubview:_descriTextView];
        
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = [UIColor colorWithHexString:@"#F3F2F6"];
        [self.view addSubview:line];
        [line makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(_descriTextView.mas_bottom);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(1);
            
        }];
        
    }
    return _descriTextView;
}

- (CGFloat)labelHeightWithString:(NSString *)resultString{
    
    CGFloat leftMar = 20, width = kWidth - leftMar * 2;
    
    CGSize maxSize = CGSizeMake(width, MAXFLOAT);
    
    // 计算内容label的高度
    CGFloat textH = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size.height;
    return textH;
}

-(void)leftAction{
    
    if (self.confirmButton.hidden) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self showAlertViewController];
    }
    
}

-(void)showAlertViewController{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"保存本次编辑?" preferredStyle:UIAlertControllerStyleAlert];
    @WeakObj(self)
    UIAlertAction *savePhotoAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @StrongObj(self)
        
        [self updateUserInfoRequest];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:savePhotoAction];
    
    
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark  --  request  --

-(void)updateUserInfoRequest{

    NSMutableDictionary * info = [NSMutableDictionary new];
    
    NSString * remark = !self.remarkTextField.text?@"":self.remarkTextField.text;
    NSString * descrip = !self.descri?@"":self.descri;
    
    [info setValue:[LoginUser shareLoginUser].shopId forKey:@"shopId"];
    [info setValue:self.userInfo[@"userId"] forKey:@"userId"];
    [info setValue:remark forKey:@"remark"];
    [info setValue:descrip forKey:@"describe"];
    
    @WeakObj(self)
    [PPNetworkHelper POST:PostUesrRemark parameters:info success:^(id responseObject) {
        
        @StrongObj(self)
        
        self.isRemarking = NO;
        
        if ([responseObject[@"state"] isEqualToString:@"success"]) {
            
            if (self.remarkSuccessedBlock) {
                self.remarkSuccessedBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            [STTextHudTool showErrorText:responseObject[@"msg"]];
        }
        
        
    } failure:^(NSError *error) {
        self.isRemarking = NO;
        [STTextHudTool showErrorText:@"网络较差,请稍后重试"];
    }];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
}

@end
