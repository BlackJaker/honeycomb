//
//  FC_SetRemarkViewController.h
//  HoneyComb
//
//  Created by afc on 2019/1/17.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

@class XMTextView;

typedef void(^SetRemarkSuccesdBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface FC_SetRemarkViewController : AllViewController

-(instancetype)initWithUserInfo:(NSDictionary *)userInfo;

@property (nonatomic,strong) UITextField * remarkTextField;

@property (nonatomic,strong)  XMTextView* descriTextView;

@property (nonatomic,strong)  UIButton* confirmButton;

@property (nonatomic,copy) SetRemarkSuccesdBlock  remarkSuccessedBlock;

@end

NS_ASSUME_NONNULL_END
