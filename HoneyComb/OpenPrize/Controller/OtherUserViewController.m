//
//  OtherUserViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/12.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OtherUserViewController.h"

#import "OrderQueryViewController.h"

#import "AF_UserHeadView.h"

#import "FC_UserInfoCell.h"

#import "FC_SetRemarkViewController.h"

@interface OtherUserViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableDictionary *detailDic;

    CGFloat  rowHeight ;
    CGFloat  headHeight ;
    CGFloat  footHeight ;
}
@property (nonatomic,strong) UITableView  *tableView;

@property (nonatomic,copy) NSArray * titles;

@property (nonatomic,strong) AF_UserHeadView  *headView;

@end

@implementation OtherUserViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self contentSetup];

}

#pragma mark  --  content setup  --

-(void)contentSetup{
    
    rowHeight = 55;
    headHeight = 150;
    footHeight = 10;
    
    self.titleLabe.text = @"用户信息";
    
    [self userInfoRequest];
}

-(UITableView *)tableView{
    
    if (!_tableView) {
        self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight - NAVIGATION_HEIGHT) style:(UITableViewStylePlain)];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.scrollEnabled = NO;
        self.tableView.backgroundColor =RGB(249, 249, 249, 1);
        [self.view addSubview:self.tableView];
    }
    return _tableView;
}

-(NSArray *)titles{
    
    if (!_titles) {
//        _titles = @[@[@"设置备注和描述"],@[@"转账"],@[@"注册时间",@"历史投注"],@[@"联系用户"]];
        _titles = @[@[@"设置备注和描述"],@[@"注册时间",@"历史投注"]];
    }
    return _titles;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titles.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.titles objectAtIndex:section] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * userInfoCell = @"userInfoCell";
    
    FC_UserInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:userInfoCell];
    
    if (!cell) {
        cell = [[FC_UserInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:userInfoCell];
    }
    
    cell.textLabel.textColor = RGB(153, 153, 153, 1);
    cell.textLabel.font = [UIFont fontWithName:kMedium size:15];
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];//分割线延长
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.text = [[self.titles objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

    if (indexPath.section != 1 || indexPath.row != 0 ) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.noteLabel updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
        }];
        cell.noteLabel.text = detailDic[@"createDate"];
    }

    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中颜色消失
    
    if (indexPath.section == 0) {
        FC_SetRemarkViewController * setRemarkVc = [[FC_SetRemarkViewController alloc]initWithUserInfo:detailDic];
        @WeakObj(self)
        setRemarkVc.remarkSuccessedBlock = ^{
            @StrongObj(self)
            
            if (self.remarkBlock) {
                self.remarkBlock();
            }
            
            [self userInfoRequest];
        };
        [self.navigationController pushViewController:setRemarkVc animated:YES];
    }
    
    if (indexPath.section == 1 && indexPath.row == 1) {
        OrderQueryViewController *otVC = [[OrderQueryViewController alloc]init];
        otVC.strId = self.userId;
        otVC.styleId = @"1";
        [self.navigationController pushViewController:otVC animated:YES];
    }
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return headHeight;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return footHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section == 0){
        return self.headView;
    }
    return nil;
}

-(AF_UserHeadView *)headView{
    
    if (!_headView) {
        _headView = [[AF_UserHeadView alloc]initWithFrame:CGRectMake(0, 0, kWidth, headHeight)];
    }
    return _headView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, footHeight)];
    v.backgroundColor = RGB(249, 249, 249, 1);
    return v;
}

#pragma mark  --  request  --

-(void)userInfoRequest
{
    NSDictionary *dic = @{
                          @"userId":self.userId,
                          @"shopId":[LoginUser shareLoginUser].shopId
                          };
    @WeakObj(self)
    [PPNetworkHelper POST:PostUserDetail
               parameters:dic
                  success:^(id responseObject) {
                      
                      @StrongObj(self)
                      
                      if ([[responseObject objectForKey:@"state"] isEqualToString:@"success"]) {
                          
                          detailDic = [NSMutableDictionary dictionaryWithDictionary:[responseObject objectForKey:@"data"]];
                          
                          [self.headView reloadWithInfo:detailDic];
                          
                          [self.tableView reloadData];
                      }else{
                          [FC_Manager showToastWithText:responseObject[@"msg"]];
                      }
                      
                  } failure:^(NSError *error) {
                      [FC_Manager showToastWithText:@"网络链接错误,请稍后重试"];
                  }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
