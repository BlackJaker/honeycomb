//
//  FC_UserInfoCell.m
//  HoneyComb
//
//  Created by afc on 2019/1/16.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import "FC_UserInfoCell.h"

@implementation FC_UserInfoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    CGFloat  width = 240;
    
    if (self) {
        
        _noteLabel = [UILabel createLabelWithFrame:CGRectZero text:@"" fontSize:14 textColor:[UIColor colorWithHexString:@"#7C8495"]];
        _noteLabel.textAlignment = NSTextAlignmentRight;
        
        [self.contentView addSubview:_noteLabel];
        
        [_noteLabel makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.width.mas_equalTo(width);
        }];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
