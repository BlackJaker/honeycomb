//
//  FC_UserInfoCell.h
//  HoneyComb
//
//  Created by afc on 2019/1/16.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_UserInfoCell : UITableViewCell

@property (nonatomic,strong) UILabel * noteLabel;

@end

NS_ASSUME_NONNULL_END
