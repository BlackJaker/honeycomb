//
//  UserClassificationView.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "UserClassificationView.h"

#define kContentHeight 270

@interface UserClassificationView ()
{
    UIView *v;
    NSInteger  dateSelectedIndex;
    NSInteger  orderMoneyIndex;
}
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIWindow *window;
@property (nonatomic,strong) NSMutableArray *clickArr;

@property (nonatomic,strong) NSMutableArray *clickArrTwo;

@property (nonatomic,strong)UIButton * allButton;

@property (nonatomic,copy)NSString * dateStr;

@property (nonatomic,copy)NSString * orderMoneyStr;

@property (nonatomic,strong)UIButton * currentSelectedDateButton;
@property (nonatomic,strong)UIButton * currentSelectedOrderMoneyButton;

@end
@implementation UserClassificationView

-(instancetype)initWithFrame:(CGRect)frame withDateStr:(nullable NSString *)dateStr orderMoneyStr:(nullable NSString *)orderMoneyStr{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.dateStr = dateStr;
        self.orderMoneyStr = orderMoneyStr;
        
        [self exchangeDateString];
        [self exchangeOrderMoneyString];
        
        [self layoutView];
    }
    return self;

}
-(void)layoutView
{
    [self backViewSetup];
    
    [self contentViewSetup];
    
}

-(void)backViewSetup{
    
    _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    _bgView.alpha = 0.4;
    _bgView.backgroundColor = [UIColor blackColor];
    [self addSubview:_bgView];
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissContactView:)];
    [_bgView addGestureRecognizer:tapGes];
    
}

-(NSMutableArray *)clickArr{
    
    if (!_clickArr) {
        _clickArr = [NSMutableArray array];
    }
    
    return _clickArr;
    
}

-(NSMutableArray *)clickArrTwo{
    
    if (!_clickArrTwo) {
        _clickArrTwo = [NSMutableArray array];
    }
    
    return _clickArrTwo;
    
}

-(void)contentViewSetup{
    
    v = [[UIView alloc]init];
    v.backgroundColor = [UIColor whiteColor];
    [self addSubview:v];
    
    [v mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(kContentHeight);
        make.top.mas_equalTo(0);
    }];
    
    [self.allButton setTitle:@"全部" forState:(UIControlStateNormal)];
    
    CGFloat  leftMar = 15,centerMar = 12,width = (kWidth - leftMar *  5)/4,height = 33,topMargin = 15,recentHeight = 20;
    // note
    UILabel *recenLabel = [[UILabel alloc]init];
    recenLabel.text =@"近期没有下单用户";
    recenLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    recenLabel.textColor = RGB(51, 51, 51, 1);
    recenLabel.textAlignment = NSTextAlignmentLeft;
    [v addSubview:recenLabel];
    
    [recenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftMar);
        make.right.mas_equalTo(-leftMar);
        make.height.mas_equalTo(recentHeight);
        make.top.mas_equalTo(self.allButton.mas_bottom).offset(topMargin);
    }];
    
    
    // titles
    NSArray *titles = @[@"近3天",@"近7天",@"近15天",@"近一月",@"近两月",@"近三月"];

    for (int i = 0; i < titles.count; i++) {
        
        UIButton * button = [[UIButton alloc]init];
        
        button.selected = NO;
        if ([self.dateStr isEqualToString:[titles objectAtIndex:i]]) {
            button.selected = YES;
            self.currentSelectedDateButton = button;
        }
        [self updateButton:button];
        [button setTitle:titles[i] forState:(UIControlStateNormal)];
        [button addTarget:self action:@selector(recentlyAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [v addSubview:button];
        
        [self.clickArr addObject:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar + (width + leftMar)*(i%4));
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(recenLabel.mas_bottom).offset(centerMar+(height + centerMar)*(i/4));
        }];
    }
    
    // 下单金额
    UIButton * lastButton = [self.clickArr lastObject];
    
    UILabel *priceLabel = [[UILabel alloc]init];
    priceLabel.text =@"下单金额";
    priceLabel.font = [UIFont fontWithName:kPingFangRegular size:13];
    priceLabel.textColor = RGB(51, 51, 51, 1);
    priceLabel.textAlignment = NSTextAlignmentLeft;
    [v addSubview:priceLabel];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftMar);
        make.right.mas_equalTo(-leftMar);
        make.height.mas_equalTo(recentHeight);
        make.top.mas_equalTo(lastButton.mas_bottom).offset(topMargin);
    }];

    NSArray *arrPrice = @[@"从低到高",@"从高到低"];
    
    for (int i = 0; i<arrPrice.count; i++) {
        
        UIButton * button = [[UIButton alloc]init];
        
        button.selected = NO;
        if ([self.orderMoneyStr isEqualToString:[arrPrice objectAtIndex:i]]) {
            button.selected = YES;
            self.currentSelectedOrderMoneyButton = button;
        }
        [self updateButton:button];
        [button setTitle:arrPrice[i] forState:(UIControlStateNormal)];
        [button addTarget:self action:@selector(priceAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [v addSubview:button];
        
        [self.clickArrTwo addObject:button];
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar + (width + leftMar)*(i%4));
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(priceLabel.mas_bottom).offset(centerMar+(height + centerMar)*(i/4));
        }];

    }
}

-(UIButton *)allButton{
    
    CGFloat  leftMar = 15,topMar = 15,height = 33,width = (kWidth - leftMar *  5)/4;
    
    if (!_allButton) {
        
        _allButton = [[UIButton alloc]init];
        _allButton.selected = self.dateStr.length == 0  && self.orderMoneyStr.length == 0?YES:NO;
        if (_allButton.selected) {
            self.currentSelectedDateButton = self.allButton;
        }
        [_allButton addTarget:self action:@selector(recentlyAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self updateButton:_allButton];
        [v addSubview:_allButton];
        
        [_allButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(height);
            make.top.mas_equalTo(topMar);
        }];
        
        [self.clickArr addObject:_allButton];
        
    }
    return _allButton;
}

-(void)updateButton:(UIButton *)button{
    
    [button.layer setCornerRadius:3];
    button.layer.masksToBounds = YES;
    if (button.selected) {
        [button.layer setBorderColor:[UIColor colorWithHexString:@"#fd4040"].CGColor];
    }else{
        [button.layer setBorderColor:RGB(153, 153, 153, 1).CGColor];
    }
    [button.layer setBorderWidth:1.0];
    button.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:12];
    [button setTitleColor:RGB(51, 51, 51, 1) forState:(UIControlStateNormal)];
    [button setTitleColor:[UIColor colorWithHexString:@"#fd4040"] forState:UIControlStateSelected];
}

-(void)recentlyAction:(UIButton *)btn
{
    if ([self.currentSelectedDateButton isEqual:btn]) {
        return;
    }
    
    self.currentSelectedDateButton.selected = NO;
    [self.currentSelectedDateButton.layer setBorderColor:RGB(153, 153, 153, 1).CGColor];
    
    btn.selected = YES;
    [btn.layer setBorderColor:[UIColor colorWithHexString:@"#fd4040"].CGColor];
    self.currentSelectedDateButton = btn;
    
    if ([btn.titleLabel.text isEqualToString:@"全部"]) {
        self.dateStr = @"";
    }else if ([btn.titleLabel.text isEqualToString:@"近3天"]) {
        self.dateStr = @"3";
    }else if ([btn.titleLabel.text isEqualToString:@"近7天"]){
        self.dateStr = @"7";
    }else if ([btn.titleLabel.text isEqualToString:@"近15天"]){
        self.dateStr = @"15";
    }else if ([btn.titleLabel.text isEqualToString:@"近一月"]){
        self.dateStr = @"30";
    }else if ([btn.titleLabel.text isEqualToString:@"近两月"]){
        self.dateStr = @"60";
    }else if ([btn.titleLabel.text isEqualToString:@"近三月"]){
        self.dateStr = @"90";
    }
    
    self.orderMoneyStr = @"";
    self.currentSelectedOrderMoneyButton.selected = NO;
    [self.currentSelectedOrderMoneyButton.layer setBorderColor:RGB(153, 153, 153, 1).CGColor];
    self.currentSelectedOrderMoneyButton = nil;
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.dateStr forKey:@"date"];
    [dic setValue:self.orderMoneyStr forKey:@"orderMoneyStr"];
    
    if (self.choosedBlock) {
        self.choosedBlock(dic);
    }
    
    [self dismissView];
}

-(void)exchangeDateString{
    
   if ([self.dateStr isEqualToString:@"3"]) {
        self.dateStr = @"近3天";
    }else if ([self.dateStr isEqualToString:@"7"]){
        self.dateStr = @"近7天";
    }else if ([self.dateStr isEqualToString:@"15"]){
        self.dateStr = @"近15天";
    }else if ([self.dateStr isEqualToString:@"30"]){
        self.dateStr = @"近一月";
    }else if ([self.dateStr isEqualToString:@"60"]){
        self.dateStr = @"近两月";
    }else if ([self.dateStr isEqualToString:@"90"]){
        self.dateStr = @"近三月";
    }
    
}

-(void)exchangeOrderMoneyString{
    
    if ([self.orderMoneyStr isEqualToString:@"asc"]) {
        self.orderMoneyStr = @"从低到高";
    }
    if([self.orderMoneyStr isEqualToString:@"desc"]){
        self.orderMoneyStr = @"从高到低";
    }
    
}

-(void)priceAction:(UIButton *)btn
{
    if ([self.currentSelectedOrderMoneyButton isEqual:btn]) {
        return;
    }
    
    self.currentSelectedOrderMoneyButton.selected = NO;
    [self.currentSelectedOrderMoneyButton.layer setBorderColor:RGB(153, 153, 153, 1).CGColor];
    
    btn.selected = YES;
    [btn.layer setBorderColor:[UIColor colorWithHexString:@"#fd4040"].CGColor];
    self.currentSelectedOrderMoneyButton = btn;
    
    if ([btn.titleLabel.text isEqualToString:@"从低到高"]) {
        self.orderMoneyStr = @"asc";
    }else{
        self.orderMoneyStr = @"desc";
    }
    
    self.dateStr = @"";
    self.currentSelectedDateButton.selected = NO;
    [self.currentSelectedDateButton.layer setBorderColor:RGB(153, 153, 153, 1).CGColor];
    self.currentSelectedDateButton = nil;
   
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.dateStr forKey:@"date"];
    [dic setValue:self.orderMoneyStr forKey:@"orderMoneyStr"];
    
    if (self.choosedBlock) {
        self.choosedBlock(dic);
    }
    
    [self dismissView];
}
-(void)dismissContactView:(UITapGestureRecognizer *)tagGes
{
    [self dismissView];
}
-(void)showView
{
    _window = [[UIApplication sharedApplication].windows lastObject];
    [_window addSubview:self];
}
-(void)dismissView
{

    if (self.dismissBlock) {
        self.dismissBlock();
    }
    
    @WeakObj(self)
    [UIView animateWithDuration:0.5 animations:^{
        @StrongObj(self)
        self.alpha = 0;
    } completion:^(BOOL finished) {
        @StrongObj(self)
        [self removeFromSuperview];
        [self removeFromSuperview];
    }];
}
@end
