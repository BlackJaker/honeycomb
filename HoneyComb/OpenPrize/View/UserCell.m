//
//  UserCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/12.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell
-(void)setModel:(AllModel *)model
{
    _model = model;
    
    if (model.orderDate) {
        self.dateLabel.text = [NSString stringWithFormat:@"最近下单:%@",model.orderDate];
    }
    
    self.nameLabel.text =[NSString stringWithFormat:@"%@",model.nickName];
    NSURL *url = [NSURL URLWithString:model.img];
    [_headerImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"默认头像"]];
    //   根据字符串长度计算label宽度
    NSString *str = [NSString stringWithFormat:@"消费金额: %@元",model.cost];
    NSRange  subRange = [str rangeOfString:model.cost];
    
    NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:str];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 152, 153, 1),NSFontAttributeName:[UIFont systemFontOfSize:kWidth/31.25]} range:NSMakeRange(0, str.length)];

    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(231, 31, 25, 1)} range:subRange];

    CGRect tempRect = [rushAttributed boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    self.priceLabel.attributedText = rushAttributed;
    [self.priceLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(tempRect.size.width+kWidth/75);
      
    }];
    
    //   根据字符串长度计算label宽度
    NSString *strT = [NSString stringWithFormat:@"余额: %@元",model.userMoney];
    NSRange  moneyRange = [strT rangeOfString:model.userMoney];
    
    NSMutableAttributedString *balanceS = [[NSMutableAttributedString alloc]initWithString:strT];
    
    [balanceS setAttributes:@{NSForegroundColorAttributeName:RGB(153, 152, 153, 1),NSFontAttributeName:[UIFont systemFontOfSize:kWidth/31.25]} range:NSMakeRange(0, strT.length)];
    
    [balanceS setAttributes:@{NSForegroundColorAttributeName:RGB(231, 31, 25, 1)} range:moneyRange];

    CGRect balanceRect = [balanceS boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    self.balanceLabel.attributedText = balanceS;
    [self.balanceLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(balanceRect.size.width+1);
        
    }];
    
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}
-(void)layoutView
{
    
    
    self.backView = [[UIImageView alloc]init];
    self.backView.image = [UIImage imageNamed:@"listbg2"];
    [self.contentView addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth);
        make.height.mas_equalTo(kWidth/4.9342);
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    self.numberLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.numberLabel];
    self.numberLabel.text = @"-";
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
    self.numberLabel.textColor = RGB(255,255, 255, 1);
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/15.625);
        make.height.mas_equalTo(kWidth/20.8333);
        make.left.mas_equalTo(kWidth/93.75);
        make.top.mas_equalTo(kWidth/62.5);
    }];
    
    
    self.headerImage = [[UIImageView alloc]init];
    self.headerImage.image = [UIImage imageNamed:@"默认头像"];
    self.headerImage.layer.cornerRadius = (kWidth/9.6153)/2;
    self.headerImage.layer.masksToBounds = YES;
    [self.backView addSubview:self.headerImage];
    [self.headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(kWidth/9.6153);
        make.top.mas_equalTo(kWidth/25);
        make.left.mas_equalTo(kWidth/10.4166);
    }];
    self.nameLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.nameLabel];
    self.nameLabel.text = @"-";
    self.nameLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
    self.nameLabel.textColor = RGB(50, 51, 51, 1);
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2);
        make.height.mas_equalTo(kWidth/22.0588);
        make.left.mas_equalTo(self.headerImage.mas_right).offset(kWidth/37.5);
        make.top.mas_equalTo(kWidth/23.4375);
    }];
    
    self.dateLabel = [[UILabel alloc]init];
    [self.backView addSubview:self.dateLabel];
    self.dateLabel.textAlignment = NSTextAlignmentRight;
    self.dateLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/34.0909];
    self.dateLabel.textColor = RGB(153, 152, 153, 1);
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kWidth/2-kWidth/12.5);
        make.height.mas_equalTo(kWidth/28.8461);
        make.right.mas_equalTo(-kWidth/37.5);
        make.top.mas_equalTo(kWidth/20.8333);
    }];

    
    
//   根据字符串长度计算label宽度
    NSString *str = @"消费金额:0.00元";
    NSString *sss = @"0.00";
    
    NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:str];
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 152, 153, 1)} range:NSMakeRange(0, 5)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(0, 5)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(153, 152, 153, 1)} range:NSMakeRange(5+sss.length, 1)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(5+sss.length, 1)];
    
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:RGB(231, 31, 25, 1)} range:NSMakeRange(5, sss.length)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(5, sss.length)];
    CGRect tempRect = [rushAttributed boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//  boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil
//    //      CGRect tempRect = [str   boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000)options:NSStringDrawingUsesLineFragmentOriginattributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}context:nil];
    self.priceLabel = [[YYLabel alloc]init];
    [self.contentView addSubview:self.priceLabel];
//    self.priceLabel.attributedText = rushAttributed;
    self.priceLabel.numberOfLines = 1;
    self.priceLabel.textAlignment = NSTextAlignmentLeft;
//    self.priceLabel.font = [UIFont systemFontOfSize:12];
//    self.priceLabel.textColor = [UIColor blackColor];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(tempRect.size.width+kWidth/75);
        make.height.mas_equalTo(kWidth/25);
        make.left.mas_equalTo(self.headerImage.mas_right).offset(kWidth/37.5);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(kWidth/37.5);
    }];

    
    //   根据字符串长度计算label宽度
    NSString *strT = @"余额: 0.00元";
    NSString *aaa = @" 0.00";
    NSMutableAttributedString *balanceS = [[NSMutableAttributedString alloc]initWithString:strT];
    [balanceS setAttributes:@{NSForegroundColorAttributeName:RGB(153, 152, 153, 1)} range:NSMakeRange(0, 3)];
    [balanceS addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(0, 3)];
    
    [balanceS setAttributes:@{NSForegroundColorAttributeName:RGB(153, 152, 153, 1)} range:NSMakeRange(3+aaa.length, 1)];
    [balanceS addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(3+aaa.length, 1)];
    
    [balanceS setAttributes:@{NSForegroundColorAttributeName:RGB(231, 31, 25, 1)} range:NSMakeRange(3, aaa.length)];
    [balanceS addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(3, aaa.length)];
    CGRect balanceRect = [balanceS boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//    NSString *strTwo = @"余额:11110.00元";
//    CGRect tempRectTwo = [str boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil];
//    
//    //    //      CGRect tempRect = [str   boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width-40,2000)options:NSStringDrawingUsesLineFragmentOriginattributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}context:nil];
    self.balanceLabel = [[YYLabel alloc]init];
    [self.contentView addSubview:self.balanceLabel];
//    self.balanceLabel.attributedText = balanceS;
    self.balanceLabel.numberOfLines = 1;
    self.balanceLabel.textAlignment = NSTextAlignmentRight;
//    self.balanceLabel.font = [UIFont systemFontOfSize:13];
//    self.balanceLabel.textColor = [UIColor blackColor];
    [self.balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(balanceRect.size.width);
        make.height.mas_equalTo(kWidth/25);
        make.right.mas_equalTo(-kWidth/37.5);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).offset(kWidth/37.5);
    }];
 
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
