//
//  UserClassificationView.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/23.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UserClassChoosedBlock)(NSDictionary * info);

typedef void(^DismissBlock)(void);

@interface UserClassificationView : UIView

-(instancetype)initWithFrame:(CGRect)frame withDateStr:(nullable NSString *)dateStr orderMoneyStr:(nullable NSString *)orderMoneyStr;

-(void)showView;

@property(nonatomic,copy) UserClassChoosedBlock choosedBlock;

@property(nonatomic,copy) DismissBlock dismissBlock;

@end
