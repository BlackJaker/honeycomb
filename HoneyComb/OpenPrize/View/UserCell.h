//
//  UserCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/12.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCell : UITableViewCell
@property (nonatomic,strong) AllModel *model;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *phoneLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) YYLabel *priceLabel;
@property (nonatomic,strong) YYLabel *balanceLabel;
@property (nonatomic,strong) UIImageView *headerImage;
@property (nonatomic,strong) UIImageView *backView;
@property (nonatomic,strong) UILabel *numberLabel;

@end
