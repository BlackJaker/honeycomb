//
//  AF_UserHeadView.m
//  HoneyComb
//
//  Created by afc on 2019/1/16.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import "AF_UserHeadView.h"

@interface AF_UserHeadView ()
{
    CGFloat  remarkWidth;
}
@property (nonatomic,copy) NSString * phoneNumber;
@end

@implementation AF_UserHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        remarkWidth = 0;
        
        self.phoneNumber = nil;
        
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = RGB(227, 227, 227,1.0f);
        [self addSubview:line];
        [line makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(-1);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(1);

        }];
    }
    return self;
}

-(UIImageView *)imgView{
    
    CGFloat  leftMar = 16,topMar = 22,width = 60;
    
    if (!_imgView) {
        _imgView = [[UIImageView alloc]init];
        _imgView.image = [UIImage imageNamed:@"默认头像"];
        _imgView.layer.cornerRadius = 5.0f;
        _imgView.clipsToBounds = YES;
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imgView];
        
        [_imgView makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar);
            make.top.mas_equalTo(topMar);
            make.size.mas_equalTo(CGSizeMake(width, width));
        }];
    }
    return _imgView;
}

-(UILabel *)remarkLabel{
    
    CGFloat  leftMar = 16,topMar = 22,height = 23;
    
    if (!_remarkLabel) {
        _remarkLabel = [UILabel createLabelWithFrame:CGRectZero text:@"-" fontSize:23 textColor:[UIColor colorWithHexString:@"#333333"]];
        _remarkLabel.font = [UIFont boldSystemFontOfSize:23];
        _remarkLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_remarkLabel];
        
        [_remarkLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imgView.mas_right).offset(leftMar);
            make.top.mas_equalTo(topMar);
            make.right.mas_equalTo(-leftMar);
            make.height.mas_equalTo(height);
        }];
    }
    return _remarkLabel;
}

-(UILabel *)nameLabel{
    
    CGFloat  leftMar = 16,topMar = 12,height = 13;
    
    if (!_nameLabel) {
        _nameLabel = [UILabel createLabelWithFrame:CGRectZero text:@"" fontSize:13 textColor:[UIColor colorWithHexString:@"#7C8495"]];
        _nameLabel.font = [UIFont fontWithName:kMedium size:13];
        _nameLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_nameLabel];
        
        [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imgView.mas_right).offset(leftMar);
            make.top.mas_equalTo(self.remarkLabel.mas_bottom).offset(topMar);
            make.width.mas_equalTo(remarkWidth);
            make.height.mas_equalTo(height);
        }];
    }
    return _nameLabel;
}

-(UILabel *)stateLabel{
    
    CGFloat  topMar = 11,height = 15,stateWidth = 40;
    
    if (!_stateLabel) {
        _stateLabel = [UILabel createLabelWithFrame:CGRectZero text:@"" fontSize:10 textColor:[UIColor colorWithHexString:@"#7C8495"]];
        _stateLabel.font = [UIFont boldSystemFontOfSize:10];
        _stateLabel.layer.cornerRadius = 5.0f;
        _stateLabel.clipsToBounds = YES;
        [self addSubview:_stateLabel];
        
        [_stateLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.nameLabel.mas_right);
            make.top.mas_equalTo(self.remarkLabel.mas_bottom).offset(topMar);
            make.width.mas_equalTo(stateWidth);
            make.height.mas_equalTo(height);
        }];
    }
    return _stateLabel;
}

-(UILabel *)phoneLabel{
    
    CGFloat  leftMar = 16,topMar = 12,height = 13;
    
    if (!_phoneLabel) {
        _phoneLabel = [UILabel createLabelWithFrame:CGRectZero text:@"绑定手机 :" fontSize:height textColor:[UIColor colorWithHexString:@"#7C8495"]];
        _phoneLabel.font = [UIFont boldSystemFontOfSize:height];
        _phoneLabel.textAlignment = NSTextAlignmentLeft;
        _phoneLabel.userInteractionEnabled = YES;
        
        UITapGestureRecognizer * tapGuest = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPhoneCallAction)];
        
        [self addSubview:_phoneLabel];
        [_phoneLabel addGestureRecognizer:tapGuest];
        
        [_phoneLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imgView.mas_right).offset(leftMar);
            make.top.mas_equalTo(self.stateLabel.mas_bottom).offset(topMar);
            make.right.mas_equalTo(self.phoneCallButton.mas_left);
            make.height.mas_equalTo(height);
        }];
    }
    return _phoneLabel;
}

-(UIButton *)phoneCallButton{
    
    CGFloat  leftMar = 16,topMar = 10.5,height = 16;
    
    if (!_phoneCallButton) {
        _phoneCallButton = [[UIButton alloc]init];
        [_phoneCallButton setImage:[UIImage imageNamed:@"user_phone"] forState:UIControlStateNormal];
        [_phoneCallButton addTarget:self action:@selector(userPhoneCallAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_phoneCallButton];
        
        [_phoneCallButton makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(height);
            make.top.mas_equalTo(self.stateLabel.mas_bottom).offset(topMar);
            make.right.mas_equalTo(-leftMar);
            make.height.mas_equalTo(height);
        }];
    }
    return _phoneCallButton;
}

-(void)userPhoneCallAction{
    
    if (self.phoneNumber) {
            NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.phoneNumber];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }
    
}

-(UILabel *)descriLabel{
    
    CGFloat  leftMar = 16,topMar = 12,height = 13;
    
    if (!_descriLabel) {
        _descriLabel = [UILabel createLabelWithFrame:CGRectZero text:@"-" fontSize:height textColor:[UIColor colorWithHexString:@"#7C8495"]];
        _descriLabel.font = [UIFont boldSystemFontOfSize:height];
        _descriLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_descriLabel];
        
        [_descriLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imgView.mas_right).offset(leftMar);
            make.top.mas_equalTo(self.phoneLabel.mas_bottom).offset(topMar);
            make.right.mas_equalTo(-leftMar);
            make.height.mas_equalTo(height);
        }];
    }
    return _descriLabel;
}

-(void)reloadWithInfo:(NSDictionary *)info{
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:info[@"img"]] placeholderImage:[UIImage imageNamed:@"默认头像"]];
    
    if ([info[@"remark"] length] > 0) {
        
        self.remarkLabel.text = info[@"remark"];
        
        NSString * remark = [NSString stringWithFormat:@"用户昵称 : %@",info[@"nickName"]];
        CGFloat width = [self labelWidthWithString:remark font:[UIFont systemFontOfSize:13] height:13];
        [self.nameLabel updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
        }];
        self.nameLabel.text = remark;
        
    }else{
        self.remarkLabel.text = info[@"nickName"];
        [self.nameLabel updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(0);
        }];
    }

    if (info[@"mobile"]) {
        self.phoneNumber = info[@"mobile"];
        self.phoneLabel.text = [NSString stringWithFormat:@"绑定手机 : %@",info[@"mobile"]];
    }
    
    self.stateLabel.text = [info[@"isReal"] isEqualToString:@"0"]?@"未认证":@"已认证";
    
    if ([info[@"isReal"] isEqualToString:@"1"]) {
        self.stateLabel.backgroundColor = RGB(229, 62, 101, 1.0f);
        self.stateLabel.textColor = [UIColor whiteColor];
    }else{
        _stateLabel.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
        _stateLabel.layer.borderWidth = 1.0f;
    }
    
    if ([info[@"describe"] length] > 0) {
        self.descriLabel.text = [NSString stringWithFormat:@"用户描述 : %@",info[@"describe"]];
    }else{
        self.descriLabel.text = @"";
    }
    
}

- (CGFloat)labelWidthWithString:(NSString *)resultString font:(UIFont *)font height:(CGFloat)height{
    
    CGSize maxSize = CGSizeMake(MAXFLOAT, height);
    
    // 计算内容label的高度
    
    CGFloat width = [resultString boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName :font} context:nil].size.width;
    return width + 5;
}

@end
