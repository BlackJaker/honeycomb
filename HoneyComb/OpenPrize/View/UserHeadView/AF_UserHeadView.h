//
//  AF_UserHeadView.h
//  HoneyComb
//
//  Created by afc on 2019/1/16.
//  Copyright © 2019年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AF_UserHeadView : UIView

@property (nonatomic,strong) UIImageView * imgView;

@property (nonatomic,strong) UILabel * remarkLabel;

@property (nonatomic,strong) UILabel * nameLabel;

@property (nonatomic,strong) UILabel * stateLabel;

@property (nonatomic,strong) UILabel * phoneLabel;

@property (nonatomic,strong) UIButton * phoneCallButton;

@property (nonatomic,strong) UILabel * descriLabel;

-(void)reloadWithInfo:(NSDictionary *)info;

@end

NS_ASSUME_NONNULL_END
