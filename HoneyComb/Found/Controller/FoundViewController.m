//
//  FoundViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FoundViewController.h"
#import "UserCell.h"
#import "FoundCell.h"
@interface FoundViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *dataDic;
}
@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,copy) NSArray * titles;

@property (nonatomic,copy) NSArray * notes;

@end

@implementation FoundViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.left.hidden = YES;
    self.titleLabe.text = @"数据统计";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self layoutView];
}
-(void)creatData
{
    NSDictionary *dic = @{@"shopId":[[LoginUser shareLoginUser] shopId]};
    
    @WeakObj(self)
    
    [PPNetworkHelper POST:PostAllStatistics parameters:dic success:^(id responseObject) {
        
        @StrongObj(self)
        
        [self.tableView.mj_header endRefreshing];
        
        if ([[responseObject objectForKey:@"state"]  isEqualToString:@"success"]) {
            dataDic = [solveJsonData changeType:[responseObject objectForKey:@"data"]];
            [self.tableView reloadData];
        }else{
            [FC_Manager showToastWithText:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
    }];
}
-(void)layoutView
{
    self.tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self creatData];
    }];
    self.tableView.dataSource =self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = RGB(249, 249, 249, 1);
    [self.view addSubview:self.tableView];
}

-(NSArray *)titles{
    
    if (!_titles) {
        _titles = @[@"财务统计数据",@"用户数据",@"订单数据"];
    }
    return _titles;
}

-(NSArray *)notes{
    
    if (!_notes) {
        _notes = @[@[@"今日营收",@"总营收"],@[@"今日注册",@"总注册"],@[@"今日订单",@"总订单"]];
    }
    return _notes;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 10)];
    view.backgroundColor = RGB(249, 249, 249, 1);
    return view;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * foundCell = @"foundCell";
    
    FoundCell *cell = [tableView dequeueReusableCellWithIdentifier:foundCell];
    
    if (!cell) {
        cell = [[FoundCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:foundCell];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.titLabel.text = [self.titles objectAtIndex:indexPath.section];
    for (int i = 0 ; i < cell.noteLabels.count;i ++) {
        UILabel * label = [cell.noteLabels objectAtIndex:i];
        label.text = [[self.notes objectAtIndex:indexPath.section] objectAtIndex:i];
    }
    if (dataDic) {
        for (int i = 0 ; i < cell.infoLabels.count;i ++) {
            UILabel * label = [cell.infoLabels objectAtIndex:i];
            NSString * string = indexPath.section == 0?@"0.00":@"0";
            NSString * note = @"元";
            switch (indexPath.section) {
                case 0:
                    string = i == 0?dataDic[@"todayMoney"]:dataDic[@"allMoney"];
                    break;
                case 1:
                    note = @"人";
                    string = i == 0?dataDic[@"todayUser"]:dataDic[@"allUser"];
                    break;
                case 2:
                    note = @"单";
                    string = i == 0?dataDic[@"todayOrder"]:dataDic[@"allOrder"];
                    break;
                default:
                    break;
            }
            if (!string) {
                string = indexPath.section == 0?@"0.00":@"0";
            }
            label.text = [NSString stringWithFormat:@"%@%@",string,note];
        }
    }

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
