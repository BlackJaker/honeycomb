//
//  FoundCell.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/15.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FoundCell.h"

@implementation FoundCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutView];
    }
    return self;
}

-(NSMutableArray *)noteLabels{
    
    if (!_noteLabels) {
        _noteLabels = [NSMutableArray new];
    }
    return _noteLabels;
}

-(NSMutableArray *)infoLabels{
    
    if (!_infoLabels) {
        _infoLabels = [NSMutableArray new];
    }
    return _infoLabels;
}

-(void)layoutView
{
    
    NSArray * array = @[@"-",@"-"];
    
    CGFloat  top = 23,infoTopMar = 10,width  = kWidth/array.count,noteHeight = 12,infoHeight = 14;
    
    for (int i = 0; i < array.count; i ++) {
        
        UILabel * noteLabel = [UILabel createLabelWithFrame:CGRectZero text:@"" fontSize:12 textColor:[UIColor colorWithHexString:@"#AFB4C3"]];
        
        [self.backView addSubview:noteLabel];
        
        [noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(width*i);
            make.width.mas_equalTo(width);
            make.top.mas_equalTo(self.lineView.mas_bottom).offset(top);
            make.height.mas_equalTo(noteHeight);
        }];
        
        [self.noteLabels addObject:noteLabel];
        
        UILabel * infoLabel = [UILabel createLabelWithFrame:CGRectZero text:@"" fontSize:14 textColor:[UIColor colorWithHexString:@"#5C6272"]];
        infoLabel.font = [UIFont boldSystemFontOfSize:14];
        [self.backView addSubview:infoLabel];
        
        [infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(width*i);
            make.width.mas_equalTo(width);
            make.top.mas_equalTo(noteLabel.mas_bottom).offset(infoTopMar);
            make.height.mas_equalTo(infoHeight);
        }];
        
        [self.infoLabels addObject:infoLabel];
        
    }
    
    
//    self.todayRegist = [[UILabel alloc]init];
//    self.todayRegist.text = @"今日注册";
//    self.todayRegist.textColor = kGrayColor;
//    self.todayRegist.textAlignment = NSTextAlignmentCenter;
//    self.todayRegist.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
//    [self.contentView addSubview:_todayRegist];
//    [self.todayRegist mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/31.25);
//        make.left.mas_equalTo(0);
//        make.top.mas_equalTo(kWidth/25);
//    }];
//
//    self.weekRegist = [[UILabel alloc]init];
//        self.weekRegist.text = @"本周注册";
//    self.weekRegist.textColor = kGrayColor;
//    self.weekRegist.textAlignment = NSTextAlignmentCenter;
//    self.weekRegist.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
//    [self.contentView addSubview:_weekRegist];
//    [self.weekRegist mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/31.25);
//        make.left.mas_equalTo(self.todayRegist.mas_right).offset(0);
//        make.top.mas_equalTo(kWidth/25);
//    }];
//
//    self.monthLabel = [[UILabel alloc]init];
//        self.monthLabel.text = @"本月注册";
//    self.monthLabel.textColor = kGrayColor;
//    self.monthLabel.textAlignment = NSTextAlignmentCenter;
//    self.monthLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
//    [self.contentView addSubview:_monthLabel];
//    [self.monthLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/31.25);
//        make.left.mas_equalTo(self.weekRegist.mas_right).offset(0);
//        make.top.mas_equalTo(kWidth/25);
//    }];
//    self.todayPerson = [[UILabel alloc]init];
//        self.todayPerson.text = @"0人";
//    self.todayPerson.textColor = kBlackColor;
//    self.todayPerson.textAlignment = NSTextAlignmentCenter;
//    self.todayPerson.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
//    [self.contentView addSubview:_todayPerson];
//    [self.todayPerson mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/25);
//        make.left.mas_equalTo(0);
//        make.top.mas_equalTo(self.todayRegist.mas_bottom).offset(kWidth/37.5);
//    }];
//
//    self.weekPerson = [[UILabel alloc]init];
//        self.weekPerson.text = @"4人";
//    self.weekPerson.textColor = kBlackColor;
//    self.weekPerson.textAlignment = NSTextAlignmentCenter;
//    self.weekPerson.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
//    [self.contentView addSubview:_weekPerson];
//    [self.weekPerson mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/25);
//        make.left.mas_equalTo(self.todayPerson.mas_right).offset(0);
//        make.top.mas_equalTo(self.todayRegist.mas_bottom).offset(kWidth/37.5);
//    }];
//    self.monthPerson = [[UILabel alloc]init];
//        self.monthPerson.text = @"6人";
//    self.monthPerson.textColor = kBlackColor;
//    self.monthPerson.textAlignment = NSTextAlignmentCenter;
//    self.monthPerson.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
//    [self.contentView addSubview:_monthPerson];
//    [self.monthPerson mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/25);
//        make.left.mas_equalTo(self.weekPerson.mas_right).offset(0);
//        make.top.mas_equalTo(self.todayRegist.mas_bottom).offset(kWidth/37.5);
//    }];
//
//    self.todayPrice = [[UILabel alloc]init];
//    self.todayPrice.text = @"0元";
//    self.todayPrice.textColor = kBlackColor;
//    self.todayPrice.textAlignment = NSTextAlignmentCenter;
//    self.todayPrice.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
//    [self.contentView addSubview:_todayPrice];
//    [self.todayPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/25);
//        make.left.mas_equalTo(0);
//        make.top.mas_equalTo(self.todayPerson.mas_bottom).offset(kWidth/37.5);
//    }];
//
//    self.weekPrice = [[UILabel alloc]init];
//    self.weekPrice.text = @"4元";
//    self.weekPrice.textColor = kBlackColor;
//    self.weekPrice.textAlignment = NSTextAlignmentCenter;
//    self.weekPrice.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
//    [self.contentView addSubview:_weekPrice];
//    [self.weekPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/25);
//        make.left.mas_equalTo(self.todayPerson.mas_right).offset(0);
//        make.top.mas_equalTo(self.weekPerson.mas_bottom).offset(kWidth/37.5);
//    }];
//    self.monthPrice = [[UILabel alloc]init];
//    self.monthPrice.text = @"6元";
//    self.monthPrice.textColor = kBlackColor;
//    self.monthPrice.textAlignment = NSTextAlignmentCenter;
//    self.monthPrice.font = [UIFont fontWithName:kPingFangRegular size:kWidth/25];
//    [self.contentView addSubview:_monthPrice];
//    [self.monthPrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/3);
//        make.height.mas_equalTo(kWidth/25);
//        make.left.mas_equalTo(self.weekPerson.mas_right).offset(0);
//        make.top.mas_equalTo(self.monthPerson.mas_bottom).offset(kWidth/37.5);
//    }];
    
//    self.lookBtn = [[UIButton alloc]init];
//    [self.lookBtn setTitle:@"查看详情 >" forState:(UIControlStateNormal)];
//    [self.lookBtn setTitleColor:kRedColorTwo forState:(UIControlStateNormal)];
//    self.lookBtn.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:kWidth/31.25];
//    [self.contentView addSubview:self.lookBtn];
//    [self.lookBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(kWidth/4.1666);
//        make.height.mas_equalTo(kWidth/28.8461);
//        make.left.mas_equalTo(kWidth/2-kWidth/8.3333);
//        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-kWidth/25);
//    }];
}

-(UIView *)backView{
    
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_backView];
        
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
        
    }
    return _backView;
}

-(UILabel *)titLabel{
    
    if (!_titLabel) {
        
        CGFloat top = 13,height = 44;
        
        _titLabel = [UILabel createLabelWithFrame:CGRectZero text:@"" fontSize:16 textColor:[UIColor colorWithHexString:@"#464E5F"]];
        _titLabel.font = [UIFont boldSystemFontOfSize:16];
        [self.backView addSubview:_titLabel];
        
        [_titLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(top);
            make.height.mas_equalTo(height);
        }];
        
    }
    return _titLabel;
}

-(UIView *)lineView{
    
    if (!_lineView) {
        
        CGFloat height = 1;
        
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
        [self.backView addSubview:_lineView];
        
        [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(self.titLabel.mas_bottom);
            make.height.mas_equalTo(height);
        }];
        
    }
    return _lineView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
