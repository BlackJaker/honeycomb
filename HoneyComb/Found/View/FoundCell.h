//
//  FoundCell.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/15.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoundCell : UITableViewCell

@property (nonatomic,strong) UIView *backView;
@property (nonatomic,strong) UILabel *titLabel;
@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,copy) NSMutableArray *noteLabels;
@property (nonatomic,copy) NSMutableArray *infoLabels;

@property (nonatomic,strong) UILabel *todayRegist; //当日注册
@property (nonatomic,strong) UILabel *weekRegist;//本周注册
@property (nonatomic,strong) UILabel *monthLabel;//本月注册
@property (nonatomic,strong) UILabel *todayPerson;//今日的人
@property (nonatomic,strong) UILabel *weekPerson;//本周的人
@property (nonatomic,strong) UILabel *monthPerson;//本月的人
@property (nonatomic,strong) UIView *oneView;
@property (nonatomic,strong) UIView *twoView;
@property (nonatomic,strong) UILabel *todayPrice;//今日的钱
@property (nonatomic,strong) UILabel *weekPrice;//本周的钱
@property (nonatomic,strong) UILabel *monthPrice;//本月的钱
@property (nonatomic,strong) UIButton *lookBtn;
@end
