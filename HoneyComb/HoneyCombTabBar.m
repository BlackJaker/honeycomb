//
//  HoneyCombTabBar.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "HoneyCombTabBar.h"
#import "FoundViewController.h"
#import "HomeViewController.h"
#import "MeViewController.h"
#import "OpenViewController.h"

@interface HoneyCombTabBar ()<UITabBarControllerDelegate>

@end

@implementation HoneyCombTabBar

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
     [UITabBar appearance].clipsToBounds = YES;
     [self creatNCs];
  
     [[UITabBarItem appearance]setTitleTextAttributes:@{NSFontAttributeName:[UIFont   fontWithName:kPingFangRegular size:10],NSForegroundColorAttributeName:RGB(153, 153, 153, 1)}   forState:UIControlStateNormal];

    UIImage *tabbarimage=[UIImage imageNamed:@"tabbar"];
    UIImageView *tabBarBackgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.tabBar.frame.size.width, self.tabBar.frame.size.height)];
    tabBarBackgroundImageView.image =tabbarimage;
    [self.tabBar insertSubview:tabBarBackgroundImageView atIndex:0];
}

-(void)creatNCs
{
    [self creatHomeNC];
    [self creatOpenNC];
    [self creatFoundNC];
    [self creatMeNC];
    self.viewControllers = @[_homeNC,_openNC,_foundNC,_meNC];
    self.selectedIndex = 0;
}
-(void)creatHomeNC
{
    self.homeVC = [[HomeViewController alloc]init];
    self.homeNC = [[UINavigationController alloc]initWithRootViewController:self.homeVC];
    _homeNC.tabBarItem.title = @"订单管理";
    _homeNC.tabBarItem.image = [UIImage imageNamed:@"订单管理"];
    _homeNC.tabBarItem.selectedImage = [[UIImage imageNamed:@"订单管理1"]imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
     [_homeNC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:(UIControlStateSelected)];
    
}
-(void)creatOpenNC
{
    self.openVC = [[OpenViewController alloc]init];
    self.openNC = [[UINavigationController alloc]initWithRootViewController:self.openVC];
    _openNC.tabBarItem.title = @"用户管理";
    _openNC.tabBarItem.image = [[UIImage imageNamed:@"用户管理"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _openNC.tabBarItem.selectedImage = [[UIImage imageNamed:@"用户管理1"]imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];

     [_openNC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:(UIControlStateSelected)];
}
-(void)creatFoundNC
{
    self.foundVC = [[FoundViewController alloc]init];
    self.foundNC= [[UINavigationController alloc]initWithRootViewController:self.foundVC];
    _foundNC.tabBarItem.title = @"数据统计";
    _foundNC.tabBarItem.image = [UIImage imageNamed:@"数据9"];
    _foundNC.tabBarItem.selectedImage = [[UIImage imageNamed:@"数据8"]imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
     [_foundNC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:(UIControlStateSelected)];
}
-(void)creatMeNC
{
    self.meVC = [[MeViewController alloc]init];
    self.meNC = [[UINavigationController alloc]initWithRootViewController:self.meVC];
    _meNC.tabBarItem .title = @"个人中心";
    _meNC.tabBarItem.image = [[UIImage imageNamed:@"个人中心1"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _meNC.tabBarItem.selectedImage = [[UIImage imageNamed:@"个人中心"]imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
     [_meNC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:(UIControlStateSelected)];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
