//
//  AppDelegate.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AppDelegate.h"
#import "HoneyCombTabBar.h"
#import "LoginViewController.h"
#import <UserNotifications/UserNotifications.h>
#import "FC_AlertController.h"
#import "LoginUser.h"
#import "HomeViewController.h"
#import "SegmentViewController.h"
#import "BettingViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Bugtags/Bugtags.h>

#import "FC_UpdateView.h"

@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
#if 1
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    if (application.applicationIconBadgeNumber > 0) {
        application.applicationIconBadgeNumber = 0;
    }
    
    [self configureKeyBoard];  // 键盘
    
    //状态栏字体颜色
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    if (@available(iOS 11.0,*)) {
        [[UIScrollView appearance]setContentInsetAdjustmentBehavior:(UIScrollViewContentInsetAdjustmentNever)];
    }
    
    [NSObject mj_setupReplacedKeyFromPropertyName:^NSDictionary *{
         return @{@"ID": @"id"};
     }];
    
    NSString * shopStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"shopStatus"];
    
    if (!shopStatus) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"shopStatus"];  // 首页验证
    }
    
    [self UMCConfirgue];  // 分享配置
    
    [self configueUserNotificatoin];  // 注册推送通知
    
    [self initMainVIew];
    [self popAllController];
    [self.window makeKeyAndVisible];
    #endif
    
//    //Bugtags
//    [Bugtags startWithAppKey:@"e700338d0fef332667220f8ae5a4f784" invocationEvent:BTGInvocationEventBubble];
    
    return YES;

}

-(void)configureKeyBoard{
    
    /**
     *  IQKeyBoardManager
     */
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = NO;
    manager.enableAutoToolbar = YES;
    manager.toolbarManageBehaviour = IQAutoToolbarByPosition;
    manager.toolbarTintColor = COLOR_HEX(0xf84848, 1.0f);
    manager.keyboardDistanceFromTextField = 10.0f; // 输入框距离键盘的距离
    manager.toolbarManageBehaviour = IQAutoToolbarBySubviews;
    
}

#pragma mark  --  友盟配置  --

-(void)UMCConfirgue{
    
    [UMConfigure initWithAppkey:UMENG_APP_KEY channel:CHANNEL];
    
    // U-Share 平台设置
    [self configUSharePlatforms];
    
}

- (void)configUSharePlatforms
{
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WX_APP_KEY appSecret:WX_APP_SECRET redirectURL:BACK_CALL_URL];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    [[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite),@(UMSocialPlatformType_Sina)]];
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     */
    //    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1105821097"/*设置QQ平台的appID*/  appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];
    //    /* 设置新浪的appKey和appSecret */
    //    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:@"3921700954"  appSecret:@"04b48b094faeb16683c32669824ebdad" redirectURL:@"https://sns.whalecloud.com/sina2/callback"];
    //    /* 钉钉的appKey */
    
}

#pragma mark  --  通知注册  --

-(void)configueUserNotificatoin{
    
    if (@available(iOS 10.0, *)) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( granted ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
                
            }
        }];
        
    } else {
        
        UIApplication *app = [UIApplication sharedApplication];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        // 8.0后注册通知
        if ([app respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            [app registerUserNotificationSettings:settings];
        }
        
    }
    
}

- (void)initMainVIew {
    HoneyCombTabBar *chooseVC = [[HoneyCombTabBar alloc]init];
    self.window.rootViewController = chooseVC;
    if ([[LoginUser shareLoginUser] shopId] == nil) {
        LoginViewController *login = [[LoginViewController alloc]init];
        UINavigationController *homeNC = chooseVC.viewControllers[0];
        homeNC.tabBarController.tabBar.hidden = YES;
        [homeNC pushViewController:login animated:NO];
    }
    
    [self shopUpdateRequest];
}


- (void)popAllController{
    NSArray *array = ((UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController).viewControllers;
    if ([array count] >= 1)
    {
        for (NSUInteger index= [array count] - 1;index > 0; index-- )
        {
            UIViewController *controller = [array objectAtIndex:index];
            [controller.navigationController popViewControllerAnimated:NO];
        }
    }
}


#pragma mark  --    update  --

-(void)shopUpdateRequest{
    
    NSDictionary *dic = @{
                          @"version":[InfoDictionary valueForKey:@"CFBundleShortVersionString"],
                          @"device":@"2"
                          };
    [PPNetworkHelper POST:ShopUpdate
               parameters:dic
                  success:^(id responseObject) {
                      
                   
                   if ([[responseObject objectForKey:@"state"]  isEqualToString:@"success"]) {
                       
                       if ([responseObject[@"data"][@"isUpdate"] boolValue]) {
                           
                           FC_UpdateView * updateView = [[FC_UpdateView alloc]initWithFrame:self.window.bounds data:responseObject[@"data"]];
                           
                           [updateView show];
                       }
                   }
               } failure:^(NSError *error) {
                   
               }];
    
}


//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
//- (void)setStatusBarBackgroundColor:(UIColor *)color {
//
//    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//        statusBar.backgroundColor = color;
//    }
//}

#pragma mark  --  第三方友盟  --

// 支持所有iOS系统
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *deviceTokenStr = [[[[deviceToken description]
                                  
                                  stringByReplacingOccurrencesOfString:@"<" withString:@""]
                                 
                                 stringByReplacingOccurrencesOfString:@">" withString:@""]
                                
                                stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"deviceToken == \n%@",deviceTokenStr);
    
    [[LoginUser shareLoginUser] setDevToken:deviceTokenStr];
    
}

// 注册deviceToken失败

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    NSLog(@"error -- %@",error);
    
}

//iOS10以下使用这两个方法接收通知
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    if([[[UIDevice currentDevice] systemVersion]intValue] < 10){
        //        [UMessage didReceiveRemoteNotification:userInfo];
        
        [self removeToHomeWithInfo:userInfo];
        
    }
    completionHandler(UIBackgroundFetchResultNewData);
}
//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler API_AVAILABLE(ios(10.0)){
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        DYLog(@"前台接受到通知   %@",userInfo);
        
        completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);
//        [self showAlertWithInfo:userInfo];
        
    }else{
        //应用处于前台时的本地推送接受
    }
    
    
}

-(void)showAlertWithInfo:(NSDictionary *)userInfo{
    
    NSString * title = @"温馨提示";
    NSString * content = nil;
    
    if ([userInfo[@"aps"][@"alert"] isKindOfClass:[NSString class]]) {
        content = userInfo[@"aps"][@"alert"];
    }else{
        title = [userInfo[@"aps"][@"alert"] valueForKey:@"title"];
        content = [userInfo[@"aps"][@"alert"] valueForKey:@"body"];
    }
    
    FC_AlertController *alertVc = [FC_AlertController alertControllerWithTitle:title message:content preferredStyle:UIAlertControllerStyleAlert];
    @WeakObj(self)
    alertVc.checkBlock = ^{
        @StrongObj(self)
        
        [self removeToHomeWithInfo:userInfo];
    };
    [alertVc show];
    
}
// 更新当前显示
-(void)removeToHomeWithInfo:(NSDictionary *)userInfo{
    
    HoneyCombTabBar * vc = (HoneyCombTabBar *)self.window.rootViewController;
    HomeViewController * homeVc = vc.homeVC;
    SegmentViewController * segVc = homeVc.vc;
    [segVc uploadCurrentTag:10000];
    if (vc.selectedIndex == 0) {
        [segVc.navigationController popToRootViewControllerAnimated:YES];
    }else{
        vc.selectedIndex = 0;
    }
    
    if (userInfo[@"datas"]) {
        
        NSData *jsonData = [userInfo[@"datas"] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        
        if ([dict[@"type"] isEqualToString:@"print"]) {
          BettingViewController * betVc =  [segVc.subViewControllers firstObject];
          [betVc reloadData];
        }
    }
    
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler API_AVAILABLE(ios(10.0)){
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //        //应用处于后台时的远程推送接受
        //        //必须加这句代码
        //        [UMessage didReceiveRemoteNotification:userInfo];
        DYLog(@"应用在后台接受到通知  %@",userInfo);
        [self removeToHomeWithInfo:userInfo];
    }else{
        //应用处于后台时的本地推送接受
    }
//     completionHandler(UNNotificationPresentationOptionSound);
}


static SystemSoundID push = 0;

-(void) playSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"push" ofType:@"wav"];
    NSLog(@"path = %@",path);
    if (path) {
        //注册声音到系统
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path],&push);
        AudioServicesPlaySystemSound(push);
        //        AudioServicesPlaySystemSound(push);//如果无法再下面播放，可以尝试在此播放
    }
    
    AudioServicesPlaySystemSound(push);   //播放注册的声音，（此句代码，可以在本类中的任意位置调用，不限于本方法中）
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);   //让手机震动
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
