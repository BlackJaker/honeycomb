//
//  MyOSSClient.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "MyOSSClient.h"
NSString *const endPoint = @"oss-cn-huhehaote.aliyuncs.com";
NSString *const AccessKey = @"<AccessKey>";
NSString *const SecretKey = @"<SecretKey>";
@implementation MyOSSClient
+ (instancetype)sharedInstance {
    static MyOSSClient *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self _initClient];
    });
    return instance;
}

+ (MyOSSClient *)_initClient {
    // 打开调试log
    [OSSLog enableLog];
    
    // oss-cn-shenzhen    oss-cn-shenzhen.aliyuncs.com    oss-cn-shenzhen-internal.aliyuncs.com
    
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:AccessKey secretKey:SecretKey];
    
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.maxRetryCount = 3; // 网络请求遇到异常失败后的重试次数
    conf.timeoutIntervalForRequest = 30; // 网络请求的超时时间
    conf.timeoutIntervalForResource = 24 * 60 * 60; // 允许资源传输的最长时间
    
    MyOSSClient *client = [[MyOSSClient alloc] initWithEndpoint:endPoint credentialProvider:credential clientConfiguration:conf];
    return client;
}

@end
