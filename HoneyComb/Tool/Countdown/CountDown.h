//
//  CountDown.h
//  LotteryApp
//
//  Created by syqaxldy on 2018/8/28.
//  Copyright © 2018年 new. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountDown : NSObject
{
    dispatch_source_t _timer;
}
@property (nonatomic,copy) NSString *s;
//-(NSString *)activeCountDownAction:(NSString *)str;
-(NSString *)activeCountDownAction:(NSString *)str surplusTime:(void(^)(NSString *surplusTime))surplusTime;
@end
