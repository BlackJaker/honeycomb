//
//  UIButton+Timer.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Timer)
/**
 *  计时时间
 */
@property(nonatomic,assign,readwrite)NSInteger time;

/**
 *  format   
 */
@property(nonatomic,copy)NSString *  format;

/**
 开启计时器
 */
- (void)startTimer;

/**
 干掉计时器
 */
- (void)endTimer;
@end
