//
//  FC_Manager.h
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FC_Manager : NSObject

/**
 *  得到app名称
 *
 *  @return app名称
 */
+ (NSString *)getAppName;

/**
 *  得到app版本
 *
 *  @return app版本
 */
+ (NSString *)getAppVersion;

/**
 *  得到UUID（不唯一）
 *
 *  @return 获得的UUID
 */
//+ (NSString *)getUUID;

/**
 *  得到ios系统版本
 *
 *  @return ios系统版本
 */
+ (float)getIOSVersion;


//   分享
/*
 *    文本
 */

+(void)shareText:(NSString *)text;

/*
 *    图片
 */

+(void)shareThumImage:(UIImage *)thumbImage
           shareImage:(id)shareImage;

/*
 *    文本图片
 */

+(void)shareText:(NSString *)text
       thumImage:(UIImage *)thumImage
      shareImage:(id)shareImage;

/*
 *    链接
 */

+ (void)shareTitle:(NSString *)title
             descr:(NSString *)descr
         thumImage:(UIImage *)thumImage
            urlStr:(NSString *)urlStr;

/**
 *  分享
 *  @param title          标题
 *  @param text           文本
 *  @param descr          描述
 *  @param thumImage      缩略图
 *  @param shareImage     图片 id类型多种形式
 *  @param urlStr         链接地址
 */

+ (void)shareTitle:(nullable NSString *)title
              text:(nullable NSString *)text
             descr:(nullable NSString *)descr
         thumImage:(nullable UIImage *)thumImage
        shareImage:(nullable id)shareImage
            urlStr:(nullable NSString *)urlStr;

/**
 *  创建statusBar上的toast
 *
 *  @param text 要显示的文字
 */
+ (void)showToastWithText:(NSString *)text;

/**
 *  将int快速转为string
 *
 *  @param value 要转换的int
 *
 *  @return 返回的string
 */
+ (NSString *)stringValueWithInt:(int)value;

///**
// * 注册推送
// */
//+ (void)registerRemoteNotification;


/**
 *  根据宽度求高度
 */

+ (CGFloat)getHeightWithContent:(NSString *)content width:(CGFloat)width font:(CGFloat)font;

/**
 * 根据高度度求宽度
 */

+ (CGFloat)getWidthWithContent:(NSString *)content height:(CGFloat)height font:(CGFloat)font;

/**
 * 根据颜色转换成图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 * 给UILabel设置行间距和字间距
 */
+(void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font;
/**
 * 计算UILabel的高度(带有行间距的情况)
 */
+(CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width;

@end

NS_ASSUME_NONNULL_END
