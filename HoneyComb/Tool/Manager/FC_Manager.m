//
//  FC_Manager.m
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_Manager.h"

#import <MBProgressHUD.h>

#define UILABEL_LINE_SPACE 5

@implementation FC_Manager

/**
 *  得到app名称
 *
 *  @return app名称
 */
+ (NSString *)getAppName
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleDisplayName"];
}

/**
 *  得到app版本
 *
 *  @return app版本
 */
+ (NSString *)getAppVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [infoDictionary objectForKey:@"CFBundleShortVersionString"];
}

/**
 *  得到UUID（唯一）
 *
 *  @return 获得的UUID
 */
//+ (NSString *)getUUID
//{
//    NSLog(@"-------%@--------", [FCUUID uuidForDevice]);
//    return [FCUUID uuidForDevice];
//}

/**
 *  得到ios系统版本
 *
 *  @return ios系统版本
 */
+ (float)getIOSVersion
{
    
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

#pragma mark  --  share  分享  --

#pragma mark  --  文本  --

+ (void)shareText:(NSString *)text
{
    [self shareTitle:nil text:text descr:nil thumImage:nil shareImage:nil urlStr:nil];
}

#pragma mark  --  图片  --

+(void)shareThumImage:(UIImage *)thumbImage
           shareImage:(id)shareImage{
    
    [self shareTitle:nil text:nil descr:nil thumImage:thumbImage shareImage:shareImage urlStr:nil];
    
}

#pragma mark  --  图文  --

+(void)shareText:(NSString *)text thumImage:(UIImage *)thumImage shareImage:(id)shareImage{
    
    [self shareTitle:nil text:text descr:nil thumImage:thumImage shareImage:shareImage urlStr:nil];
    
}

+ (void)shareTitle:(NSString *)title descr:(NSString *)descr thumImage:(UIImage *)thumImage urlStr:(NSString *)urlStr
{
    [self shareTitle:title text:nil descr:descr thumImage:thumImage shareImage:nil urlStr:urlStr];
}

+ (void)shareTitle:(nullable NSString *)title
              text:(nullable NSString *)text
             descr:(nullable NSString *)descr
         thumImage:(nullable UIImage *)thumImage
        shareImage:(nullable id)shareImage
            urlStr:(nullable NSString *)urlStr{
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    UMShareWebpageObject * webShareObject;
    UMShareImageObject *imgShareObject;
    
    if (text) {
        messageObject.text = text;
    }
    if (title || descr) {
        webShareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:descr thumImage:thumImage];
        webShareObject.webpageUrl = urlStr;
        messageObject.shareObject = webShareObject;
    }else{
        if (thumImage || shareImage) {
            imgShareObject = [[UMShareImageObject alloc]init];
            if (thumImage) {
                imgShareObject.thumbImage = thumImage;
            }
            if (shareImage) {
                imgShareObject.shareImage = shareImage;
            }
            messageObject.shareObject = imgShareObject;
        }
    }
    
    [self showMenuWithMessageObject:messageObject];
    
}

#pragma mark  --  调用分享面板  --

+(void)showMenuWithMessageObject:(UMSocialMessageObject *)messageObject{
    
    @WeakObj(self)
    //显示分享面板
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        // 根据获取的platformType确定所选平台进行下一步操作
        
        @StrongObj(self)
        
        [self shareWithPlatformType:platformType messageObject:messageObject];
        
    }];
    
}

// 分享

+(void)shareWithPlatformType:(UMSocialPlatformType)platformType messageObject:(UMSocialMessageObject *)messageObject{
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
            [self showToastWithText:@"分享失败"];
        }else{
            NSLog(@"response data is %@",data);
            [self showToastWithText:@"分享成功"];
        }
    }];
    
}


/**
 *  创建statusBar上的toast
 *
 *  @param text 要显示的文字
 */
+ (void)showToastWithText:(NSString *)text
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.animationType = MBProgressHUDAnimationZoomIn;
    hud.removeFromSuperViewOnHide = YES;
    hud.userInteractionEnabled = NO;
    [hud hideAnimated:YES afterDelay:2];
}

/**
 *  将int快速转为string
 *
 *  @param value 要转换的int
 *
 *  @return 返回的string
 */
+ (NSString *)stringValueWithInt:(int)value
{
    return [NSString stringWithFormat:@"%d", value];
}

//根据宽度求高度  content 计算的内容  width 计算的宽度 font字体大小
+ (CGFloat)getHeightWithContent:(NSString *)content width:(CGFloat)width font:(CGFloat)font{
    
    
    CGRect rect = [content boundingRectWithSize:CGSizeMake(width, 999)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]}
                                        context:nil];
    return rect.size.height;
    
    
}


//根据高度求宽度  content 计算的内容  Height 计算的高度 font字体大小

+ (CGFloat)getWidthWithContent:(NSString *)content height:(CGFloat)height font:(CGFloat)font{
    
    
    CGRect rect = [content boundingRectWithSize:CGSizeMake(999, height)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]}
                                        context:nil];
    
    return rect.size.width;
    
    
}




+ (UIImage *)imageWithColor:(UIColor *)color
{
    
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    
    return image;
}


+(void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = UILABEL_LINE_SPACE; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.f
                          };
    
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:str attributes:dic];
    label.attributedText = attributeStr;
}

+(CGFloat)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = UILABEL_LINE_SPACE;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.f
                          };
    
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, kHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}

@end
