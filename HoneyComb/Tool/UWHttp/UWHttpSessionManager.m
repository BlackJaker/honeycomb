//
//  UWHttpManager.m
//  MeiYou
//
//  Created by 陈亚勃 on 2017/11/30.
//  Copyright © 2017年 Sheldon. All rights reserved.
//

#import "UWHttpSessionManager.h"
#import "AFNetworkActivityIndicatorManager.h"

//  网络静态参数设置
static NSTimeInterval uw_requestTimeout = 40;
//  请求头
static NSDictionary *uw_httpHeaders = nil;


@implementation UWHttpSessionManager
static UWHttpSessionManager *instance = nil;
+ (UWHttpSessionManager *)defaultManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!instance) {
            [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
            //  初始化BaseUrl
            instance = [[UWHttpSessionManager alloc] init];
            
            instance.requestSerializer.HTTPShouldHandleCookies = YES;
            
            //  JSON序列化
            instance.requestSerializer = [AFJSONRequestSerializer serializer];
            instance.requestSerializer.stringEncoding = NSUTF8StringEncoding;
            instance.requestSerializer.timeoutInterval = uw_requestTimeout;
            instance.responseSerializer = [AFJSONResponseSerializer serializer];
          
            instance.responseSerializer.acceptableContentTypes = [NSSet
                                                                  setWithArray:@[ @"application/json", @"text/html", @"text/json", @"text/javascript", @"text/plain"]];
    
            [instance.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        }
        
        // 设置允许同时最大并发数量，过大容易出问题
        instance.operationQueue.maxConcurrentOperationCount = 5;
    });
    return instance;
}

- (void)updateCommonHttpHeaders:(NSDictionary *)httpHeaders {
    uw_httpHeaders = httpHeaders;
    for (NSString *key in uw_httpHeaders.allKeys) {
        if (uw_httpHeaders[key]) {
            [instance.requestSerializer setValue:uw_httpHeaders[key] forHTTPHeaderField:key];
        }
    }
}

@end
