//
//  UWHttpManager.h
//  MeiYou
//
//  Created by 陈亚勃 on 2017/11/30.
//  Copyright © 2017年 Sheldon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
@interface UWHttpSessionManager : AFHTTPSessionManager
+ (UWHttpSessionManager *)defaultManager;

//设置请求头
- (void)updateCommonHttpHeaders:(NSDictionary *)httpHeaders;

@end
