//
//  UWHttpTool.h
//  uworks-library
//
//  Created by SheldonLee on 15/10/30.
//  Copyright © 2015年 U-Works. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static NSString *NeedLoginErrorMsg = @"请登录";
static NSString *NeedBindErrorMsg = @"请绑定手机号";

@interface UWHttpTool : NSObject

typedef void (^httpSuccessBlock)(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject);
typedef void (^httpFailureBlock)(NSURLSessionDataTask *_Nullable task, NSString *_Nonnull errorMsg);
typedef void (^httpProgressBlock)(NSProgress *_Nonnull uploadProgress);

/**
 *  get请求
 *
 *  @param url     请求地址
 *  @param params  请求参数
 *  @param success 请求成功回调
 *  @param failure 请求失败回调
 */
+ (void)getWithURL:(NSString *_Nonnull)url
            params:(NSDictionary *_Nullable)params
           success:(httpSuccessBlock _Nullable)success
           failure:(httpFailureBlock _Nullable)failure;

/**
 *  post请求
 *
 *  @param url     请求地址
 *  @param params  请求参数
 *  @param success 请求成功回调
 *  @param failure 请求失败回调
 */
+ (void)postWithURL:(NSString *_Nonnull)url
             params:(NSDictionary *_Nullable)params
            success:(httpSuccessBlock _Nullable)success
            failure:(httpFailureBlock _Nullable)failure;

/**
 *  get请求(带请求进度回调)
 *
 *  @param url      请求地址
 *  @param params   请求参数
 *  @param progress 请求进度回调
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 */
+ (void)getWithURL:(NSString *_Nonnull)url
            params:(NSDictionary *_Nullable)params
          progress:(httpProgressBlock _Nullable)progress
           success:(httpSuccessBlock _Nullable)success
           failure:(httpFailureBlock _Nullable)failure;

/**
 *  post请求(带请求进度回调)
 *
 *  @param url      请求地址
 *  @param params   请求参数
 *  @param progress 请求进度回调
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 */
+ (void)postWithURL:(NSString *_Nonnull)url
             params:(NSDictionary *_Nullable)params
           progress:(httpProgressBlock _Nullable)progress
            success:(httpSuccessBlock _Nullable)success
            failure:(httpFailureBlock _Nullable)failure;

/**
 表单请求

 @param url url
 @param params param description
 @param success success description
 @param failure failure description
 */
+ (void)formDataRequestWithURL:(NSString * _Nonnull)url
                        params:(NSDictionary *)params
                       success:(httpSuccessBlock _Nullable)success
                       failure:(httpFailureBlock _Nullable)failure;

/**
 带有token的表单请求

 @param url url
 @param params param description
 @param success success description
 @param failure failure description
 */
+ (void)formDataTokenRequestWithURL:(NSString * _Nonnull)url
                             params:(NSDictionary *)params
                            success:(httpSuccessBlock _Nullable)success
                            failure:(httpFailureBlock _Nullable)failure;


@end
