//
//  UWHttpTool.m
//  uworks-library
//
//  Created by SheldonLee on 15/10/30.
//  Copyright © 2015年 U-Works. All rights reserved.
//

#import "UWHttpTool.h"
#import <AFNetworkActivityIndicatorManager.h>
#import <AFNetworking.h>
#import "UWHttpSessionManager.h"
#import "TRObjectToData.h"
#import <AVKit/AVKit.h>
//  是否开启调试
static BOOL uw_isEnableInterfaceDebug = YES;
//  是否打印json字符串调试
static BOOL uw_isLogJsonStringDebug = NO;
//  请求头
static NSDictionary *uw_httpHeaders = nil;

static NSString *API_VERSION = @"1.0";
static NSString *API_SAFE_CODE = @"apisafecode";


@implementation UWHttpTool

+ (void)getWithURL:(NSString *)url
            params:(NSDictionary *)params
           success:(httpSuccessBlock)success
           failure:(httpFailureBlock)failure {
    [self getWithURL:url params:params progress:nil success:success failure:failure];
}

+ (void)postWithURL:(NSString *)url
             params:(NSDictionary *)params
            success:(httpSuccessBlock)success
            failure:(httpFailureBlock)failure {
    [self postWithURL:url params:params progress:nil success:success failure:failure];
}

+ (void)postWithURL:(NSString *)url
             params:(NSDictionary *)params
           progress:(httpProgressBlock)progress
            success:(httpSuccessBlock)success
            failure:(httpFailureBlock)failure {
    
    [self starNetworkActivityIndicatorVisible];
    
    UWHttpSessionManager *manager = [UWHttpSessionManager defaultManager];
    
    [manager POST:url
       parameters:params
         progress:^(NSProgress *_Nonnull uploadProgress) {
             if (progress) {
                 progress(uploadProgress);
             }
         }
          success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
              [self endNetworkActivityIndicatorVisible];
              
              [self handleHttpSuccessWithSuccessBlock:success
                                       responseObject:responseObject
                                         failureBlock:failure
                                                 task:task];
              
              
              if (uw_isEnableInterfaceDebug) {
                  [self logWithSuccessResponse:responseObject
                                           url:task.response.URL.absoluteString
                                        params:params];
              }
              if (uw_isLogJsonStringDebug) {
                  [self logJsonStringWithResponseObject:responseObject];
              }
              
          }
          failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              [self endNetworkActivityIndicatorVisible];
              [self handleFailureWithFailureBlock:failure task:task error:error];
              
              if (uw_isEnableInterfaceDebug) {
                  [self logWithFailError:error url:task.response.URL.absoluteString params:params];
              }
              
          }];
}

+ (void)getWithURL:(NSString *)url
            params:(NSDictionary *)params
          progress:(httpProgressBlock)progress
           success:(httpSuccessBlock)success
           failure:(httpFailureBlock)failure {
    
    [self starNetworkActivityIndicatorVisible];
    
    UWHttpSessionManager *manager = [UWHttpSessionManager defaultManager];
    
    [manager GET:url
      parameters:params
        progress:^(NSProgress *_Nonnull downloadProgress) {
            if (progress) {
                progress(downloadProgress);
            }
        }
         success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
             [self endNetworkActivityIndicatorVisible];
             
             [self handleHttpSuccessWithSuccessBlock:success
                                      responseObject:responseObject
                                        failureBlock:failure
                                                task:task];
             
             if (uw_isEnableInterfaceDebug) {
                 [self logWithSuccessResponse:responseObject
                                          url:task.response.URL.absoluteString
                                       params:params];
             }
             if (uw_isLogJsonStringDebug) {
                 [self logJsonStringWithResponseObject:responseObject];
             }
             
         }
         failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
             [self endNetworkActivityIndicatorVisible];
             
             [self handleFailureWithFailureBlock:failure task:task error:error];
             
             if (uw_isEnableInterfaceDebug) {
                 [self logWithFailError:error url:task.response.URL.absoluteString params:params];
             }
         }];
}

+ (void)uploadImageWithURL:(NSString *)url
                     image:(UIImage *)image
                    params:(NSDictionary *)params
                  progress:(httpProgressBlock)progress
                   success:(httpSuccessBlock)success
                   failure:(httpFailureBlock)failure {
    [self starNetworkActivityIndicatorVisible];
    UWHttpSessionManager *manager = [UWHttpSessionManager defaultManager];
    [manager POST:url
       parameters:params
constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    [formData appendPartWithFileData:imageData
                                name:@"file"
                            fileName:@"image.jpg"
                            mimeType:@"image/jpeg"];
    [formData appendPartWithFormData:[TRObjectToData objectToData:API_SAFE_CODE] name:@"safecode"];
    [formData appendPartWithFormData:[TRObjectToData objectToData:API_VERSION] name:@"apiversion"];
}
         progress:^(NSProgress *_Nonnull uploadProgress) {
             
         }
          success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
              [self endNetworkActivityIndicatorVisible];
              
              [self handleHttpSuccessWithSuccessBlock:success
                                       responseObject:responseObject
                                         failureBlock:failure
                                                 task:task];
              
              if (uw_isEnableInterfaceDebug) {
                  [self logWithSuccessResponse:responseObject
                                           url:task.response.URL.absoluteString
                                        params:params];
              }
              if (uw_isLogJsonStringDebug) {
                  [self logJsonStringWithResponseObject:responseObject];
              }

          }
          failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              [self endNetworkActivityIndicatorVisible];
              if (uw_isEnableInterfaceDebug) {
                  [self logWithFailError:error url:task.response.URL.absoluteString params:params];
              }
              
              [self handleFailureWithFailureBlock:failure task:task error:error];
              
          }];
}

+ (void)uploadVideoWithURL:(NSString *)url
                     videoData:(NSData *)videoData
                    params:(NSDictionary *)params
                  progress:(httpProgressBlock)progress
                   success:(httpSuccessBlock)success
                   failure:(httpFailureBlock)failure {
    [self starNetworkActivityIndicatorVisible];
    UWHttpSessionManager *manager = [UWHttpSessionManager defaultManager];
    [manager POST:url
       parameters:params
constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {

    [formData appendPartWithFileData:videoData
                                name:@"file"
                            fileName:@"video.mp4"
                            mimeType:@"video/mp4"];
    [formData appendPartWithFormData:[TRObjectToData objectToData:API_SAFE_CODE] name:@"safecode"];
    [formData appendPartWithFormData:[TRObjectToData objectToData:API_VERSION] name:@"apiversion"];
}
         progress:^(NSProgress *_Nonnull uploadProgress) {

         }
          success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
              [self endNetworkActivityIndicatorVisible];

              [self handleHttpSuccessWithSuccessBlock:success
                                       responseObject:responseObject
                                         failureBlock:failure
                                                 task:task];

              if (uw_isEnableInterfaceDebug) {
                  [self logWithSuccessResponse:responseObject
                                           url:task.response.URL.absoluteString
                                        params:params];
              }
              if (uw_isLogJsonStringDebug) {
                  [self logJsonStringWithResponseObject:responseObject];
              }

          }
          failure:^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              [self endNetworkActivityIndicatorVisible];
              if (uw_isEnableInterfaceDebug) {
                  [self logWithFailError:error url:task.response.URL.absoluteString params:params];
              }

              [self handleFailureWithFailureBlock:failure task:task error:error];

          }];
}


/**
 表单请求
 
 @param url url
 @param params param description
 @param success success description
 @param failure failure description
 */
+ (void)formDataRequestWithURL:(NSString * _Nonnull)url
                        params:(NSDictionary *)params
                       success:(httpSuccessBlock _Nullable)success
                       failure:(httpFailureBlock _Nullable)failure{
    [self starNetworkActivityIndicatorVisible];
    
    UWHttpSessionManager *manager = [UWHttpSessionManager defaultManager];
    [manager POST:url
       parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (NSString *key in params.allKeys) {
            id value = params[key];
            NSData *data = [TRObjectToData objectToData:value];
            [formData appendPartWithFormData:data name:key];
        }
    
        [formData appendPartWithFormData:[TRObjectToData objectToData:API_SAFE_CODE] name:@"safecode"];
        [formData appendPartWithFormData:[TRObjectToData objectToData:API_VERSION] name:@"apiversion"];
    
    } progress:^(NSProgress * _Nonnull uploadProgress) {
    
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self endNetworkActivityIndicatorVisible];
        
        [self handleHttpSuccessWithSuccessBlock:success
                                 responseObject:responseObject
                                   failureBlock:failure
                                           task:task];
        
        if (uw_isEnableInterfaceDebug) {
            [self logWithSuccessResponse:responseObject
                                     url:task.response.URL.absoluteString
                                  params:params];
        }
        if (uw_isLogJsonStringDebug) {
            [self logJsonStringWithResponseObject:responseObject];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self endNetworkActivityIndicatorVisible];
        if (uw_isEnableInterfaceDebug) {
            [self logWithFailError:error url:task.response.URL.absoluteString params:params];
        }

        [self handleFailureWithFailureBlock:failure task:task error:error];
    }];
}

/**
 带有token的表单请求
 
 @param url url
 @param params param description
 @param success success description
 @param failure failure description
 */
+ (void)formDataTokenRequestWithURL:(NSString * _Nonnull)url
                             params:(NSDictionary *)params
                            success:(httpSuccessBlock _Nullable)success
                            failure:(httpFailureBlock _Nullable)failure{
    [self starNetworkActivityIndicatorVisible];
    
    UWHttpSessionManager *manager = [UWHttpSessionManager defaultManager];
    [manager POST:url
       parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
           for (NSString *key in params.allKeys) {
               id value = params[key];
               NSData *data = [TRObjectToData objectToData:value];
               [formData appendPartWithFormData:data name:key];
           }
           
           [formData appendPartWithFormData:[TRObjectToData objectToData:API_SAFE_CODE] name:@"safecode"];
           [formData appendPartWithFormData:[TRObjectToData objectToData:API_VERSION] name:@"apiversion"];
           
//           NSString *token = UserModel.token;
//           [formData appendPartWithFormData:[TRObjectToData objectToData:token] name:@"token"];
//           [formData appendPartWithFormData:[TRObjectToData objectToData:@"4a9bd19b3b8676199592a34602097771"] name:@"token"];
           
           
       } progress:^(NSProgress * _Nonnull uploadProgress) {
           
       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           [self endNetworkActivityIndicatorVisible];
           
           [self handleHttpSuccessWithSuccessBlock:success
                                    responseObject:responseObject
                                      failureBlock:failure
                                              task:task];
           
           if (uw_isEnableInterfaceDebug) {
               [self logWithSuccessResponse:responseObject
                                        url:task.response.URL.absoluteString
                                     params:params];
           }
           if (uw_isLogJsonStringDebug) {
               [self logJsonStringWithResponseObject:responseObject];
           }
           
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           [self endNetworkActivityIndicatorVisible];
           if (uw_isEnableInterfaceDebug) {
               [self logWithFailError:error url:task.response.URL.absoluteString params:params];
           }
           
           [self handleFailureWithFailureBlock:failure task:task error:error];
       }];
}


+ (void)handleHttpSuccessWithSuccessBlock:(httpSuccessBlock)success
                           responseObject:(id _Nullable)responseObject
                             failureBlock:(httpFailureBlock)failure
                                 task:(NSURLSessionDataTask *)task{
    if ([task.response.URL.absoluteString containsString:@".html"]) {
        NSString *code = [NSString stringWithFormat:@"%@",responseObject[@"code"]];
        if ([code isEqualToString:@"200"]) {
            if (success) {
                success(task, responseObject);
            }
        }else{
            if (failure) {
                failure(task, responseObject[@"msg"]);
            }
        }
    }else if ([task.response.URL.absoluteString containsString:BASE_URL_STR]){
        NSString *state = [NSString stringWithFormat:@"%@",responseObject[@"state"]];
        if ([state isEqualToString:@"success"]) {
            if (success) {
                success(task, responseObject);
            }
        }else{
            if (failure) {
                failure(task, responseObject[@"msg"]);
            }
        }
    }else{
        NSString *code = [NSString stringWithFormat:@"%@",responseObject[@"status"]];
        if ([code isEqualToString:@"0"]) {
            if (success) {
                success(task, responseObject);
            }
        }else if([code isEqualToString:@"301"]){
           
            if (failure) {
                failure(task, NeedLoginErrorMsg);
            }
            
        }else if ([code isEqualToString:@"-1"]){
            failure(task,NeedBindErrorMsg);
        }else{
            if (failure) {
                failure(task, responseObject[@"msg"]);
            }
        }
    }
}

/*
 请求失败处理
 */
+ (void)handleFailureWithFailureBlock:(httpFailureBlock)failure
                                 task:(NSURLSessionDataTask *)task
                                error:(NSError *)error{
    if (failure) {
        failure(task,[error localizedDescription]);
    }
}

//打开状态栏小菊花
+ (void)starNetworkActivityIndicatorVisible{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
}
//关闭状态栏小菊花
+ (void)endNetworkActivityIndicatorVisible{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    });
}

+ (void)logWithSuccessResponse:(id)response url:(NSString *)url params:(NSDictionary *)params {
    NSLog(@"\nabsoluteUrl: %@\n params:%@ \n\n response:%@", url, params, response);
}

+ (void)logWithFailError:(NSError *)error url:(NSString *)url params:(NSDictionary *)params {
    NSLog(@"\nabsoluteUrl: %@\n params:%@\n errorInfos:%@\n\n", url, params, [error localizedDescription]);
}

+ (void)logJsonStringWithResponseObject:(id)responseObject{
    NSData *data = [NSJSONSerialization dataWithJSONObject:responseObject options:0 error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"\n-------------jsonString-------------\n%@\n-------------jsonString-------------\n",jsonStr);
}



@end

