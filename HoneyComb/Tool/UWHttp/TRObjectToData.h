//
//  TRObjectToData.h
//  iOS_TaoRen
//
//  Created by 陈亚勃 on 2018/9/11.
//  Copyright © 2018年 code.sogou.fengur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRObjectToData : NSObject
+ (NSData *)objectToData:(id)object;
@end
