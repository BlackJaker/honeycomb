//
//  TRObjectToData.m
//  iOS_TaoRen
//
//  Created by 陈亚勃 on 2018/9/11.
//  Copyright © 2018年 code.sogou.fengur. All rights reserved.
//

#import "TRObjectToData.h"

@implementation TRObjectToData
+ (NSData *)objectToData:(id)object{
    if (!object || object == (id)kCFNull) return [@"" dataUsingEncoding:NSUTF8StringEncoding];
    if ([object isKindOfClass:[NSString class]]) return [object dataUsingEncoding:NSUTF8StringEncoding];
    if ([object isKindOfClass:[NSNumber class]]) return [[NSString stringWithFormat:@"%@",object] dataUsingEncoding:NSUTF8StringEncoding];
    return [object mj_JSONData];
}
@end
