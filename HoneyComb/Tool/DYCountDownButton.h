//
//  DYCountDownButton.h
//  Guangfa
//
//  Created by 罗大勇 on 2017/11/28.
//  Copyright © 2017年 ldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DYCountDownButton;
typedef void (^ClickCountDownButtonBlock)();

@interface DYCountDownButton : UIButton
@property(nonatomic , assign) int second; //开始时间数

@property(nonatomic , copy) ClickCountDownButtonBlock countDownButtonBlock; //点击按钮

@end
