//
//  FC_UpdateView.h
//  HoneyComb
//
//  Created by afc on 2018/11/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DownloadBlock)(NSString * url);

NS_ASSUME_NONNULL_BEGIN

@interface FC_UpdateView : UIView

-(instancetype)initWithFrame:(CGRect)frame data:(NSDictionary *)data;

@property (nonatomic,strong) UIImageView * infoBackImgView;
@property (nonatomic,strong) UILabel * newVersionNumLabel;
@property (nonatomic,strong) UILabel * sizeNumLabel;

@property (nonatomic,strong) UITableView * infoTableView;

@property (nonatomic,strong) UIButton * updateButton;

@property (nonatomic,copy)  DownloadBlock  downloadBlock;

-(void)show;
-(void)dismiss;

@end

NS_ASSUME_NONNULL_END
