//
//  FC_UpdateView.m
//  HoneyComb
//
//  Created by afc on 2018/11/26.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_UpdateView.h"

#define updateLeftMar  30
#define updateHeight 408

@interface FC_UpdateView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,copy) NSDictionary * data;

@property (nonatomic,copy) NSArray * infoList;

@end

@implementation FC_UpdateView

-(instancetype)initWithFrame:(CGRect)frame data:(NSDictionary *)data{
    
    if (self = [super initWithFrame:frame]) {
        
        _data = data;

        [self backViewSetup];
        
        [self  titleLabelSetup];
        
        [self contentLabelSetup];
        
        [self.updateButton setTitle:@"立即升级" forState:UIControlStateNormal];

        [self reload];
        
    }
    return self;
}

#pragma mark  --  setup  --

-(void)backViewSetup{
    
    UIView * backView = [[UIView alloc]initWithFrame:self.bounds];
    backView.backgroundColor = [UIColor blackColor];
    backView.alpha = 0.4;
    [self addSubview:backView];
}

-(void)titleLabelSetup{
    
    CGFloat  top = 100,height = 17;
    
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"发现新版本";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:kBold size:17];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.infoBackImgView addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(height);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(top);
    }];
    
}

-(void)contentLabelSetup{
    
   CGFloat  topMar = 167,height = 15,leftMar = 21,newVersionWidth = 75,sizeWidth = 90;
    
    UILabel * newVersionLabel = [[UILabel alloc]init];
    newVersionLabel.text = @"最新版本:";
    newVersionLabel.textColor = [UIColor colorWithHexString:@"#464e5f"];
    newVersionLabel.font = [UIFont fontWithName:kBold size:height];
    [self.infoBackImgView addSubview:newVersionLabel];
    
    [newVersionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftMar);
        make.height.mas_equalTo(height);
        make.width.mas_equalTo(newVersionWidth);
        make.top.mas_equalTo(topMar);
    }];
    
    CGFloat  centerMar = 10;
    UILabel * sizeLabel = [[UILabel alloc]init];
    sizeLabel.text = @"新版本大小:";
    sizeLabel.textColor = [UIColor colorWithHexString:@"#464e5f"];
    sizeLabel.font = [UIFont fontWithName:kBold size:height];
    [self.infoBackImgView addSubview:sizeLabel];
    
    [sizeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftMar);
        make.height.mas_equalTo(height);
        make.width.mas_equalTo(sizeWidth);
        make.top.mas_equalTo(topMar + centerMar + height);
    }];
    
}

#pragma mark  --  lazy  --

-(UIImageView *)infoBackImgView{
    
    if (!_infoBackImgView) {
        _infoBackImgView = [[UIImageView alloc]init];
        _infoBackImgView.image = [UIImage imageNamed:@"update_back_img"];
        _infoBackImgView.userInteractionEnabled = YES;
        [self addSubview:_infoBackImgView];
        
        [_infoBackImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(updateLeftMar);
            make.height.mas_equalTo(updateHeight);
            make.right.mas_equalTo(-updateLeftMar);
            make.centerY.mas_equalTo(0);
        }];
        
    }
    return _infoBackImgView;
}

-(UILabel *)newVersionNumLabel{
    
    if (!_newVersionNumLabel) {
        CGFloat  topMar = 167,height = 15,leftMar = 96,width = 100;
        
        _newVersionNumLabel = [[UILabel alloc]init];
        _newVersionNumLabel.textColor = [UIColor colorWithHexString:@"#7f8491"];
        _newVersionNumLabel.font = [UIFont fontWithName:kMedium size:height];
        [self.infoBackImgView addSubview:_newVersionNumLabel];
        
        [_newVersionNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar);
            make.height.mas_equalTo(height);
            make.width.mas_equalTo(width);
            make.top.mas_equalTo(topMar);
        }];
    }
    return _newVersionNumLabel;
}

-(UILabel *)sizeNumLabel{
    
    if (!_sizeNumLabel) {
        CGFloat  topMar = 192,height = 15,leftMar = 111,width = 100;
        
        _sizeNumLabel = [[UILabel alloc]init];
        _sizeNumLabel.textColor = [UIColor colorWithHexString:@"#7f8491"];
        _sizeNumLabel.font = [UIFont fontWithName:kMedium size:height];
        [self.infoBackImgView addSubview:_sizeNumLabel];
        
        [_sizeNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMar);
            make.height.mas_equalTo(height);
            make.width.mas_equalTo(width);
            make.top.mas_equalTo(topMar);
        }];
    }
    return _sizeNumLabel;
}

-(UITableView *)infoTableView{
    
    CGFloat  topMar = 30,left = 21,height = 90;
    
    if (!_infoTableView) {
        _infoTableView = [[UITableView alloc]init];
        _infoTableView.delegate = self;
        _infoTableView.dataSource = self;
        _infoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.infoBackImgView addSubview:_infoTableView];
        
        [_infoTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(left);
            make.height.mas_equalTo(height);
            make.right.mas_equalTo(-left);
            make.top.mas_equalTo(self.sizeNumLabel.mas_bottom).offset(topMar);
        }];
        
    }
    return _infoTableView;
}

-(UIButton *)updateButton{
    
    CGFloat  width = 125,height = 37,bottom = 22;
    
    if (!_updateButton) {
        _updateButton = [[UIButton alloc]init];
        [_updateButton setBackgroundColor:[UIColor redColor]];
        _updateButton.layer.cornerRadius = 5.0f;
        _updateButton.clipsToBounds = YES;
        [_updateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_updateButton addTarget:self action:@selector(appUpdateAction) forControlEvents:UIControlEventTouchUpInside];
        [self.infoBackImgView addSubview:_updateButton];
        
        [_updateButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(height);
            make.width.mas_equalTo(width);
            make.bottom.mas_equalTo(-bottom);
        }];
    }
    return _updateButton;
}

-(void)appUpdateAction{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.data[@"appUrl"]]];
}

#pragma mark  --  table view datasource  --

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.infoList.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static  NSString *  updateInfoCell = @"updateInfoCell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:updateInfoCell];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:updateInfoCell];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = [self.infoList objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:kMedium size:14];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#7f8491"];
    
    return cell;
}

#pragma mark  --  table view delegate  --

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 22;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (self.infoList.count == 0) {
        return 0;
    }
    return 19;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (self.infoList.count == 0) {
        return nil;
    }
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth - updateLeftMar * 2, 19)];
    view.backgroundColor = [UIColor whiteColor];
    CGFloat labelHeight = 14;
    
    UILabel * label = [[UILabel alloc]init];
    label.text = @"新版本特性:";
    label.textColor = [UIColor colorWithHexString:@"#464e5f"];
    label.font = [UIFont fontWithName:kBold size:labelHeight];
    [view addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(labelHeight);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    
    return view;
}

#pragma mark  --  action  --

-(void)show{
    
    UIViewController * rootViewController =  [UIApplication sharedApplication].keyWindow.rootViewController;
    
    [rootViewController.view addSubview:self];
    
}

-(void)dismiss{
    
    [self removeFromSuperview];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UIView * view = [touches anyObject].view;
    
    NSString * isConstraint = self.data[@"isConstraint"];
    
    if (![view isEqual:self.infoBackImgView] && [isConstraint isEqualToString:@"0"]) {
        [self dismiss];
    }
 
}

#pragma mark  --   reload  --

-(void)reload{
    
    self.newVersionNumLabel.text = [NSString stringWithFormat:@"V%@",self.data[@"version"]];
    self.sizeNumLabel.text = self.data[@"size"];
    
    if ([self.data[@"text"] length] > 0) {
        NSString * text = self.data[@"text"];
        self.infoList = [text componentsSeparatedByString:@"/n"];
        if (self.infoList.count > 3) {
            self.infoTableView.scrollEnabled = YES;
        }else{
            self.infoTableView.scrollEnabled = NO;
        }
        [self.infoTableView reloadData];
    }
}

@end
