//
//  LoginUser.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "LoginUser.h"

@implementation LoginUser
-(void)setShopId:(NSString *)shopId
{
    [[NSUserDefaults standardUserDefaults]setObject:shopId forKey:@"shopId"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
-(void)setPhoneNumber:(NSString *)phoneNumber
{
    [[NSUserDefaults standardUserDefaults]setObject:phoneNumber forKey:@"phoneNumber"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
-(NSString *)phoneNumber
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"phoneNumber"];
}
-(void)setToken:(NSString *)token
{
    [[NSUserDefaults standardUserDefaults]setObject:token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
-(NSString *)shopId
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"shopId"];
}
- (NSString *)token
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"token"];
}
- (NSString *)devToken
{
    return [[NSUserDefaults standardUserDefaults]valueForKey:@"devToken"];
}

-(void)setDevToken:(NSString *)devToken
{
    [[NSUserDefaults standardUserDefaults]setValue:devToken forKey:@"devToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (NSString *)drawMoney
{
    return [[NSUserDefaults standardUserDefaults]valueForKey:@"drawMoney"];
}

-(void)setDrawMoney:(NSString *)drawMoney
{
    [[NSUserDefaults standardUserDefaults]setValue:drawMoney forKey:@"drawMoney"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
- (NSString *)bankName
{
    return [[NSUserDefaults standardUserDefaults]valueForKey:@"bankName"];
}

-(void)setBankName:(NSString *)bankName
{
    [[NSUserDefaults standardUserDefaults]setValue:bankName forKey:@"bankName"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
- (NSString *)bankNumber
{
    return [[NSUserDefaults standardUserDefaults]valueForKey:@"bankNumber"];
}

-(void)setBankNumber:(NSString *)bankNumber
{
    [[NSUserDefaults standardUserDefaults]setValue:bankNumber forKey:@"bankNumber"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

+(instancetype)shareLoginUser
{
    static LoginUser *user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[LoginUser alloc]init];
    });
    return user;
}



@end
