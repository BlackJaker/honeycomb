//
//  DYNetworking.m
//  Guangfa
//
//  Created by 罗大勇 on 2017/11/21.
//  Copyright © 2017年 ldy. All rights reserved.
//

#import "DYNetworking.h"
#import "AFNetworking.h"

@implementation DYNetworking
//默认网络请求时间
static const NSUInteger kDefaultTimeoutInterval = 20;

#pragma GET请求--------------
+(void)requestMethodGetUrl:(NSString*)url
                       dic:(NSDictionary*)dic
                    Succed:(Success)succed
                   failure:(Failure)failure{
    //1.数据请求接口 2.请求方法 3.参数
    //请求成功   返回数据
    //请求失败   返回错误
    [DYNetworking Manager:url Method:@"GET"  dic:dic requestSucced:^(id responseObject) {
        
        succed(responseObject);
        
    } requestfailure:^(NSError *error) {
        
        failure(error);
        
    }];
}
#pragma POST请求--------------
+(void)requestMethodPOSTUrl:(NSString*)url
                        dic:(NSDictionary*)dic
                     Succed:(Success)succed
                    failure:(Failure)failure{
    [DYNetworking Manager:url Method:@"POST"  dic:dic requestSucced:^(id responseObject) {
        
        succed(responseObject);
        
    } requestfailure:^(NSError *error) {
        if (error.code == -1001) {
            [STTextHudTool showText:@"连接超时!"];
        }
        if (error.code == -1009) {
            [STTextHudTool showText:@"似乎已断开网络连接!"];
        }
        failure(error);
    }];
}

//=====================
+(void)Manager:(NSString*)url Method:(NSString*)Method dic:(NSDictionary*)dic requestSucced:(Success)Succed requestfailure:(Failure)failure
{
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval; //默认网络请求时间
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; //申明返回的结果是json类型
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/plain", @"text/json", @"text/javascript",@"text/html",@"charset=UTF-8", nil];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
  
//    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
//    [securityPolicy setAllowInvalidCertificates:YES];
//    [manager setSecurityPolicy:securityPolicy];
 //   manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    //======POST=====
    if ([Method isEqualToString:@"POST"]) {
        [manager POST:url parameters:dic success:^(NSURLSessionDataTask *task, id responseObject) {
            
            Succed(responseObject);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            failure(error);
           
            
        }];

        
        
        //=========GET======
    }else{
        
        [manager GET:url parameters:dic success:^(NSURLSessionDataTask *task, id responseObject) {
            
             Succed(responseObject);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
             failure(error);
            
        }];
//        [manager GET:url parameters:dic progress:^(NSProgress * _Nonnull downloadProgress) {
//
//        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//
//            Succed(responseObject);
//
//        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//
//            failure(error);
//
//        }];
    }
    
}
@end
