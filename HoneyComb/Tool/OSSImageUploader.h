//
//  OSSImageUploader.h
//  OSSImsgeUpload
//
//  Created by cysu on 5/31/16.
//  Copyright © 2016 cysu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, UploadImageState) {
    UploadImageFailed   = 0,
    UploadImageSuccess  = 1
};

@interface OSSImageUploader : NSObject

+ (void)asyncUploadImage:(NSArray<UIImage *> *)image folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSArray<NSString *> *names,UploadImageState state))complete;

+ (void)syncUploadImage:(UIImage *)image folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSString  *names,UploadImageState state))complete;

+ (void)asyncUploadImages:(NSArray<UIImage *> *)images folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete;

+ (void)syncUploadImages:(NSArray<UIImage *> *)images folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete;

@end
