//
//  FC_ENUM.h
//  HoneyComb
//
//  Created by afc on 2018/11/30.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#ifndef FC_ENUM_h
#define FC_ENUM_h

// order state

typedef NS_ENUM(NSUInteger, FC_OrderStateType) {

    FC_OrderStateTypeNoPay = 0,
    FC_OrderStateTypeStayTicket = 1,     // 待出票 (已付款)
    FC_OrderStateTypeStayDraw = 2,       // 待开奖 (已出票)
    FC_OrderStateTypeWin = 3,
    FC_OrderStateTypeLose = 4,
    FC_OrderStateTypeEndSend = 5,     // 已派奖
    FC_OrderStateTypeCancle = 6,
    FC_OrderStateTypeWillTakeTicket = 7,
    FC_OrderStateTypeTakeTicket = 8
};


#endif /* FC_ENUM_h */
