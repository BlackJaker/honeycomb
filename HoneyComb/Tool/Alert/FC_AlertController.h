//
//  FC_AlertController.h
//  HoneyComb
//
//  Created by afc on 2018/11/9.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CheckBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface FC_AlertController : UIAlertController

@property (nonatomic,copy) CheckBlock  checkBlock;

-(void)show;

@end

NS_ASSUME_NONNULL_END
