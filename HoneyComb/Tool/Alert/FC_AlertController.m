//
//  FC_AlertController.m
//  HoneyComb
//
//  Created by afc on 2018/11/9.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "FC_AlertController.h"

@interface FC_AlertController ()

@end

@implementation FC_AlertController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIAlertAction *checkAction = [UIAlertAction actionWithTitle:@"查看" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (self.checkBlock) {
            self.checkBlock();
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"忽略" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [self addAction:cancelAction];
    [self addAction:checkAction];
    
}

-(void)show{

    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:self animated:YES completion:^{
        
    }];
}

@end
