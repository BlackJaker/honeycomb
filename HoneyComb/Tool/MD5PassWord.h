//
//  MD5PassWord.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MD5PassWord : NSObject
+ (NSString *) md5:(NSString *) input;
@end
