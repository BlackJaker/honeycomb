//
//  LoginUser.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/18.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginUser : NSObject
@property (nonatomic,copy) NSString *phoneNumber;
@property (nonatomic,copy) NSString *token;
@property (nonatomic,copy) NSString *shopId;
@property (nonatomic,copy) NSString *devToken;

@property (nonatomic,copy) NSString *drawMoney;

@property (nonatomic,copy) NSString *bankNumber;

@property (nonatomic,copy) NSString *bankName;

+(instancetype)shareLoginUser;


@end
