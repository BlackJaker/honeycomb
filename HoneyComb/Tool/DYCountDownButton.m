//
//  DYCountDownButton.m
//  Guangfa
//
//  Created by 罗大勇 on 2017/11/28.
//  Copyright © 2017年 ldy. All rights reserved.
//

#import "DYCountDownButton.h"

@implementation DYCountDownButton
{
    NSTimer *_timer;
    int _initialTimer;
}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setTitle:@"获取验证码" forState:0];
        self.titleLabel.font = [UIFont fontWithName:kPingFangRegular size:14];
//        self.backgroundColor = [UIColor redColor];
        [self setTitleColor:[UIColor redColor] forState:0];
        self.layer.cornerRadius = 5.0;
//        [self setTitleColor:[UIColor whiteColor] forState:0];
        [self addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
-(void)setSecond:(int)second{
    _second = second;
    _initialTimer = _second;
}
-(void)click:(DYCountDownButton *)semder{
    
    //获取验证码
    _countDownButtonBlock();
    //
    self.userInteractionEnabled = NO;
    [self setTitle:[NSString stringWithFormat:@"%02ds后重发",_second] forState:0];
//    self.backgroundColor = [UIColor grayColor];
  [self setTitleColor:RGB(51, 51, 51,1) forState:0];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerStart:) userInfo:nil repeats:YES];
}
-(void)timerStart:(NSTimer *)theTimer {
    if (_initialTimer == 1){
        _initialTimer = _second;
        [self stop];
    }else{
        _initialTimer--;
        [self setTitle:[NSString stringWithFormat:@"%02ds后重发",_initialTimer] forState:0];
//        self.backgroundColor = [UIColor grayColor];
          [self setTitleColor:RGB(51, 51, 51,1) forState:0];
    }
}
- (void)stop{
    self.userInteractionEnabled = YES;
    if (_timer) {
        [_timer invalidate]; //停止计时器
        [self setTitle:@"重新获取" forState:UIControlStateNormal];
//        self.backgroundColor = [UIColor redColor];
          [self setTitleColor:[UIColor redColor] forState:0];
    }
}
-(void)dealloc{  //销毁计时器
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

@end
