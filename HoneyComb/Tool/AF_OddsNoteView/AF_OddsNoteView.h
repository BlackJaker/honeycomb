//
//  AF_OddsNoteView.h
//  LotteryApp
//
//  Created by afc on 2019/1/18.
//  Copyright © 2019年 afc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AF_OddsNoteView : UIView

@property (nonatomic,strong) UILabel * noteLabel;
@property (nonatomic,strong) UIImageView * noteImgView;

@end

NS_ASSUME_NONNULL_END
