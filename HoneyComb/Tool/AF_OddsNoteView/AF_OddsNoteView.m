//
//  AF_OddsNoteView.m
//  LotteryApp
//
//  Created by afc on 2019/1/18.
//  Copyright © 2019年 afc. All rights reserved.
//

#import "AF_OddsNoteView.h"

@implementation AF_OddsNoteView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {

        self.noteImgView.image = [UIImage imageNamed:@"odds_note_img"];
        
        self.noteLabel.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

-(UIImageView *)noteImgView{
    
    if (!_noteImgView) {
        _noteImgView = [[UIImageView alloc]init];
        _noteImgView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_noteImgView];
        
        CGFloat leftMargin = 20,imgWidth = 20;
        
        [_noteImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(leftMargin);
            make.size.mas_equalTo(CGSizeMake(imgWidth, imgWidth));
            make.centerY.mas_equalTo(0);
        }];
        
    }
    return _noteImgView;
}

-(UILabel *)noteLabel{
    
    if (!_noteLabel) {
        
        CGFloat  leftMar = 12;
        
        _noteLabel = [UILabel createLabelWithFrame:CGRectZero text:@"确认出票后，请保管好您的彩票，避免纠纷。" fontSize:14 textColor:[UIColor colorWithHexString:@"#FB5B54"]];
        _noteLabel.textAlignment = NSTextAlignmentLeft;
        
        [self addSubview:_noteLabel];
        
        [_noteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.noteImgView.mas_right).offset(leftMar);
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.right.mas_equalTo(-leftMar);
        }];
    }
    return _noteLabel;
}

@end
