
#import <UIKit/UIKit.h>

@interface UIView (YG)

@property (assign, nonatomic) CGFloat x;
@property (assign, nonatomic) CGFloat y;
@property (assign, nonatomic) CGFloat centerx;
@property (assign, nonatomic) CGFloat centery;
@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGSize size;
@property (assign, nonatomic) CGPoint origin;

//q弹效果
- (void)showQAnimate;

//渐变效果
- (void)showFadeAnimate;

#pragma mark ---- view生成image ----

+ (UIImage *)makeImageWithView:(UIView *)view withSize:(CGSize)size;

+(UIImage *)zipImageWithImage:(UIImage *)img;

/**
 快速给View添加4边阴影
 参数:阴影透明度，默认0
 */
- (void)addProjectionWithShadowOpacity:(CGFloat)shadowOpacity;
/**
 快速给View添加4边框
 参数:边框宽度
 */
- (void)addBorderWithWidth:(CGFloat)width;
/**
 快速给View添加4边框
 width:边框宽度
 borderColor:边框颜色
 */
- (void)addBorderWithWidth:(CGFloat)width borderColor:(UIColor *)borderColor;
/**
 快速给View添加圆角
 参数:圆角半径
 */
- (void)addRoundedCornersWithRadius:(CGFloat)radius;
/**
 快速给View添加圆角
 radius:圆角半径
 corners:且那几个角
 类型共有以下几种:
 typedef NS_OPTIONS(NSUInteger, UIRectCorner) {
 UIRectCornerTopLeft,
 UIRectCornerTopRight ,
 UIRectCornerBottomLeft,
 UIRectCornerBottomRight,
 UIRectCornerAllCorners
 };
 使用案例:[self.mainView addRoundedCornersWithRadius:10 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight]; // 切除了左下 右下
 */
- (void)addRoundedCornersWithRadius:(CGFloat)radius byRoundingCorners:(UIRectCorner)corners;

+ (UIView *)viewWithBackgroundColor:(UIColor *)color;

@property (nonatomic , assign) CGFloat cornerRadius;

- (void)borderWidth:(CGFloat)width color:(UIColor *)color;

@end


@interface UIButton (YG)

+ (UIButton *)buttonWithImage:(UIImage *)image
                selectedImage:(UIImage *)selectedImage;

- (void)setBackGroundColor:(UIColor *)color
              controlState:(UIControlState)controlState;


- (void)setTitle:(NSString *)title
            font:(UIFont *)font
      titleColor:(UIColor *)color
    controlState:(UIControlState)controlState;

+ (UIButton *)buttonWithTitle:(NSString *)title
                         font:(UIFont *)font
                   titleColor:(UIColor *)color
                 controlState:(UIControlState)controlState;

+ (UIButton *)buttonWithTitle:(NSString *)title
                         font:(UIFont *)font
              backgroundColor:(UIColor *)color
          highBackgroundColor:(UIColor *)highColor;

+ (UIButton *)buttonWithTitle:(NSString *)title
                         font:(UIFont *)font
              backgroundImage:(UIImage *)image
          highBackgroundImage:(UIImage *)highImage;

@end
