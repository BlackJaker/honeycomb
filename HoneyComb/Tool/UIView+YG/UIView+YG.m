

#import "UIView+YG.h"
@implementation UIView (YG)

- (void)setCenterx:(CGFloat)centerx
{
    CGPoint center = self.center;
    center.x = centerx;
    self.center = center;
}
- (CGFloat)centerx
{
    return self.center.x;
}
- (void)setCentery:(CGFloat)centery
{
    CGPoint center = self.center;
    center.y = centery;
    self.center = center;
}
- (CGFloat)centery
{
    return self.center.y;
}

- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
    
}
- (CGFloat)x{
    return self.frame.origin.x;
}
- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y=y;
    self.frame = frame;
}
- (CGFloat)y{

    return self.origin.y;
}
- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}
- (CGSize)size
{
    return self.frame.size;
}
- (void)setOrigin:(CGPoint)origin

{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}
- (CGPoint)origin
{
    return  self.frame.origin;
}
- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}
- (CGFloat)width
{
    return self.frame.size.width;
}
- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}
- (CGFloat)height
{
    return  self.frame.size.height;
}

- (void)showQAnimate
{
    //需要实现的帧动画,这里根据需求自定义
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"transform.scale";
    animation.values = @[@1.0,@1.3,@0.9,@1.15,@0.95,@1.02,@1.0];
    animation.duration = 0.5;
    animation.calculationMode = kCAAnimationCubic;
    [self.layer addAnimation:animation forKey:nil];
}

- (void)showFadeAnimate
{
    self.alpha = 0;
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.alpha = 1;
    } completion:nil];
}

#pragma mark ---- view生成image ----

+ (UIImage *)makeImageWithView:(UIView *)view withSize:(CGSize)size
{
    
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了，关键就是第三个参数 [UIScreen mainScreen].scale。
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    image = [self zipImageWithImage:image];  //压缩一下图片
    
    UIGraphicsEndImageContext();
    return image;
    
}

+(UIImage *)zipImageWithImage:(UIImage *)img
{
    NSData * imageData = UIImagePNGRepresentation(img);
    CGFloat maxFileSize = 32*1024;
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    UIImage *image = [UIImage imageWithData:imageData];
    NSData *compressedData = UIImageJPEGRepresentation(image, compression);
    while ([compressedData length] > maxFileSize && compression > maxCompression) {
        compression -= 0.1;
        compressedData = UIImageJPEGRepresentation(image, compression);
    }
    UIImage *compressedImage = [UIImage imageWithData:imageData];
    return compressedImage;
}

#pragma mark - 快速添加阴影
- (void)addProjectionWithShadowOpacity:(CGFloat)shadowOpacity {
    self.layer.shadowColor = RGB(0, 0, 0, 0.08).CGColor;//shadowColor阴影颜色
    self.layer.shadowOffset = CGSizeMake(0,3);//shadowOffset阴影偏移,x向右偏移2，y向下偏移6，默认(0, -3),这个跟shadowRadius配合使用
    self.layer.shadowOpacity = shadowOpacity;//阴影透明度，默认0
    self.layer.shadowRadius = 10;//阴影半径，默认3
}
- (void)addBorderWithWidth:(CGFloat)width {
    self.layer.borderWidth = width;
    self.layer.borderColor = [UIColor blackColor].CGColor;
}
- (void)addBorderWithWidth:(CGFloat)width borderColor:(UIColor *)borderColor {
    self.layer.borderWidth = width;
    self.layer.borderColor = borderColor.CGColor;
}
- (void)addRoundedCornersWithRadius:(CGFloat)radius {
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}
- (void)addRoundedCornersWithRadius:(CGFloat)radius byRoundingCorners:(UIRectCorner)corners{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

+ (UIView *)viewWithBackgroundColor:(UIColor *)color{
    UIView *view = [[self alloc] init];
    [view setBackgroundColor:color];
    return view;
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    
    objc_setAssociatedObject(self, &cornerRadius, @(cornerRadius), OBJC_ASSOCIATION_ASSIGN);
    
    self.layer.cornerRadius = cornerRadius;
    
    self.layer.masksToBounds = YES;
    
}

- (CGFloat)cornerRadius{
    NSNumber *cornerRadius = objc_getAssociatedObject(self, &cornerRadius);
    return [cornerRadius floatValue];
}

- (void)borderWidth:(CGFloat)width color:(UIColor *)color{
    self.layer.borderWidth = width;
    self.layer.borderColor = color.CGColor;
}


@end


@implementation UIButton (YG)

+ (UIButton *)buttonWithImage:(UIImage *)image
                selectedImage:(UIImage *)selectedImage{
    UIButton *btn = [self buttonWithType:UIButtonTypeCustom];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setImage:selectedImage forState:UIControlStateSelected];
    return btn;
}

+ (UIButton *)buttonWithTitle:(NSString *)title
                         font:(UIFont *)font
                   titleColor:(UIColor *)color
                 controlState:(UIControlState)controlState{
    UIButton *btn = [self buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title font:font titleColor:color controlState:controlState];
    
    return btn;
}

- (void)setBackGroundColor:(UIColor *)color
              controlState:(UIControlState)controlState{
    [self setBackgroundImage:[UIImage imageWithColor:color] forState:controlState];
}

- (void)setTitle:(NSString *)title
            font:(UIFont *)font
      titleColor:(UIColor *)color
    controlState:(UIControlState)controlState{
    
    [self setTitle:title forState:controlState];
    [self setTitleColor:color forState:controlState];
    self.titleLabel.font = font;
    //    return self;
}


+ (UIButton *)buttonWithTitle:(NSString *)title
                         font:(UIFont *)font
              backgroundImage:(UIImage *)image
          highBackgroundImage:(UIImage *)highImage {
    UIButton *btn = [self buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = font;
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    [btn setBackgroundImage:highImage forState:UIControlStateHighlighted];
    return btn;
}

+ (UIButton *)buttonWithTitle:(NSString *)title
                         font:(UIFont *)font
              backgroundColor:(UIColor *)color
          highBackgroundColor:(UIColor *)highColor{
    
    UIButton *btn = [self buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = font;
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [btn setBackgroundColor:color];
    [btn setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:highColor] forState:UIControlStateHighlighted];
    return btn;
}

@end


