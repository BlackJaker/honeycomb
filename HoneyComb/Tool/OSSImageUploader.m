//
//  OSSImageUploader.m
//  OSSImsgeUpload
//
//  Created by cysu on 5/31/16.
//  Copyright © 2016 cysu. All rights reserved.
//

#import "OSSImageUploader.h"
#import <AliyunOSSiOS/OSSService.h>

@implementation OSSImageUploader
static NSString *const AccessKey = @"STS.NK16oE8LTUc29Qn21QDWpQm2w";
static NSString *const SecretKey = @"HsG41esKoD3Vr9DeETxRMBXgjPJqqTvveWrZHeZyzFyx";
static NSString *const token = @"CAISkwJ1q6Ft5B2yfSjIr4iEfdXx1ZN14qHZO3ff1jEESNhcvqiZlTz2IH1LeXZsAO4cv/0xmWFY6f8elqB6T55OSAmcNZIoUFPxCbjlMeT7oMWQweEuuv/MQBquaXPS2MvVfJ+OLrf0ceusbFbpjzJ6xaCAGxypQ12iN+/m6/Ngdc9FHHP7D1x8CcxROxFppeIDKHLVLozNCBPxhXfKB0ca3WgZgGhku6Ok2Z/euFiMzn+Ck7RF/NmrfsT5NZM9Z88gC+3YhrImKvDztwdL8AVP+atMi6hJxCzKpNn1ASMKvkrbbLaNrIc+dVEkN/hlQvJe3/H4lOxlvOvIjJjwyBtLMuxTXj7WWIe62szAFfMP7S/N8xcpURqAAT8OAcrR2wewZ295rz/TC2BGkgGoGa6GQP8KS1BAcYBmIS4BNhUbDiKDwFbcW1pKu3XXVFVr+kH+qLq3PZ/ZcvpSwsyMoDnrJNZ8XEwhx5Ick1ttcOaGeKWFHGWYPf+stKsj+ZUNng4nCriI62oFyakwTQVwNo5P73s8ugQ9x+Dx";
static NSString *const BucketName = @"afcplay-app";
static NSString *const AliYunHost = @"https://oss-cn-huhehaote.aliyuncs.com/";

static NSString *kTempFolder = @"temp";


+ (void)asyncUploadImage:(UIImage *)image folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSArray<NSString *> *names,UploadImageState state))complete
{
//    [self uploadImages:@[image] isAsync:YES complete:complete];
    [self uploadImages:@[image] folder:folder accessKey:accessKey secretKey:secret token:token isAsync:YES complete:complete];
}

+ (void)syncUploadImage:(UIImage *)image folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSString  *names,UploadImageState state))complete
{
//    [self uploadImages:@[image] isAsync:NO complete:complete];
//     [self uploadImages:@[image] folder:folder accessKey:accessKey secretKey:secret token:token isAsync:NO complete:complete];
    [self pushImage:image folder:folder accessKey:accessKey secretKey:secret token:token isAsync:NO complete:complete];
}

+ (void)asyncUploadImages:(NSArray<UIImage *> *)images folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete
{
//    [self uploadImages:images isAsync:YES complete:complete];
     [self uploadImages:images folder:folder accessKey:accessKey secretKey:secret token:token isAsync:YES complete:complete];
    
}

+ (void)syncUploadImages:(NSArray<UIImage *> *)images folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete
{
//    [self uploadImages:images isAsync:NO complete:complete];
     [self uploadImages:images folder:folder accessKey:accessKey secretKey:secret token:token isAsync:NO complete:complete];
}

+ (void)uploadImages:(NSArray<UIImage *> *)images folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token isAsync:(BOOL)isAsync complete:(void(^)(NSArray<NSString *> *names, UploadImageState state))complete
{
    
    id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:accessKey secretKeyId:secret   securityToken:token];
   
    
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:AliYunHost credentialProvider:credential];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.maxConcurrentOperationCount = images.count;
    
    NSMutableArray *callBackNames = [NSMutableArray array];
    int i = 0;
    for (UIImage *image in images) {
        if (image) {
            NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                //任务执行
                OSSPutObjectRequest * put = [OSSPutObjectRequest new];
                put.bucketName = BucketName;
                NSString *imageName = [folder stringByAppendingPathComponent:[[NSUUID UUID].UUIDString stringByAppendingString:@".jpg"]];
                put.objectKey = imageName;
                [callBackNames addObject:imageName];
                NSData *data = UIImageJPEGRepresentation(image, 1);
                put.uploadingData = data;
              
                OSSTask * putTask = [client putObject:put];
                [putTask waitUntilFinished]; // 阻塞直到上传完成
                if (!putTask.error) {
                    NSLog(@"upload object success!");
                } else {
                    NSLog(@"upload object failed, error: %@" , putTask.error);
                }
                if (isAsync) {
                    if (image == images.lastObject) {
                        NSLog(@"upload object finished!");
                        if (complete) {
                            complete([NSArray arrayWithArray:callBackNames] ,UploadImageSuccess);
                        }
                    }
                }
            }];
            if (queue.operations.count != 0) {
                [operation addDependency:queue.operations.lastObject];
            }
            [queue addOperation:operation];
        }
        i++;
    }
    if (!isAsync) {
        [queue waitUntilAllOperationsAreFinished];
        NSLog(@"haha");
        if (complete) {
            if (complete) {
                complete([NSArray arrayWithArray:callBackNames], UploadImageSuccess);
            }
        }
    }
}
+ (void)pushImage:(UIImage *)images folder:(NSString *)folder accessKey:(NSString *)accessKey secretKey:(NSString *)secret token:(NSString *)token isAsync:(BOOL)isAsync complete:(void(^)(NSString  *names, UploadImageState state))complete
{
    
    id<OSSCredentialProvider> credential = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:accessKey secretKeyId:secret   securityToken:token];
    
    
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:AliYunHost credentialProvider:credential];
    
//    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//    queue.maxConcurrentOperationCount = images;
    
    NSString *callBackNames ;
   
//            NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                //任务执行
                OSSPutObjectRequest * put = [OSSPutObjectRequest new];
                put.bucketName = BucketName;
                NSString *imageName = [folder stringByAppendingPathComponent:[[NSUUID UUID].UUIDString stringByAppendingString:@".jpg"]];
                put.objectKey = imageName;
//                [callBackNames addObject:imageName];
                  callBackNames = imageName;
                NSData *data = UIImageJPEGRepresentation(images, 1);
                put.uploadingData = data;
                
                OSSTask * putTask = [client putObject:put];
                [putTask waitUntilFinished]; // 阻塞直到上传完成
                if (!putTask.error) {
                    NSLog(@"upload object success!");
                } else {
                    NSLog(@"upload object failed, error: %@" , putTask.error);
                }
              
//            }];
//            if (queue.operations.count != 0) {
//                [operation addDependency:queue.operations.lastObject];
//            }
//            [queue addOperation:operation];
    


    if (!isAsync) {
//        [queue waitUntilAllOperationsAreFinished];
        NSLog(@"haha");
        if (complete) {
            if (complete) {
                complete(callBackNames, UploadImageSuccess);
            }
        }
    }
}


@end
