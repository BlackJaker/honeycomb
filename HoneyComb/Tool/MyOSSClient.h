//
//  MyOSSClient.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/8/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "OSSClient.h"

@interface MyOSSClient : OSSClient
+ (instancetype)sharedInstance;
@end
