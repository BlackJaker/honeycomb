//
//  UITextField+FC_TextField.h
//  HoneyComb
//
//  Created by afc on 2018/12/22.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (FC_TextField)

- (NSRange) selectedRange;
- (void) setSelectedRange:(NSRange) range;

@end

NS_ASSUME_NONNULL_END
