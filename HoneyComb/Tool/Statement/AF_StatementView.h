//
//  AF_StatementView.h
//  LotteryApp
//
//  Created by afc on 2019/1/16.
//  Copyright © 2019年 afc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DismissBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface AF_StatementView : UIView

@property (nonatomic,strong) UIButton * affirmButton;

@property (nonatomic,strong) UIView * backView;

@property (nonatomic,strong) UIView * infoView;

@property (nonatomic,strong) UIView * contentView;

@property (nonatomic,copy) DismissBlock dismissBlock;

-(void)show;

-(void)dismiss;

@end

NS_ASSUME_NONNULL_END
