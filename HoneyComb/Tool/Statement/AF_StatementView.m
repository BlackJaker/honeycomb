//
//  AF_StatementView.m
//  LotteryApp
//
//  Created by afc on 2019/1/16.
//  Copyright © 2019年 afc. All rights reserved.
//

#import "AF_StatementView.h"

@interface AF_StatementView ()
{
    CGFloat  infoViewHeight;
}
@end

@implementation AF_StatementView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {

        self.backView.backgroundColor = [UIColor colorWithHexString:@"#000000"];
        self.backView.alpha = 0.4;
        infoViewHeight = 375;
        
        self.infoView.backgroundColor = [UIColor clearColor];
        
        [self.affirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    }
    return self;
}

#pragma mark  --  lazy  --

-(UIView *)backView{
    
    if (!_backView) {
        _backView = [[UIView alloc]init];
        [self addSubview:_backView];
        
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(kHeight);
        }];
    }
    return _backView;
}

-(UIView *)infoView{
    
    CGFloat  infoLeftMargin = 25,headHeight = 40,centMar = 80,imgTop = 10,imgWidth = 20;
    
    if (!_infoView) {
        _infoView = [[UIView alloc]init];
        _infoView.backgroundColor = [UIColor redColor];
        [self addSubview:_infoView];
        
        [_infoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-infoLeftMargin);
            make.left.mas_equalTo(infoLeftMargin);
            make.centerY.mas_equalTo(0);
            make.height.mas_equalTo(infoViewHeight);
        }];
        
        UILabel * label = [UILabel createLabelWithFrame:CGRectZero text:@"平台特别声明" fontSize:17 textColor:[UIColor whiteColor]];
        label.font = [UIFont boldSystemFontOfSize:17];
        [_infoView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(0);
            make.height.mas_equalTo(headHeight);
        }];
        
        UIImageView * leftImgView = [[UIImageView alloc]init];
        leftImgView.image = [UIImage imageNamed:@"fwsm"];
        leftImgView.contentMode = UIViewContentModeScaleAspectFit;
        [_infoView addSubview:leftImgView];
        
        [leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(imgTop);
            make.left.mas_equalTo(kWidth/2-(centMar + imgWidth));
            make.size.mas_equalTo(CGSizeMake(imgWidth, imgWidth));
        }];
        
        UIImageView * rightImgView = [[UIImageView alloc]init];
        rightImgView.image = [UIImage imageNamed:@"fwsm"];
        rightImgView.contentMode = UIViewContentModeScaleAspectFit;
        [_infoView addSubview:rightImgView];
        
        [rightImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(imgTop);
            make.right.mas_equalTo( - (kWidth/2-(centMar + imgWidth )));
            make.size.mas_equalTo(CGSizeMake(imgWidth, imgWidth));
        }];
    }
    return _infoView;
}

-(UIView *)contentView{
    
    CGFloat top = 40;
    
    if (!_contentView) {
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.layer.cornerRadius = 5.0f;
        _contentView.clipsToBounds = YES;
        [self.infoView addSubview:_contentView];
        
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(top);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
        
        CGFloat  top = 25,left = 24,height = 242;
        
        NSString * statementString = @"1、平台仅为投注站业主与彩民用户之间提供信息发布和管理技术服务，平台不涉及互联网售彩。\n\n2、平台所绑定的店主端均为由国家体育彩票和福利彩票中心授权，具有线下合法从事销售中国体育彩票或中国福利彩票牌照的实体站点。实体投注站点真实拍照出票，保障彩民用户的合法权益。\n\n3、坚决拥护国家体育彩票和福利彩票中心政策规定，坚决打击黑彩、杜绝吃票、杜绝未成年人购彩等违法行为。";
        
        NSString * subString = @"4、未满18周岁的人士，不能购买彩票。";
        
        UILabel * label = [UILabel createLabelWithFrame:CGRectZero text:@"" fontSize:13 textColor:[UIColor colorWithHexString:@"#FF605F"]];
        label.textAlignment = NSTextAlignmentLeft;
        label.numberOfLines = 0;
        label.attributedText = [self attributedStrWith:statementString subString:subString];
        [_contentView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(top);
            make.left.mas_equalTo(left);
            make.right.mas_equalTo(-left);
            make.height.mas_equalTo(height);
        }];
    }
    return _contentView;
}

-(NSMutableAttributedString *)attributedStrWith:(NSString *)string subString:(NSString *)subString{
    
    NSMutableAttributedString *rushAttributed = [[NSMutableAttributedString alloc]initWithString:string];
    [rushAttributed setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#FF605F"],NSFontAttributeName:[UIFont fontWithName:kMedium size:13]} range:NSMakeRange(0, string.length)];
    [rushAttributed addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSRangeFromString(subString)];
    
    return  rushAttributed;
}

-(UIButton *)affirmButton{
    
    CGFloat  width = 122,height = 38,bottom = 16;
    
    if (!_affirmButton) {
        _affirmButton = [[UIButton alloc]init];
        [self.contentView addSubview:_affirmButton];
        
        [_affirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(0);
            make.height.mas_equalTo(height);
            make.width.mas_equalTo(width);
            make.bottom.mas_equalTo(-bottom);
        }];
        
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = CGRectMake(0, 0, width, height);
        gl.startPoint = CGPointMake(0, 0);
        gl.endPoint = CGPointMake(1, 1);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:255/255.0 green:8/255.0 blue:127/255.0 alpha:1.0].CGColor,(__bridge id)[UIColor colorWithRed:255/255.0 green:51/255.0 blue:41/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0.0),@(1.0)];
        
        [_affirmButton.layer addSublayer:gl];
        
        _affirmButton.layer.cornerRadius = 5.0f;
        _affirmButton.clipsToBounds = YES;
        _affirmButton.titleLabel.font = [UIFont fontWithName:kMedium size:16];
        [_affirmButton addTarget:self action:@selector(affirmAction) forControlEvents:UIControlEventTouchUpInside];
        [_affirmButton setTitle:@"我同意" forState:UIControlStateNormal];
    }
    return _affirmButton;
    
}

-(void)affirmAction{
    
    [self dismiss];
    
}

#pragma mark  --  action  --

-(void)show{
    
    UIViewController * rootViewController =  [UIApplication sharedApplication].keyWindow.rootViewController;
    
    [rootViewController.view addSubview:self];
    
}

-(void)dismiss{
    
    if (self.dismissBlock) {
        self.dismissBlock();
    }
    
    [self removeFromSuperview];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:[LoginUser shareLoginUser].shopId];
}

@end
