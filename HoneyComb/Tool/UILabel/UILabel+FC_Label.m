//
//  UILabel+FC_Label.m
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "UILabel+FC_Label.h"

@implementation UILabel (FC_Label)

+(UILabel *)createLabelWithFrame:(CGRect)frame text:(NSString*)text fontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    UILabel * label = [[UILabel alloc]initWithFrame:frame];
    label.text = text;
    label.textColor = textColor;
    label.font = [UIFont fontWithName:kPingFangRegular size:fontSize];
    label.textAlignment = NSTextAlignmentCenter;
    
    return label;
}

+ (void)changeLineSpaceForLabel:(UILabel *)label WithSpace:(float)space {
    
    NSString *labelText = label.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:space];
    label.numberOfLines = 0;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    label.attributedText = attributedString;
    //    [label sizeToFit];
    
}

+ (UILabel *)lableWithText:(NSString *)text
                      font:(UIFont *)font
                 textColor:(UIColor *)color{
    UILabel *label = [[self alloc] init];
    label.font = font;
    label.text = text;
    label.textColor = color;
    label.userInteractionEnabled = YES;
    return label;
}

@end
