//
//  UILabel+FC_Label.h
//  HoneyComb
//
//  Created by afc on 2018/11/19.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (FC_Label)

+(UILabel *)createLabelWithFrame:(CGRect)frame
                            text:(NSString*)text
                        fontSize:(CGFloat)fontSize
                       textColor:(UIColor *)textColor;

+ (void)changeLineSpaceForLabel:(UILabel *)label
                      WithSpace:(float)space;

+ (UILabel *)lableWithText:(NSString *)text
                      font:(UIFont *)font
                 textColor:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
