//
//  HoneyCombTabBar.h
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeViewController;
@class MeViewController;
@class FoundViewController;
@class OpenViewController;


@interface HoneyCombTabBar : UITabBarController

@property (nonatomic,strong) HomeViewController *homeVC;
@property (nonatomic,strong) UINavigationController *homeNC;
@property (nonatomic,strong) MeViewController *meVC;
@property (nonatomic,strong) UINavigationController *meNC;
@property (nonatomic,strong) FoundViewController *foundVC;
@property (nonatomic,strong) UINavigationController *foundNC;
@property (nonatomic,strong) OpenViewController *openVC;
@property (nonatomic,strong) UINavigationController *openNC;

@end
