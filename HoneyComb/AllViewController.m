//
//  AllViewController.m
//  HoneyComb
//
//  Created by syqaxldy on 2018/7/10.
//  Copyright © 2018年 syqaxldy. All rights reserved.
//

#import "AllViewController.h"

@interface AllViewController ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

@end

@implementation AllViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f7f7f7"];
    
    self.navigationController.delegate = self;
    self.view.userInteractionEnabled = YES;
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    self.navigationController.navigationBar.translucent = NO;  //导航栏颜色变浅的话,加上这行
    self.navigationController.navigationBar.hidden =NO;
    self.titleLabe = [[YYLabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    self.titleLabe.textAlignment = NSTextAlignmentCenter;
    self.titleLabe.font = [UIFont fontWithName:kBold size:18];
    self.titleLabe.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = self.titleLabe;
    
    self.left = [[UIButton alloc]initWithFrame:CGRectMake(-20, 0, 60, 30)];
    
    [self.left setImage:[UIImage imageNamed:@"返回"] forState:(UIControlStateNormal)];
    //    [self.left setTitle:@"" forState:(UIControlStateNormal)];
    //    self.left.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
   
    self.left.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 50);
    [self.left addTarget:self action:@selector(leftAction) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIBarButtonItem *le = [[UIBarButtonItem alloc]initWithCustomView:self.left];
    self.navigationItem.leftBarButtonItem = le;
    
    
}
-(void)leftAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  --  lazy  --

-(UIView *)topNaviView{
    
    if (!_topNaviView) {
        _topNaviView = [[UIView alloc]init];
        _topNaviView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_topNaviView];
        
        [_topNaviView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(NAVIGATION_HEIGHT);
            make.top.mas_equalTo(0);
        }];
        
    }
    return _topNaviView;
}

-(UILabel *)titleLabel{
    
    CGFloat  titleLabelWidth = 160,top = 20;
    CGRect  titleRect = CGRectMake((kWidth - titleLabelWidth)/2, NAVI_SAFE_HEIGHT + 20, titleLabelWidth, NAVI_ITEM_HEIGHT - top);
    
    if (!_titleLabel) {
        _titleLabel = [UILabel createLabelWithFrame:titleRect text:@"" fontSize:18 textColor:[UIColor colorWithHexString:@"#333333"]];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont fontWithName:kBold size:18];
        [self.topNaviView addSubview:_titleLabel];
        
    }
    return _titleLabel;
}

-(UIButton *)backButton{
    
    CGFloat  backWidth = 30,top = 20;
    
    if (!_backButton) {
        _backButton = [[UIButton alloc]init];
        [_backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
        [self.topNaviView addSubview:_backButton];
        
        [_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLeftMar);
            make.size.mas_equalTo(backWidth);
            make.top.mas_equalTo(NAVI_SAFE_HEIGHT + top + (NAVI_ITEM_HEIGHT - top - backWidth)/2);
        }];
        
    }
    return _backButton;
}

-(UIButton *)rightButton{
    
    CGFloat  rightWidth = 30,top = 20;
    
    if (!_rightButton) {
        _rightButton = [[UIButton alloc]init];
        _rightButton.tag = -111;
        [_rightButton addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.topNaviView addSubview:_rightButton];
        
        [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-kLeftMar);
            make.size.mas_equalTo(rightWidth);
            make.top.mas_equalTo(NAVI_SAFE_HEIGHT + top + (NAVI_ITEM_HEIGHT - top - rightWidth)/2);
        }];
        
    }
    return _rightButton;
}

-(void)rightAction:(UIButton *)sender{
    
}

-(void)setRightButtonWithTitle:(NSString *)rightTitle{
    
    CGFloat  width = 30,centerMar = 0,top = 20;
    
    if (rightTitle) {
        NSArray * titles = [rightTitle componentsSeparatedByString:@","];
        
        for (int i = 0; i < titles.count; i ++) {
            UIButton * button = [[UIButton alloc]init];
            button.tag =  - 222 + i;
            [button addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.topNaviView addSubview:button];
            
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(-(kLeftMar + (width + centerMar) * i));
                make.size.mas_equalTo(width);
                make.top.mas_equalTo(NAVI_SAFE_HEIGHT + top + (NAVI_ITEM_HEIGHT - top - width)/2);
            }];
            
            [self.rightButtons addObject:button];
        }
    }
    
}

-(NSMutableArray *)rightButtons{
    
    if (!_rightButtons) {
        _rightButtons = [NSMutableArray new];
    }
    return _rightButtons;
}

-(void)updateFrameWithMargin:(CGFloat)margin{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
